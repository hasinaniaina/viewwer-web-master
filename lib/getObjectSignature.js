const crypto = require('crypto');

// generate a simple signature for any object in input
// to check : compare good and expected signature
module.exports = function (obj) {
  const hmac = crypto.createHmac('sha256', 'sEcrEt.1TCmRWAuM4UpUeWcZvS2!');
  let objAsString = JSON.stringify(obj);
  hmac.update(objAsString, "utf8");
  return hmac.digest('hex'); 
};
