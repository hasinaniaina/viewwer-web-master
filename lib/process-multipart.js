/**
 * A simple multipart/form-data processor
 */

const _ = require('lodash');
const path = require('path');
const fs = require('fs-extra');
const Busboy = require('busboy');

module.exports = function(req, options) {
  return new Promise(function(resolve, reject) {
    const busboy = new Busboy({headers: req.headers});

    let processing = 0, interrupted = false;

    function readyToResolve() {
      if (!processing && !interrupted) resolve();
    }

    function fileSuccess() {
      if (interrupted) return;

      processing--;
      readyToResolve();
    }

    function fileFailure(err) {
      if (interrupted) return;

      interrupted = true;
      reject(err);
    }

    busboy.on('file', function(fieldName, file, originalFileName) {
      fieldName = fieldName.replace('[]', '');

      processing++;

      req.files = req.files || {};

      const fileName = _
        .deburr(options.fileName ? options.fileName(fieldName, originalFileName) : originalFileName)
        .replace(/[^a-zA-Z0-9\-.]/g, '_');

      const filePath = path.join(options.destination, fileName);
      const fileProps = {name: fileName, originalName: originalFileName, path: filePath};

      if (req.files[fieldName]) {
        if (!Array.isArray(req.files[fieldName])) req.files[fieldName] = [req.files[fieldName]];
        req.files[fieldName].push(fileProps);
      } else {
        req.files[fieldName] = fileProps;
      }

      file
        .pipe(fs.createWriteStream(filePath))
        .on('finish', fileSuccess)
        .on('error', fileFailure);
    });

    busboy.on('finish', readyToResolve);

    fs.ensureDir(options.destination).then(() => req.pipe(busboy));
  });
};
