const crypto = require('crypto');

const rp = require('request-promise-native');

const SUPPORT_ID = 'support';
module.exports.SUPPORT_ID = SUPPORT_ID;

const SUPPORT_ICON = '/assets/shared/images/logo-chat-avatar.png';
module.exports.SUPPORT_ICON = SUPPORT_ICON;

module.exports.getAppId = function () {
  return process.env.TALKJS_APPID;
};

module.exports.getSignature = function (id) {
  return crypto.createHmac('sha256', process.env.TALKJS_SECRET).update(id).digest('hex');
};

function getRole(prefix, locale) {
  if (locale === 'fr') return prefix + '_fr';
  return prefix + '_en';
}

module.exports.getRole = getRole;

function apiRequest(method, path, data) {
  return new Promise((resolve, reject) => {
    rp({
      method: method,
      url: 'https://api.talkjs.com/v1/' + process.env.TALKJS_APPID + '/' + path,
      headers: {
        'Authorization': 'Bearer ' + process.env.TALKJS_SECRET
      },
      body: data,
      json: true
    })
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
}

module.exports.apiRequest = apiRequest;

function checkConversationExistance(conversation_id) {
  return new Promise((resolve, reject) => {
    apiRequest('GET', 'conversations/' + conversation_id)
      .then(() => resolve(true))
      .catch(e => {
        if (e.statusCode === 404) return resolve(false);
        reject(e);
      });
  });
}

module.exports.syncUser = function (user, locale, __) {

  const SUPPORT_CONVERSATION_ID = 'support_' + user.id;

  let supportConversationExists = false;

  return new Promise((resolve, reject) => {
    Promise.all([
      apiRequest('PUT', `users/${user.id}`, {
        name: user.name,
        email: [user.email],
        role: getRole('user', locale)
      }),
      apiRequest('PUT', `users/${SUPPORT_ID}`, {
        name: process.env.CHAT_SUPPORT_NAME,
        email: [process.env.CHAT_SUPPORT_EMAIL]
      })
    ])
      .then(() => checkConversationExistance(SUPPORT_CONVERSATION_ID))
      .then(r => {
        supportConversationExists = r;
        return apiRequest(
          'PUT', `conversations/${SUPPORT_CONVERSATION_ID}`,
          {
            participants: [user.id, SUPPORT_ID],
            subject: __('_chat_support_conversation_subject'),
            custom: {}
          });
      })
      .then(() => {
        if (supportConversationExists) return;
        return apiRequest('POST',
          `conversations/${SUPPORT_CONVERSATION_ID}/messages`,
          [
            {
              text: __('_chat_support_welcome_message'),
              sender: SUPPORT_ID,
              type: 'UserMessage'
            }
          ]);
      })
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};
