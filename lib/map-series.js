/* kind of Bluebird.mapSeries, but with native promises and simpler */
module.exports = function(elements, f) {
  if (!elements || !elements.length) return Promise.resolve();

  function createPromise(element, idx) {
    return function() {
      let promise;

      try {
        const runner = f(element, idx);

        if (runner && runner.then) {
          promise = runner;
        } else {
          promise = Promise.resolve(runner);
        }
      } catch (err) {
        promise = Promise.reject(err);
      }

      return promise;
    };
  }

  return new Promise(function(resolve, reject) {
    const results = [];
    let current = 0;
    const promises = elements.map(createPromise);

    function processNext() {
      promises[current]()
        .then(function(result) {
          results.push(result);

          if (current === promises.length - 1) {
            resolve(results);
          } else {
            current++;
            processNext();
          }
        })
        .catch(reject);
    }

    processNext();
  });
};
