const winston = require('winston');

require('winston-mongodb');

const WinstonNodemailer = require('./winston-nodemailer');
const emailTransporter = require('./email').transporter;

const sharedLoggerOptions = {
  level: process.env.LOG_LEVEL
};

const loggers = {};
const transports = [];

const consoleTransport = new winston.transports.Console({
  level: process.env.LOG_CONSOLE_LEVEL || 'info',
  colorize: true
});

transports.push(consoleTransport);

loggers.console = new winston.Logger(Object.assign({}, sharedLoggerOptions, {transports: [consoleTransport]}));

const dbTransport = new winston.transports.MongoDB({
  level: process.env.LOG_DB_LEVEL || 'debug',
  db: process.env.DB_URI,
  collection: 'logs',
  storeHost: true,
  capped: true,
  cappedSize: (process.env.LOG_DB_CAP || 50) * 1000 * 1000,
  tryReconnect: true,
  decolorize: true
});

dbTransport.on('error', function(err) {
  loggers.console.error(err);
});

transports.push(dbTransport);

loggers.db = new winston.Logger(Object.assign({}, sharedLoggerOptions, {transports: [dbTransport]}));

if (process.env.LOG_EMAIL && process.env.SMTP_HOST) {
  const emailTransport = new WinstonNodemailer({
    level: 'error',
    handleExceptions: true,

    mailOptions: {
      from: 'logger@' + process.env.DOMAIN,
      to: process.env.LOG_EMAIL,
      subject: process.env.DOMAIN + ' error'
    },
    transporter: emailTransporter
  });

  emailTransport.on('error', function(err) {
    loggers.console.error(err);

    if (loggers.db) loggers.db.error(err);
  });

  transports.push(emailTransport);

  loggers.email = new winston.Logger(Object.assign({}, sharedLoggerOptions, {transports: [emailTransport]}));
}

loggers.all = new winston.Logger(Object.assign({}, sharedLoggerOptions, {transports: transports}));
loggers.all.handleExceptions(transports);

module.exports = loggers.all;
