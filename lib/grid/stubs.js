const _ = require('lodash');

const users = rootRequire('routes/back/users/constants');
const apartments = rootRequire('routes/back/apartments/constants');

const stubs = {
  users: {
    dataSource: '/admin/users/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Id',
        use: '_id',
        filterable: true,
        sortable: true
      },
      {
        title: 'Email',
        use: 'email'
      },
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Verified',
        use: 'verified',
        filterable: false
      },
      {
        title: 'Role',
        use: 'role',
        filterOptions: _.map(users.ROLES, (v, k) => {
          return {value: k, title: v};
        })
      },
      {
        title: 'Developer',
        use: 'developer',
        filterable: false,
        sortable: false
      },
      {
        title: 'Group',
        use: 'group',
        filterable: false,
        sortable: false
      },
      {
        title: 'Group Developer',
        use: 'group',
        filterable: false,
        sortable: false
      },
      { title: 'Partner',
        user: 'partner',
        filterable: true,
        sortable: true
      }
    ]
  },
  groups: {
    dataSource: '/admin/groups/grid',
    sort: 'name',
    columns: [
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Developer',
        use: 'developer',
        filterable: false,
        sortable: false
      },
    ]
  },
  sceneTypes: {
    dataSource: '/admin/scene-types/grid',
    sort: 'name',
    columns: [
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Icon',
        use: 'icon',
        filterable: false,
        sortable: false
      },
      {
        title: 'Mobile',
        use: 'mobile',
        filterOptions: [
          {value: 'true', title: 'Mobile'}
        ],
      },
      {
        title: 'Fr',
        use: 'locales.fr'
      },
      {
        title: 'Weight',
        use: 'localesWeight.fr',
        filterable: false
      },
      {
        title: 'En',
        use: 'locales.en'
      },
      {
        title: 'Weight',
        use: 'localesWeight.en',
        filterable: false
      },
    ]
  },
  sequences: {
    dataSource: '/admin/sequences/grid',
    sort: 'name',
    columns: [
      {
        title: 'Name',
        use: 'name'
      }
    ]
  },
  layerTypes: {
    dataSource: '/admin/layer-types/grid',
    sort: 'name',
    columns: [
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Icon',
        use: 'icon',
        filterable: false,
        sortable: false
      },
      {
        title: 'Weight',
        use: 'weight',
        filterable: false
      },
      {
        title: 'Base Layer',
        use: 'isBaseLayer',
        filterable: false
      }
    ]
  },
  developers: {
    dataSource: '/admin/developers/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Programs',
        use: 'counterPrograms',
        filterable: false
      },
      {
        title: 'Apartments',
        use: 'counterApartments',
        filterable: false
      },
      {
        title: 'Rating',
        use: 'counterRating',
        filterable: false
      },
      {
        title: 'Shares',
        use: 'counterShares',
        filterable: false
      },
      {
        title: 'Requests',
        use: 'counterRequests',
        filterable: false
      },
      {
        title: 'Alias',
        use: 'alias'
      }
    ]
  },
  programs: {
    dataSource: '/admin/programs/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Path',
        use: 'path'
      },
      {
        title: 'Rating',
        use: 'counterRating',
        filterable: false
      },
      {
        title: 'Shares',
        use: 'counterShares',
        filterable: false
      }
    ]
  },
  buildings: {
    dataSource: '/admin/buildings/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Path',
        use: 'path'
      }
    ]
  },
  apartments: {
    dataSource: '/admin/apartments/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Path',
        use: 'path'
      },
      {
        title: 'Lot',
        use: 'lot'
      },
      {
        title: 'Type',
        use: 'type',
        filterOptions: _.map(apartments.TYPES, (v, k) => {
          return {value: k, title: v};
        })
      },
      {
        title: 'Area',
        use: 'area',
        filterable: false
      },
      {
        title: 'Price',
        use: 'price',
        filterable: false
      },
      {
        title: 'Rating',
        use: 'counterRating',
        filterable: false
      },
      {
        title: 'Shares',
        use: 'counterShares',
        filterable: false
      }
    ]
  },
  requests: {
    dataSource: '/admin/requests/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Email',
        use: 'email',
        sortable: false,
      },
      {
        title: 'Phone',
        use: 'phone',
        sortable: false,
      },/*
      {
        title: 'Message',
        use: 'message',
        sortable: false,
      },*/
      {
        title: 'Developer',
        use: 'developer',
        sortable: false,
      },
      {
        title: 'Program',
        use: 'program',
        sortable: false,
      },
      {
        title: 'Budget from',
        use: 'budgetFrom',
        filterable: false,
      },
      {
        title: 'Budget to',
        use: 'budgetTo',
        filterable: false,
      },
      {
        title: 'Type',
        use: 'type',
        filterOptions: [
          {value: 'apartment', title: 'Apartment'},
          {value: 'info', title: 'Info'},
          {value: 'glasses', title: 'Glasses'}
        ]
      },
      {
        title: 'Status',
        use: 'status',
        filterOptions: [
          {value: 'new', title: 'New'},
          {value: 'processed', title: 'Processed'},
        ]
      }
    ]
  },
  tours: {
    dataSource: '/admin/tours/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Type',
        use: 'type',
        filterOptions: [
          {value: 'exterior', title: 'Exterior tour'},
          {value: 'interior', title: 'Interior tour'},
        ],
        sortable: false,
      },
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Path',
        use: 'path'
      },
      {
        title: 'Rating',
        use: 'counterRating',
        filterable: false
      },
      {
        title: 'Shares',
        use: 'counterShares',
        filterable: false
      }
    ]
  },
  imagesets: {
    dataSource: '/admin/imagesets/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Name',
        use: 'name'
      }
    ]
  },
  tasks: {
    dataSource: '/admin/tasks/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Id',
        use: '_id',
        filterable: true,
        sortable: true
      },
      {
        title: 'Type',
        use: 'type',
        filterable: false,
      },
      {
        title: 'Status',
        use: 'status',
        filterable: false,
      },
      {
        title: 'Host',
        use: 'host',
        filterable: false,
      },
      {
        title: 'Duration',
        use: 'duration',
        filterable: false,
      },
      {
        title: 'Error',
        use: 'error',
        filterable: false,
      }
    ]
  },
  logs: {
    dataSource: '/admin/logs/grid',
    sort: '-timestamp',
    columns: [
      {
        title: 'Date',
        use: 'timestamp',
        filterable: false,
      },
      {
        title: 'Message',
        use: 'message',
        filterable: true,
        searchable: true,
      },
      {
        title: 'Url',
        use: '',
        filterable: false,
      },
      {
        title: 'Referer',
        use: '',
        filterable: false,
      }
    ]
  },
  apptours: {
    dataSource: '/admin/apptours/grid',
    sort: '-createdAt',
    columns: [
      {
        title: 'Tour thumbnail',
        filterable: false
      },
      {
        title: 'Date',
        use: 'createdAt',
        filterable: false,
      },
      {
        title: 'Id',
        use: '_id',
        sortable: false,
      },
      {
        title: 'Status',
        use: 'generateStatus',
        filterable: false,
        sortable: false,
      },
      {
        title: 'Rating',
        use: 'counterRating',
        filterable: false
      },
      {
        title: 'Shares',
        use: 'counterShares',
        filterable: false
      },
      {
        title: 'Moderation',
        use: 'moderationStatus',
        filterOptions: [
          {value: 'pending', title: 'Pending'},
          {value: 'approved', title: 'Approved'},
          {value: 'rejected', title: 'Rejected'},
        ],
        sortable: false
      }
    ]
  },
  phrases: {
    dataSource: '/admin/phrases/grid',
    sort: 'name',
    columns: [
      {
        title: 'Name',
        use: 'name'
      }
    ]
  },
  partners: {
    dataSource: '/admin/partners/grid',
    sort: 'name',
    columns: [
      {
        title: 'Name',
        use: 'name'
      },
      {
        title: 'Code',
        use: 'code'
      },
      {
        title: 'Email',
        use: 'email'
      },
      { title: 'Exports',
        use: ''
      }
    ]
  },
};

module.exports = stubs;
