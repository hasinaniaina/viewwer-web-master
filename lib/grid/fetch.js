const _ = require('lodash');
const mongoose = require('mongoose');
const parseFilter = require('./parse-filter');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

/**
 * Fetches data from the model using general filtering settings and taking into account URL parameters
 * @param req
 * @param model
 * @param filter
 * @param populate
 * @returns {Promise}
 */
function fetch(req, model, filter, populate) {
  const Model = mongoose.model(model);
  const queryFilter = _.extend({}, parseFilter(req.query.filter), filter);

  const rowsQuery = Model.find(applyPermissions(queryFilter, req.user));
  const countQuery = Model.countDocuments(applyPermissions(queryFilter, req.user));

  if (populate) rowsQuery.populate(populate);

  if (req.query.limit) rowsQuery.limit(parseInt(req.query.limit, 10));
  if (req.query.sort) rowsQuery.sort(req.query.sort);
  if (req.query.skip) rowsQuery.skip(parseInt(req.query.skip, 10));

  return Promise.all([rowsQuery.exec(), countQuery.exec()])
    .then(function(results) {
      const rows = results[0];
      const totalCount = results[1];
      return {
        rows: rows,
        totalCount: totalCount
      };
    });
}

module.exports = fetch;
