const _ = require('lodash');

/**
 * Converts filter parameters sent via URL query into format usable in mongo queries
 * @param {Object} filter - URL parameters
 * @returns {Object} - converted values
 */
function parseFilter(filter) {
  return _.mapValues(filter, function(value) {
    if (!_.isString(value)) return value;

    const specialParams = value.match(/^(\w+):(.+)$/);

    if (specialParams) {
      const command = specialParams[1];
      const param = specialParams[2];

      switch (command) {
        case 'boolean':
          value = Boolean(parseInt(param, 10));
          break;
        case 'exists':
          value = {$exists: Boolean(parseInt(param, 10))};
          break;
        case 'regex':
          value = {$regex: param, $options: 'i'};
          break;
      }
    }

    return value;
  });
}

module.exports = parseFilter;
