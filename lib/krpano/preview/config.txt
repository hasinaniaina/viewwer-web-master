# Documentation: http://krpano.com/tools/kmakemultires/config?version=119
# krpano 1.19

flash=false
html=false
preview=false
askforxmloverwrite=false
panotype=sphere
hfov=360
converttocube=true
converttocubelimit=360x45
xmlpath=%INPUTPATH%/preview/preview.xml
tilepath=%INPUTPATH%/preview/%BASENAME%.tiles/l%Al[_c]_%Av_%Ah.jpg
tempcubepath=%INPUTPATH%/preview/tmp/%BASENAME%_cube%UID%
