const sanitize = require('sanitize-filename');

const AWS = require('aws-sdk');
AWS.config.update(
  {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET,
    region: process.env.AWS_REGION_S3
  }
);
const s3 = new AWS.S3();

// for some advanced functions, for example - recursive removal
const s3client = require('s3-client').createClient({
  s3Client: s3,
});

const AWS_DIR_TASKS = 'tasks';
const AWS_DIR_SOURCE = 'src';
const AWS_DIR_RESULT = 'result';

module.exports.downloadResultBuffer = function(task_id, path) {
  return new Promise((resolve, reject) => {
    const d = s3client.downloadBuffer({
      Bucket: process.env.AWS_BUCKET,
      Key: AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_RESULT + path
    });
    d.on('error', function (err) {
      reject(err);
    });
    d.on('end', function (r) {
      resolve(r);
    });
  });
};

module.exports.uploadFile = function (task_id, file, filename) {
  return new Promise((resolve, reject) => {
    const u = s3client.uploadFile({
      localFile: file,
      s3Params: {
        Bucket: process.env.AWS_BUCKET,
        Key: AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_SOURCE + '/' + filename
      }
    });
    u.on('error', function (err) {
      reject(err);
    });
    u.on('end', function () {
      resolve();
    });
  });
};

module.exports.uploadDir = function (task_id, dir) {
  return new Promise((resolve, reject) => {
    const prefix = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_SOURCE;
    const u = s3client.uploadDir({
      localDir: dir,
      s3Params: {
        Bucket: process.env.AWS_BUCKET,
        Prefix: prefix
      }
    });
    u.on('error', function (err) {
      reject(err);
    });
    u.on('end', function () {
      resolve();
    });
  });
};

module.exports.downloadDir = function (task_id, dir) {
  return new Promise((resolve, reject) => {
    const prefix = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_RESULT;
    const d = s3client.downloadDir({
      localDir: dir,
      s3Params: {
        Bucket: process.env.AWS_BUCKET,
        Prefix: prefix
      },
      getS3Params: function (localFile, s3Object, callback) {
        if (endsWith(localFile, '/')) {
          callback(null, null); // Skip folders
        } else {
          callback(null, {});
        }
      }
    });
    d.on('error', function (err) {
      reject(err);
    });
    d.on('end', function () {
      resolve();
    });
  });
};

module.exports.removeFile = function (task_id, filename) {
  return new Promise((resolve, reject) => {
    s3.deleteObject({
      Bucket: process.env.AWS_BUCKET,
      Key: AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_SOURCE + '/' + sanitize(filename)
    }, function (err, data) {
      if (err) return reject(err);
      resolve(data);
    });
  });
};

module.exports.removeTask = function (task_id) {
  return new Promise((resolve, reject) => {
    if (!task_id) return reject();
    const d = s3client.deleteDir({
      Bucket: process.env.AWS_BUCKET,
      Prefix: AWS_DIR_TASKS + '/' + task_id + '/'
    });
    d.on('error', function (err) {
      reject(err);
    });
    d.on('end', function () {
      resolve();
    });
  });
};

module.exports.getPublicTasksUrl = function (task_id) {
  return 'https://' + process.env.AWS_BUCKET
    + '.s3.amazonaws.com/' + AWS_DIR_TASKS
    + '/' + task_id + '/' + AWS_DIR_RESULT;
};

module.exports.getPublicHost = function () {
  return 'https://' + process.env.AWS_BUCKET + '.s3.amazonaws.com';
};

function endsWith(str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
