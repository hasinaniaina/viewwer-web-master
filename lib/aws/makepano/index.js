const AWS = require('aws-sdk');
AWS.config.update(
  {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET,
    region: process.env.AWS_REGION_LAMBDA,
    httpOptions: {
      // timeout on socket inactivity, default - 120000
      timeout: parseInt(process.env.AWS_LAMBDA_SOCKET_TIMEOUT)
    }
  }
);
const lambda = new AWS.Lambda();

const TaskError = rootRequire('/routes/api/task/task-error');

module.exports.makepano = function (task_id, parameters) {
  return new Promise((resolve, reject) => {
    invokeLambda(task_id, parameters)
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

function invokeLambda(task_id, parameters) {
  return new Promise((resolve, reject) => {
    let payload = {
      taskId: task_id,
      parameters: parameters
    };
    lambda.invoke({
      FunctionName: process.env.AWS_LAMBDA_MAKEPANO,
      Payload: JSON.stringify(payload, null, 2)
    }, function (error, data) {

      if (error) return reject(new TaskError('service_error', error.message));

      try {
        let p = JSON.parse(data.Payload);

        if (!p) return reject(new TaskError('service_empty_payload'));

        if (p.result) return resolve(p.result);

        if (p.errorMessage && p.errorMessage.includes('Task timed out')) {
          return reject(new TaskError('task_timeout', p.errorMessage));
        }

        reject(new TaskError('service_unknown_error', p.errorMessage));

      } catch (e) {
        reject(new TaskError('service_payload_parse_error', e.message));
      }

    });
  });
}

