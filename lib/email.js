const nodemailer = require('nodemailer');

const transporterOptions = {
  host: process.env.SMTP_HOST
};

if (process.env.SMTP_PORT) transporterOptions.port = process.env.SMTP_PORT;
if (process.env.SMTP_SECURE) transporterOptions.secure = true;

if (process.env.SMTP_LOGIN) {
  Object.assign(transporterOptions, {
    auth: {
      user: process.env.SMTP_LOGIN,
      pass: process.env.SMTP_PASSWORD
    }
  });
}

const transporter = nodemailer.createTransport(transporterOptions);

module.exports = {
  send: transporter.sendMail.bind(transporter),
  transporter: transporter
};
