const mongoose = require('mongoose');

module.exports = function (type, id, req) {
  if (!req.user || req.user.role !== 'admin') return;
  return mongoose.model(type).findById(id).exec();
};
