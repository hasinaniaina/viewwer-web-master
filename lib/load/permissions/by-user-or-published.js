const mongoose = require('mongoose');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

/*
  Allows view unpublished objects in frontend for logged-in users
 */
module.exports = function (type, id, req) {
  const Model = mongoose.model(type);
  if (req.user) {
    return Model.findOne(applyPermissions({_id: id}, req.user, true)).exec();
  } else {
    return Model.findOne({_id: id, isPublished: true}).exec();
  }
};
