const mongoose = require('mongoose');

module.exports = function (type, id) {
  return mongoose.model(type).findOne({_id: id, isPublished: true}).exec();
};
