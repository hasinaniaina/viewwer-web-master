const mongoose = require('mongoose');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

module.exports = function(type, id, req) {
  return mongoose.model(type).findOne(applyPermissions({_id: id}, req.user)).exec();
};
