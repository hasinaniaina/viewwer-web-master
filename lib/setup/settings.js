const mongoose = require('mongoose');

/**
 * Creates the initial Settings object.
 * @returns {Promise}
 */
function createSettings() {
  const Setting = mongoose.model('Setting');

  return Setting
    .countDocuments({})
    .exec()
    .then(function(count) {
      if (count) return;

      return new Setting({}).save();
    });
}

module.exports = createSettings;
