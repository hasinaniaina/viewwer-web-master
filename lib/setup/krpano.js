const child_process = require('child_process');
const os = require('os');
const path = require('path');
const mongoose = require('mongoose');
const Setting = mongoose.model('Setting');
const logger = require('../logger');
const fs = require('fs');

module.exports = function() {
  const key = process.env.KRPANO_KEY;

  if (!key) return;

  let ext = os.platform() == "win32" ? ".exe" : "";
  let executable = path.resolve('lib/krpano/bin/' + os.platform() + '/' + os.arch() + '/krpanotools' + ext);
  logger.info("using krpano executable '" + executable + "' for register and protect");
  fs.access(executable, fs.constants.X_OK, function(err) {
    if (err) logger.info("krpano execute permission : " + err);
  });

  child_process.execFile(executable, ['register', key], function(err, stdout) {
    if (stdout.indexOf('ERROR') != -1) throw new Error(stdout);
    if (err) throw err;

    child_process.execFile(executable, ['protect', '-o=' + path.resolve('public/assets/shared/krpano/krpano.licensed.js')], function(err, stdout) {
      if (stdout.indexOf('ERROR') != -1) throw new Error(stdout);
      if (err) throw err;

      Setting
        .update({}, {krpanoLicensed: true}, {upsert: true})
        .exec()
        .catch(function(err) {
          throw err;
        });
    });
  });
};
