/**
 * Loads documents encoded in request parameters, so every and each route doesn't have to.
 * @param {*} app
 */
function setupParamsLoading(app) {

  const typesBack = [
    'developer',
    'program',
    'building',
    'apartment',
    'tour',
    'sceneType',
    'layerType',
    'sequence',
    'phrase',
    'user',
    'group',
    'request',
    'imageset',
    'task',
    'log',
    'apptour',
    'partner'
  ];

  const typesFront = [
    'developerFront',
    'programFront',
    'apartmentFront',
    'tourFront',
    'apptourFront'
  ];

  const typesApi = [
    'taskApi',
    'tourApi',
    'favoriteApi'
  ];

  typesBack.forEach(type => {
    app.param(type + 'Id', (req, res, next, id) => {
      rootRequire('lib/load/back/' + type)(id, req)
        .then(doc => {
          if (!doc) return next(res.createError(404));
          res.locals[type] = doc;
          next();
        })
        .catch(next);
    });
  });

  typesFront.forEach(type => {
    app.param(type + 'Id', (req, res, next, id) => {
      rootRequire('lib/load/front/' + type)(id, req)
        .then(doc => {
          if (!doc) return next(res.createError(404));
          res.locals[type] = doc;
          next();
        })
        .catch(next);
    });
  });

  typesApi.forEach(type => {
    app.param(type + 'Id', (req, res, next, id) => {
      rootRequire('lib/load/api/' + type)(id, req)
        .then(doc => {
          if (!doc) return next(res.createError(404));
          res.locals[type] = doc;
          next();
        })
        .catch(next);
    });
  });

}

module.exports = setupParamsLoading;

