const _ = require('lodash');
const shortid = require('shortid');

const i18n = require('i18n');

const nunjucksEnv = rootRequire('lib/nunjucks-env');

const requests = rootRequire('routes/back/requests/constants');
const programs = rootRequire('routes/back/programs/constants');
const apartments = rootRequire('routes/back/apartments/constants');
const apptours = rootRequire('routes/back/apptours/constants');
const users = rootRequire('routes/back/users/constants');

const talkjs = rootRequire('lib/talkjs');

/**
 * Configures app to use template engine for page rendering.
 * @param {Object} app - Express app
 */
function setupTemplating(app) {
  nunjucksEnv.express(app);
  app.set('view engine', 'njk');

  nunjucksEnv.addGlobal('getLocaleStrings', function (locale) {
    return JSON.stringify(i18n.getCatalog(locale));
  });

  nunjucksEnv.addGlobal('shortid', shortid.generate);

  nunjucksEnv.addGlobal('dateNow', Date.now);

  nunjucksEnv.addGlobal('talkjsSignature', function (user_id) {
    return talkjs.getSignature(user_id);
  });

  nunjucksEnv.addGlobal('talkjsAppId', function () {
    return talkjs.getAppId();
  });

  nunjucksEnv.addGlobal('talkjsGetRole', function (prefix, locale) {
    return talkjs.getRole(prefix, locale);
  });

  // using unix timestamp to prevent nunjucks automatic isodate convert
  nunjucksEnv.addFilter('timestamp', function(isodate) {
    return new Date(isodate).getTime();
  });

  nunjucksEnv.addFilter('find', function(arr, search) {
    return _.find(arr, search);
  });

  nunjucksEnv.addFilter('stringify', function(obj) {
    return JSON.stringify(obj, null, '\t');
  });

  nunjucksEnv.addFilter('userRole', function(role) {
    return users.ROLES[role];
  });

  nunjucksEnv.addFilter('usersGroups', function (users) {
    return _.map(users, function (u) {
      return {
        user_id: u._id,
        group_id: _.get(u, 'group._id')
      };
    });
  });

  nunjucksEnv.addFilter('requestTimeframe', function(timeframe) {
    return requests.TIMEFRAMES[timeframe];
  });

  nunjucksEnv.addFilter('requestType', function(type) {
    return requests.TYPES[type];
  });

  nunjucksEnv.addFilter('requestStatus', function(status) {
    const statuses = {
      new: 'New',
      processed: 'Processed'
    };

    return statuses[status];
  });

  nunjucksEnv.addFilter('apartmentType', function(type) {
    return apartments.TYPES[type];
  });

  nunjucksEnv.addFilter('saleType', function(type) {
    return apartments.SALE_TYPES[type];
  });

  nunjucksEnv.addFilter('programDevStatus', function(status) {
    return programs.DEV_STATUSES[status];
  });

  nunjucksEnv.addFilter('tourGenerateStatus', function(status) {
    const statuses = {
      processing: 'Tour generation in progress',
      failed: 'Tour generation failed',
      completed: 'Tour generation complete'
    };

    return statuses[status];
  });

  nunjucksEnv.addFilter('apptourType', function(type) {
    return apptours.TYPES[type];
  });

}

module.exports = setupTemplating;
