const list = require('../../routes/list');

/**
 * Attaches routes to the app.
 * @param {Object} app - Express app
 */
function setupRouting(app) {
  list.forEach(function(record) {
    ['use', 'get', 'post', 'delete'].forEach(function(method) {
      if (record[method]) {
        if (Array.isArray(record[method])) {
          record[method].forEach(function(path) {
            app[method](record.route || '/', require('../../routes/' + path));
          });
        } else {
          app[method](record.route || '/', require('../../routes/' + record[method]));
        }
      }
    });
  });
}

module.exports = setupRouting;
