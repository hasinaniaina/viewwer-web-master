const globby = require('globby');

/**
 * Preloads models so that they're available via mongoose.model
 */
function preloadModels() {
  return globby(['models/*.js', '!models/base.js']).then(files => files.forEach(file => require('../../' + file)));
}

module.exports = preloadModels;
