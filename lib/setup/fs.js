const fs = require('fs-extra');

module.exports = function() {
  fs.ensureDirSync(process.env.STORAGE_PATH + '/private/tmp');
  fs.ensureDirSync(process.env.STORAGE_PATH + '/public/tours');
  fs.ensureDirSync(process.env.STORAGE_PATH + '/public/apptours');
  fs.ensureDirSync(process.env.STORAGE_PATH + '/public/files/hotspots');
};
