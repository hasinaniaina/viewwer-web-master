function setupWwwRedirect(app) {
  app.all('*', function redirect(req, res, next) {
    if (req.headers.host.slice(0, 4) === 'www.') {
      return res.redirect(301, req.protocol + '://' + process.env.DOMAIN + req.originalUrl);
    }
    next();
  });
}

module.exports = setupWwwRedirect;
