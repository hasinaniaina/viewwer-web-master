const mongoose = require('mongoose');

/**
 * Creates the initial admin account.
 * @returns {Promise}
 */
function createAdmin() {
  if (!(process.env.ADMIN_EMAIL && process.env.ADMIN_PASSWORD)) return Promise.resolve();

  const User = mongoose.model('User');

  return User
    .findOne({
      email: process.env.ADMIN_EMAIL
    })
    .lean()
    .exec()
    .then(function(alreadyExisting) {
      if (alreadyExisting) return;

      const user = new User({
        email: process.env.ADMIN_EMAIL,
        name: 'Admin',
        role: 'admin'
      });

      return User.register(user, process.env.ADMIN_PASSWORD);
    });
}

module.exports = createAdmin;
