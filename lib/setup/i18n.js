const i18n = require('i18n');
const locales = rootRequire('locales/list');

module.exports = function() {
  i18n.configure({
    locales: locales.map(locale => {
      return locale.code;
    }),
    defaultLocale: locales.find(locale => {
      return locale.isDefault === true;
    }).code,
    queryParameter: 'locale',
    cookie: 'locale',
    directory: __dirname + '/../../locales',
    syncFiles: true,
    autoReload: process.env.NODE_ENV === 'development'
  });
};
