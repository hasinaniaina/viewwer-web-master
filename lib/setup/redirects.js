const list = require('../../routes/redirects');

function setupRedirects(app) {
  list.forEach(function (record) {
    app.all(record.path, function redirect(req, res) {
      return res.redirect(301, req.protocol + '://' + record.target + req.originalUrl);
    });
  });
}

module.exports = setupRedirects;
