/**
 * @file Templating engine configuration.
 */

const nunjucks = require('nunjucks');

const nunjucksEnv = nunjucks.configure('views', {
  trimBlocks: true,
  lstripBlocks: true,
  watch: true
});

module.exports = nunjucksEnv;
