const mongoose = require('mongoose');
const User = mongoose.model('User');

const logger = rootRequire('lib/logger');

const admin = require('firebase-admin');

const settings = rootRequire('firebase.json');

let account;
if (process.env.NODE_ENV === 'production') {
  account = settings.prod;
} else {
  account = settings.dev;
}

admin.initializeApp({
  credential: admin.credential.cert(account.serviceAccount),
  databaseURL: account.databaseURL
});

module.exports.send = function (user_id, message, data) {
  return new Promise((resolve, reject) => {

    User
      .findById(user_id)
      .exec()
      .then(r => {
        if (!r) throw new Error('user_not_found');
        if (!r.firebaseRegistrationToken) return;

        const payload = {
          notification: {
            title: 'Viewwer',
            body: message,
            sound: 'default'
          },
          data: (data) ? data : {}
        };

        const options = {};

        return admin.messaging().sendToDevice(r.firebaseRegistrationToken, payload, options);
      })
      .then(r => {
        if (!r) return resolve();
        if (r.successCount > 0) {
          logger.debug('Firebase notification user:', user_id, 'message:', message, data, r);
          return resolve();
        } else {
          logger.error('Firebase error', r);
          return reject(new Error('firebase_error'));
        }
      })
      .catch(e => reject(e));

  });
};

module.exports.test = function (token) {

  return new Promise((resolve, reject) => {

    const payload = {
      notification: {
        title: 'This is a Notification',
        body: 'This is the body of the notification message.'
      }
    };

    const options = {
      priority: 'high',
      timeToLive: 60 * 60 * 24
    };

    admin.messaging().sendToDevice(token, payload, options)
      .then(r => {
        if (r && r.successCount > 0) return resolve();
        reject();
      })
      .catch(e => {
        reject(e);
      });
  });

};
