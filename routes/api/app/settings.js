const _ = require('lodash');

const mongoose = require('mongoose');
const SceneType = mongoose.model('SceneType');

const DEFAULT_ICON = '/assets/shared/images/marker.png';
const HOTSPOTS_STORAGE = '/storage/files/hotspots/';

const constants = rootRequire('routes/back/apptours/constants');

module.exports = function (req, res, next) {

  const APP_SETTINGS = {
    app: {
      minimumVersion: res.locals.site.settings.mobileAppMinimalVersion,
      maintenance: !!(res.locals.site.settings.maintenanceMobileApp),
      urlPasswordReset: '/auth/request-password-reset?mobile=true',
      urlMessages: '/dashboard/messages'
    },
    signup: {
      minimalPasswordScore: parseInt(process.env.PASSWORD_SCORE),
      minimalPasswordLength: 8,
      minimalUserNameLength: 3
    },
    thumbnails: {
      width: 540,
      height: 300
    },
    scenes: {
      types: []
    },
    apartmentTypes: getApartmentTypes(),
    locales: ['en', 'fr'],
    activities: [
      { id : 'agence', locales : { fr : 'Agence immobilière', en : 'Real estate agency' } },
      { id : 'mandataire', locales: { fr : 'Mandataire', en : 'Representative' }},
      { id : 'promoteur/constructeur', locales: { fr : 'Promoteur / constructeur', en : 'Developer / builder' } },
      { id : 'gestionnaire', locales: { fr : 'Syndic et gestionnaire', en : 'Manager' } },
      { id : 'marchand_de_bien', locales: { fr : 'Marchand de bien', en : 'Housing merchant' } },
      { id : 'architecte', locales: { fr : 'Architecte', en : 'Architect' } },
      { id : 'autre', locales: { fr : 'Autre', en : 'Other' } }
    ]
  };

  SceneType.find({mobile: true}).exec()
    .then(r => {
      APP_SETTINGS.scenes.types = _.map(r, st => {
        const icon = (st.icon) ? HOTSPOTS_STORAGE + st.icon : DEFAULT_ICON;
        return {
          id: st._id,
          locales: st.locales,
          localesWeight: st.localesWeight,
          iconUrl: icon
        };
      });
      res.json(APP_SETTINGS);
    })
    .catch(next);
};

function getApartmentTypes() {
  const types = [];
  _.each(constants.TYPES, (v, k) => {
    types.push({
      type: k,
      locales: {en: v, fr: v},
    });
  });
  return types;
}
