const _ = require('lodash');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);

  const update = {
    firebaseRegistrationToken: body.token,
  };

  req.user
    .set(update)
    .save()
    .then(() => res.json({status: 'success'}))
    .catch(next);
};
