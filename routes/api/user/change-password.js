const _ = require('lodash');

const logger = rootRequire('lib/logger');

module.exports = function (req, res) {
  const body = _.cloneDeep(req.body);

  if (req.user.loginProvider) {
    return res.json({
      status: 'failed',
      error: 'cant_change_password',
      errorDescription: 'unable to change password for external login'
    });
  }
  
  req.user
    .changePassword(body.oldPassword, body.newPassword)
    .then(() => req.user.save())
    .then(() => res.json({status: 'success'}))
    .catch(e => {
      logger.error('Api change password', 'error', e);
      return res.json({
        status: 'failed',
        error: 'wrong_credentials',
        errorDescription: 'wrong credentials'
      });
    });
};
