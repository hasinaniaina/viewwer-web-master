module.exports = function (req, res) {
  console.log('get profile', req.user);
  res.json({
    firstName: (req.user.firstName) ? req.user.firstName : '',
    lastName: (req.user.lastName) ? req.user.lastName : '',
    phone: (req.user.phone) ? req.user.phone : '',
    canChangePassword: (req.user.loginProvider) ? false : true
  });
};
