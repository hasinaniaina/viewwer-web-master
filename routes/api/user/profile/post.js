const _ = require('lodash');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);

  const update = {
    firstName: body.firstName,
    lastName: body.lastName,
    phone: body.phone
  };

  req.user
    .set(update)
    .save()
    .then(() => res.json({status: 'success'}))
    .catch(next);
};
