// register a user (if new) and log him with a facebook login
// input = facebook access token
const passport = require('passport');
const request = require('request');
const logger = rootRequire('lib/logger');
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = function (req, res, next) {

    function queryAccessToken(loginProvider, token, callback) {
        let url = "";
        if (loginProvider == "facebook") url = "https://graph.facebook.com/me?fields=name,email&access_token=";
        if (loginProvider == "google") url = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=";
        
        // make a facebook graph request
        request(url + token, { json: true }, (err, r, body) => {
            if (err) {
                logger.error('Api Auth', 'Unable to check facebook access token', err);

                callback(err, null);
            }
            else {
                console.log('queryAccessToken response', body);
                callback(null, {
                    email: body.email,
                    name: body.name,
                    id: body.id
                });
            }
        });
    }

    let accessToken = req.query.accessToken;
    let loginProvider = req.query.loginProvider;

    if (!accessToken) {
        return res.json({
            status: 'failed',
            error: 'internal',
            errorDescription: 'missing access token'
        });
    }

    if (!loginProvider) {
        return res.json({
            status: 'failed',
            error: 'internal',
            errorDescription: 'missing login provider'
        });
    }

    logger.info("register-and-auth-with-access-token : Login with Facebook from mobile app (with access token)");

    // make a facebook graph request
    queryAccessToken(loginProvider, accessToken, (err, tokenInfo) => {
        if (err) {

            return res.json({
                status: 'failed',
                error: 'internal',
                errorDescription: 'unable to check access token'
            });
        }

        // here we have graph information: name, email, id
        // we use the same algorithm as in setup-passport.js :
        // first: check if the user exists, if not create it
        // finally: log the user in
        console.log('search for user with email ' + tokenInfo.email);

        User.findOne({ email: tokenInfo.email }, function (err, user) {
            if (err) {
                logger.error('Api auth', 'register-and-auth-with-access-token : unable to query database to find a user', err);

                return res.json({
                    status: 'failed',
                    error: 'internal',
                    errorDescription: 'unable to query database'
                });
            }

            console.log('user found', user);
            
            if (!user) {
                // the user doesn't exist, we create a new user
                logger.info("register-and-auth-with-access-token : registering a new user with email " + tokenInfo.email);

                let newUser = new User({
                    email: tokenInfo.email,
                    name: tokenInfo.name,
                    loginProvider: loginProvider,
                    providerDisplayName: loginProvider,
                    providerKey: tokenInfo.id
                });

                newUser.save(function (err) {
                    if (err) {
                        logger.error('Api auth', 'register-and-auth-with-access-token : unable to create a new user', err);

                        return res.json({
                            status: 'failed',
                            error: 'internal',
                            errorDescription: 'unable to create a new user in database'
                        });
                    }
                    else {
                        // user is created, we must log him
                        req.logIn(newUser, (err) => {
                            if (err) {
                                logger.error('Api auth', 'register-and-auth-with-access-token : log in internal error', err);

                                return res.json({
                                    status: 'failed',
                                    error: 'internal',
                                    errorDescription: 'internal server error'
                                });
                            }

                            return res.json({
                                status: 'success'
                            });
                        });
                    }
                });
            }
            else {
                // the user exists, we must log him
                if (user.role === 'blocked') {
                    return res.json({
                        status: 'failed',
                        error: 'blocked',
                        errorDescription: 'account is blocked'
                    });
                }
                else {
                    // not blocked          

                    req.logIn(user, (err) => {
                        if (err) {
                            logger.error('Api auth', 'register-and-auth-with-access-token : log in error', err);

                            return res.json({
                                status: 'failed',
                                error: 'internal',
                                errorDescription: 'internal server error'
                            });
                        }

                        logger.info("register-and-auth-with-access-token : Logged an existing user with email " + tokenInfo.email);

                        return res.json({
                            status: 'success'
                        });
                    });
                }
            }
        });
    });
};
