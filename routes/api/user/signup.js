const _ = require('lodash');

const {check, validationResult} = require('express-validator/check');

const mongoose = require('mongoose');
const User = mongoose.model('User');

const sendVerifyEmail = rootRequire('/routes/back/auth/verify/send-verify-email');
const sendVerifyPartnershipEmail = rootRequire('/routes/back/auth/verify/send-verify-partnership');
const checkBadNames = rootRequire('/routes/back/auth/sign-up/check-bad-names');
const Partner = mongoose.model('Partner');

const logger = rootRequire('lib/logger');

module.exports =[[

  check('email')
    .escape()
    .isEmail()
    .withMessage('bad_request'),
  check('firstName')
    .escape()
    .isLength({ min: 6 })
    .withMessage('wrong_user_name')
    .custom(value => checkBadNames(value))
    .withMessage('wrong_user_name'),
  check('lastName')
    .escape()
    .isLength({ min: 3 })
    .withMessage('wrong_user_name'),
  check('phone').escape()

], (req, res) => {

  const body = _.cloneDeep(req.body);

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.json({
      status: 'failed',
      error: errors.array()[0].msg,
      errorDescription: 'bad request'
    });
  }

  let user;
  let responseSent = false;

  Partner.findOne({ code: body.partnerCode, isEnabled: true })
  .then(partner => {

    if (body.partnerCode && !partner) {        
      return res.json({
        status: 'failed',
        error: 'internal',
        errorDescription: 'bad partner code'
      });
    }

    return User
      .register({
        email: body.email,
        firstName: body.firstName,
        lastName: body.lastName,
        phone: body.phone,
        partner: partner,
        isPro: body.isPro,
        companyName: body.companyName,
        activity: body.activity
      }, body.password)
  })  
  .then(r => {
    user = r;
    responseSent = true;
    res.json({
      status: 'success'
    });
  })
  .then(() => sendVerifyEmail(user, req))
  .then(() => sendVerifyPartnershipEmail(user, req))
  .catch(e => {
    logger.error('Api signup', 'internal error', e);
    if (responseSent) return;

    if (e.name === 'UserExistsError') {
      return res.json({
        status: 'failed',
        error: 'user_exists',
        errorDescription: 'user already exists'
      });
    }

    res.json({
      status: 'failed',
      error: 'internal',
      errorDescription: 'internal server error'
    });
  });
}];
