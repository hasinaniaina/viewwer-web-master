const passport = require('passport');

const logger = rootRequire('lib/logger');

module.exports = function (req, res, next) {

  passport.authenticate('local', (err, user, info) => {

    if (err) {
      logger.error('Api auth', 'internal error', err, info);
      return res.json({
        status: 'failed',
        error: 'internal',
        errorDescription: 'internal server error'
      });
    }

    if (!user) {
      logger.info('Api auth', 'wrong credentials error', info);
      return res.json({
        status: 'failed',
        error: 'wrong_credentials',
        errorDescription: 'wrong credentials'
      });
    }

    if (user.role === 'blocked') {
      return res.json({
        status: 'failed',
        error: 'blocked',
        errorDescription: 'account is blocked'
      });
    }

    req.logIn(user, (err) => {
      if (err) {
        logger.error('Api auth', 'internal error', err);
        return res.json({
          status: 'failed',
          error: 'internal',
          errorDescription: 'internal server error'
        });
      }

      res.json({
        status: 'success'
      });
    });
  })(req, res, next);
};
