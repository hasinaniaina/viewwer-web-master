const _ = require('lodash');
const EasyXml = require('easyxml');
const mongoose = require('mongoose');
const Partner = mongoose.model('Partner');
const AppTour = mongoose.model('AppTour');

// export ads for a given partner
// input : partnerCode & key (can be found in db collection Partners)

module.exports = function (req, res, next) {

  function getResponseFormat(request) {
    // output format (json / xml)
    let format = "json";

    // auto-detect format from request  
    if (request.accepts('json') || request.accepts('text/html')) {
      format = "json";
    }
    else if (request.accepts('application/xml')) {
      format = "xml";
    }

    // force format ?
    if (request.query.format) {
      format = (request.query.format == 'xml') ? 'xml' : 'json';
    }

    return format;
  }

  function sendXMLResponse(result, response) {
    response.header('Content-Type', 'text/xml');

    let serializer = new EasyXml({
      singularize: true,
      rootElement: 'response',
      dateFormat: 'ISO',
      manifest: true
    });

    let xml = serializer.render(result);
    res.send(xml);
  }

  function sendJSONResponse(result, response) {
    response.header('Content-Type', 'application/json');
    response.json(result);
  }

  let format = getResponseFormat(req);
  let result = { status: 'success' };
  let days = parseInt(req.query.days || '1') || 1;

  let partnerCode = req.query.partnerCode;  
  let partnerKey = req.query.key;

  if (!partnerCode || !partnerKey)
    return next(res.createError(403, 'Partner code or Key are missing'));

  var minDate = new Date();
  minDate.setDate(minDate.getDate() - days);
  minDate.setHours(0,0,0,0);

  Partner.findOne({ code : partnerCode, key: partnerKey })
  .exec()
  .then(partner => {
    if (!partner) throw res.createError(404, 'Partner not found');

    return AppTour
            .find({ moderationStatus: 'approved', isPublished: true, partnerCode: partnerCode, updatedAt: { $gte : minDate } })
            .sort({ updatedAt : -1 })
            .populate('user')
            .exec();
  })
  .then(apptours => {
    result.ads = _.map(apptours, apptour => {
      return {
        id: apptour._id,
        type: apptour.type,
        description: apptour.description,
        gasClass: apptour.gasClass,
        energyClass: apptour.energyClass,
        isSale: apptour.saleType == "sale",
        isRent: apptour.saleType == "rent",
        isAvailable: !apptour.isSold,
        address: apptour.address,
        area: apptour.area,
        price: apptour.price,
        createdAt: apptour.createdAt,
        updatedAt: apptour.updatedAt,
        thumbnail: process.env.BASE_URL + apptour.thumbnail,
        tourUrl: process.env.BASE_URL + "/apptours/" + apptour._id,
        contactEmail: apptour.user.email,
        contactName: apptour.user.name,
        contactPhone: apptour.user.phone
      }
    });   

    result.minDate = minDate;

    if (format == 'xml') {
      sendXMLResponse(result, res);    
    }
    else {
      sendJSONResponse(result, res);
    }
  })
  .catch(next);
}
