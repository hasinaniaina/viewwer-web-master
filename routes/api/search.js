const _ = require('lodash');

const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const AppTour = mongoose.model('AppTour');
const Favorite = mongoose.model('Favorite');

const onlyPublished = rootRequire('models/helpers/only-published');

const TYPES_APARTMENTS = rootRequire('routes/back/apartments/constants').TYPES;
const TYPES_APPTOURS = rootRequire('routes/back/apptours/constants').TYPES;

const SEARCH_DEFAULT_SORT = {date: -1};

module.exports = function (req, res, next) {

  const body = _.cloneDeep(req.body);

  const SEARCH_RESULTS_PER_PAGE = _.get(res.locals.site.settings, 'searchResultsPerPage', 9);
  const SEARCH_GEO_MAX_DISTANCE = _.get(res.locals.site.settings, 'searchGeoMaxDistance', 10000);
  const SEARCH_GEO_MAX_DISTANCE_USER = _.get(res.locals.site.settings, 'searchGeoMaxDistanceUser', 20000);
  const SEARCH_SOLD_ITEMS = _.get(res.locals.site.settings, 'searchSoldItems', 90) * 24 * 60 * 60 * 1000;

  const userId = _.get(req.user, '_id');

  // apartments search requirements
  const queryApartments = {
    $and: [
      {building: {$nin: [null, '']}},
      {
        $or: [
          {isSold: false},
          {isSold: {$exists: false}},
          {isSold: true, isSoldUpdatedAt: {$gte: Date.now() - SEARCH_SOLD_ITEMS}}
        ]
      }
    ]
  };

  // apptours search requirements
  const queryAppTours = {
    $and: [
      {isSold: {$ne: true}},
      {moderationStatus: 'approved'},
      {isPublished: true},
    ]
  };

  // placeType
  if (body.placeType === 'house') {
    queryApartments.$and.push({
      house: true
    });
    queryAppTours.$and.push({
      house: true
    });
  } else if (body.placeType === 'apartment') {
    queryApartments.$and.push({
      house: {$ne: true}
    });
    queryAppTours.$and.push({
      house: {$ne: true}
    });
  }

  // saleType
  if (body.saleType === 'rent') {
    queryApartments.$and.push({
      saleType: 'rent'
    });
    queryAppTours.$and.push({
      saleType: 'rent'
    });
  } else if (body.saleType === 'sale') {
    queryApartments.$and.push({
      saleType: {$in: ['sale', null]}
    });
    queryAppTours.$and.push({
      saleType: {$in: ['sale', null]}
    });
  }

  // budget
  if (body.budgetFrom) {
    const b = {price: {$gte: parseInt(body.budgetFrom, 10)}};
    queryApartments.$and.push(b);
    queryAppTours.$and.push(b);
  }

  if (body.budgetTo) {
    const b = {price: {$lte: parseInt(body.budgetTo, 10)}};
    queryApartments.$and.push(b);
    queryAppTours.$and.push(b);
  }

  // area
  if (body.areaFrom) {
    const b = {area: {$gte: parseInt(body.areaFrom, 10)}};
    queryApartments.$and.push(b);
    queryAppTours.$and.push(b);
  }

  if (body.areaTo) {
    const b = {area: {$lte: parseInt(body.areaTo, 10)}};
    queryApartments.$and.push(b);
    queryAppTours.$and.push(b);
  }

  // rooms count
  if (body.roomsFrom || body.roomsTo) {
    queryApartments.$and.push(
      {type: {$in: getTypes(TYPES_APARTMENTS, body.roomsFrom, body.roomsTo)}}
    );
    queryAppTours.$and.push(
      {type: {$in: getTypes(TYPES_APPTOURS, body.roomsFrom, body.roomsTo)}}
    );
  }

  const sort = preprocessSort(body.sort);
  const skip = SEARCH_RESULTS_PER_PAGE * body.page;

  const location_lng = _.get(body, 'location.lng');
  const location_lat = _.get(body, 'location.lat');
  const location_user = _.get(body, 'location.user');

  let max_distance = SEARCH_GEO_MAX_DISTANCE;
  if (location_user) max_distance = SEARCH_GEO_MAX_DISTANCE_USER;

  if (location_lng && location_lat) {

    let results = [];

    geoSearchBuildings(location_lng, location_lat, max_distance)
      .then(r => {
        if (!_.isEmpty(r)) {
          queryApartments.$and.push({building: {$in: _.map(r, '_id')}});
          return searchApartments(queryApartments, sort, skip, SEARCH_RESULTS_PER_PAGE, userId);
        }
        return [];
      })
      .then(r => {
        results = _.concat(results, r);
        return geoSearchAppTours(
          queryAppTours, sort, skip, SEARCH_RESULTS_PER_PAGE,
          location_lng, location_lat, max_distance, userId
        );
      })
      .then(r => res.json(_.concat(results, r)))
      .catch(next);

  } else {

    Promise.all([
      searchApartments(queryApartments, sort, skip, SEARCH_RESULTS_PER_PAGE, userId),
      searchAppTours(queryAppTours, sort, skip, SEARCH_RESULTS_PER_PAGE, userId)
    ])
      .then(results => res.json(_.concat(results[0], results[1])))
      .catch(next);

  }
};

function getTypes(types, from, to) {
  const tps = Object.keys(types);
  let s = 0, l = tps.length;
  if (!isNaN(parseInt(from, 10))) s = parseInt(from, 10) - 1;
  if (!isNaN(parseInt(to, 10))) l = parseInt(to, 10);
  return _.slice(tps, s, l);
}

function preprocessSort(sort) {
  let result = SEARCH_DEFAULT_SORT;
  if (!_.isEmpty(sort)) result = _.cloneDeep(sort);

  if (result.date) {
    result.createdAt = result.date;
    delete result.date;
  }

  return result;
}

function geoSearchBuildings(lng, lat, max_distance) {
  return new Promise((resolve, reject) => {
    Program.find(onlyPublished({
      location: {
        $near: {
          $geometry: {
            type: 'Point',
            coordinates: [lng, lat]
          },
          $maxDistance: max_distance,
        }
      }
    })).exec()
      .then(r => {
        if (!_.isEmpty(r)) return Building.find(onlyPublished({
          program: {$in: _.map(r, '_id')}
        })).exec();
        return [];
      })
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
}

function geoSearchAppTours(query, sort, skip, limit, lng, lat, max_distance, user_id) {
  const q = _.cloneDeep(query);
  q.$and.push({
    location: {
      $near: {
        $geometry: {
          type: 'Point',
          coordinates: [lng, lat]
        },
        $maxDistance: max_distance,
      }
    }
  });
  return searchAppTours(q, sort, skip, limit, user_id);
}

function searchFavorites(entity_type, entities, user_id) {
  if (!user_id || _.isEmpty(entities)) return [];
  return Favorite.find({
    user: user_id,
    entityType: entity_type,
    entityId: {$in: entities}
  });
}

function searchApartments(query, sort, skip, limit, user_id) {
  return new Promise((resolve, reject) => {
    let apartments = [];
    Apartment
      .find(onlyPublished(query))
      .sort(sort)
      .skip(skip)
      .limit(limit)
      .exec()
      .then(r => {
        apartments = r;
        return searchFavorites('Apartment', _.map(apartments, '_id'), user_id);
      })
      .then(r => resolve(postProcessApartments(apartments, r)))
      .catch(e => reject(e));
  });
}

function searchAppTours(query, sort, skip, limit, user_id) {
  return new Promise((resolve, reject) => {
    let apptours = [];
    AppTour
      .find(query)
      .sort(sort)
      .skip(skip)
      .limit(limit)
      .exec()
      .then(r => {
        apptours = r;
        return searchFavorites('AppTour', _.map(apptours, '_id'), user_id);
      })
      .then(r => resolve(postProcessAppTours(apptours, r)))
      .catch(e => reject(e));
  });
}

function postProcessApartments(apartments, favorites) {
  let results = [];

  _.each(apartments, a => {

    let thumbnail = _.get(a, 'linkedTour.thumbnail', '');
    if (!thumbnail) thumbnail = _.get(a, 'linkedTourAdditional.thumbnail', '');

    let program;
    if (_.get(a, 'building.program._id')) {
      program = {
        url: '/programs/' + a.building.program._id,
        name: a.building.program.name,
        isPublished: a.building.program.isPublished
      };
    }

    const favorite = _.find(favorites, {entityId: a._id});

    results.push({
      entityType: 'Apartment',
      entityId: a._id,
      isFavorite: !!(favorite),
      favoriteId: (favorite) ? favorite._id : null,

      createdAt: a.createdAt,

      url: '/apartments/' + a._id,
      thumbnail: (thumbnail) ? thumbnail : null,

      type: a.type,
      duplex: !!(a.duplex),
      house: !!(a.house),

      price: a.price,
      area: a.area,

      isSold: a.isSold,
      saleType: a.saleType,

      program: program,

      counterRating: a.counterRating,
    });
  });
  return results;
}

function postProcessAppTours(apptours, favorites) {
  let results = [];
  _.each(apptours, a => {

    const favorite = _.find(favorites, {entityId: a._id});

    results.push({
      entityType: 'AppTour',
      entityId: a._id,
      isFavorite: !!(favorite),
      favoriteId: (favorite) ? favorite._id : null,

      createdAt: a.createdAt,

      url: '/apptours/' + a._id,
      thumbnail: (a.thumbnail) ? a.thumbnail : null,

      type: a.type,
      duplex: false,
      house: !!(a.house),

      price: a.price,
      area: a.area,

      isSold: a.isSold,
      saleType: a.saleType,

      program: null,

      counterRating: a.counterRating,
    });
  });
  return results;
}

