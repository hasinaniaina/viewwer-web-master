const _ = require('lodash');
const os = require('os');

const mongoose = require('mongoose');
const Task = mongoose.model('Task');

const apptourPreview = rootRequire('lib/aws/apptour').apptour;
const apptourSave = require('./apptour-save');

const TaskError = rootRequire('/routes/api/task/task-error');

module.exports = function (req, res, next) {

  const task = res.locals.taskApi;
  const body = _.cloneDeep(req.body);

  let start = Date.now();
  let processing = false;

  // lock
  Task.findOneAndUpdate(
    {
      _id: task._id,
      status: {
        $ne: 'processing'
      }
    },
    {
      status: 'processing',
      host: os.hostname(),
      error: null,
      duration: null,
      $push: {requests: body}
    }
  )
    .then(r => {
      if (!r) return res.json({
        status: 'failed',
        error: 'already_processing',
        errorDescription: 'task already processing'
      });

      if (task.type === 'apptour') {

        processing = true;
        res.json({status: 'success'});
        return apptourPreview(task._id, body);

      } else if (task.type === 'apptour-save') {

        processing = true;
        res.json({status: 'success'});
        return apptourSave(task._id, req, res);

      } else {

        res.json({
          status: 'failed',
          error: 'unknown_type',
          errorDescription: 'unknown task type'
        });

      }
    })
    .then(r => {
      if (!processing) return;
      const u = {
        status: 'completed',
        duration: Date.now() - start,
        host: os.hostname()
      };
      if (!_.isEmpty(r)) u.result = r;
      u.$push = {
        results: (!_.isEmpty(r)) ? {result: r} : {result: null}
      };
      return task.updateOne(u).exec();
    })
    .catch(e => {
      let error_code = 'internal';
      if (e instanceof TaskError) {
        error_code = e.status;
      }

      if (processing) return task.updateOne({
        status: 'failed',
        error: error_code,
        $push: {
          errorLogs: {
            request: body,
            errorCode: error_code,
            errorFull: e
          }
        },
        host: os.hostname()
      }).exec();
    })
    .catch(next);
};

