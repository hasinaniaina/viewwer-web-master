const _ = require('lodash');

module.exports = function (req, res/*, next*/) {
  const task = _.cloneDeep(res.locals.taskApi);

  const r = {};

  r.status = task.status;

  if (task.status === 'failed') r.error = task.error;

  if (task.status === 'completed') {
    r.result = {};

    if (task.type === 'apptour') {
      r.result.url = '/apptours/' + task._id + '/preview';
    }

    if (task.type === 'apptour-save') r.result = task.result;
  }

  res.json(r);
};
