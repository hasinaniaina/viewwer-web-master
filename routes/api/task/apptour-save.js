const _ = require('lodash');
const path = require('path');

const mongoose = require('mongoose');
const AppTour = mongoose.model('AppTour');

const awstask = rootRequire('lib/aws/aws-task');

const logger = rootRequire('lib/logger');
const notify = rootRequire('routes/back/apptours/notify');

const retry = rootRequire('lib/retry-promise');
const RETRY_DOWNLOAD = 3;
const RETRY_DOWNLOAD_BACKOFF = 2000;

const TaskError = rootRequire('/routes/api/task/task-error');

module.exports = function (task_id, req, res) {
  return new Promise((resolve, reject) => {
    save(task_id, req, res)
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

function save(task_id, req, res) {
  return new Promise((resolve, reject) => {

    const body = _.cloneDeep(req.body);
    const taskApiId = body.previewTaskId;

    if (!checkAddress(body)) return reject(new TaskError('wrong_coordinates'));

    let tour, tourJson, settingsJson;
    let partnerCode = "";
    
    if (req.user.partner && req.user.partnershipValidation == "allowed") partnerCode = req.user.partner.code;

    logger.info("Trying to save an AppTour for user '" + req.user._id + "' with partnerCode '" + partnerCode + "'.");

    AppTour
      .create({
        user: req.user._id,
        partnerCode: partnerCode,
        previewTaskId: taskApiId,
        saveTaskId: task_id,

        type: body.type,
        house: body.house,
        description: body.description,

        address: body.address,
        location: {
          type: 'Point',
          coordinates: [body.longitude, body.latitude]
        },

        area: body.area,
        price: body.price,
        gasClass: body.gasClass,
        energyClass: body.energyClass,
        saleType: body.saleType,

        locale: req.locale
      })
      .then(r => {
        tour = r;
        const tourDir = path.resolve(path.join(process.env.STORAGE_PATH, 'public/apptours/', tour._id));
        return retry({
          max: RETRY_DOWNLOAD,
          backoff: RETRY_DOWNLOAD_BACKOFF
        }, (attempt) => {
          if (attempt > 1) {
            logger.info('Api tour', tour._id, 'task_id', taskApiId,
              'lambda results download,', 'attempt:', attempt);
          }
          return awstask.downloadDir(taskApiId, tourDir);
        });
      })
      .then(() => awstask.downloadResultBuffer(taskApiId, '/tour/tour.json'))
      .then(r => {
        tourJson = JSON.parse(r.toString());
      })
      .then(() => awstask.downloadResultBuffer(taskApiId, '/tour/settings.json'))
      .then(r => {
        settingsJson = JSON.parse(r.toString());
      })
      .then(() => {
        let main = _.find(tourJson.scenes, {isMain: true});
        if (!main) main = tourJson.scenes[0];

        const th = _.find(tourJson.thumbnails, {sceneId: main.sceneId});

        let thumbnail = '';
        if (th) {
          thumbnail = '/storage/apptours/'
            + tour._id + '/scenes/' + main.id
            + '/' + th.filename;
        }

        return tour.set({
          thumbnail: thumbnail,
          generateStatus: 'completed',
          tour: tourJson,
          settings: settingsJson
        }).save();
      })
      .then(() => {
        resolve({
          entityType: 'AppTour',
          entityId: tour._id,
          url: '/apptours/' + tour._id
        });
        if (res.locals.site.settings.notifyNewAppTours) return notify.send(req, tour._id);
      })
      .catch(e => {
        logger.error('Api tour', tour._id, 'internal error', e);
        reject(new TaskError('internal', e.message));

        if (tour) return tour.set({
          generateStatus: 'failed'
        }).save();
      });
  });
}

function checkAddress(body) {
  return !(
    !body ||
    !body.address ||
    isNaN(body.longitude) ||
    isNaN(body.latitude)
  );
}
