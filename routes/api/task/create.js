const urljoin = require('url-join');

const _ = require('lodash');
const mongoose = require('mongoose');
const Task = mongoose.model('Task');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);

  Task
    .create({
      user: req.user._id,
      type: body.type
    })
    .then(r => {
      let result = {
        status: 'success',
        taskId: r._id,
      };
      if (body.type === 'apptour') {
        result.uploadUrl = urljoin(process.env.AWS_TASKS_UPLOAD, r._id);
        result.uploadApiKey = process.env.AWS_TASKS_UPLOAD_APIKEY;
      }
      res.json(result);
    })
    .catch(next);
};
