const awstask = rootRequire('lib/aws/aws-task');

module.exports = function (req, res, next) {
  awstask.removeFile(res.locals.taskApi._id, req.body.file)
    .then(() => {
      res.json({
        status: 'success'
      });
    })
    .catch(next);
};
