module.exports = class TaskError extends Error {
  constructor (status, description) {
    super(status);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
    this.status = status || 'internal';
    this.description = description || '';
  }
};
