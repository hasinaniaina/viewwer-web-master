const _ = require('lodash');
const logger = rootRequire('lib/logger');
const firebase = rootRequire('lib/firebase');

module.exports = function (req, res) {
  let event = req.body;

  switch (event.type) {
    case 'message.sent':
      logger.debug(`TalkJS message sent from ${event.data.message.senderId}`, event);
      break;

    case 'message.read':
      logger.debug(`TalkJS user ${event.data.recipient.id} read a message from ${event.data.message.senderId}`, event);
      break;

    case 'user.created':
      logger.debug(`TalkJS user created: ${event.data.user.id} email: ${event.data.user.email[0]}`, event);
      break;

    case 'user.updated':
      logger.debug(`TalkJS user updated: ${event.data.user.id} email: ${event.data.user.email[0]}`, event);
      break;

    case 'notification.triggered':
      logger.debug('TalkJS notification triggered', event);
      sendNotification(event, req);
      break;

    case 'notification.sent':
      logger.debug('TalkJS notification sent', event);
      break;

    case 'notification.delivered':
      logger.debug('TalkJS notification delivered', event);
      break;

    case 'notification.bounced':
      logger.debug('TalkJS notification bounced', event);
      break;

    case 'notification.opened':
      logger.debug('TalkJS notification opened', event);
      break;

    case 'notification.link.clicked':
      logger.debug('TalkJS notification link clicked', event);
      break;

    default:
  }

  res.sendStatus(200);
};

function sendNotification(event, req) {
  const recipient = _.trimStart(_.get(event, 'data.recipient.id', ''), 'user_');

  const locale = _.get(event, 'data.recipient.locale', '');
  const conversationId = _.get(event, 'data.conversation.id', '');

  if (!recipient) return;

  if (locale) req.setLocale(locale);
  const message = req.__('You have new message!');

  logger.debug('TalkJS sendNotification', recipient, locale, event);

  firebase.send(
    recipient,
    message,
    {
      notificationType: 'message',
      conversationId: conversationId
    }
  )
    .catch(() => {
    });
}
