const _ = require('lodash');

const talkjs = rootRequire('lib/talkjs');

module.exports = function (req, res, next) {
  let userPath = `users/user_${req.user._id}/conversations?unreadsOnly=true`;

  const role = _.get(req, 'user.role');
  if (role === 'editor' || role === 'admin') {
    userPath = `users/${talkjs.SUPPORT_ID}/conversations?unreadsOnly=true`;
  }

  talkjs.apiRequest('GET', userPath)
    .then(r => res.json({ count: r.data.length}))
    .catch(next);
};
