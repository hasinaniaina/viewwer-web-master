const _ = require('lodash');

const mongoose = require('mongoose');
const AppTour = mongoose.model('AppTour');

const filterTour = require('./commons').filterTour;

module.exports = function (req, res, next) {
  AppTour
    .find({user: req.user._id})
    .sort({createdAt: -1})
    .exec()
    .then(r => res.json(_.map(r, t => filterTour(t))))
    .catch(next);
};
