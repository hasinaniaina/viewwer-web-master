const filterTour = require('./commons').filterTour;

module.exports = function (req, res) {
  return res.json(filterTour(res.locals.tourApi));
};
