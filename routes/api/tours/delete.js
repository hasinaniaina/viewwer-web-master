const fs = require('fs-extra');

const logger = rootRequire('lib/logger');

module.exports = function (req, res, next) {
  const tour = res.locals.tourApi;

  tour
    .remove()
    .then(() => fs.remove(process.env.STORAGE_PATH + '/public/apptours/' + tour._id))
    .then(() => {
      logger.info('App Tour', tour._id, 'removed by user', req.user._id, req.user.email);
      res.json({status: 'success'});
    })
    .catch(next);
};
