module.exports.filterTour = function (tour) {
  return {
    entityType: 'AppTour',
    entityId: tour._id,

    url: '/apptours/' + tour._id,
    thumbnail: tour.thumbnail,
    createdAt: tour.createdAt,

    type: tour.type,
    house: tour.house,
    description: tour.description,

    address: tour.address,
    longitude: tour.location.coordinates[0],
    latitude: tour.location.coordinates[1],

    area: tour.area,
    price: tour.price,
    gasClass: tour.gasClass,
    energyClass: tour.energyClass,
    saleType: tour.saleType,
    isSold: tour.isSold,

    moderationStatus: tour.moderationStatus,
    moderationMessage: tour.moderationMessage,

    counterRating: tour.counterRating,

    generateStatus: tour.generateStatus,

    isPublished: tour.isPublished
  };
};
