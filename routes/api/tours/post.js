const _ = require('lodash');

module.exports = function (req, res, next) {
  const tour = res.locals.tourApi;
  const body = _.cloneDeep(req.body);

  const update = {};

  if (body.type) update.type = body.type;
  if (body.house !== undefined) update.house = body.house;
  if (body.description) update.description = body.description;

  if (body.address) update.address = body.address;

  if (body.longitude !== undefined && body.latitude !== undefined) {
    update.location = {
      type: 'Point',
      coordinates: [body.longitude, body.latitude]
    };
  }

  if (body.area) update.area = body.area;
  if (body.price) update.price = body.price;
  if (body.gasClass) update.gasClass = body.gasClass;
  if (body.energyClass) update.energyClass = body.energyClass;
  if (body.saleType) update.saleType = body.saleType;
  if (body.isSold !== undefined) update.isSold = body.isSold;
  if (body.isPublished !== undefined) update.isPublished = body.isPublished;

  tour
    .set(update)
    .save()
    .then(() => res.json({status: 'success'}))
    .catch(next);
};
