module.exports = function (req, res, next) {
  const favorite = res.locals.favoriteApi;

  favorite
    .remove()
    .then(() => res.json({status: 'success'}))
    .catch(next);
};
