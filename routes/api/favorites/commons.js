const _ = require('lodash');

module.exports.filterFavorite = function (favorite) {
  return {
    favoriteId: favorite._id,
    entityType: favorite.entityType,
    entityId: favorite.entityId,
    url: getUrl(favorite.entityType, favorite.entityId),
    entity: {
      thumbnail: getThumbnail(favorite.entityType, favorite.entity),
      type: getType(favorite.entityType, favorite.entity),
      house: getHouse(favorite.entityType, favorite.entity),
      counterRating: getRating(favorite.entityType, favorite.entity),
      moderationStatus: getModerationStatus(favorite.entityType, favorite.entity)
    }
  };
};

function getUrl(type, id) {
  return '/' + _.toLower(type) + 's' + '/' + id;
}

function getThumbnail(type, entity) {
  if (!entity) return '';
  if (type === 'AppTour') return _.get(entity, 'thumbnail', '');
  if (type === 'Apartment') {
    let t = _.get(entity, 'linkedTour.thumbnail', '');
    if (t) return t;

    t = _.get(entity, 'linkedTourAdditional.thumbnail', '');
    if (t) return t;
  }
  return '';
}

function getType(type, entity) {
  if (!entity) return '';
  if (type === 'AppTour') return entity.type;
  if (type === 'Apartment') return entity.type;
  return '';
}

function getHouse(type, entity) {
  if (!entity) return false;
  if (type === 'AppTour') return !!(entity.house);
  if (type === 'Apartment') return !!(entity.house);
  return false;
}

function getRating(type, entity) {
  if (!entity) return 0;
  if (type === 'AppTour') return entity.counterRating;
  if (type === 'Apartment') return entity.counterRating;
  return 0;
}

function getModerationStatus(type, entity) {
  if (!entity) return '';
  if (type === 'AppTour') return entity.moderationStatus;
  if (type === 'Apartment') return 'approved';
  return '';
}
