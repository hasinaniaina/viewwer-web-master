const _ = require('lodash');
const mongoose = require('mongoose');
const Favorite = mongoose.model('Favorite');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);

  const f = {
    user: req.user._id,
    entityType: body.entityType,
    entityId: body.entityId
  };

  Favorite
    .findOneAndUpdate(f, f, {upsert: true, new: true, setDefaultsOnInsert: true})
    .then(r => res.json({
      status: 'success',
      favoriteId: r._id,
    }))
    .catch(next);
};
