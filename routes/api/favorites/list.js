const _ = require('lodash');

const mongoose = require('mongoose');
const Favorite = mongoose.model('Favorite');

const filterFavorite = require('./commons').filterFavorite;

module.exports = function (req, res, next) {
  Favorite
    .find({user: req.user._id})
    .sort({createdAt: -1})
    .populate('entity')
    .exec()
    .then(r => res.json(_.map(r, t => filterFavorite(t))))
    .catch(next);
};
