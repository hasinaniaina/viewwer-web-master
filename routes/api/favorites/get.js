const mongoose = require('mongoose');
const Favorite = mongoose.model('Favorite');

const filterFavorite = require('./commons').filterFavorite;

module.exports = function (req, res, next) {
  Favorite
    .findOne({user: req.user._id, _id: req.params.favoriteApiId})
    .populate('entity')
    .exec()
    .then(r => {
      if (!r) return next(res.createError(404));
      res.json(filterFavorite(r));
    })
    .catch(next);
};
