const _ = require('lodash');

const mongoose = require('mongoose');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);

  const model = mongoose.model(body.entityType);
  let action;

  if (body.action === 'like') {
    action = model.increaseCounter(body.entityId, 'Rating');
  } else if (body.action === 'dislike') {
    action = model.decreaseCounter(body.entityId, 'Rating');
  } else if (body.action === 'share') {
    action = model.increaseCounter(body.entityId, 'Shares');
  } else {
    return next(new Error('Unknown action'));
  }

  action
    .then(() => res.json({
      status: 'success'
    }))
    .catch(next);
};
