const logger = rootRequire('lib/logger');

module.exports = function (req, res) {

  if (req.body) {
    logger.info('CSP Violation', req.body);
  } else {
    logger.info('CSP Violation No data received!');
  }
  res.status(204).end();

};
