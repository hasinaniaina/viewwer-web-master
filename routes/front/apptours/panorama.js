const _ = require('lodash');

const DEFAULT_ICON = '/assets/shared/images/marker.png';
const HOTSPOTS_STORAGE = '/storage/files/hotspots/';

const KRPANO_PLUGINS = '/assets/shared/krpano/plugins';
const KRPANO_INCLUDES = '/assets/shared/krpano/includes';

const EFFECT_OPENSCENE = 'depth|alpha|oy|rx, 4000|0.0|-50|-60, 0.5';
const WEBVR_MOBILEVR_IPD = 57;

const VLOOKAT_LIMIT_ADDITION = 1;

module.exports = function (tour_id, url, converted_xml, scenes, settings, scene_types) {
  const panorama = _.cloneDeep(converted_xml);

  // hide krpano error console
  if (process.env.NODE_ENV === 'production') {
    panorama.krpano._attributes = {
      showerrors: false
    };
  }

  if (!_.isArray(panorama.krpano.scene)) {
    panorama.krpano.scene = [panorama.krpano.scene];
  }

  _.each(panorama.krpano.scene, s => {

    /*
      sceneId - scene id in the app
      id - scene id generated in lambda function
     */
    const scene = _.find(scenes, {id: s._attributes.title});

    // limit vlookat for partial sphere
    const vfov = 360 * scene.height / scene.width;
    s.view._attributes.vlookatmin = -1 * vfov / 2 - VLOOKAT_LIMIT_ADDITION;
    s.view._attributes.vlookatmax = vfov / 2 + VLOOKAT_LIMIT_ADDITION;
    s.view._attributes.limitview = 'range';

    s.hotspot = [];

    const sceneHotspots = _.filter(settings.hotspots, {sceneId: scene.sceneId});

    _.each(sceneHotspots, sh => {
      const loadSceneAction = _.get(sh, 'action.loadScene');
      if (!loadSceneAction) return;

      const targetScene = _.find(scenes, {sceneId: loadSceneAction.sceneId});
      if (!targetScene) return;

      let icon = DEFAULT_ICON;
      const targetSceneType = _.find(settings.sceneTypes, {sceneId: targetScene.sceneId});
      if (targetSceneType) {
        const sceneType = _.find(scene_types, {_id: targetSceneType.type});

        if (sceneType && sceneType.icon) {
          icon = HOTSPOTS_STORAGE + sceneType.icon;
        }
      }

      // fit hotspot atv to pano
      if (sh.atv < -1 * (vfov / 2)) sh.atv = -1 * (vfov / 2);
      if (sh.atv > vfov / 2) sh.atv = vfov / 2 ;

      const action = `tween(${EFFECT_OPENSCENE}, default, js(app.apptours['${tour_id}'].openScene("${targetScene.id}")))`;
      s.hotspot.push({
        _attributes: {
          url: icon,
          ath: sh.ath,
          atv: sh.atv,
          onclick: action,
          distorted: false,
          vr: true,
          style: 'hotspot_normal'
        }
      });
    });

    if (_.isArray(s.image.level)) {
      _.each(s.image.level, i => {
        i.cube._attributes.url = url + i.cube._attributes.url;
      });
    } else {
      s.image.level.cube._attributes.url = url + s.image.level.cube._attributes.url;
    }
  });

  let mainScene = _.find(scenes, {isMain: true});
  if (!mainScene) mainScene = scenes[0];

  postprocess(panorama, tour_id, mainScene.id);

  return panorama;
};

function postprocess(panorama, tour_id, main_scene) {
  /* Tour actions */
  panorama.krpano.action = [];

  panorama.krpano.action.push({
    _attributes: {
      name: 'startup',
      autorun: 'onstart'
    },
    _text: `js(app.apptours['${tour_id}'].openScene("${main_scene}"))`
  });

  // krpano is loaded event
  panorama.krpano.action.push({
    _attributes: {
      name: 'krpano_loaded',
      autorun: 'onstart'
    },
    _text: `js(app.apptours['${tour_id}'].krpanoLoaded())`
  });

  /* Tour events */
  panorama.krpano.events = {
    _attributes:
      {
        name: 'tour_events',
        keep: true,
        onresize: `js(app.apptours['${tour_id}'].krpanoResized(get(stagewidth), get(stageheight)));`,
        onloadcomplete: `js(app.apptours['${tour_id}'].sceneLoaded())`
      }
  };

  /* Tour includes */
  panorama.krpano.include = [];

  /* Tour plugins */
  panorama.krpano.plugin = [];

  /* Shared styles */
  panorama.krpano.include.push({
    _attributes:
      {
        url: KRPANO_INCLUDES + '/styles.xml'
      }
  });

  /* WebVR - includes must be before plugin! */
  panorama.krpano.include.push({
    _attributes:
      {
        url: KRPANO_PLUGINS + '/webvr.xml'
      }
  });

  panorama.krpano.include.push({
    _attributes:
      {
        url: KRPANO_INCLUDES + '/webvr-actions.xml'
      }
  });

  panorama.krpano.plugin.push({
    _attributes:
      {
        name: 'WebVR',
        // for testing in desktop browser
        mobilevr_fake_support: true,
        mobilevr_ipd: WEBVR_MOBILEVR_IPD,
        mobilevr_wakelock: true,
        onentervr: `webvr_onentervr(); webvr_setup(); js(app.apptours['${tour_id}'].enableKrpanoToggle('vr')); js(app.apptours['${tour_id}'].vrEnabled());`,
        onexitvr: `webvr_onexitvr(); webvr_setup(); js(app.apptours['${tour_id}'].disableKrpanoToggle('vr')); js(app.apptours['${tour_id}'].vrDisabled());`,
        onavailable: `js(app.apptours['${tour_id}'].vrAvailable());`
      }
  });

  panorama.krpano.plugin.push({
    _attributes:
      {
        name: 'gyro',
        devices: 'html5',
        keep: true,
        url: KRPANO_PLUGINS + '/gyro2.js',
        enabled: false,
        onavailable: `js(app.apptours['${tour_id}'].gyroAvailable());`,
      }
  });

}
