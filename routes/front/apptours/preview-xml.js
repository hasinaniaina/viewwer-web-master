const mongoose = require('mongoose');
const SceneType = mongoose.model('SceneType');

const convert = require('xml-js');

const awstask = rootRequire('lib/aws/aws-task');

const panorama = require('./panorama');

module.exports = function (req, res, next) {
  const apptourTaskId = req.params.apptourTaskId;

  let settings;
  let sceneTypes;

  Promise.all([
    SceneType.find({}).exec(),
    awstask.downloadResultBuffer(apptourTaskId, '/tour/settings.json'),
    awstask.downloadResultBuffer(apptourTaskId, '/tour/tour.json')
  ])
    .then(r => {
      sceneTypes = r[0];
      settings = JSON.parse(r[1].toString());
      const tour = JSON.parse(r[2].toString());

      const x = panorama(
        apptourTaskId,
        awstask.getPublicTasksUrl(apptourTaskId) + '/tour/',
        tour.panorama,
        tour.scenes,
        settings,
        sceneTypes
      );

      const xml = convert.js2xml(x, {compact: true, spaces: 2});

      res.set('Content-Type', 'text/xml');
      res.send(xml);
    })
    .catch(next);
};


