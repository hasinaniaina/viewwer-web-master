const _ = require('lodash');

const mongoose = require('mongoose');
const SceneType = mongoose.model('SceneType');
const User = mongoose.model('User');
const Favorite = mongoose.model('Favorite');

module.exports = function (req, res, next) {

  const apptour = res.locals.apptourFront;

  Promise.all([
    User.findOne({_id: apptour.user}).exec(),
    SceneType.find({}).exec(),
    Favorite.findOne({
      user: _.get(req.user, '_id'),
      entityType: 'AppTour',
      entityId: apptour._id
    })
  ])
    .then(r => {
      const user = r[0];
      const sceneTypes = r[1];
      const favorite = r[2];

      if (!apptour) return next(res.createError(404));

      const scenes = [];

      _.each(apptour.tour.scenes, s => {

        const scene = {
          id: s.id
        };

        const settingsSceneType = _.find(apptour.settings.sceneTypes, {sceneId: s.sceneId});
        if (settingsSceneType) {
          const type = _.find(sceneTypes, {_id: settingsSceneType.type});
          if (type) scene.locales = type.locales;
        }

        const thumb = _.find(apptour.tour.thumbnails, {sceneId: s.sceneId});
        if (thumb) {
          scene.thumbnail = '/storage/apptours/'
            + apptour._id + '/scenes/' + s.id + '/' + thumb.filename;
        }

        scenes.push(scene);
      });

      const result = {
        tour: apptour,
        tourId: apptour._id,
        scenes: scenes,
        favoriteId: ''
      };

      if (user) {
        result.owner = {
          id: user._id,
          name: user.name,
          email: user.email
        };
      }

      if (favorite) result.favoriteId = favorite._id;

      res.render('front/pages/apptours/view', result);
    })
    .catch(next);
};
