const mongoose = require('mongoose');
const SceneType = mongoose.model('SceneType');
const AppTour = mongoose.model('AppTour');

const convert = require('xml-js');

const panorama = require('./panorama');

module.exports = function (req, res, next) {
  const apptourFrontId = req.params.apptourFrontId;

  Promise.all([
    AppTour.findOne({_id: apptourFrontId}).exec(),
    SceneType.find({}).exec(),
  ])
    .then(r => {
      const apptour = r[0];
      const sceneTypes = r[1];

      if (!apptour) return next(res.createError(404));

      const x = panorama(
        apptourFrontId,
        '/storage/apptours/' + apptourFrontId + '/tour/',
        apptour.tour.panorama,
        apptour.tour.scenes,
        apptour.settings,
        sceneTypes
      );

      const xml = convert.js2xml(x, {compact: true, spaces: 2});

      res.set('Content-Type', 'text/xml');
      res.send(xml);
    })
    .catch(next);
};


