const _ = require('lodash');

const mongoose = require('mongoose');
const SceneType = mongoose.model('SceneType');

const awstask = rootRequire('lib/aws/aws-task');

module.exports = function (req, res, next) {
  const apptourTaskId = req.params.apptourTaskId;

  let settings;
  let sceneTypes;

  Promise.all([
    SceneType.find({}).exec(),
    awstask.downloadResultBuffer(apptourTaskId, '/tour/settings.json'),
    awstask.downloadResultBuffer(apptourTaskId, '/tour/tour.json')
  ])
    .then(r => {
      sceneTypes = r[0];
      settings = JSON.parse(r[1].toString());
      const tour = JSON.parse(r[2].toString());

      const scenes = [];

      _.each(tour.scenes, s => {

        const scene = {
          id: s.id
        };

        const settingsSceneType = _.find(settings.sceneTypes, {sceneId: s.sceneId});
        if (settingsSceneType) {
          const type = _.find(sceneTypes, {_id: settingsSceneType.type});
          if (type) scene.locales = type.locales;
        }

        const thumb = _.find(tour.thumbnails, {sceneId: s.sceneId});
        if (thumb) {
          scene.thumbnail = awstask.getPublicTasksUrl(apptourTaskId)
            + '/scenes/' + s.id + '/' + thumb.filename;
        }

        scenes.push(scene);
      });

      res.render('front/pages/apptours/preview', {
        tourId: apptourTaskId,
        scenes: scenes
      });
    })
    .catch(next);
};
