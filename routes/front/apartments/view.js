const _ = require('lodash');
const mongoose = require('mongoose');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');
const Favorite = mongoose.model('Favorite');

const onlyPublished = rootRequire('models/helpers/only-published');

module.exports = function (req, res, next) {
  let apartment = res.locals.apartmentFront;
  let building = res.locals.apartmentFront.building;
  let program = _.get(building, 'program');
  let developer = _.get(building, 'program.developer');
  let allApartments = [];
  let allBuildings = [];
  let exterior, interior;
  let favorite;

  Promise.all([
    // exterior tour
    Tour.findOne(onlyPublished({program: (program) ? program._id : '__unknown'})).exec(),
    // possible interior tours
    Tour.find(onlyPublished({
      $or: [
        {apartment: apartment._id},
        {apartmentsAdditional: apartment._id}
      ]
    })).exec(),
    // all program's buildings
    Building.find(onlyPublished({program: (program) ? program._id : '__unknown'})).sort({name: 1}).exec(),
    Favorite.findOne({
      user: _.get(req.user, '_id'),
      entityType: 'Apartment',
      entityId: apartment._id
    })
  ])
    .then(r => {
      exterior = r[0];
      allBuildings = r[2];
      favorite = r[3];

      let interiors = r[1];
      interior = _.find(interiors, {apartment: {_id: apartment._id}});
      if (!interior) interior = _.find(interiors, t => {
        return (_.find(t.apartmentsAdditional, {_id: apartment._id}));
      });
      if (interior) apartment.interior = interior;

      if (!_.isEmpty(allBuildings)) {
        return Apartment.find(onlyPublished({building: {$in: _.map(allBuildings, '_id')}})).sort({name: 1}).exec();
      }
    })
    .then(r => {
      if (r) allApartments = r;

      res.render('front/pages/apartments/view', {
        apartment: apartment,
        building: building,
        program: program,
        developer: developer,
        exterior: exterior,
        interior: interior,
        allApartments: allApartments,
        allBuildings: allBuildings,
        favoriteId: (favorite) ? favorite._id : ''
      });
    })
    .catch(next);
};
