const _ = require('lodash');
const convert = require('xml-js');

const DEFAULT_ICON = '/assets/shared/images/marker.png';
const HOTSPOTS_STORAGE = '/storage/files/hotspots/';
const KRPANO_PLUGINS = '/assets/shared/krpano/plugins';
const KRPANO_INCLUDES = '/assets/shared/krpano/includes';

const HOTSPOT_TEMPLATE_VR = {
  _attributes: {
    name: 'hotspot_layer_vr_',
    onloaded: 'show_if_vr();',
    onhover: 'tween(alpha, 1.0);',
    onout: 'tween(alpha, 0.6);',
    alpha: 0.6,
    ath: 0,
    atv: 45,
    visible: false,
    url: DEFAULT_ICON,
    style: 'hotspot_normal',
    vronly: true
  }
};

const LAYER_VR_ATH_STEP = 15;

const PLAN_WIDTH = 500;
const PLAN_WIDTH_EDITOR = 500;

const PLAN_SPOT_ICON = '/assets/shared/images/planspot.svg';
const PLAN_SPOT_WIDTH = 50;
const PLAN_SPOT_HEIGHT = 50;

const RADAR_COLOR_FILL = '0xA73734';
const RADAR_COLOR_LINE = '0x992b1f';

const ZORDER_PLAN = 5;
const ZORDER_RADAR = 6;
const ZORDER_PLANSPOT = 7;

const WEBVR_MOBILEVR_IPD = 57;

const EFFECT_OPENSCENE = 'depth|alpha|oy|rx, 4000|0.0|-50|-60, 0.5';

const VLOOKAT_LIMIT_ADDITION = 1;

module.exports = function (req, res) {
  const tour = res.locals.tourFront;
  const panorama = generatePanorama(tour, req.query);
  const xml = convert.js2xml(panorama, {compact: true, spaces: 2});
  res.set('Content-Type', 'text/xml');
  res.send(xml);
};

function generatePanorama(tour, params) {

  // hide krpano error console
  if (process.env.NODE_ENV === 'production') {
    tour.panorama.krpano._attributes = {
      showerrors: false
    };
  }

  tour.panorama.krpano.scene.forEach(scene => {
    const currentScene = _.find(tour.scenes, s => {
      return s._id === scene._attributes.title;
    });
    if (!currentScene) return;

    /* scene xml settings override */
    scene._attributes.title = currentScene.name;
    scene._attributes.scene_id = currentScene._id;

    if (currentScene.previewSettings && scene.view._attributes) {
      scene.view._attributes.hlookat = currentScene.previewSettings.hlookat;
      scene.view._attributes.vlookat = currentScene.previewSettings.vlookat;
    }

    // limit vlookat for partial sphere
    if (currentScene.file.vfov < 180) {
      scene.view._attributes.vlookatmin = -1 * currentScene.file.vfov / 2 - VLOOKAT_LIMIT_ADDITION;
      scene.view._attributes.vlookatmax = currentScene.file.vfov / 2 + VLOOKAT_LIMIT_ADDITION;
      scene.view._attributes.limitview = 'range';
    }

    /* scene layers */
    scene.layer = [];

    const sameTypeScenes = _.filter(tour.scenes, s => {
      return (s.type && s.type._id && currentScene.type && currentScene.type._id && s.type._id === currentScene.type._id);
    });
    const baseScene = _.find(sameTypeScenes, s => (s && s.layer && s.layer.isBaseLayer));

    let baseSceneHotspots = currentScene.hotspots;
    if (baseScene) baseSceneHotspots = baseScene.hotspots;

    // only "load scene" hotspots for hidden layer scene
    if (currentScene.layer && !currentScene.layer.isBaseLayer) {
      baseSceneHotspots = _.filter(baseSceneHotspots, h => (h.action.loadScene));
    }

    if (!_.isEmpty(baseSceneHotspots)) scene.hotspot = [];

    if (params.nohotspots) return;
    baseSceneHotspots.forEach(sourceHotspot => {

      /* icon */
      let iconUrl = DEFAULT_ICON;
      if (sourceHotspot.icon) iconUrl = HOTSPOTS_STORAGE + sourceHotspot.icon;

      /* actions */
      let action = '';
      /* hide in vr */
      let vrhide = false;

      if (sourceHotspot.action.loadScene) {

        let targetSceneId = sourceHotspot.action.loadScene.scene;
        const targetScene = _.find(tour.scenes, {_id: targetSceneId});
        if (!targetScene) return;

        /* custom icon */
        if (!sourceHotspot.icon && targetScene.type) {
          if (targetScene.type.icon) iconUrl = HOTSPOTS_STORAGE + targetScene.type.icon;
        }

        action = `tween(${EFFECT_OPENSCENE}, default, js(app.tours['${tour._id}'].openScene("${targetSceneId}")))`;
      }

      if (sourceHotspot.action.openUrl) {
        action = `js(app.tours['${tour._id}'].openUrl("${sourceHotspot.action.openUrl.url}"))`;
        vrhide = true;
      }

      if (sourceHotspot.action.openModal) {
        action = `js(app.tours['${tour._id}'].openModal("` + _.escape(sourceHotspot.action.openModal.heading) + '", "' + _.escape(sourceHotspot.action.openModal.content) + '"))';
        vrhide = true;
      }

      scene.hotspot.push({
        _attributes: {
          url: iconUrl,
          ath: sourceHotspot.ath,
          atv: sourceHotspot.atv,
          onclick: action,
          vrhide: vrhide,
          onloaded: vrhide ? 'hide_if_vr();' : '',
          style: 'hotspot_normal'
        }
      });
    });

    /* layers vr hotspots */
    if (!_.isEmpty(sameTypeScenes) && sameTypeScenes.length > 1) {
      // sort scenes by layers weights
      const sameTypeScenesSorted = _.orderBy(sameTypeScenes, s => {
        return _.get(s, 'layer.weight', 0);
      });

      let ath = 0;
      _.each(sameTypeScenesSorted, s => {
        const hotspot = _.cloneDeep(HOTSPOT_TEMPLATE_VR);
        hotspot._attributes.url = (s.layer && s.layer.icon) ? HOTSPOTS_STORAGE + s.layer.icon : DEFAULT_ICON;
        hotspot._attributes.ath = ath;
        ath += LAYER_VR_ATH_STEP;
        hotspot._attributes.name += s._id;
        hotspot._attributes.onclick = `js(app.tours['${tour._id}'].openScene(${s._id}, true))`;
        if (!scene.hotspot) scene.hotspot = [];
        scene.hotspot.push(hotspot);
      });
    }
  });

  /* tour actions */
  tour.panorama.krpano.action = [];

  tour.panorama.krpano.action.push({
    _attributes: {
      name: 'krpano_loaded',
      autorun: 'onstart'
    },
    // krpano is loaded event
    _text: `js(app.tours['${tour._id}'].krpanoLoaded())`
  });

  /* tour layers */
  tour.panorama.krpano.layer = [];

  /* tour includes */
  tour.panorama.krpano.include = [];

  /* tour plugins */
  tour.panorama.krpano.plugin = [];

  /* main scene */
  let mainScene;
  if (params.mainscene) {
    mainScene = _.find(tour.panorama.krpano.scene, {_attributes: {scene_id: params.mainscene}});
  } else {
    let scene = _.find(tour.scenes, {isMain: true});
    if (scene) {
      mainScene = _.find(tour.panorama.krpano.scene, {_attributes: {scene_id: scene._id}});
    }
  }
  if (mainScene) {
    tour.panorama.krpano.action.push({
      _attributes: {
        name: 'startup',
        autorun: 'onstart'
      },
      _text: `js(app.tours['${tour._id}'].openScene("${mainScene._attributes.scene_id}"))`
    });
  }

  if (tour.plan && !params.noplan) {

    /* plan map */
    const planImage = '/storage/tours/' + tour._id + '/' + ((tour.plan.image) ? tour.plan.image : 'plan.png');
    const planSpots = generatePlanSpots(tour);

    /* plan image layer */
    tour.panorama.krpano.layer.push(
      {
        _attributes:
          {
            name: 'plan',
            onloaded: `rescale_plan(${PLAN_WIDTH});`,
            align: 'centertop',
            y: 0,
            x: 0,
            url: planImage,
            keep: true,
            zorder: ZORDER_PLAN,
            handcursor: false,
            scalechildren: true,
            visible: false
          },
        layer: planSpots
      }
    );

    tour.panorama.krpano.layer.push(
      {
        _attributes:
          {
            name: 'plan_minimize',
            parent: 'plan',
            style: 'plan_minimize',
            zorder: ZORDER_PLAN,
            onclick: 'minimize_plan();'
          }
      }
    );

    tour.panorama.krpano.layer.push(
      {
        _attributes:
          {
            name: 'plan_minimize_icon',
            parent: 'plan_minimize',
            style: 'plan_minimize_icon',
            zorder: ZORDER_PLAN,
            onclick: 'minimize_plan();',
          }
      }
    );

    if (!_.isEmpty(planSpots)) {
      tour.panorama.krpano.layer.push({
        _attributes:
          {
            name: 'radar',
            keep: true,
            visible: false,
            url: `${KRPANO_PLUGINS}/radar.js`,
            align: 'center',
            fillcolor: RADAR_COLOR_FILL,
            fillalpha: '0.4',
            headingoffset: '0',
            zorder: ZORDER_RADAR,
            linewidth: '1.0',
            linecolor: RADAR_COLOR_LINE,
            linealpha: '0.5',
            width: '200%',
            height: '200%'
          }
      });
    }
  }

  if (!params.novr) {
    /* WebVR includes must be before plugin! */
    tour.panorama.krpano.include.push({
      _attributes:
        {
          url: KRPANO_PLUGINS + '/webvr.xml'
        }
    });

    tour.panorama.krpano.include.push({
      _attributes:
        {
          url: KRPANO_INCLUDES + '/webvr-actions.xml'
        }
    });

    tour.panorama.krpano.plugin.push({
      _attributes:
        {
          name: 'WebVR',
          // for testing in desktop browser
          mobilevr_fake_support: true,
          mobilevr_ipd: WEBVR_MOBILEVR_IPD,
          mobilevr_wakelock: true,
          onentervr: `webvr_onentervr(); webvr_setup(); js(app.tours['${tour._id}'].enableKrpanoToggle('vr')); js(app.tours['${tour._id}'].vrEnabled());`,
          onexitvr: `webvr_onexitvr(); webvr_setup(); js(app.tours['${tour._id}'].disableKrpanoToggle('vr')); js(app.tours['${tour._id}'].vrDisabled());`,
          onavailable: `js(app.tours['${tour._id}'].vrAvailable());`
        }
    });
  }

  tour.panorama.krpano.plugin.push({
    _attributes:
      {
        name: 'gyro',
        devices: 'html5',
        keep: true,
        url: KRPANO_PLUGINS + '/gyro2.js',
        enabled: false,
        onavailable: `js(app.tours['${tour._id}'].gyroAvailable());`,
      }
  });

  tour.panorama.krpano.display = {
    _attributes:
      {
        // disable auto fullscreen on android orientation change
        autofullscreen: false
      }
  };

  /* Tour include */
  tour.panorama.krpano.include.push({
    _attributes:
      {
        url: KRPANO_INCLUDES + '/tour-actions.xml'
      }
  });

  /* Events */
  tour.panorama.krpano.events = {
    _attributes:
      {
        name: 'tour_events',
        keep: true,
        'onresize': `krpano_resized(${tour._id}); rescale_plan(${PLAN_WIDTH});`,
        'onloadcomplete': `js(app.tours['${tour._id}'].sceneLoaded());`
      }
  };

  /* Shared styles */
  tour.panorama.krpano.include.push({
    _attributes:
      {
        url: KRPANO_INCLUDES + '/styles.xml'
      }
  });

  /* Polygonal editor */
  if (params.polygonal_editor) {
    tour.panorama.krpano.include.push({
      _attributes:
        {
          url: KRPANO_INCLUDES + '/polygonal-editor.xml'
        }
    });
  }

  return tour.panorama;
}

function generatePlanSpots(tour) {
  const planspots = [];

  const planRatio = tour.plan.width / PLAN_WIDTH_EDITOR;

  _.each(tour.planspots, p => {
    if (!p) return;

    const scene = _.find(tour.scenes, {_id: p.scene});
    if (!scene) return;

    let headingoffset = 0;

    if (scene.previewSettings &&
      !isNaN(parseFloat(scene.previewSettings.headingoffset))) {
      headingoffset = scene.previewSettings.headingoffset;
    }

    planspots.push({
      _attributes:
        {
          name: 'planspot_' + scene._id,
          url: PLAN_SPOT_ICON,
          align: 'lefttop',
          zorder: ZORDER_PLANSPOT,
          x: 100 * p.left / PLAN_WIDTH_EDITOR + '%',
          y: 100 * p.top / (tour.plan.height / planRatio) + '%',
          width: 100 * PLAN_SPOT_WIDTH / PLAN_WIDTH_EDITOR + '%',
          height: 100 * PLAN_SPOT_HEIGHT / (tour.plan.height / planRatio) + '%',
          headingoffset: headingoffset,
          onclick: `js(app.tours['${tour._id}'].openScene("${p.scene}"))`
        }
    });
  });
  return planspots;
}
