module.exports = function (req, res) {
  res.render('front/pages/tours/view', {
    tour: res.locals.tourFront
  });
};
