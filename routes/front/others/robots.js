module.exports = function (req, res) {
  if (process.env.NODE_ENV === 'production') {
    res.type('text/plain');
    res.send('User-agent: *\nDisallow: /admin');
  } else {
    res.type('text/plain');
    res.send('User-agent: *\nDisallow: /');
  }
};
