const _ = require('lodash');

const mongoose = require('mongoose');

const Developer = mongoose.model('Developer');
const Program = mongoose.model('Program');
const Apartment = mongoose.model('Apartment');

const convert = require('xml-js');

module.exports = function (req, res, next) {

  if (process.env.NODE_ENV !== 'production' || req.subdomains[0]) {
    return next(res.createError(404));
  }

  const args = [
    {isPublished: true},
    {_id: 1, updatedAt: 1},
    {autopopulate: false}
  ];

  Promise.all([
    Developer.find(...args).exec(),
    Program.find(...args).exec(),
    Apartment.find(...args).exec(),
  ])
    .then(r => {
      const sitemap = {
        urlset: {
          _attributes:
            {
              xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9'
            },
          url: []
        }
      };

      _.each(r[0], d => {
        sitemap.urlset.url.push(getUrl('developers', d));
      });

      _.each(r[1], p => {
        sitemap.urlset.url.push(getUrl('programs', p));
      });

      _.each(r[2], a => {
        sitemap.urlset.url.push(getUrl('apartments', a));
      });

      function getUrl(type, obj) {
        return {
          loc: {_text: `https://${process.env.DOMAIN}/${type}/${obj._id}`},
          lastmod: {_text: obj.updatedAt.toISOString().substring(0, 10)},
          changefreq: {_text: 'weekly'},
          priority: {_text: '1'}
        };
      }

      const xml = convert.js2xml(sitemap, {compact: true, spaces: 2});
      res.set('Content-Type', 'text/xml');
      res.send(xml);
    })
    .catch(next);

};
