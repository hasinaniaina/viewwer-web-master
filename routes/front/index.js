const mongoose = require('mongoose');
const Developer = mongoose.model('Developer');

module.exports = function(req, res, next) {
  if (req.subdomains.length === 1) {
    Developer
      .findOne({alias: req.subdomains[0], isPublished: true})
      .exec()
      .then(dev => {
        if (!dev) throw res.createError(404);
        res.locals.developerFront = dev;
        require('./developers/view')(req, res, next);
      })
      .catch(next);
  } else {
    res.render('front/pages/index-coming-soon');
  }
};
