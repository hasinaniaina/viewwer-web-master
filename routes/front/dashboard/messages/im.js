module.exports = function (req, res) {
  const q = req.url.split('?');
  let r = '/dashboard/messages';
  if (q[1]) r += '?' + q[1];
  res.redirect(r);
};
