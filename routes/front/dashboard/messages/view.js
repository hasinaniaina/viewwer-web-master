const _ = require('lodash');

const talkjs = rootRequire('lib/talkjs');

module.exports = function (req, res, next) {

  const role = _.get(req, 'user.role');

  if (role === 'editor' || role === 'admin') {

    res.render('front/pages/dashboard/messages/view', {
      talkjsUser: {
        id: talkjs.SUPPORT_ID,
        name: process.env.CHAT_SUPPORT_NAME,
        email: [process.env.CHAT_SUPPORT_EMAIL],
        photoUrl: talkjs.SUPPORT_ICON
      }
    });

  } else {

    const user = {
      id: 'user_' + req.user._id,
      name: req.user.name,
      email: req.user.email
    };

    talkjs.syncUser(user, req.locale, req.__)
      .then(() => res.render('front/pages/dashboard/messages/view', {
        talkjsUser: user
      }))
      .catch(next);

  }

};
