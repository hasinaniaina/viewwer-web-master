const _ = require('lodash');
const mongoose = require('mongoose');

const Developer = mongoose.model('Developer');
const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');
const Request = mongoose.model('Request');

const applyPermissions = rootRequire('models/helpers/apply-permissions');
const onlyPublished = rootRequire('models/helpers/only-published');

const APARTMENTS_TYPES = rootRequire('routes/back/apartments/constants').TYPES;

const STATS_LIMIT = 20;

module.exports = function (req, res, next) {

  let programsNames = [];
  let programs = [];
  let exteriors = [];

  let buildings = [];
  let programsApartments = [];

  let apartments = [];
  let interiors = [];

  Program.find(applyPermissions({}, req.user), {name: true}, {autopopulate: false})
    .sort({name: 1}).exec()
    .then(r => {
      programsNames = r;
      return Program.find(applyPermissions(onlyPublished({}), req.user))
        .sort({createdAt: -1}).limit(STATS_LIMIT).exec();
    })
    .then(r => {
      programs = r;
      if (!_.isEmpty(programs)) {
        return Tour.find(applyPermissions(
          onlyPublished({program: {$in: _.map(programs, '_id')}})
          , req.user)).exec();
      }
    })
    .then(r => {
      if (r) exteriors = r;
      if (!_.isEmpty(exteriors)) {
        _.each(programs, p => {
          let exterior = _.find(exteriors, {program: {_id: p._id}});
          if (exterior) p.exterior = exterior;
        });
      }
    })
    .then(() => {
      if (_.isEmpty(programs)) return;
      return Building.find(applyPermissions({
        program: {$in: _.map(programs, '_id')}
      }, req.user)).exec();
    })
    .then(r => {
      if (r) buildings = r;
      if (_.isEmpty(buildings)) return;
      return Apartment.find(applyPermissions(onlyPublished({
        building: {$in: _.map(buildings, '_id')}
      }), req.user)).exec();
    })
    .then(r => {
      if (r) programsApartments = r;
      let ids = _.map(programsApartments, p => {
        return _.get(p, 'building.program._id');
      });
      ids = _.countBy(ids);
      _.each(programs, p => {
        p.countApartments = ids[p._id] ? ids[p._id] : 0;
      });
    })
    .then(() => {
      return Apartment.find(applyPermissions(onlyPublished({}), req.user))
        .sort({createdAt: -1})
        .limit(STATS_LIMIT)
        .exec();
    })
    .then(r => {
      apartments = r;
      if (!_.isEmpty(apartments)) {
        return Tour.find(applyPermissions(onlyPublished({
          apartment: {
            $in: _.map(apartments, '_id')
          }
        }), req.user)).exec();
      }
    })
    .then(r => {
      if (r) interiors = r;
      _.each(apartments, a => {
        let interior = _.find(interiors, {apartment: {_id: a._id}});
        if (interior) a.interior = interior;
      });
    })
    .then(() => getCounters(req.user))
    .then(r => {
      res.render('front/pages/dashboard/stats/view', {
        apartmentsTypes: Object.keys(APARTMENTS_TYPES),
        programsNames: programsNames,
        developer: req.user.developer,
        programs: programs,
        apartments: apartments,
        counters: r
      });
    })
    .catch(next);
};

function getCounters(user) {
  return new Promise((resolve, reject) => {
    const counters = {
      rating: {
        all: 0,
        developers: 0,
        programs: 0,
        apartments: 0
      },
      shares: {
        all: 0,
        developers: 0,
        programs: 0,
        apartments: 0
      },
      requests: {
        all: 0,
        new: 0,
        old: 0
      }
    };

    function getPipeline(query) {
      return [
        {
          $match: applyPermissions(query, user)
        },
        {
          $group: {
            _id: null,
            rating: {$sum: '$counterRating'},
            shares: {$sum: '$counterShares'}
          }
        }
      ];
    }

    Promise.all([
      Request.countDocuments(applyPermissions({}, user)).exec(),
      Request.countDocuments(applyPermissions({status: 'new'}, user)).exec(),
      Developer.aggregate(getPipeline({})).exec(),
      Program.aggregate(getPipeline({})).exec(),
      Apartment.aggregate(getPipeline({})).exec(),
      // adding standalone tour's shares and likes to apartments
      Tour.aggregate(getPipeline({type: 'interior'})).exec(),
      // adding standalone tour's shares and likes to programs
      Tour.aggregate(getPipeline({type: 'exterior'})).exec()
    ])
      .then(r => {

        counters.requests.all = r[0];
        counters.requests.new = r[1];
        counters.requests.old = counters.requests.all - counters.requests.new;

        if (!_.isEmpty(r[2])) {
          counters.rating.developers += r[2][0].rating;
          counters.shares.developers += r[2][0].shares;
        }

        if (!_.isEmpty(r[3])) {
          counters.rating.programs += r[3][0].rating;
          counters.shares.programs += r[3][0].shares;
        }

        if (!_.isEmpty(r[4])) {
          counters.rating.apartments += r[4][0].rating;
          counters.shares.apartments += r[4][0].shares;
        }

        if (!_.isEmpty(r[5])) {
          counters.rating.apartments += r[5][0].rating;
          counters.shares.apartments += r[5][0].shares;
        }

        if (!_.isEmpty(r[6])) {
          counters.rating.programs += r[6][0].rating;
          counters.shares.programs += r[6][0].shares;
        }

        counters.rating.all =
          counters.rating.developers +
          counters.rating.programs +
          counters.rating.apartments;

        counters.shares.all =
          counters.shares.developers +
          counters.shares.programs +
          counters.shares.apartments;
      })
      .then(() => resolve(counters))
      .catch(e => reject(e));
  });
}
