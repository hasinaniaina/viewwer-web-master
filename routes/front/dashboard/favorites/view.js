const _ = require('lodash');

const mongoose = require('mongoose');
const Favorite = mongoose.model('Favorite');

const filterFavorite = rootRequire('/routes/api/favorites/commons').filterFavorite;

module.exports = function (req, res, next) {

  Favorite
    .find({user: req.user._id})
    .sort({createdAt: -1})
    .populate('entity')
    .exec()
    .then(r => {
      r = _.map(r, t => filterFavorite(t));
      res.render('front/pages/dashboard/favorites/view', {
        favorites: r
      });
    })
    .catch(next);
};
