const _ = require('lodash');
const mongoose = require('mongoose');
const Request = mongoose.model('Request');
const Developer = mongoose.model('Developer');

const logger = rootRequire('lib/logger');
const notify = rootRequire('routes/back/requests/notify');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);
  let request_id;
  Request
    .create(_.assign({
      developerPerm: body.developer,
      ip: req.ip,
      useragent: req.get('User-Agent')
    }, body))  
    .then(r => {
      request_id = r._id;
      logger.info('Request', request_id, 'received');
      res.json({
        status: 'success'
      });
    })
    .then(() => Developer.increaseCounter(body.developer, 'Requests'))
    .then(() => notify.send(req, request_id, body.developer))
    .catch(next);
};
