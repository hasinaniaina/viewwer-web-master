const _ = require('lodash');
const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const Tour = mongoose.model('Tour');

const onlyPublished = rootRequire('models/helpers/only-published');

module.exports = function (req, res, next) {
  let programs = [];
  let exteriors = [];

  Program.find(onlyPublished({developer: res.locals.developerFront._id})).sort({name: 1}).exec()
    .then(r => {
      programs = r;
      if (!_.isEmpty(programs)) {
        return Tour.find(onlyPublished({program: {$in: _.map(programs, '_id')}})).exec();
      }
    })
    .then(r => {
      if (r) exteriors = r;
      if (!_.isEmpty(exteriors)) {
        _.each(programs, p => {
          let exterior = _.find(exteriors, {program: {_id: p._id}});
          if (exterior) p.exterior = exterior;
        });
      }

      res.render('front/pages/developers/view', {
        developer: res.locals.developerFront,
        programs: programs
      });
    })
    .catch(next);
};
