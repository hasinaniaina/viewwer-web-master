const _ = require('lodash');
const mongoose = require('mongoose');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');
const Favorite = mongoose.model('Favorite');

const onlyPublished = rootRequire('models/helpers/only-published');

module.exports = function (req, res, next) {
  let buildings = [];
  let apartments = [];
  let interiors = [];
  let favorites = [];
  let exterior;

  const userId = _.get(req.user, '_id');

  Promise.all([
    Tour.findOne(onlyPublished({program: res.locals.programFront._id})).exec(),
    Building.find(onlyPublished({program: res.locals.programFront._id})).sort({name: 1}).exec()
  ])
    .then(r => {
      exterior = r[0];
      buildings = r[1];
      if (!_.isEmpty(buildings)) {
        return Apartment
          .find(onlyPublished({building: {$in: _.map(buildings, '_id')}}))
          .sort({area: 1}).exec();
      }
    })
    .then(r => {
      if (r) apartments = r;
      if (!_.isEmpty(apartments)) {
        return Promise.all([
          Tour.find(onlyPublished({
            $or: [
              {apartment: {$in: _.map(apartments, '_id')}},
              {apartmentsAdditional: {$elemMatch: {$in: _.map(apartments, '_id')}}}
            ]
          })).exec(),
          searchFavorites(_.map(apartments, '_id'), userId)
        ]);
      }
    })
    .then(r => {
      if (r && r[0]) interiors = r[0];
      if (r && r[1]) favorites = r[1];

      _.each(apartments, a => {
        let interior = _.find(interiors, {apartment: {_id: a._id}});
        if (!interior) interior = _.find(interiors, t => {
          return (_.find(t.apartmentsAdditional, {_id: a._id}));
        });
        if (interior) a.interior = interior;

        const favorite = _.find(favorites, {entityId: a._id});
        if (favorite) a.favoriteId = favorite._id;
      });

      res.render('front/pages/programs/view', {
        program: res.locals.programFront,
        developer: res.locals.programFront.developer,
        buildings: buildings,
        apartments: apartments,
        exterior: exterior
      });
    })
    .catch(next);
};

function searchFavorites(entities, user_id) {
  if (!user_id || _.isEmpty(entities)) return [];
  return Favorite.find({
    user: user_id,
    entityType: 'Apartment',
    entityId: {$in: entities}
  });
}
