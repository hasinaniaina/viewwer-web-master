const redirects = [
  {path: '/immobilier*', target: 'legacy.viewwer.com'},
  {path: '/galerie-artpropulsion*', target: 'legacy.viewwer.com'},
  {path: '/deganya_49*', target: 'legacy.viewwer.com'},
  {path: '/somco*', target: 'legacy.viewwer.com'},
  {path: '/demo*', target: 'legacy.viewwer.com'},
  {path: '/grgroupe*', target: 'legacy.viewwer.com'},
  {path: '/democolo*', target: 'legacy.viewwer.com'}
];

module.exports = redirects;
