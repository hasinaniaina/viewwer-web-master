/**
 * @file The list and the order of routes handled by the app.
 */

const list = [

  {use: 'middleware/enable-csp'},
  {use: 'middleware/compression'},
  {use: 'middleware/serve-static'},
  {use: 'middleware/serve-favicons'},
  {route: '/storage', use: 'middleware/serve-storage'},
  {use: 'middleware/enable-body-parsing-urlencoded'},
  {use: 'middleware/enable-body-parsing-json'},
  {use: 'middleware/enable-body-parsing-csp-report'},
  {use: 'middleware/enable-cookies'},
  {use: 'middleware/enable-flash'},
  {use: 'middleware/enable-i18n'},
  {use: 'middleware/extend-res'},
  {use: 'middleware/enable-sessions'},
  {use: 'middleware/setup-passport'},
  {use: 'middleware/authenticate-session'},
  {use: 'middleware/expose-request'},
  {use: 'middleware/expose-env'},
  {use: 'middleware/provide-settings'},
  {use: 'middleware/provide-template-variables'},
  {use: 'middleware/trim-request'},

  /* CSP */
  {route: '/report-violation', post: 'shared/report-violation'},

  /* API */

  {route: '/api', use: 'middleware/api-require-auth'},
  {route: '/api', use: 'middleware/user-prevent-blocked'},

  {route: '/api/search', post: 'api/search'},

  {route: '/api/export', get: 'api/export/get'},

  {route: '/api/rating', post: 'api/rating'},

  {route: '/api/messages/webhooks', post: 'api/messages/webhooks'},
  {route: '/api/messages/unreads/count', get: 'api/messages/unreads/count'},

  {route: '/api/task', post: 'api/task/create'},
  {route: '/api/task/:taskApiId', get: 'api/task/get'},
  {route: '/api/task/:taskApiId/remove-file', post: 'api/task/remove-file'},
  {route: '/api/task/:taskApiId/start', post: 'api/task/start'},

  {route: '/api/app/settings', get: 'api/app/settings'},
  {route: '/api/app/firebase/registration-token', post: 'api/app/firebase/registration-token'},

  {route: '/api/user/signup', post: 'api/user/signup'},
  {route: '/api/user/auth', post: 'api/user/auth'},
  {route: '/api/user/register-and-auth-with-access-token', get: 'api/user/register-and-auth-with-access-token'}, // register and auth with facebook
  {route: '/api/user/change-password', post: 'api/user/change-password'},
  {route: '/api/user/profile', get: 'api/user/profile/get', post: 'api/user/profile/post'},

  {route: '/api/tours/list', get: 'api/tours/list'},
  {route: '/api/tours/:tourApiId', get: 'api/tours/get', post: 'api/tours/post'},
  {route: '/api/tours/:tourApiId/delete', post: 'api/tours/delete'},

  {route: '/api/favorites', post: 'api/favorites/create'},
  {route: '/api/favorites/list', get: 'api/favorites/list'},
  {route: '/api/favorites/:favoriteApiId', get: 'api/favorites/get'},
  {route: '/api/favorites/:favoriteApiId/delete', post: 'api/favorites/delete'},

  /* BACK */

  {route: '/admin', use: 'middleware/user-require-login'},
  {route: '/admin', use: 'middleware/user-prevent-blocked'},
  {route: '/admin', use: 'middleware/user-check-role-admin'},

  {route: '/admin/switch', get: 'back/switch'},

  /* users */
  {route: '/admin/users', get: 'back/users/get'},
  {route: '/admin/users/grid', get: 'back/users/grid/get'},
  {route: '/admin/users/create', get: 'back/users/create/get', post: 'back/users/create/post'},
  {
    route: '/admin/users/:userId/edit',
    get: 'back/users/user/edit/get',
    post: 'back/users/user/edit/post'
  },
  {route: '/admin/users/:userId/delete', get: 'back/users/user/delete/get'},

  /* groups */
  {route: '/admin/groups', get: 'back/groups/get'},
  {route: '/admin/groups/grid', get: 'back/groups/grid/get'},
  {route: '/admin/groups/create', post: 'back/groups/create'},
  {
    route: '/admin/groups/:groupId/edit',
    get: 'back/groups/group/edit/get',
    post: 'back/groups/group/edit/post'
  },
  {route: '/admin/groups/:groupId/delete', get: 'back/groups/group/delete/get'},

  /* developers */
  {route: '/admin/developers', get: 'back/developers/get'},
  {route: '/admin/developers/grid', get: 'back/developers/grid/get'},
  {route: '/admin/developers/create', post: 'back/developers/create'},
  {
    route: '/admin/developers/:developerId/edit',
    get: 'back/developers/developer/edit/get',
    post: 'back/developers/developer/edit/post'
  },
  {
    route: '/admin/developers/:developerId/permissions',
    get: 'back/developers/developer/permissions/get',
    post: 'back/developers/developer/permissions/post'
  },
  {route: '/admin/developers/:developerId/delete', get: 'back/developers/developer/delete/get'},
  {route: '/admin/developers/developer/deleteAll', post: 'back/developers/developer/deleteAll/post'},
  {route: '/admin/developers/:developerId/save-logo', post: 'back/developers/save-logo'},
  {route: '/admin/developers/:developerId/remove-logo', post: 'back/developers/remove-logo'},

  /* programs */
  {route: '/admin/programs', get: 'back/programs/get'},
  {route: '/admin/programs/grid', get: 'back/programs/grid/get'},
  {route: '/admin/programs/create', post: 'back/programs/create'},
  {
    route: '/admin/programs/:programId/edit',
    get: 'back/programs/program/edit/get',
    post: 'back/programs/program/edit/post'
  },
  {route: '/admin/programs/export', get: 'back/programs/export/get'},
  {route: '/admin/programs/:programId/delete', get: 'back/programs/program/delete/get'},
  {route: '/admin/programs/program/deleteAll', post: 'back/programs/program/deleteAll/post'},
  {route: '/admin/programs/:programId/save-logo', post: 'back/programs/save-logo'},
  {route: '/admin/programs/:programId/remove-logo', post: 'back/programs/remove-logo'},
  {route: '/admin/programs/:programId/save-pdf', post: 'back/programs/save-pdf'},
  {route: '/admin/programs/:programId/remove-pdf', post: 'back/programs/remove-pdf'},

  /* buildings */
  {route: '/admin/buildings', get: 'back/buildings/get'},
  {route: '/admin/buildings/grid', get: 'back/buildings/grid/get'},
  {route: '/admin/buildings/create', post: 'back/buildings/create'},
  {
    route: '/admin/buildings/:buildingId/edit',
    get: 'back/buildings/building/edit/get',
    post: 'back/buildings/building/edit/post'
  },
  /* get exterior tour json by building-id, needed for apartment editor */
  {
    route: '/admin/buildings/:buildingId/exterior',
    get: 'back/buildings/exterior/get',
  },
  {route: '/admin/buildings/:buildingId/delete', get: 'back/buildings/building/delete/get'},

  /* apartments */
  {route: '/admin/apartments', get: 'back/apartments/get'},
  {route: '/admin/apartments/grid', get: 'back/apartments/grid/get'},
  {route: '/admin/apartments/create', post: 'back/apartments/create'},
  {
    route: '/admin/apartments/:apartmentId/edit',
    get: 'back/apartments/apartment/edit/get',
    post: 'back/apartments/apartment/edit/post'
  },
  {route: '/admin/apartments/:apartmentId/delete', get: 'back/apartments/apartment/delete/get'},

  /* tours */
  {route: '/admin/tours', get: 'back/tours/get'},
  {route: '/admin/tours/grid', get: 'back/tours/grid/get'},
  {route: '/admin/tours/create', post: 'back/tours/create'},
  {route: '/admin/tours/:tourId', get: 'back/tours/view'},
  {route: '/admin/tours/:tourId/create-scene', post: 'back/tours/create-scene'},
  {route: '/admin/tours/:tourId/save', post: 'back/tours/save'},
  // returns tour generation status
  {route: '/admin/tours/:tourId/status/generate', get: 'back/tours/status/generate'},
  {route: '/admin/tours/:tourId/generate-scene-preview', get: 'back/tours/generate-scene-preview'},
  {route: '/admin/tours/:tourId/save-plan', post: 'back/tours/plan/save-plan'},
  {route: '/admin/tours/:tourId/remove-plan', post: 'back/tours/plan/remove-plan'},
  {route: '/admin/tours/:tourId/delete', get: 'back/tours/delete'},

  /* layer-types */
  {route: '/admin/layer-types', get: 'back/layer-types/get'},
  {route: '/admin/layer-types/grid', get: 'back/layer-types/grid/get'},
  {route: '/admin/layer-types/create', post: 'back/layer-types/create'},
  {
    route: '/admin/layer-types/:layerTypeId/edit',
    get: 'back/layer-types/layer-type/edit/get',
    post: 'back/layer-types/layer-type/edit/post'
  },
  {route: '/admin/layer-types/:layerTypeId/delete', get: 'back/layer-types/layer-type/delete/get'},

  /* scene-types */
  {route: '/admin/scene-types', get: 'back/scene-types/get'},
  {route: '/admin/scene-types/grid', get: 'back/scene-types/grid/get'},
  {route: '/admin/scene-types/create', post: 'back/scene-types/create'},
  {
    route: '/admin/scene-types/:sceneTypeId/edit',
    get: 'back/scene-types/scene-type/edit/get',
    post: 'back/scene-types/scene-type/edit/post'
  },
  {route: '/admin/scene-types/:sceneTypeId/delete', get: 'back/scene-types/scene-type/delete/get'},
  {
    route: '/admin/scene-types/:sceneTypeId/tours',
    get: 'back/scene-types/scene-type/tours/get'
  },

  /* sequences */
  {route: '/admin/sequences', get: 'back/sequences/get'},
  {route: '/admin/sequences/grid', get: 'back/sequences/grid/get'},
  {route: '/admin/sequences/create', post: 'back/sequences/create'},
  {
    route: '/admin/sequences/:sequenceId/edit',
    get: 'back/sequences/sequence/edit/get',
    post: 'back/sequences/sequence/edit/post'
  },
  {route: '/admin/sequences/:sequenceId/delete', get: 'back/sequences/sequence/delete/get'},

  /* phrases */
  {route: '/admin/phrases', get: 'back/phrases/get'},
  {route: '/admin/phrases/grid', get: 'back/phrases/grid/get'},
  {route: '/admin/phrases/create', post: 'back/phrases/create'},
  {
    route: '/admin/phrases/:phraseId/edit',
    get: 'back/phrases/phrase/edit/get',
    post: 'back/phrases/phrase/edit/post'
  },
  {route: '/admin/phrases/:phraseId/delete', get: 'back/phrases/phrase/delete/get'},

  /* imagesets */
  {route: '/admin/imagesets', get: 'back/imagesets/get'},
  {route: '/admin/imagesets/grid', get: 'back/imagesets/grid/get'},
  {route: '/admin/imagesets/create', post: 'back/imagesets/create'},
  {
    route: '/admin/imagesets/:imagesetId/edit',
    get: 'back/imagesets/imageset/edit/get',
    post: 'back/imagesets/imageset/edit/post'
  },
  {route: '/admin/imagesets/:imagesetId/delete', get: 'back/imagesets/imageset/delete/get'},
  {route: '/admin/imagesets/:imagesetId/save-image', post: 'back/imagesets/save-image'},
  {route: '/admin/imagesets/:imagesetId/remove-image', post: 'back/imagesets/remove-image'},

  /* files */
  {route: '/admin/files', get: 'back/files/list'},
  {route: '/admin/files/api/', use: 'back/files/api/storage-path'},
  {
    route: /admin\/files\/api(.*)?/,
    get: 'back/files/api/get',
    post: 'back/files/api/post',
    delete: 'back/files/api/delete'
  },

  /* requests */
  {route: '/admin/requests', get: 'back/requests/get'},
  {route: '/admin/requests/grid', get: 'back/requests/grid/get'},
  {
    route: '/admin/requests/:requestId/edit',
    get: 'back/requests/request/edit/get',
    post: 'back/requests/request/edit/post'
  },
  {
    route: '/admin/requests/:requestId/delete',
    get: 'back/requests/request/delete/get'
  },
  {
    route: '/admin/requests/request/deleteAll',
    post: 'back/requests/request/deleteAll/post'
  },
  {route: '/admin/requests/export/new', get: 'back/requests/export/new'},
  {route: '/admin/requests/count/new', get: 'back/requests/count/new'},

  /* tasks */
  {route: '/admin/tasks', get: 'back/tasks/get'},
  {route: '/admin/tasks/grid', get: 'back/tasks/grid/get'},
  {route: '/admin/tasks/delete', post: 'back/tasks/delete'},
  {
    route: '/admin/tasks/:taskId/edit',
    get: 'back/tasks/task/edit/get'
  },
  {route: '/admin/tasks/:taskId/delete', get: 'back/tasks/task/delete/get'},

  /* logs */
  {route: '/admin/logs', get: 'back/logs/get'},
  {route: '/admin/logs/grid', get: 'back/logs/grid/get'},
  {
    route: '/admin/logs/:logId/edit',
    get: 'back/logs/log/edit/get'
  },

  /* apptours */
  {route: '/admin/apptours', get: 'back/apptours/get'},
  {route: '/admin/apptours/grid', get: 'back/apptours/grid/get'},
  {
    route: '/admin/apptours/:apptourId/edit',
    get: 'back/apptours/apptour/edit/get',
    post: 'back/apptours/apptour/edit/post'
  },
  {route: '/admin/apptours/:apptourId/delete', get: 'back/apptours/apptour/delete/get'},
  {route: '/admin/apptours/count/new', get: 'back/apptours/count/new'},

  /* settings */
  {route: '/admin/settings', get: 'back/settings/get', post: 'back/settings/post'},
  {route: '/admin/settings/rebuild-paths', post: 'back/settings/rebuild-paths'},
  {route: '/admin/settings/rebuild-counters', post: 'back/settings/rebuild-counters'},
  {route: '/admin/settings/mail-test', post: 'back/settings/mail-test'},
  {route: '/admin/settings/fcm-test', post: 'back/settings/fcm-test'},
  {route: '/admin/settings/fix', post: 'back/settings/fix'},

  /* changelog */
  {route: '/admin/changelog', get: 'back/changelog/get'},
  
  /* partners */
  {route: '/admin/partners', get: 'back/partners/get'},
  {route: '/admin/partners/grid', get: 'back/partners/grid/get'},
  {route: '/admin/partners/create', post: 'back/partners/create'},
  {
    route: '/admin/partners/:partnerId/edit',
    get: 'back/partners/partner/edit/get',
    post: 'back/partners/partner/edit/post'
  },
  {route: '/admin/partners/:partnerId/delete', get: 'back/partners/partner/delete/get'},
  {route: '/auth/enable-partner-member', get: 'back/partners/enable-member/get'},

  /* index */
  {route: '/admin', get: 'back'},

  /* FRONT */

  /* auth */
  {route: '/auth/log-in', get: 'back/auth/log-in/get', post: 'back/auth/log-in/post'},
  {route: '/auth/log-out', get: 'back/auth/log-out'},
  {
    route: '/auth/request-password-reset',
    get: 'back/auth/request-password-reset/get',
    post: 'back/auth/request-password-reset/post'
  },
  {route: '/auth/reset-password', get: 'back/auth/reset-password/get', post: 'back/auth/reset-password/post'},
  {route: '/auth/verify', get: 'back/auth/verify/get'},
  {route: '/auth/sign-up', get: 'back/auth/sign-up/get', post: 'back/auth/sign-up/post'},

  /* facebook login */
  { route: '/auth/facebook', get: 'back/auth/facebook/get'},
  { route: '/auth/facebook/callback', get: 'back/auth/facebook/callback'},
  
  /* google login */
  { route: '/auth/google', get: 'back/auth/google/get'},
  { route: '/auth/google/callback', get: 'back/auth/google/callback'},
  
  /* index */
  {route: '/', get: 'front'},

  /* TODO  */
  {route: '/test', get: 'front/test'},

  /* search */
  {route: '/search', get: 'front/search/view'},

  /* developers */
  {route: '/developers/:developerFrontId', get: 'front/developers/view'},

  /* programs */
  {route: '/programs/:programFrontId', get: 'front/programs/view'},

  /* apartments */
  {route: '/apartments/:apartmentFrontId', get: 'front/apartments/view'},

  /* tours */
  {route: '/tours/:tourFrontId', get: 'front/tours/view'},
  {route: '/storage/tours/:tourFrontId/final/panorama.xml', get: 'front/tours/panorama'},

  /* app tours */
  // view
  {route: '/apptours/:apptourFrontId', get: 'front/apptours/view'},
  {route: '/apptours/:apptourFrontId/tour.xml', get: 'front/apptours/view-xml'},
  // preview
  {route: '/apptours/:apptourTaskId/preview', get: 'front/apptours/preview'},
  {route: '/apptours/:apptourTaskId/preview/tour.xml', get: 'front/apptours/preview-xml'},

  /* request */
  {route: '/request', post: 'front/request/post'},

  /* static pages */
  {route: '/about', get: 'front/others/about'},
  {route: '/terms', get: 'front/others/terms'},
  {route: '/pages/gyro-help', get: 'front/others/gyro-help'},

  /* DASHBOARD */
  {route: '/dashboard', use: 'middleware/user-require-login'},
  {route: '/dashboard', use: 'middleware/user-prevent-blocked'},
  {route: '/dashboard', use: 'middleware/user-check-role-dashboard'},

  /* statistics */
  {route: '/dashboard/stats', get: 'front/dashboard/stats/view'},

  /* short url for messages */
  {route: '/im', get: 'front/dashboard/messages/im'},

  /* messages */
  {route: '/dashboard/messages', get: 'front/dashboard/messages/view'},

  /* favorites */
  {route: '/dashboard/favorites', get: 'front/dashboard/favorites/view'},

  /* robots */
  {route: '/robots.txt', get: 'front/others/robots'},

  /* sitemap */
  {route: '/sitemap.xml', get: 'front/others/sitemap'},

  /* ERROR HANDLERS */

  {use: 'middleware/trigger-404'},
  {use: 'middleware/output-error'}
];

if (process.env.HTTPS_REDIRECT === 'yes') {
  list.unshift({use: 'middleware/https'});
}

module.exports = list;
