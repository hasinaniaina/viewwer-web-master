const _ = require('lodash');
const mongoose = require('mongoose');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

module.exports = function (req, res, next) {
  if (!req.user) return next(res.createError(403));

  const Model = mongoose.model(_.capitalize(req.query.model));

  Model
    .findOne(applyPermissions({_id: req.query._id}, req.user))
    .exec()
    .then(doc => {
      doc[req.query.property] = !(doc[req.query.property]);
      if (req.query.updated) doc[req.query.updated] = new Date().toISOString();

      return doc
        .save()
        .then(() => {
          res.json({status: 'success', on: doc[req.query.property]});
        });
    })
    .catch(next);
};
