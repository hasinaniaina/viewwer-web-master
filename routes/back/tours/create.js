const mongoose = require('mongoose');
const Tour = mongoose.model('Tour');
const savePermissions = rootRequire('routes/back/save-permissions');

module.exports = function(req, res, next) {
  const tour = new Tour({name: req.body.name});

  savePermissions.onCreate(req, tour);

  tour
    .save()
    .then(() => {
      const redirect = '/admin/tours/' + tour._id;

      if (req.xhr) {
        res.json({
          status: 'success',
          redirect: redirect
        });
      } else {
        res.redirect(redirect);
      }
    })
    .catch(next);
};
