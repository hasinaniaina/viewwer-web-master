const fs = require('fs-extra');

const logger = rootRequire('lib/logger');

module.exports = function(req, res, next) {
  const tour = res.locals.tour;

  if (req.query.confirmed) {
    tour
      .remove()
      .then(() => {
        return fs.remove(process.env.STORAGE_PATH + '/public/tours/' + tour._id);
      })
      .then(() => {
        logger.info('Tour', tour._id,
          'removed by user', req.user._id, req.user.email);
        res.redirect('/admin/tours');
      })
      .catch(next);
  } else {
    res.render('back/pages/tours/delete');
  }
};
