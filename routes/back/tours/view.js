const mongoose = require('mongoose');
const Tour = mongoose.model('Tour');
const SceneType = mongoose.model('SceneType');
const LayerType = mongoose.model('LayerType');
const Program = mongoose.model('Program');
const Apartment = mongoose.model('Apartment');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const Sequence = mongoose.model('Sequence');
const applyPermissions = rootRequire('models/helpers/apply-permissions');
const roleDevelopers = rootRequire('models/helpers/role-developers');

const THUMB_WIDTH = 540;
const THUMB_HEIGHT = 300;

module.exports = function(req, res, next) {
  Promise.all([
    Tour.findById(res.locals.tour._id, {}, {autopopulate: false}),
    SceneType.find({}).sort({name: 1}).exec(),
    LayerType.find({}).sort({name: 1}).exec(),
    Program.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    Apartment.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Group.find({}).sort({name: 1}).exec(),
    Sequence.find({}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/tours/view', {
        tour: results[0],
        sceneTypes: results[1],
        layerTypes: results[2],
        programs: results[3],
        apartments: results[4],
        users: results[5],
        groups: results[6],
        sequences: results[7],
        THUMB_WIDTH: THUMB_WIDTH,
        THUMB_HEIGHT: THUMB_HEIGHT
      });
    })
    .catch(next);
};
