const _ = require('lodash');
const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const convert = require('xml-js');

const mongoose = require('mongoose');
const Tour = mongoose.model('Tour');
const savePermissions = rootRequire('routes/back/save-permissions');

const mapSeries = rootRequire('lib/map-series');
const execFile = require('child_process').execFile;
const EXEC_OPTIONS = {maxBuffer: 10000 * 1024};

const shortid = require('shortid');

const awstask = rootRequire('lib/aws/aws-task');
const makepano = rootRequire('lib/aws/makepano').makepano;

const logger = rootRequire('lib/logger');

const retry = rootRequire('lib/retry-promise');
const RETRY_DOWNLOAD = 3;
const RETRY_DOWNLOAD_BACKOFF = 2000;

module.exports = function (req, res, next) {
  const tour = res.locals.tour;
  const tourDir = path.join(process.env.STORAGE_PATH, 'public/tours', tour._id);

  if (!req.body.tour.apartmentsAdditional) {
    req.body.tour.apartmentsAdditional = [];
  }

  let tourData;

  // if we want to update only the settings
  if (req.body.tour.updateOnlyProperties === 'true') {
    tourData = {
      name: req.body.tour.name,
      user: req.body.tour.user,
      group: req.body.tour.group,
      type: req.body.tour.type,
      program: req.body.tour.program,
      apartment: req.body.tour.apartment,
      apartmentsAdditional: req.body.tour.apartmentsAdditional,
      planspots: req.body.tour.planspots,
      scenes: req.body.tour.scenes,
    };
  } else {
    tourData = _.omit(req.body.tour, '_id', '__v');
  }

  savePermissions.onEdit(req, tourData);

  if (req.body.tour.updateOnlyProperties === 'true') {
    Promise.all([
      Tour.clearApartment((tourData.apartment) ? tourData.apartment : '__unknown'),
      Tour.clearProgram((tourData.program) ? tourData.program : '__unknown')
    ])
      .then(() => tour.updateOne(tourData).exec())
      .then(() => Tour.updatePath(tour._id))
      .then(() => {
        res.json({
          status: 'success'
        });
      })
      .catch(next);
  } else {
    let durationGenerate = Date.now();
    let responseSent = false;

    Promise.all([
      Tour.clearApartment((tourData.apartment) ? tourData.apartment : '__unknown'),
      Tour.clearProgram((tourData.program) ? tourData.program : '__unknown')
    ])
      .then(() => tour.updateOne(tourData).exec())
      .then(() => Tour.updatePath(tour._id))
      .then(() => saveThumbnails(tourDir, tourData.scenes))
      .then(() => removePreviews(tourDir, tourData.scenes))
      .then(() => fs.remove(path.join(tourDir, 'final')))
      .then(() => tour.updateOne({
        generateStatus: 'processing',
        generateStartedAt: new Date().toISOString(),
        generateDuration: null
      }).exec())
      .then(() => {
        responseSent = true;
        res.json({
          status: 'processing'
        });
        logger.info('Tour', tour._id, 'saving');
      })
      .then(() => {
        if (res.locals.site.settings.lambdaTours) {
          return generateLambda(tour._id, tourDir, tourData.scenes);
        } else {
          return generateInternal(tour._id, tourDir, tourData.scenes);
        }
      })
      .then(() => fs.readFile(tourDir + '/final/panorama.xml', 'utf8'))
      .then(xml => tour.updateOne({panorama: xmlToObject(xml)}).exec())
      .then(() => fs.remove(tourDir + '/final/panorama.xml'))
      .then(() => {
        durationGenerate = Date.now() - durationGenerate;
        logger.info('Tour', tour._id, 'saved,', 'duration:', durationGenerate);
        return tour.updateOne({
          generateStatus: 'completed',
          generateDuration: durationGenerate
        }).exec();
      })
      .catch((e) => {
        logger.error('Tour', tour._id, 'save error', e);
        if (!responseSent) {
          res.json({
            status: 'failed'
          });
        }
        return tour.updateOne({
          generateStatus: 'failed',
          generateDuration: durationGenerate
        }).exec();
      });
  }
};

function saveThumbnails(tour_dir, scenes) {
  return mapSeries(scenes, scene => {
    if (!scene.thumbnail) return;
    return fs.writeFile(path.join(tour_dir, 'scenes', scene._id, 'thumbnail.jpg'), scene.thumbnail.split(',')[1], 'base64');
  });
}

function removePreviews(tour_dir, scenes) {
  return mapSeries(scenes, scene => {
    return fs.remove(path.join(tour_dir, 'scenes', scene._id, 'preview'));
  });
}

function generateInternal(tour_id, tour_dir, scenes) {
  let executable = path.resolve(path.join('lib/krpano/bin/', os.platform(), os.arch(), 'krpanotools'));

  const configFile = path.resolve('lib/krpano/final/config.txt');
  const templateFile = path.resolve('lib/krpano/final/template.xml');
  const viewTemplateFile = path.resolve('lib/krpano/final/view-template.xml');

  const sceneFiles = scenes.map(scene => {
    return path.resolve(path.join(tour_dir, 'scenes', scene._id, scene.file.path.split('/').pop()));
  });

  let params = ['makepano'];

  const flags = {
    config: configFile,
    xmltemplate: templateFile,
    xmltemplate_view: viewTemplateFile,
    xmlpath: path.resolve(path.join(tour_dir, 'final/panorama.xml')),
    tilepath: path.resolve(path.join(tour_dir, 'final/%BASENAME%.tiles/l%Al[_c]_%Av_%Ah.jpg')),
    tempcubepath: path.resolve(path.join(tour_dir, 'final/tmp/%BASENAME%_cube%UID%'))
  };

  _.each(flags, (v, k) => params.push('-' + k + '=' + v));
  params = params.concat(sceneFiles);

  return new Promise((resolve, reject) => {
    execFile(executable, params, EXEC_OPTIONS,
      (error, stdout) => {
        if (stdout.indexOf('ERROR') !== -1) {
          logger.error('Tour', tour_id, 'save krpano error', {stdout: stdout});
          return reject(stdout);
        }
        if (error) {
          logger.error('Tour', tour_id, 'save krpano error', error);
          return reject(error);
        }
        resolve();
      });
  });
}

function generateLambda(tour_id, tour_dir, scenes) {
  return new Promise((resolve, reject) => {

    const sceneFiles = scenes.map(scene => {
      return {
        id: scene._id,
        name: scene.file.path.split('/').pop(),
        path: path.resolve(path.join(tour_dir, 'scenes', scene._id, scene.file.path.split('/').pop()))
      };
    });

    const taskId = shortid();
    const tmp = path.resolve(path.join(tour_dir, 'tmp'));

    let durationGenerate = Date.now();
    let taskOutput;

    logger.info('Tour', tour_id, 'task_id', taskId, 'lambda generate');

    fs.ensureDir(tmp)
      .then(() => {
        return Promise.all(_.map(sceneFiles, s => {
          return fs.copy(s.path, path.resolve(path.join(tmp, s.name)));
        }));
      })
      .then(() => awstask.uploadDir(taskId, tmp))
      .then(() => makepano(taskId, {
        preset: 'final',
        scenes: _.map(sceneFiles, s => {
          return {id: s.id};
        })
      }))
      .then(r => {
        taskOutput = r;
        return retry({
          max: RETRY_DOWNLOAD,
          backoff: RETRY_DOWNLOAD_BACKOFF
        }, (attempt) => {
          if (attempt > 1) {
            logger.info('Tour', tour_id, 'task_id', taskId,
              'lambda results download,', 'attempt:', attempt);
          }
          return awstask.downloadDir(taskId, path.resolve(path.join(tour_dir, 'final')));
        });
      })
      .then(() => fs.remove(tmp))
      .then(() => {
        durationGenerate = Date.now() - durationGenerate;
        logger.info('Tour', tour_id, 'task_id', taskId,
          'lambda generated,', 'duration:', durationGenerate, taskOutput);
        resolve();
      })
      .catch(e => {
        logger.error('Tour', tour_id, 'task_id', taskId,
          'lambda generation error', e);
        return reject();
      });
  });
}

/* base krpano xml to js object */
function xmlToObject(xml) {
  let config = convert.xml2js(xml, {compact: true});
  if (!Array.isArray(config.krpano.scene)) config.krpano.scene = [config.krpano.scene];
  return config;
}
