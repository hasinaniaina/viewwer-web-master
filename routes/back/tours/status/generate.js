module.exports = function (req, res) {
  const tour = res.locals.tour;
  return res.json({generateStatus: tour.generateStatus});
};
