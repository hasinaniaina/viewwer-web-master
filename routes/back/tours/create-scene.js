const fs = require('fs-extra');
const path = require('path');
const shortid = require('shortid');
const probe = require('probe-image-size');
const processMultipart = rootRequire('lib/process-multipart');

module.exports = function(req, res, next) {
  const scene = {
    _id: shortid.generate()
  };

  const processingOptions = {
    destination: path.join(process.env.STORAGE_PATH, 'public/tours', req.params.tourId, 'scenes', scene._id),
    fileName: () => scene._id + '.jpg'
  };

  processMultipart(req, processingOptions)
    .then(function() {
      const file = req.files.image;

      scene.file = {
        originalName: file.originalName,
        path: path.posix.join('/storage/tours', req.params.tourId, 'scenes', scene._id, file.name)
      };

      return fs.stat(file.path);
    })
    .then(function(stats) {
      scene.file.size = stats.size;

      const imageStream = fs.createReadStream(req.files.image.path);

      return probe(imageStream)
        .then(function(result) {
          imageStream.destroy();

          Object.assign(scene.file, result);

          scene.file.hfov = 360;
          scene.file.vfov = scene.file.hfov * scene.file.height / scene.file.width;

          res.json({
            status: 'success',
            scene: scene
          });
        });
    })
    .catch(next);
};
