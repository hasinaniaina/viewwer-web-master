const os = require('os');
const path = require('path');
const fs = require('fs-extra');

const execFile = require('child_process').execFile;
const EXEC_OPTIONS = {maxBuffer: 10000 * 1024};

const shortid = require('shortid');

const awstask = rootRequire('lib/aws/aws-task');
const makepano = rootRequire('lib/aws/makepano').makepano;

const logger = rootRequire('lib/logger');

const retry = rootRequire('lib/retry-promise');
const RETRY_DOWNLOAD = 3;
const RETRY_DOWNLOAD_BACKOFF = 2000;

module.exports = function (req, res, next) {
  if (res.locals.site.settings.lambdaScenePreviews) {
    generateLambda(req.params.tourId, req.query.sceneId, req.query.fileName)
      .then(() => res.json({status: 'success'}))
      .catch(next);
  } else {
    generateInternal(req.params.tourId, req.query.sceneId, req.query.fileName)
      .then(() => res.json({status: 'success'}))
      .catch(next);
  }
};

function generateInternal(tour_id, scene_id, filename) {
  return new Promise((resolve, reject) => {
    let executable = path.resolve(path.join('lib/krpano/bin', os.platform(), os.arch(), 'krpanotools'));

    const configFile = path.resolve('lib/krpano/preview/config.txt');
    const templateFile = path.resolve('lib/krpano/preview/template.xml');
    const inputFile = path.resolve(path.join(process.env.STORAGE_PATH, 'public/tours', tour_id, 'scenes', scene_id, filename));
    const scenePreviewDir = path.join(process.env.STORAGE_PATH, 'public/tours', tour_id, 'scenes', scene_id, 'preview');

    let durationGenerate = Date.now();
    logger.info('Tour', tour_id, 'scene', scene_id, 'generate');

    fs
      .remove(scenePreviewDir)
      .then(() => {
        const params = ['makepano', '-config=' + configFile, '-xmltemplate=' + templateFile, inputFile];
        return new Promise((resolve, reject) => {
          execFile(executable, params, EXEC_OPTIONS,
            (error, stdout) => {
              if (stdout.indexOf('ERROR') !== -1) {
                logger.error('Tour', tour_id, 'scene', scene_id, 'krpano error', {stdout: stdout});
                return reject(stdout);
              }
              if (error) {
                logger.error('Tour', tour_id, 'scene', scene_id, 'krpano error', error);
                return reject(error);
              }
              resolve();
            });
        });
      })
      .then(() => {
        durationGenerate = Date.now() - durationGenerate;
        logger.info('Tour', tour_id,
          'scene', scene_id,
          'generated,', 'duration:', durationGenerate);
        resolve();
      })
      .catch(e => reject(e));
  });
}

function generateLambda(tour_id, scene_id, filename) {
  return new Promise((resolve, reject) => {
    const inputFile = path.resolve(path.join(process.env.STORAGE_PATH, 'public/tours', tour_id, 'scenes', scene_id, filename));
    const scenePreviewDir = path.join(process.env.STORAGE_PATH, 'public/tours', tour_id, 'scenes', scene_id, 'preview');

    const taskId = shortid();

    let durationGenerate = Date.now();
    let taskOutput;

    logger.info('Tour', tour_id, 'scene', scene_id, 'task_id', taskId, 'lambda generate');

    fs
      .remove(scenePreviewDir)
      .then(() => awstask.uploadFile(taskId, inputFile, filename))
      .then(() => makepano(taskId, {
        preset: 'preview',
        scenes: [{id: scene_id}]
      }))
      .then(r => {
        taskOutput = r;
        return retry({
          max: RETRY_DOWNLOAD,
          backoff: RETRY_DOWNLOAD_BACKOFF
        }, (attempt) => {
          if (attempt > 1) {
            logger.info('Tour', tour_id, 'scene', scene_id, 'task_id', taskId,
              'lambda results download,', 'attempt:', attempt);
          }
          return awstask.downloadDir(taskId, path.resolve(scenePreviewDir));
        });
      })
      .then(() => {
        durationGenerate = Date.now() - durationGenerate;
        logger.info('Tour', tour_id, 'scene', scene_id, 'task_id', taskId,
          'lambda generated,', 'duration:', durationGenerate, taskOutput);
        resolve();
      })
      .catch(e => {
        logger.error('Tour', tour_id, 'scene', scene_id, 'task_id', taskId,
          'lambda generation error', e);
        return reject();
      });
  });
}
