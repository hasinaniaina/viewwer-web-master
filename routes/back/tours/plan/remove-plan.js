const fs = require('fs-extra');
const path = require('path');

const PLAN_IMAGE = 'plan.jpg';

module.exports = function (req, res, next) {

  const tour = res.locals.tour;
  const p = path.join(process.env.STORAGE_PATH, 'public/tours', res.locals.tour._id, '/' + PLAN_IMAGE);

  tour.set('plan', undefined).save()
    .then(() => fs.remove(path.resolve(p)))
    .then(() => res.json({}))
    .catch(next);

};
