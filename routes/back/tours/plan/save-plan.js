const _ = require('lodash');
const fs = require('fs-extra');
const path = require('path');

const sharp = require('sharp');
const sizeOf = require('buffer-image-size');

const PLAN_IMAGE = 'plan.jpg';
const PLAN_WIDTH = 1000;

module.exports = function (req, res, next) {

  const plan = req.body.plan.replace(/^data:image\/jpeg;base64,/, '');
  const buf = new Buffer(plan, 'base64');

  const result = {
    image: PLAN_IMAGE
  };

  sharp(buf).resize(PLAN_WIDTH).toBuffer()
    .then(r => {
      const size = sizeOf(r);
      result.width = size.width;
      result.height = size.height;

      const p = path.join(process.env.STORAGE_PATH, 'public/tours', res.locals.tour._id, '/' + PLAN_IMAGE);
      fs.outputFile(path.resolve(p), r);
    })
    .then(() =>
      res.locals.tour.set({
        plan: result
      }).save())
    .then(() => res.json(_.assign({}, {status: 'success'}, result)))
    .catch(next);
};
