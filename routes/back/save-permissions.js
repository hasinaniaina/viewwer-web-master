module.exports.onCreate = function (req, object) {
  if (req.user.role !== 'admin' &&
    req.user.role !== 'editor') {
    object.user = req.user._id;
    object.group = (req.user.group) ? req.user.group._id : '';
  }
};

module.exports.onEdit = function (req, object) {
  if (req.user.role !== 'admin' &&
    req.user.role !== 'editor') {
    delete object.user;
    //apply user's group
    object.group = (req.user.group) ? req.user.group._id : '';
  }
};
