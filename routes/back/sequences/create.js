const mongoose = require('mongoose');
const Sequence = mongoose.model('Sequence');

module.exports = function (req, res, next) {
  const sequence = new Sequence({name: req.body.name});

  sequence
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/sequences/' + sequence._id + '/edit'
      });
    })
    .catch(next);
};
