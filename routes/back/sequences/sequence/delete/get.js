module.exports = function(req, res, next) {
  const sequence = res.locals.sequence;

  if (req.query.confirmed) {
    sequence
      .remove()
      .then(() => {
        res.redirect('/admin/sequences');
      })
      .catch(next);
  } else {
    res.render('back/pages/sequences/delete');
  }
};
