const mongoose = require('mongoose');
const SceneType = mongoose.model('SceneType');

module.exports = function (req, res, next) {
  Promise.all([
    SceneType.find({}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/sequences/edit', {
        sceneTypes: results[0]
      });
    })
    .catch(next);
};
