const _ = require('lodash');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.sceneTypesSequence = _.uniq(body.sceneTypesSequence);

  res.locals.sequence
    .set(body)
    .save()
    .then(function() {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/sequences'
      });
    })
    .catch(next);
};
