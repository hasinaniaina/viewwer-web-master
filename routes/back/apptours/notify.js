const _ = require('lodash');
const mongoose = require('mongoose');

const User = mongoose.model('User');

const email = rootRequire('lib/email');
const nunjucksEnv = rootRequire('/lib/nunjucks-env');

const logger = rootRequire('lib/logger');

module.exports.send = function (req, tour_id) {
  return new Promise((resolve /*,reject*/) => {
    let to = [];
    User.find({role: {$in: ['admin', 'editor']}}).exec()
      .then(users => {
        to = _.uniq(_.map(users, 'email'));
        logger.info('Api tour', tour_id, 'notification recipients', to);
        if (_.isEmpty(to)) return;
        return email.send({
          from: 'no-reply@' + process.env.DOMAIN,
          to: to,
          subject: req.__('You have new tour'),
          html: nunjucksEnv.render(
            'back/email/tour.njk',
            {
              tour_id: tour_id,
              __: req.__
            }
          )
        });
      })
      .then(() => resolve())
      .catch(e => {
        logger.error('Api tour', tour_id, 'notification error', e);
        resolve();
      });
  });
};
