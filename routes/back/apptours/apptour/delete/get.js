const fs = require('fs-extra');

const logger = rootRequire('lib/logger');

module.exports = function(req, res, next) {
  const apptour = res.locals.apptour;

  if (req.query.confirmed) {
    apptour
      .remove()
      .then(() => {
        return fs.remove(process.env.STORAGE_PATH + '/public/apptours/' + apptour._id);
      })
      .then(() => {
        logger.info('App Tour', apptour._id,
          'removed by user', req.user._id, req.user.email);
        res.redirect('/admin/apptours');
      })
      .catch(next);
  } else {
    res.render('back/pages/apptours/delete');
  }
};
