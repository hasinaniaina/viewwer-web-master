const _ = require('lodash');

const firebase = rootRequire('lib/firebase');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);
  body.house = (body.house === 'on');
  body.isSold = (body.isSold === 'on');
  body.isPublished = (body.isPublished === 'on');

  body.location = {
    type: 'Point',
    coordinates: [body.location_lng, body.location_lat]
  };
  delete body.location_lng;
  delete body.location_lat;

  let message;
  if (res.locals.apptour.locale) req.setLocale(res.locals.apptour.locale);

  if (res.locals.apptour.moderationStatus
    !== body.moderationStatus
    && body.moderationStatus === 'approved') {
    message = req.__('Your tour was approved!');
  }

  if (res.locals.apptour.moderationStatus
    !== body.moderationStatus
    && body.moderationStatus === 'rejected') {
    message = req.__('Your tour was rejected!');
  }

  res.locals.apptour
    .set(_.assign({}, body, {
      updatedBy: req.user._id
    }))
    .save()
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/apptours'
      });
      if (message) return firebase.send(
        res.locals.apptour.user,
        message,
        {
          notificationType: 'moderation',
          entityType: 'AppTour',
          entityId: res.locals.apptour._id,
          url: '/apptours/' + res.locals.apptour._id,
          moderationMessage: (body.moderationStatus === 'rejected') ? body.moderationMessage : ''
        }
      );
    })
    .catch(next);
};

