const mongoose = require('mongoose');
const User = mongoose.model('User');

const constants = require('../../constants');

module.exports = function (req, res, next) {
  User.findOne({_id: res.locals.apptour.user}).exec()
    .then(r => {
      res.render('back/pages/apptours/edit', {
        production: (process.env.NODE_ENV === 'production'),
        apartmentsTypes: Object.keys(constants.TYPES),
        saleTypes: Object.keys(constants.SALE_TYPES),
        user: r
      });
    })
    .catch(next);
};
