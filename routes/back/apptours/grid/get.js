const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'AppTour')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/apptours/list.njk', {
        apptours: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
