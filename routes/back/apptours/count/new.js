const mongoose = require('mongoose');
const AppTour = mongoose.model('AppTour');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

module.exports = function (req, res, next) {
  if (!req.user) return next(res.createError(403));
  AppTour.countDocuments(applyPermissions({moderationStatus: 'pending'}, req.user)).exec()
    .then(r => res.json({count: r}))
    .catch(next);
};
