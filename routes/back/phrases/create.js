const mongoose = require('mongoose');
const Phrase = mongoose.model('Phrase');

module.exports = function (req, res, next) {
  const phrase = new Phrase({name: req.body.name});

  phrase
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/phrases/' + phrase._id + '/edit'
      });
    })
    .catch(next);
};
