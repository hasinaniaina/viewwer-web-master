const _ = require('lodash');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  res.locals.phrase
    .set(body)
    .save()
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/phrases'
      });
    })
    .catch(next);
};
