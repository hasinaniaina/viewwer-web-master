module.exports = function (req, res, next) {
  const phrase = res.locals.phrase;

  if (req.query.confirmed) {
    phrase
      .remove()
      .then(() => {
        res.redirect('/admin/phrases');
      })
      .catch(next);
  } else {
    res.render('back/pages/phrases/delete');
  }
};
