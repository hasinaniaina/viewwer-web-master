const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function (req, res, next) {
  fetchGrid(req, 'Phrase')
    .then(grid => {
      grid.rows = nunjucksEnv.render('back/grids/phrases/list.njk', {
        phrases: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
