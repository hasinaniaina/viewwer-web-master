const mongoose = require('mongoose');
const User = mongoose.model('User');
const getObjectSignature = rootRequire('/lib/getObjectSignature');

module.exports = function (req, res, next) {
  let responseSent = false;

  User
    .findOne({
      email: req.query.email      
    })
    .exec()
    .then(user => {
      if (!user) {
        responseSent = true;
        return next(res.createError(404, req.__('Invalid user')));
      }

      if (!user.partner || user.partner.code != req.query.code) {
        responseSent = true;
        return next(res.createError(404, req.__('Invalid user to include in partner members')));
      }

      // generate digital signature
      let objToSign = {
        email : req.query.email,
        code: req.query.code
      };
      let signature = getObjectSignature(objToSign);

      if (req.query.key != signature) {
        responseSent = true;
        return next(res.createError(404, req.__('Invalid signature')));
      }

      return user
        .set('partnershipValidation', 'allowed')
        .save();
    })
    .then(() => {
      if (responseSent) return;
      res.render('back/pages/partners/member-allowed');
    })
    .catch(next);
};
