const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'Partner')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/partners/list.njk', {
        partners: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
