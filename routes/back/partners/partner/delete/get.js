module.exports = function(req, res, next) {
  const partner = res.locals.partner;

  if (req.query.confirmed) {
    partner
      .remove()
      .then(() => {
        res.redirect('/admin/partners');
      })
      .catch(next);
  } else {
    res.render('back/pages/partners/delete');
  }
};
