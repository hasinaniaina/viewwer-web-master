const _ = require('lodash');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);
  body.isEnabled = (body.isEnabled === 'on');

  res.locals.partner
    .set(body)
    .save()    
    .then(function() {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/partners'
      });
    })
    .catch(next);
};
