const mongoose = require('mongoose');
const Partner = mongoose.model('Partner');
const generate = require('nanoid/generate');
const crypto = require('crypto');

module.exports = function (req, res, next) {
  // chiffres et lettres en majuscules sans 0, 1, I, O pour éviter les codes ambigus
  const alphabet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
  const randomCode = generate(alphabet, 8); //=> "ARGMA43S"
  const key = crypto.randomBytes(16).toString('hex'); // unique key for export url

  const partner = new Partner({name: req.body.name, email: req.body.email, isEnabled: true, code: randomCode, key: key});

  partner
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/partners/' + partner._id + '/edit'
      });
    })
    .catch(next);
};
