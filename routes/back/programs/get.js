const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

const APARTMENTS_TYPES = require('../apartments/constants').TYPES;

module.exports = function (req, res, next) {
  Program.find(applyPermissions({}, req.user), {name: true}, {autopopulate: false})
    .sort({name: 1})
    .exec()
    .then(r => {
      res.render('back/pages/programs/list', {
        programs: r,
        apartmentsTypes: Object.keys(APARTMENTS_TYPES),
        grid: rootRequire('lib/grid/stubs').programs
      });
    })
    .catch(next);
};
