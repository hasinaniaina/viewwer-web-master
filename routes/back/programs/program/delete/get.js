const fs = require('fs-extra');

module.exports = function(req, res, next) {
  const program = res.locals.program;

  if (req.query.confirmed) {
    program
      .remove()
      .then(() => {
        return fs.remove(process.env.STORAGE_PATH + '/public/files/programs/' + program._id);
      })
      .then(() => {
        res.redirect('/admin/programs');
      })
      .catch(next);
  } else {
    res.render('back/pages/programs/delete');
  }
};
