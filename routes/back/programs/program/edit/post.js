const _ = require('lodash');
const mongoose = require('mongoose');

const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Tour = mongoose.model('Tour');
const savePermissions = rootRequire('routes/back/save-permissions');
const paths = rootRequire('models/helpers/paths');
const counters = rootRequire('models/helpers/counters');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);


  body.isPublished = (body.isPublished === 'on');
  const buildings = _.map(body.buildings, 'data').filter(v => v);
  
  body.location = {
    type: 'Point',
    coordinates: [body.location_lng, body.location_lat]
  };
  delete body.location_lng;
  delete body.location_lat;

  if (!body.decorationAndConveniences) {
    body.decorationAndConveniences = [];
  }

  savePermissions.onEdit(req, body);

  const oldDeveloper = _.get(res.locals.program, 'developer._id');

  res.locals.program
    .set(body)
    .save()
    .then(() => Building.clearProgram(res.locals.program._id))
    .then(() => Building.assignProgram(buildings, res.locals.program._id))
    .then(() => Tour.clearProgram(res.locals.program._id))
    .then(() => Tour.assignProgram(body.exterior, res.locals.program._id))
    .then(() => counters.updateDevelopersCounters(oldDeveloper))
    .then(() => counters.updateDevelopersCounters(body.developer))
    .then(() => Program.updatePath(res.locals.program._id))
    .then(() => paths.updateMarked())
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/programs'
      });
    })
    .catch(next);
};
