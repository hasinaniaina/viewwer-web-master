const _ = require('lodash');
const mongoose = require('mongoose');

const Developer = mongoose.model('Developer');
const Building = mongoose.model('Building');
const Tour = mongoose.model('Tour');
const ImageSet = mongoose.model('ImageSet');
const Phrase = mongoose.model('Phrase');

const User = mongoose.model('User');
const Group = mongoose.model('Group');
const applyPermissions = rootRequire('models/helpers/apply-permissions');
const roleDevelopers = rootRequire('models/helpers/role-developers');

const logo = require('../../logo');
const constants = require('../../constants');

module.exports = function (req, res, next) {
  Promise.all([
    Developer.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    Building.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    Tour.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    ImageSet.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Group.find({}).sort({name: 1}).exec(),
    Phrase.find({type: 'decoration'}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/programs/edit', {
        programsDevStatuses: Object.keys(constants.DEV_STATUSES),
        developers: results[0],
        buildings: results[1],
        tours: results[2],
        exterior: _.find(results[2], {program: {_id: res.locals.program._id}}),
        programBuildings: _.filter(results[1], {program: {_id: res.locals.program._id}}),
        imagesets: results[3],
        users: results[4],
        groups: results[5],
        phrases: results[6],
        decorationAndConveniences: _.map(res.locals.program.decorationAndConveniences, '_id'),
        logoProgramSettings: logo.LOGO_PROGRAM_SETTINGS
      });
    })
    .catch(next);
};
