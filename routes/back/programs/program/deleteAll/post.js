const mongoose = require('mongoose');
const Program = mongoose.model('Program');

module.exports = function(req, res, next) {
	var response = ""
  if (req.query.confirmed) {
	const programs_id = req.body.data
		if(programs_id){
			if(programs_id.length > 1){
				for (let ids of programs_id){
					Program.findByIdAndRemove(ids, function (err, task) {  
						response = {
						    message: "task successfully deleted",
						    id: ids
						};
					});
				}
			}else{
				Program.findByIdAndRemove(programs_id, function (err, task) {  
						response = {
						    message: "task successfully deleted",
						    id: programs_id
						};
					});	
			}
			res.send(response);
		}
  }
};
