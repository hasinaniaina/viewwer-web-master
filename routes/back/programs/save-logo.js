const logo = require('./logo');

module.exports = function (req, res, next) {
  const promises = [logo.saveImage(res.locals.program._id, req.body.image, logo.LOGO_PROGRAM)];

  if (req.body.imageOriginal) {
    promises.push(logo.saveImage(res.locals.program._id, req.body.imageOriginal, logo.LOGO_PROGRAM_ORIGINAL));
  }

  Promise.all(promises)
    .then(() => {
      res.json({
        status: 'success',
        name: logo.LOGO_PROGRAM,
        nameOriginal: logo.LOGO_PROGRAM_ORIGINAL
      });
    })
    .catch(next);
};
