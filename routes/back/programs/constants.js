module.exports.DEV_STATUSES = {
  preview: '_program_preview',
  commercial_launch: 'Commercial launch',
  commencement_works: 'Commencement of the works',
  work_in_progress: 'Work in progress',
  delivered: 'Delivered'
};
