const pdf = require('./pdf');

module.exports = function (req, res, next) {
  pdf.removePdf(res.locals.program._id, req.body.name)
    .then(() => {
      res.json({
        status: 'success'
      });
    })
    .catch(next);
};
