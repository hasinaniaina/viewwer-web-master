const fs = require('fs-extra');
const path = require('path');
const sanitize = require('sanitize-filename');

const FILES_PATH = '/public/files/programs/';
const PROGRAM_PDF = 'info.pdf';

module.exports.savePdf = function (program_id, base64_data, name) {
  if (!base64_data) throw new Error('no_data');
  base64_data = base64_data.replace(/^data:application\/pdf;base64,/, '');
  const p = path.join(process.env.STORAGE_PATH, FILES_PATH, program_id, name);
  return fs.outputFile(p, base64_data, 'base64');
};

module.exports.removePdf = function (program_id, name) {
  const filename = sanitize(name);
  if (!filename) throw new Error('wrong_filename');
  return fs.remove(path.join(process.env.STORAGE_PATH, FILES_PATH, program_id, filename));
};

module.exports.PROGRAM_PDF = PROGRAM_PDF;
