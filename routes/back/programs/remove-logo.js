const _ = require('lodash');
const logo = require('./logo');

module.exports = function (req, res, next) {
  Promise.all(_.map(req.body.files, f => logo.removeImage(res.locals.program._id, f)))
    .then(() => {
      res.json({
        status: 'success'
      });
    })
    .catch(next);
};
