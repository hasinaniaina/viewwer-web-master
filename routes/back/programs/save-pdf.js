const pdf = require('./pdf');

module.exports = function (req, res, next) {
  pdf.savePdf(res.locals.program._id, req.body.file, pdf.PROGRAM_PDF)
    .then(() => {
      res.json({
        status: 'success',
        name: pdf.PROGRAM_PDF
      });
    })
    .catch(next);
};
