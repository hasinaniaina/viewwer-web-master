const fs = require('fs-extra');
const path = require('path');
const sanitize = require('sanitize-filename');

const FILES_PATH = '/public/files/programs/';
const LOGO_PROGRAM = 'logo-program.png';
const LOGO_PROGRAM_ORIGINAL = 'logo-program-original.png';
const LOGO_PROGRAM_SETTINGS = {aspectRatio: 1, width: 128, height: 128, circle: true};

module.exports.saveImage = function (program_id, base64_data, name) {
  if (!base64_data) throw new Error('no_data');
  base64_data = base64_data.replace(/^data:image\/png;base64,/, '');
  const p = path.join(process.env.STORAGE_PATH, FILES_PATH, program_id, name);
  return fs.outputFile(p, base64_data, 'base64');
};

module.exports.removeImage = function (program_id, name) {
  const filename = sanitize(name);
  if (!filename) throw new Error('wrong_filename');
  return fs.remove(path.join(process.env.STORAGE_PATH, FILES_PATH, program_id, filename));
};

module.exports.LOGO_PROGRAM = LOGO_PROGRAM;
module.exports.LOGO_PROGRAM_ORIGINAL = LOGO_PROGRAM_ORIGINAL;
module.exports.LOGO_PROGRAM_SETTINGS = LOGO_PROGRAM_SETTINGS;
