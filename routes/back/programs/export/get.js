const _ = require('lodash');

const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');

const applyPermissions = rootRequire('models/helpers/apply-permissions');
const onlyPublished = rootRequire('models/helpers/only-published');

const excel = require('./excel');

module.exports = function (req, res, next) {
  if (!req.user) return next(res.createError(403));

  const body = _.cloneDeep(req.query);

  let programs = [];
  let buildings = [];
  let apartments = [];
  let tours = [];

  let apartmentsQuery = {};
  if (!_.isEmpty(body.types)) apartmentsQuery.type = {$in: body.types};
  if (body.sold) {
    if (body.sold === 'yes') {
      apartmentsQuery.isSold = true;
    } else {
      apartmentsQuery.isSold = {$ne: true};
    }
  }

  let programsQuery = {};
  if (!_.isEmpty(body.programs)) programsQuery._id = {$in: body.programs};

  Program
    .find(applyPermissions(onlyPublished(programsQuery), req.user))
    .sort({name: 1})
    .exec()
    .then(r => {
      programs = r;
      if (!_.isEmpty(programs)) {
        return Building
          .find(applyPermissions(
            onlyPublished({
              program: {
                $in: _.map(programs, '_id')
              }
            }), req.user))
          .exec();
      }
    })
    .then(r => {
      if (r) buildings = r;
      if (!_.isEmpty(buildings)) {
        return Apartment
          .find(applyPermissions(
            onlyPublished(_.assign({}, apartmentsQuery, {
              building: {
                $in: _.map(buildings, '_id')
              }
            })), req.user))
          .sort({name: 1})
          .exec();
      }
    })
    .then(r => {
      if (r) apartments = r;
      if (!_.isEmpty(apartments)) {
        return Tour
          .find(applyPermissions(
            onlyPublished({
              $or: [
                {apartment: {$in: _.map(apartments, '_id')}},
                {apartmentsAdditional: {$elemMatch: {$in: _.map(apartments, '_id')}}}
              ]
            }), req.user))
          .exec();
      }
    })
    .then(r => {
      if (r) tours = r;
      excel(req, res, programs, buildings, apartments, tours);
    })
    .catch(next);
};
