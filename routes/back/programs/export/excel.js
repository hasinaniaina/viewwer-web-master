const _ = require('lodash');
const Excel = require('exceljs');
const APARTMENTS_TYPES = require('../../apartments/constants').TYPES;

module.exports = function (req, res, programs, buildings, apartments, tours) {
  const workbook = new Excel.Workbook();
  const sheet = workbook.addWorksheet(req.__('Programs'));

  const columns = [];
  columns.push({header: req.__('Program Name'), key: 'program', width: 30});
  columns.push({header: req.__('Type'), key: 'type', width: 15});
  columns.push({header: req.__('Status'), key: 'sold', width: 15});
  columns.push({header: req.__('Lot Name'), key: 'lot', width: 30});
  columns.push({header: req.__('Floor'), key: 'floor', width: 15});
  columns.push({header: req.__('Area'), key: 'area', width: 15});
  columns.push({header: req.__('Tour link'), key: 'tour_link', width: 15});
  sheet.columns = columns;

  _.each(programs, p => {

    let host = 'https://' + process.env.DOMAIN;
    let alias = _.get(p, 'developer.alias');
    if (alias) host = 'https://' + alias + '.' + process.env.DOMAIN;

    _.each(buildings, b => {

      if (b.program._id !== p._id) return;

      const apts = _.filter(apartments, a => a.building._id === b._id);
      _.each(apts, a => {
        const row = {
          program: p.name,
          type: req.__(APARTMENTS_TYPES[a.type]) +
          ((a.duplex) ? ' ' + req.__('duplex') : '') +
          ((a.house) ? ' ' + _.lowerCase(req.__('House')) : ''),
          sold: (a.isSold) ? req.__('sold') : req.__('not sold'),
          lot: a.lot,
          floor: a.floor,
          area: a.area,
        };

        let t = _.find(tours, {apartment: {_id: a._id}});
        if (!t) t = _.find(tours, t => {
          return (_.find(t.apartmentsAdditional, {_id: a._id}));
        });

        if (t) {
          row.tour_link = {
            text: host + '/tours/' + t._id,
            hyperlink: host + '/tours/' + t._id
          };
        }

        sheet.addRow(row);
      });
    });
  });

  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.setHeader('Content-Disposition', 'attachment; filename=' + req.__('Programs') + '.xlsx');
  return workbook.xlsx.write(res);
};
