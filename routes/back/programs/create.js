const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const savePermissions = rootRequire('routes/back/save-permissions');

module.exports = function (req, res, next) {
  const program = new Program({name: req.body.name});

  savePermissions.onCreate(req, program);

  program
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/programs/' + program._id + '/edit'
      });
    })
    .catch(next);
};
