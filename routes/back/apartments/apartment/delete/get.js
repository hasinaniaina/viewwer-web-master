module.exports = function(req, res, next) {
  const apartment = res.locals.apartment;

  if (req.query.confirmed) {
    apartment
      .remove()
      .then(() => {
        res.redirect('/admin/apartments');
      })
      .catch(next);
  } else {
    res.render('back/pages/apartments/delete');
  }
};
