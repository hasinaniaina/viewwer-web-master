const _ = require('lodash');
const mongoose = require('mongoose');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');
const savePermissions = rootRequire('routes/back/save-permissions');
const paths = rootRequire('models/helpers/paths');
const counters = rootRequire('models/helpers/counters');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.isPublished = (body.isPublished === 'on');

  body.isSold = (body.isSold === 'on');
  if (res.locals.apartment.isSold !== body.isSold) {
    body.isSoldUpdatedAt = new Date().toISOString();
  }

  body.house = (body.house === 'on');
  body.duplex = (body.duplex === 'on');
  body.elevator = (body.elevator === 'on');
  body.garage = (body.garage === 'on');
  body.terrace = (body.terrace === 'on');
  body.garden = (body.garden === 'on');
  body.basement = (body.basement === 'on');
  body.parking = (body.parking === 'on');
  body.balcony = (body.balcony === 'on');

  savePermissions.onEdit(req, body);

  if (body.exteriorPolygons) {
    try {
      body.exteriorPolygons = JSON.parse(body.exteriorPolygons);
    } catch (e) {
      return next(new Error('Wrong polygons'));
    }
  } else {
    body.exteriorPolygons = [];
  }

  const oldDeveloper = _.get(res.locals.apartment, 'building.program.developer._id');

  res.locals.apartment
    .set(body)
    .save()
    .then(() => Tour.clearApartment(res.locals.apartment._id))
    .then(() => Tour.assignApartment(body.interior, res.locals.apartment._id))
    .then(() => counters.updateDevelopersCounters(oldDeveloper))
    .then(() => counters.updateDevelopersCountersForApartment(res.locals.apartment._id))
    .then(() => Apartment.updatePath(res.locals.apartment._id))
    .then(() => paths.updateMarked())
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/apartments'
      });
    })
    .catch(next);
};
