const _ = require('lodash');
const mongoose = require('mongoose');
const Building = mongoose.model('Building');
const Tour = mongoose.model('Tour');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const applyPermissions = rootRequire('models/helpers/apply-permissions');
const roleDevelopers = rootRequire('models/helpers/role-developers');

const constants = require('../../constants');

module.exports = function (req, res, next) {
  Promise.all([
    Building.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    Tour.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Group.find({}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/apartments/edit', {
        apartmentsTypes: Object.keys(constants.TYPES),
        saleTypes: Object.keys(constants.SALE_TYPES),
        buildings: results[0],
        tours: results[1],
        interior: _.find(results[1], {apartment: {_id: res.locals.apartment._id}}),
        users: results[2],
        groups: results[3]
      });
    })
    .catch(next);
};
