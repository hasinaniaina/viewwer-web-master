const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'Apartment')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/apartments/list.njk', {
        apartments: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
