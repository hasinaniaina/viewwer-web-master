const mongoose = require('mongoose');
const Apartment = mongoose.model('Apartment');
const savePermissions = rootRequire('routes/back/save-permissions');

module.exports = function (req, res, next) {
  const apartment = new Apartment({
    name: req.body.name,
    lot: req.body.name
  });

  savePermissions.onCreate(req, apartment);

  apartment
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/apartments/' + apartment._id + '/edit'
      });
    })
    .catch(next);
};
