module.exports.TYPES = {
  t01: 'T1',
  t02: 'T2',
  t03: 'T3',
  t04: 'T4',
  t05: 'T5',
  t06: 'T6',
  t07: 'T7'
};

module.exports.SALE_TYPES = {
  sale: 'For sale',
  rent: 'For rent'
};
