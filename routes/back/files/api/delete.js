const fs = require('fs-extra');

module.exports = function(req, res, next) {
  fs
    .remove(req.STORAGE_PATH)
    .then(function() {
      res.json({
        status: 'success'
      });
    })
    .catch(next);
};
