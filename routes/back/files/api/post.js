const fs = require('fs-extra');
const processMultipart = rootRequire('lib/process-multipart');

module.exports = function(req, res, next) {
  // "create a directory" request
  if (!req.headers['content-type']) return fs.ensureDir(req.STORAGE_PATH).then(() => res.json({ok: true})).catch(next);

  // "upload files" request
  processMultipart(req, {destination: req.STORAGE_PATH}).then(res.end).catch(next);
};
