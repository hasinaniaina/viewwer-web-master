const _ = require('lodash');
const fs = require('fs-extra');

module.exports = function(req, res, next) {
  fs
    .stat(req.STORAGE_PATH)
    .then(
      function(stat) {
        if (stat.isDirectory()) {
          const listed = fs.readdir(req.STORAGE_PATH);

          const collected = listed.then(function(resources) {
            return Promise.all(resources.map(resource => fs.stat(req.STORAGE_PATH + resource)));
          });

          return Promise
            .all([listed, collected])
            .then(function([resources, stats]) {
              const directory = _.map(stats, function(stat, idx) {
                return {
                  name: resources[idx],
                  size: stat.size,
                  isDirectory: stat.isDirectory()
                };
              });

              res.json({
                ok: true,
                directory: directory
              });
            });
        } else {
          return res.json({
            ok: true,
            file: {
              name: _.chain(req.url).split('/').last().value(),
              size: stat.size
            }
          });
        }
      },
      function(err) {
        if (err.code == 'ENOENT') return res.json({ok: false, reason: 404});

        return Promise.reject(err);
      })
    .catch(next);
};
