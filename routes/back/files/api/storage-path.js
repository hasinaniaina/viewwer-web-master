const path = require('path');
const _ = require('lodash');

const storageDirectory = path.normalize(process.env.STORAGE_PATH + '/public/files/');

module.exports = function(req, res, next) {
  const isDirectory = req.path.slice(-1) == '/';

  const url = isDirectory ? req.path : _.chain(req.path).split('/').map(function(value, idx, list) {
    if (idx == list.length - 1) value = _.deburr(value).replace(/[^a-zA-Z0-9\-.]/g, '_');

    return value;
  }).join('/').value();

  req.STORAGE_PATH = path.join(storageDirectory, url);

  // prevent from accessing anything above the storage directory
  if (req.STORAGE_PATH.indexOf(storageDirectory) !== 0) return next(res.createError(403));

  next();
};
