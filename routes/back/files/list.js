module.exports = function(req, res) {
  if (!req.query.path) return res.redirect(301, './files?path=.');

  res.render('back/pages/files/list');
};
