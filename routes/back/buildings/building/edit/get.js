const _ = require('lodash');
const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const Apartment = mongoose.model('Apartment');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const applyPermissions = rootRequire('models/helpers/apply-permissions');
const roleDevelopers = rootRequire('models/helpers/role-developers');

module.exports = function (req, res, next) {
  Promise.all([
    Program.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    Apartment.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Group.find({}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/buildings/edit', {
        programs: results[0],
        apartments: results[1],
        buildingApartments: _.filter(results[1], {building: {_id: res.locals.building._id}}),
        users: results[2],
        groups: results[3]
      });
    })
    .catch(next);
};
