const _ = require('lodash');
const mongoose = require('mongoose');
const Apartment = mongoose.model('Apartment');
const Building = mongoose.model('Building');
const savePermissions = rootRequire('routes/back/save-permissions');
const paths = rootRequire('models/helpers/paths');
const counters = rootRequire('models/helpers/counters');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.isPublished = (body.isPublished === 'on');
  const apartments = _.map(body.apartments, 'data').filter(v => v);

  savePermissions.onEdit(req, body);

  const oldDeveloper = _.get(res.locals.building, 'program.developer._id');

  res.locals.building
    .set(body)
    .save()
    .then(() => Apartment.clearBuilding(res.locals.building._id))
    .then(() => Apartment.assignBuilding(apartments, res.locals.building._id))
    .then(() => counters.updateDevelopersCounters(oldDeveloper))
    .then(() => counters.updateDevelopersCountersForBuilding(res.locals.building._id))
    .then(() => Building.updatePath(res.locals.building._id))
    .then(() => paths.updateMarked())
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/buildings'
      });
    })
    .catch(next);
};
