module.exports = function(req, res, next) {
  const building = res.locals.building;

  if (req.query.confirmed) {
    building
      .remove()
      .then(() => {
        res.redirect('/admin/buildings');
      })
      .catch(next);
  } else {
    res.render('back/pages/buildings/delete');
  }
};
