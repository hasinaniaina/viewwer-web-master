const mongoose = require('mongoose');
const Building = mongoose.model('Building');
const savePermissions = rootRequire('routes/back/save-permissions');

module.exports = function (req, res, next) {
  const building = new Building({name: req.body.name});

  savePermissions.onCreate(req, building);

  building
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/buildings/' + building._id + '/edit'
      });
    })
    .catch(next);
};
