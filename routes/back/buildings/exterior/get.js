const _ = require('lodash');
const mongoose = require('mongoose');
const Tour = mongoose.model('Tour');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

module.exports = function (req, res, next) {
  let program_id = _.get(res.locals.building, 'program._id');
  if (!program_id) return res.json({status: 'not_found'});

  Tour.findOne(applyPermissions({program: program_id}, req.user)).exec()
    .then(r => {
      if (r) {
        res.json(r);
      } else {
        res.json({status: 'not_found'});
      }
    })
    .catch(next);
};
