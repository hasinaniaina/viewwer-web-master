const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'Building')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/buildings/list.njk', {
        buildings: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
