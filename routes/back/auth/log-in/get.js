module.exports = function (req, res, next) {
  if (req.user) {
    if (req.query.back) return res.redirect(req.query.back);
    return next(res.createError(res.createError(404)));
  }
  
  var message = req.flash('message').join('<br />');  

  res.render('back/pages/auth/log-in', {
    message: message
  });
};
