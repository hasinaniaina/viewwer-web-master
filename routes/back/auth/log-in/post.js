const crypto = require('crypto');
const util = require('util');
const passport = require('passport');
const nunjucksEnv = rootRequire('/lib/nunjucks-env');
const email = rootRequire('/lib/email');

/**
 * Handles login requests.
 */
function handleRequest(req, res, next) {
  const redirect = req.body.back || '/';

  passport.authenticate('local', function(err, user, info) {
    if (!user) return next(new Error(req.__(info.message)));

    if (user.role === 'blocked') {
      return next(new Error(req.__('Blocked account')));
    }

    if (!user.forcePasswordChange) {
      req.logIn(user, function (err) {
        if (err) return next(err);

        if (req.xhr) {
          res.json({
            status: 'success',
            redirect: redirect
          });
        } else {
          res.redirect(redirect);
        }
      });
    } else {
      return util.promisify(crypto.randomBytes)(64)
        .then(function(key) {
          key = key.toString('hex');

          return user
            .set('passwordResetKey', key)
            .save()
            .then(function() {
              return key;
            });
        })
        .then(function(key) {
          let domain = process.env.DOMAIN;
          if (req.subdomains.length === 1) {
            domain = req.subdomains[0] + '.' + domain;
          }

          return email.send({
            from: 'no-reply@' + process.env.DOMAIN,
            to: user.email,
            subject: req.__('Reset password'),
            html: nunjucksEnv.render(
              'back/email/force-reset-password.njk',
              {
                domain: domain,
                key: key,
                __: req.__,
              }
            )
          });
        })
        .then(function() {
          res.json({
            status: 'success',
            message: req.__('Please check your email for further instructions')
          });
        });
    }
  })(req, res, next);
}

module.exports = handleRequest;
