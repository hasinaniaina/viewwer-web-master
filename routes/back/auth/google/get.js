const passport = require('passport');

module.exports = function (req, res, next) {
    passport.authenticate('google', { scope: ['email', 'profile'] })(req, res, next);
};
