const _ = require('lodash');

const zxcvbn = require('zxcvbn');

const {check, validationResult} = require('express-validator/check');

const mongoose = require('mongoose');
const User = mongoose.model('User');
const Partner = mongoose.model('Partner');

const sendVerifyEmail = rootRequire('/routes/back/auth/verify/send-verify-email');
const sendVerifyPartnershipEmail = rootRequire('/routes/back/auth/verify/send-verify-partnership');
const checkBadNames = rootRequire('/routes/back/auth/sign-up/check-bad-names');

module.exports = [[

  check('email')
    .escape()
    .isEmail()
    .withMessage('bad_request'),
  check('firstName')
    .escape()
    .isLength({ min: 6 })
    .withMessage('Wrong or reserved user name')
    .custom(value => checkBadNames(value))
    .withMessage('Wrong or reserved user name'),
  check('lastName')
    .escape()
    .isLength({ min: 3 })
    .withMessage('Wrong or reserved user name'),
  check('phone').escape()

], (req, res, next) => {

  const body = _.cloneDeep(req.body);

  const errors = validationResult(req); 

  if (body.password !== body.password_repeat) {
    return next(new Error(req.__('Your password and confirmation password do not match')));
  }

  if (zxcvbn(body.password).score < parseInt(process.env.PASSWORD_SCORE)) {
    return next(new Error(req.__('Your password is too weak')));
  }

  let user;

  Partner.findOne({ code : body.partnerCode, isEnabled: true })
  .then(partner => {

    if (body.partnerCode && !partner) return next(new Error(req.__('bad partner code')));

    return User
      .register({
        email: body.email,
        firstName: body.firstName,
        lastName: body.lastName,
        phone: body.phone,
        partner: partner
      }, body.password)
  })
  .then(r => {
    user = r;

    const message =
      '<p>' + req.__('Please check your email for account verification') + '</p>'
      + '<p>' + `<a href="/auth/log-in?back=${req.query.back}">` + req.__('Log in') + '</a>' + '</p>';

    res.json({
      status: 'success',
      message: message
    });
  })
  .then(() => sendVerifyEmail(user, req))
  .then(() => sendVerifyPartnershipEmail(user, req))
  .catch(next);
}];
