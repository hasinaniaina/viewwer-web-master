const _ = require('lodash');

const RESERVED = [
  'viewwer', 'viewer', 'support', 'admin'
];

module.exports = function (name) {
  return !(_.find(RESERVED, r => _.includes(_.toLower(name), r)));
};
