const passport = require('passport');
const flash = require('connect-flash');

module.exports = function (req, res, next) {
    passport.authenticate('facebook', function(err, user, info) {
        
        if (err) {
            // afficher un message d'erreur sur la page de login
            req.flash('message', err);
            return res.redirect('/auth/log-in');
        }
        
        if (!user) { return res.redirect('/auth/log-in'); }

        req.logIn(user, function(err) {
            if (err) { return next(err); }
            
            let back = req.query.back;
            res.redirect(back || '/');     
        });

    })(req, res, next);
};

