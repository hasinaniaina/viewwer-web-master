const passport = require('passport');

module.exports = function (req, res, next) {
    passport.authenticate('facebook', {scope: 'email'})(req, res, next);
};
