const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = function (req, res, next) {
  if (req.user) return next(res.createError(res.createError(404)));
  User
    .findOne({
      passwordResetKey: req.query.key
    })
    .exec()
    .then(function (user) {
      if (!user) return next(res.createError(404, req.__('Invalid password reset key')));

      res.render('back/pages/auth/reset-password');
    })
    .catch(next);
};
