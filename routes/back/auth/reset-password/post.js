const zxcvbn = require('zxcvbn');

const mongoose = require('mongoose');
const User = mongoose.model('User');
const Session = mongoose.model('Session');

module.exports = function(req, res, next) {
  User
    .findOne({
      passwordResetKey: req.body.key
    })
    .exec()
    .then(user => {
      if (!user) return next(res.createError(404, req.__('Invalid password reset key')));

      if (zxcvbn(req.body.password).score < parseInt(process.env.PASSWORD_SCORE)) {
        return next(new Error(req.__('Your password is too weak')));
      }

      return user
        .setPassword(req.body.password)
        .then(() => {
          return user
            .set('passwordResetKey', undefined)
            .set('forcePasswordChange', false)
            .save();
        })
        .then(() => {
          return Session
            .remove({
              'session.passport.user': user.email
            })
            .exec();
        })
        .then(() => {
          const message = '<a href="/auth/log-in?back=/im">' + req.__('Log in with new password') + '</a>';
          res.json({
            status: 'success',
            message: message
          });
        });
    })
    .catch(next);
};
