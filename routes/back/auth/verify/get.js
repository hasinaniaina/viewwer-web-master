const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = function (req, res, next) {
  let responseSent = false;

  User
    .findOne({
      verifyKey: req.query.key
    })
    .exec()
    .then(user => {
      if (!user) {
        responseSent = true;
        return next(res.createError(404, req.__('Invalid verification key')));
      }
      return user
        .set('verifyKey', undefined)
        .set('verified', true)
        .save();
    })
    .then(() => {
      if (responseSent) return;
      res.render('back/pages/auth/account-verification');
    })
    .catch(next);
};
