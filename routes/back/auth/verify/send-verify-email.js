const crypto = require('crypto');
const util = require('util');
const nunjucksEnv = rootRequire('/lib/nunjucks-env');
const email = rootRequire('/lib/email');

module.exports = function (user, req) {
  return new Promise((resolve, reject) => {
    util.promisify(crypto.randomBytes)(64)
      .then(key => {
        key = key.toString('hex');
        return user.set('verifyKey', key).save().then(() => key);
      })
      .then(key => email.send({
        from: 'no-reply@' + process.env.DOMAIN,
        to: user.email,
        subject: req.__('Account verification'),
        html: nunjucksEnv.render(
          'back/email/account-verification.njk',
          {
            key: key,
            __: req.__,
          }
        )
      }))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};
