const crypto = require('crypto');
const util = require('util');
const nunjucksEnv = rootRequire('/lib/nunjucks-env');
const email = rootRequire('/lib/email');
const getObjectSignature = rootRequire('/lib/getObjectSignature');

const mongoose = require('mongoose');
const Partner = mongoose.model('Partner');

module.exports = function (user, req) {
  return new Promise((resolve, reject) => {
    console.log('>>> send verify partnership, user.partner ?', user.partner);
    if (user.partner && user.partnershipValidation != "allowed") {
      user.set('partnershipValidation', 'in_validation').save()
      .then(() => {
        // full load (autopopulate don't load email and other fields)
        return Partner.findById(user.partner._id).exec()
      })
      .then((partner) => {
        email.send({
            from: 'no-reply@' + process.env.DOMAIN,
            to: partner.email,
            subject: req.__('Partner member validation'),
            html: nunjucksEnv.render(
              'back/email/partnership-member-validation.njk',
              { 
                email: user.email,
                code: user.partner.code,
                key: getObjectSignature({
                  email : user.email,
                  code: user.partner.code
                }),
                __: req.__,
              }
            )
        })
      })
      .then(r => resolve(r))
      .catch(e => reject(e));
    }
    else {
      resolve();
    }
  });
};
