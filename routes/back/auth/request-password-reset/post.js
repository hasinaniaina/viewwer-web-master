const crypto = require('crypto');
const util = require('util');
const nunjucksEnv = rootRequire('/lib/nunjucks-env');
const email = rootRequire('/lib/email');
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = function(req, res, next) {
  User
    .findOne({
      email: req.body.email.toLowerCase()
    })
    .exec()
    .then(function(user) {
      // create an error if the user is not found or if it's an external login (facebook, google...)
      if (!user || user.loginProvider) return next(res.createError(404, req.__('Unknown email address')));

      if (user.role === 'blocked') return next(new Error(req.__('Blocked account')));

      return util.promisify(crypto.randomBytes)(64)
        .then(function(key) {
          key = key.toString('hex');

          return user
            .set('passwordResetKey', key)
            .save()
            .then(function() {
              return key;
            });
        })
        .then(function(key) {
          let domain = process.env.DOMAIN;
          if (req.subdomains.length === 1) {
            domain = req.subdomains[0] + '.' + domain;
          }

          return email.send({
            from: 'no-reply@' + process.env.DOMAIN,
            to: user.email,
            subject: req.__('Reset password'),
            html: nunjucksEnv.render(
              'back/email/reset-password.njk',
              {
                domain: domain,
                key: key,
                __: req.__,
              }
            )
          });
        })
        .then(function() {
          res.json({
            status: 'success',
            message: req.__('Please check your email for further instructions')
          });
        });
    })
    .catch(next);
};
