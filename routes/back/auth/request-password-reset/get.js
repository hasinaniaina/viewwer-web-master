module.exports = function (req, res, next) {
  if (req.user) return next(res.createError(res.createError(404)));
  res.render('back/pages/auth/request-password-reset');
};
