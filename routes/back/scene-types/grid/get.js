const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'SceneType')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/scene-types/list.njk', {
        sceneTypes: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
