const _ = require('lodash');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.mobile = (body.mobile === 'on');

  res.locals.sceneType
    .set(body)
    .save()
    .then(function() {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/scene-types'
      });
    })
    .catch(next);
};
