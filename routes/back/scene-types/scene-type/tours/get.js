const _ = require('lodash');

const mongoose = require('mongoose');
const Tour = mongoose.model('Tour');
const AppTour = mongoose.model('AppTour');

module.exports = function (req, res, next) {
  const sceneType = res.locals.sceneType;

  Promise.all([
    Tour.find({}, {}, {autopopulate: false}),
    AppTour.find({}, {}, {autopopulate: false}),
  ])
    .then(results => {
      const toursIds = [];
      const apptoursIds = [];

      _.each(results[0], t => {
        _.each(t.scenes, s => {
          if (s.type === sceneType._id) {
            toursIds.push(t._id);
            return false;
          }
        });
      });

      _.each(results[1], at => {
        _.each(_.get(at, 'settings.sceneTypes', []), s => {
          if (s.type === sceneType._id) {
            apptoursIds.push(at._id);
            return false;
          }
        });
      });

      res.render('back/pages/scene-types/tours', {
        toursIds: toursIds,
        apptoursIds: apptoursIds
      });
    })
    .catch(next);
};
