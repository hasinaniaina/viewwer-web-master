const logger = rootRequire('lib/logger');

module.exports = function(req, res, next) {
  const sceneType = res.locals.sceneType;

  if (!res.locals.site.settings.allowSceneTypesRemoval) {
    return next(res.createError(403, req.__('Removal is forbidden in settings')));
  }

  if (req.query.confirmed) {
    sceneType
      .remove()
      .then(() => {
        logger.info('Scene Type', sceneType._id,
          'removed by user', req.user._id, req.user.email);
        res.redirect('/admin/scene-types');
      })
      .catch(next);
  } else {
    res.render('back/pages/scene-types/delete');
  }
};
