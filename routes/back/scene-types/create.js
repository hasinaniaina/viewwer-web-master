const mongoose = require('mongoose');
const SceneType = mongoose.model('SceneType');

module.exports = function (req, res, next) {
  const scene_type = new SceneType({
    name: req.body.name,
    locales: {
      en: req.body.name,
      fr: req.body.name
    }
  });

  scene_type
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/scene-types/' + scene_type._id + '/edit'
      });
    })
    .catch(next);
};
