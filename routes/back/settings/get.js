const os = require('os');

const _ = require('lodash');

const mongoose = require('mongoose');
const Setting = mongoose.model('Setting');

module.exports = function (req, res, next) {
  let settings;
  Setting
    .findOne({})
    .exec()
    .then(r => {
      settings = r;
      let memory = process.memoryUsage();
      for (let key in memory) {
        memory[key] = Math.round(memory[key] / 1024 / 1024 * 100) / 100;
      }
      res.render('back/pages/settings/edit.njk', {
        settings: settings,
        memory: memory,
        hostname: os.hostname(),
        loadavg: _.map(os.loadavg(), l => l.toFixed(2))
      });
    })
    .catch(next);
};
