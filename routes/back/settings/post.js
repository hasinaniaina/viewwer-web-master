const mongoose = require('mongoose');
const Setting = mongoose.model('Setting');

module.exports = function (req, res, next) {
  req.body.krpanoLicensed = (req.body.krpanoLicensed === 'on');
  req.body.logoTourEnabled = (req.body.logoTourEnabled === 'on');
  req.body.lambdaScenePreviews = (req.body.lambdaScenePreviews === 'on');
  req.body.lambdaTours = (req.body.lambdaTours === 'on');
  req.body.maintenanceMobileApp = (req.body.maintenanceMobileApp === 'on');
  req.body.notifyNewAppTours = (req.body.notifyNewAppTours === 'on');
  req.body.allowSceneTypesRemoval = (req.body.allowSceneTypesRemoval === 'on');
  req.body.allowLayerTypesRemoval = (req.body.allowLayerTypesRemoval === 'on');

  Setting
    .update({}, req.body, {upsert: true, overwrite: true})
    .exec()
    .then(() => res.json({
      status: 'success',
      goback: false,
      redirect: req.body.back || '/admin'
    }))
    .catch(next);
};
