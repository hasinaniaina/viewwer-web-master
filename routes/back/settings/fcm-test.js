const firebase = rootRequire('lib/firebase');

module.exports = function (req, res, next) {

  firebase.test(req.body.token)
    .then(() => res.json({status: 'success'}))
    .catch(next);

};
