const mongoose = require('mongoose');
const async = require('async');

const Developer = mongoose.model('Developer');

const REBUILD_LIMIT = 100;

module.exports = function (req, res, next) {
  Developer.find({}).distinct('_id').exec()
    .then(r => {
      return new Promise((resolve, reject) => {
        async.eachLimit(r, REBUILD_LIMIT, (id, cb) => {
          Promise.all([
            Developer.updateApartmentsCounter(id),
            Developer.updateProgramsCounter(id),
            Developer.updateRequestsCounter(id)
          ])
            .then(() => cb())
            .catch(e => cb(e));
        }, err => {
          if (err) return reject(err);
          resolve();
        });
      });
    })
    .then(() => res.json({status: 'success'}))
    .catch(next);
};
