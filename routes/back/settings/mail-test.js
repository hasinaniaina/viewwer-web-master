const email = rootRequire('lib/email');

module.exports = function (req, res, next) {

  email.send({
    from: 'no-reply@' + process.env.DOMAIN,
    to: req.body.email,
    subject: 'Test message',
    html: 'Test message'
  })
    .then(() => res.json({status: 'success'}))
    .catch(next);

};
