const _ = require('lodash');
const mongoose = require('mongoose');
const async = require('async');

const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');

const REBUILD_LIMIT = 100;

module.exports = function (req, res, next) {

  let docs = [];

  Promise.all([
    Program.find({}).distinct('_id').exec(),
    Building.find({}).distinct('_id').exec(),
    Apartment.find({}).distinct('_id').exec(),
    Tour.find({}).distinct('_id').exec(),
  ])
    .then(results => {
      _.each(results[0], r => {
        docs.push({model: 'Program', id: r});
      });

      _.each(results[1], r => {
        docs.push({model: 'Building', id: r});
      });

      _.each(results[2], r => {
        docs.push({model: 'Apartment', id: r});
      });

      _.each(results[3], r => {
        docs.push({model: 'Tour', id: r});
      });
    })
    .then(() => {
      return new Promise((resolve, reject) => {
        async.eachLimit(docs, REBUILD_LIMIT, (doc, cb) => {
          mongoose.model(doc.model).updatePath(doc.id)
            .then(() => cb())
            .catch(e => cb(e));
        }, err => {
          if (err) return reject(err);
          resolve();
        });
      });
    })
    .then(() => res.json({status: 'success'}))
    .catch(next);
};
