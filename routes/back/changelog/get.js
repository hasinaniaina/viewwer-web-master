const fs = require('fs');
const showdown  = require('showdown');

module.exports = function (req, res) {

  fs.readFile("./ChangeLog.md", "utf8", function(err, data) {
    if (err) {
      res.set('Content-Type', 'text/html');
      res.send("Impossible de générer le changelog !");
      console.log(err);
    }
    else {
      let converter = new showdown.Converter();
      
      let html = converter.makeHtml(data);

      res.render('front/pages/simple', {
        message: html
      });  
    }
  });
};
