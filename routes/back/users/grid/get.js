const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'User')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/users/list.njk', {
        users: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
