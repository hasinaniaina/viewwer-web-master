module.exports.ROLES = {
  admin: 'Administrator',
  editor: 'Editor',
  developer: 'Developer',
  user: 'User',
  blocked: 'Blocked'
};
