const _ = require('lodash');
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.forcePasswordChange = (body.forcePasswordChange === 'on');

  User
    .register(body, body.password)
    .then(function() {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/users'
      });
    })
    .catch(next);
};
