const _ = require('lodash');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);

  body.forcePasswordChange = (body.forcePasswordChange === 'on');

  if (body.password) {
    res.locals.user
      .set(body)
      .setPassword(body.password)
      .then(() => res.locals.user.save())
      .then(redirect)
      .catch(next);
  } else {
    res.locals.user
      .set(body)
      .save()
      .then(redirect)
      .catch(next);
  }

  function redirect() {
    res.json({
      status: 'success',
      goback: true,
      redirect: body.back || '/admin/users'
    });
  }
};
