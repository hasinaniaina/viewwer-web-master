const mongoose = require('mongoose');
const Group = mongoose.model('Group');
const Developer = mongoose.model('Developer');

const USERS_ROLES = require('../../constants').ROLES;

module.exports = function (req, res, next) {
  Promise.all([
    Group.find({}).sort({name: 1}).exec(),
    Developer.find({}).sort({name: 1}).exec(),
  ])
    .then(results => {
      res.render('back/pages/users/edit', {
        groups: results[0],
        developers: results[1],
        usersRoles: Object.keys(USERS_ROLES)
      });
    })
    .catch(next);
};
