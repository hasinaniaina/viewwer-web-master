module.exports = function(req, res, next) {
  const user = res.locals.user;

  /* do not delete default admin */
  if (user.email === process.env.ADMIN_EMAIL) return next(res.createError(403));

  if (req.query.confirmed) {
    user
      .remove()
      .then(() => {
        res.redirect('/admin/users');
      })
      .catch(next);
  } else {
    res.render('back/pages/users/delete');
  }
};
