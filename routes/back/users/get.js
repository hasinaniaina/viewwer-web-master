module.exports = function(req, res) {
  res.render('back/pages/users/list', {
    usersGrid: require('../../../lib/grid/stubs').users
  });
};
