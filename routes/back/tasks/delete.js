const mongoose = require('mongoose');
const Task = mongoose.model('Task');

module.exports = function (req, res, next) {
  Task.remove({}).exec()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/tasks/'
      });
    })
    .catch(next);
};
