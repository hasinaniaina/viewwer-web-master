const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = function (req, res, next) {
  User.findOne({_id: res.locals.task.user}).exec()
    .then(r => {
      res.render('back/pages/tasks/edit', {
        user: r
      });
    })
    .catch(next);
};
