const awstask = rootRequire('lib/aws/aws-task');

module.exports = function(req, res, next) {
  const task = res.locals.task;

  if (req.query.confirmed) {
    awstask.removeTask(task._id)
      .then(() => task.remove())
      .then(() => {
        res.redirect('/admin/tasks');
      })
      .catch(next);
  } else {
    res.render('back/pages/tasks/delete');
  }
};
