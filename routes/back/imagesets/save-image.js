const image = require('./image');

module.exports = function (req, res, next) {
  image.saveImage(res.locals.imageset._id, req.body.image, req.body.name)
    .then(r => {
      res.json({
        status: 'success',
        name: r.name
      });
    })
    .catch(next);
};
