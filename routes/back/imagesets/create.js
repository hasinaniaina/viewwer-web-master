const mongoose = require('mongoose');
const ImageSet = mongoose.model('ImageSet');
const savePermissions = rootRequire('routes/back/save-permissions');

module.exports = function (req, res, next) {
  const imageset = new ImageSet({name: req.body.name});

  savePermissions.onCreate(req, imageset);

  imageset
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/imagesets/' + imageset._id + '/edit'
      });
    })
    .catch(next);
};
