const image = require('./image');

module.exports = function (req, res, next) {
  image.removeImage(res.locals.imageset._id, req.body.files[0])
    .then(() => {
      res.json({
        status: 'success'
      });
    })
    .catch(next);
};
