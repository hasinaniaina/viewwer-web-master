const fs = require('fs-extra');
const path = require('path');
const shortid = require('shortid');
const sanitize = require('sanitize-filename');

const IMAGESETS_PATH = '/public/files/imagesets/';
const SLIDER_SETTINGS = {aspectRatio: 1.778, width: 1170, height: 658};

module.exports.saveImage = function (imageset_id, base64_data, name) {
  if (!base64_data) throw new Error('no_data');

  return new Promise((resolve, reject) => {
    let format = 'png';
    if (base64_data.search(/data:image\/jpeg/) >= 0) format = 'jpg';

    base64_data = base64_data.replace(/^(data:image\/png;base64,|data:image\/jpeg;base64,)/, '');

    const buf = new Buffer(base64_data, 'base64');

    let filename = name;
    if (!filename) {
      filename = shortid.generate() + '.' + format;
    } else {
      filename = path.basename(filename, path.extname(filename)) + '.' + format;
    }

    const p = path.join(process.env.STORAGE_PATH, IMAGESETS_PATH, imageset_id, filename);
    fs.outputFile(p, buf)
      .then(() => resolve({name: filename}))
      .catch(e => reject(e));
  });
};

module.exports.removeImage = function (imageset_id, name) {
  const filename = sanitize(name);
  if (!filename) throw new Error('wrong_filename');
  return fs.remove(path.join(process.env.STORAGE_PATH, IMAGESETS_PATH, imageset_id, filename));
};

module.exports.removeImages = function (imageset_id) {
  return fs.remove(path.join(process.env.STORAGE_PATH, IMAGESETS_PATH, imageset_id));
};

module.exports.SLIDER_SETTINGS = SLIDER_SETTINGS;
