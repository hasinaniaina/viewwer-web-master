const image = require('../../image');

module.exports = function(req, res, next) {
  const imageset = res.locals.imageset;

  if (req.query.confirmed) {
    imageset
      .remove()
      .then(() => image.removeImages(imageset._id))
      .then(() => res.redirect('/admin/imagesets'))
      .catch(next);
  } else {
    res.render('back/pages/imagesets/delete');
  }
};
