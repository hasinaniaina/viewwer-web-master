const mongoose = require('mongoose');
const Developer = mongoose.model('Program');
const Program = mongoose.model('Program');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const applyPermissions = rootRequire('models/helpers/apply-permissions');
const roleDevelopers = rootRequire('models/helpers/role-developers');
const image = require('../../image');

module.exports = function (req, res, next) {

  Promise.all([
    Developer.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    Program.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Group.find({}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/imagesets/edit', {
        developers: results[0],
        programs: results[1],
        users: results[2],
        groups: results[3],
        sliderSettings: image.SLIDER_SETTINGS
      });
    })
    .catch(next);

};
