const _ = require('lodash');
const savePermissions = rootRequire('routes/back/save-permissions');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.images = body.images.filter(v => v);

  savePermissions.onEdit(req, body);

  res.locals.imageset
    .set(body)
    .save()
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/imagesets'
      });
    })
    .catch(next);
};
