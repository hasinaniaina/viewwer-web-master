const mongoose = require('mongoose');
const LayerType = mongoose.model('LayerType');

module.exports = function (req, res, next) {
  const layer_type = new LayerType({name: req.body.name});

  layer_type
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/layer-types/' + layer_type._id + '/edit'
      });
    })
    .catch(next);
};
