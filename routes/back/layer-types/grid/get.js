const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'LayerType')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/layer-types/list.njk', {
        layerTypes: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
