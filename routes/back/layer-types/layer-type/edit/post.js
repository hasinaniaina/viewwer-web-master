const _ = require('lodash');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.isBaseLayer = (body.isBaseLayer === 'on');

  res.locals.layerType
    .set(body)
    .save()
    .then(function() {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/layer-types'
      });
    })
    .catch(next);
};
