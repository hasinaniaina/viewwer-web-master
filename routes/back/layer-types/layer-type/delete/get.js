const logger = rootRequire('lib/logger');

module.exports = function(req, res, next) {
  const layerType = res.locals.layerType;

  if (!res.locals.site.settings.allowLayerTypesRemoval) {
    return next(res.createError(403, req.__('Removal is forbidden in settings')));
  }

  if (req.query.confirmed) {
    layerType
      .remove()
      .then(() => {
        logger.info('Layer Type', layerType._id,
          'removed by user', req.user._id, req.user.email);
        res.redirect('/admin/layer-types');
      })
      .catch(next);
  } else {
    res.render('back/pages/layer-types/delete');
  }
};
