module.exports = function(req, res, next) {
  const group = res.locals.group;

  if (req.query.confirmed) {
    group
      .remove()
      .then(() => {
        res.redirect('/admin/groups');
      })
      .catch(next);
  } else {
    res.render('back/pages/groups/delete');
  }
};
