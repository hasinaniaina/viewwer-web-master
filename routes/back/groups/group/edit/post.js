const _ = require('lodash');
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  const users = _.map(body.users, 'data').filter(v => v);

  res.locals.group
    .set(body)
    .save()
    .then(() => User.clearGroup(res.locals.group._id))
    .then(() => User.assignGroup(users, res.locals.group._id))
    .then(function() {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/groups'
      });
    })
    .catch(next);
};
