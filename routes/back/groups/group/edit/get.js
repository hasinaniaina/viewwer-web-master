const _ = require('lodash');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Developer = mongoose.model('Developer');
const roleDevelopers = rootRequire('models/helpers/role-developers');

module.exports = function (req, res, next) {

  Promise.all([
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Developer.find({}).sort({name: 1}).exec(),
  ])
    .then(results => {
      res.render('back/pages/groups/edit', {
        users: results[0],
        groupUsers: _.filter(results[0], u => {
          return (u.group && res.locals.group && u.group._id === res.locals.group._id);
        }),
        developers: results[1]
      });
    })
    .catch(next);
};
