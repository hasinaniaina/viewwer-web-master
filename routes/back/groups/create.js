const mongoose = require('mongoose');
const Group = mongoose.model('Group');

module.exports = function (req, res, next) {
  const group = new Group({name: req.body.name});

  group
    .save()
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/groups/' + group._id + '/edit'
      });
    })
    .catch(next);
};
