const _ = require('lodash');

const mongoose = require('mongoose');
const Developer = mongoose.model('Developer');
const savePermissions = rootRequire('routes/back/save-permissions');

const alias = require('./alias');

module.exports = function (req, res, next) {
  const body = _.cloneDeep(req.body);

  const developer = new Developer({name: body.name});
  const dns = alias.format(body.name);

  savePermissions.onCreate(req, developer);

  developer
    .save()
    .then(() => {
      if (alias.test(dns)) return Developer.findOne({alias: dns});
    })
    .then(r => {
      if (r) return;
      if (alias.test(dns)) return developer.set({alias: dns}).save();
    })
    .then(() => {
      res.json({
        status: 'success',
        redirect: '/admin/developers/' + developer._id + '/edit'
      });
    })
    .catch(next);
};
