const _ = require('lodash');
const slug = require('slug');

module.exports.test = function (name) {
  return (/^[a-zA-Z0-9][a-zA-Z0-9]{0,61}$/.test(name));
};

module.exports.format = function (name) {
  return _.lowerCase(slug(name)).replace(/\s/g, '');
};
