const logo = require('./logo');

module.exports = function (req, res, next) {
  const promises = [logo.saveImage(res.locals.developer._id, req.body.image, logo.LOGO_DEVELOPER)];

  if (req.body.imageOriginal) {
    promises.push(logo.saveImage(res.locals.developer._id, req.body.imageOriginal, logo.LOGO_DEVELOPER_ORIGINAL));
  }

  Promise.all(promises)
    .then(() => {
      res.json({
        status: 'success',
        name: logo.LOGO_DEVELOPER,
        nameOriginal: logo.LOGO_DEVELOPER_ORIGINAL
      });
    })
    .catch(next);
};
