const fs = require('fs-extra');
const path = require('path');
const sanitize = require('sanitize-filename');

const FILES_PATH = '/public/files/developers/';
const LOGO_DEVELOPER = 'logo-avatar.png';
const LOGO_DEVELOPER_ORIGINAL = 'logo-avatar-original.png';
const LOGO_DEVELOPER_SETTINGS = {aspectRatio: 1, width: 128, height: 128, circle: true};

module.exports.saveImage = function (developer_id, base64_data, name) {
  if (!base64_data) throw new Error('no_data');
  base64_data = base64_data.replace(/^data:image\/png;base64,/, '');
  const p = path.join(process.env.STORAGE_PATH, FILES_PATH, developer_id, name);
  return fs.outputFile(p, base64_data, 'base64');
};

module.exports.removeImage = function (developer_id, name) {
  const filename = sanitize(name);
  if (!filename) throw new Error('wrong_filename');
  return fs.remove(path.join(process.env.STORAGE_PATH, FILES_PATH, developer_id, filename));
};

module.exports.LOGO_DEVELOPER = LOGO_DEVELOPER;
module.exports.LOGO_DEVELOPER_ORIGINAL = LOGO_DEVELOPER_ORIGINAL;
module.exports.LOGO_DEVELOPER_SETTINGS = LOGO_DEVELOPER_SETTINGS;
