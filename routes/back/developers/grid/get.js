const fetchGrid = rootRequire('lib/grid/fetch');
const nunjucksEnv = rootRequire('lib/nunjucks-env');

module.exports = function(req, res, next) {
  fetchGrid(req, 'Developer')
    .then(function(grid) {
      grid.rows = nunjucksEnv.render('back/grids/developers/list.njk', {
        userRole: req.user.role,
        developers: grid.rows,
        __: req.__
      });

      res.json(grid);
    })
    .catch(next);
};
