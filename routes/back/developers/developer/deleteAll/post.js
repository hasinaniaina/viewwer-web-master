const mongoose = require('mongoose');
const Developer = mongoose.model('Developer');

module.exports = function(req, res, next) {
	var response = "";
	if (req.query.confirmed) {
		const developers_id = req.body.data
		if(developers_id){
			if(developers_id.length > 1){
				for (let ids of developers_id){
					Developer.findByIdAndRemove(ids, function (err, task) {  
						response = {
						    message: "task successfully deleted",
						    id: ids
						};
					});
				}
			}else{
				Developer.findByIdAndRemove(developers_id, function (err, task) {  
						response = {
						    message: "task successfully deleted",
						    id: developers_id
						};
				});		
			}
			
			res.send(response);
		}
  	}
};
