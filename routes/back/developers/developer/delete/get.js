module.exports = function(req, res, next) {
  const developer = res.locals.developer;
  if (req.query.confirmed) {
    developer
      .remove()
      .then(() => {
        res.redirect('/admin/developers');
      })
      .catch(next);
  } else {
    res.render('back/pages/developers/delete');
  }
};
