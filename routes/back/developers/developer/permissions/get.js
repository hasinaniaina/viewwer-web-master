const mongoose = require('mongoose');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const roleDevelopers = rootRequire('models/helpers/role-developers');

module.exports = function (req, res, next) {
  if (req.user.role !== 'admin' &&
      req.user.role !== 'editor')
    return next(res.createError(403));

  Promise.all([
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Group.find({}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/developers/permissions', {
        users: results[0],
        groups: results[1]
      });
    })
    .catch(next);
};
