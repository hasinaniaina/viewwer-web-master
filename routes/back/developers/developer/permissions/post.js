const _ = require('lodash');

const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');
const ImageSet = mongoose.model('ImageSet');

module.exports = function (req, res, next) {
  if (req.user.role !== 'admin' &&
    req.user.role !== 'editor')
    return next(res.createError(403));

  const body = _.cloneDeep(req.body);

  const permissions = {
    user: body.user,
    group: body.group
  };

  let programs = [];
  let buildings = [];
  let apartments = [];
  let interiors = [];
  let exteriors = [];
  let imagesets = [];

  res.locals.developer
    .set(permissions)
    .save()
    .then(() => Program.find({developer: res.locals.developer._id}, {}, {autopopulate: false}).exec())
    .then(r => {
      programs = r;
      if (!_.isEmpty(programs)) {
        return Building.find({program: {$in: _.map(programs, '_id')}}, {}, {autopopulate: false}).exec();
      }
    })
    .then(r => {
      if (r) buildings = r;
      if (!_.isEmpty(buildings)) {
        return Apartment.find({building: {$in: _.map(buildings, '_id')}}, {}, {autopopulate: false}).exec();
      }
    })
    .then(r => {
      if (r) apartments = r;
      if (!_.isEmpty(apartments)) {
        return Tour.find({apartment: {$in: _.map(apartments, '_id')}}, {}, {autopopulate: false}).exec();
      }
    })
    .then(r => {
      if (r) interiors = r;
      if (!_.isEmpty(programs)) {
        return Tour.find({program: {$in: _.map(programs, '_id')}}, {}, {autopopulate: false}).exec();
      }
    })
    .then(r => {
      if (r) exteriors = r;
      _.each(programs, p => {
        if (p.slider) imagesets.push(p.slider);
      });
      if (res.locals.developer.slider &&
        res.locals.developer.slider._id) imagesets.push(res.locals.developer.slider._id);
      imagesets = _.uniq(imagesets);
    })
    .then(() => {
      const models = [
        {model: Program, ids: _.map(programs, '_id')},
        {model: Building, ids: _.map(buildings, '_id')},
        {model: Apartment, ids: _.map(apartments, '_id')},
        {model: Tour, ids: _.map(interiors, '_id')},
        {model: Tour, ids: _.map(exteriors, '_id')},
        {model: ImageSet, ids: imagesets},
      ];
      const promises = [];
      _.each(models, m => {
        if (!_.isEmpty(m.ids))
          promises.push(m.model.updateMany({_id: {$in: m.ids}}, {$set: permissions}).exec());
      });
      if (_.isEmpty(promises)) return;
      return Promise.all(promises);
    })
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/developers'
      });
    })
    .catch(next);
};
