const _ = require('lodash');

const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const savePermissions = rootRequire('routes/back/save-permissions');
const paths = rootRequire('models/helpers/paths');
const counters = rootRequire('models/helpers/counters');

const alias = require('../../alias');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  body.isPublished = (body.isPublished === 'on');
  const programs = _.map(body.programs, 'data').filter(v => v);

  if (!body.alias) {
    body.alias = undefined;
  } else {
    body.alias = alias.format(body.alias);
  }

  body.requestGlasses = (body.requestGlasses === 'on');

  savePermissions.onEdit(req, body);

  res.locals.developer
    .set(body)
    .save()
    .then(() => Program.clearDeveloper(res.locals.developer._id))
    .then(() => Program.assignDeveloper(programs, res.locals.developer._id))
    .then(() => counters.updateDevelopersCounters(res.locals.developer._id))
    .then(() => paths.updateMarked())
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/developers'
      });
    })
    .catch(next);
};
