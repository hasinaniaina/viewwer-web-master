const _ = require('lodash');
const mongoose = require('mongoose');
const Program = mongoose.model('Program');
const ImageSet = mongoose.model('ImageSet');
const User = mongoose.model('User');
const Group = mongoose.model('Group');
const applyPermissions = rootRequire('models/helpers/apply-permissions');
const roleDevelopers = rootRequire('models/helpers/role-developers');
const logo = require('../../logo');

module.exports = function (req, res, next) {

  Promise.all([
    Program.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    ImageSet.find(applyPermissions({}, req.user)).sort({name: 1}).exec(),
    User.find(roleDevelopers({})).sort({name: 1}).exec(),
    Group.find({}).sort({name: 1}).exec()
  ])
    .then(results => {
      res.render('back/pages/developers/edit', {
        programs: results[0],
        developerPrograms: _.filter(results[0], {developer: {_id: res.locals.developer._id}}),
        imagesets: results[1],
        users: results[2],
        groups: results[3],
        logoAvatarSettings: logo.LOGO_DEVELOPER_SETTINGS
      });
    })
    .catch(next);

};
