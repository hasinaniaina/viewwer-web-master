const _ = require('lodash');
const mongoose = require('mongoose');

const Request = mongoose.model('Request');
const User = mongoose.model('User');

const email = rootRequire('lib/email');
const nunjucksEnv = rootRequire('/lib/nunjucks-env');

const logger = rootRequire('lib/logger');

module.exports.send = function (req, request_id, developer_id) {
  return new Promise((resolve, reject) => {
    let to = [];
    User.find({developer: developer_id}).exec()
      .then(users => {
        to = _.uniq(_.map(users, 'email'));
        logger.info('Request', request_id, 'notification recipients', to);
        if (_.isEmpty(to)) return;
        return email.send({
          from: 'no-reply@' + process.env.DOMAIN,
          to: to,
          subject: req.__('You have new request'),
          html: nunjucksEnv.render(
            'back/email/request.njk',
            {
              request_id: request_id,
              __: req.__
            }
          )
        });
      })
      .then(() => {
        if (_.isEmpty(to)) return;
        return Request.updateOne({_id: request_id},
          {
            notificationsEmailsSent: true,
            notificationsEmails: to
          });
      })
      .then(() => resolve())
      .catch(e => {
        logger.error('Request', request_id, 'notification error', e);
        reject(e);
      });
  });
};
