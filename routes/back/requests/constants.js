module.exports.TYPES = {
  apartment: 'Apartment',
  info: 'Info',
  glasses: 'Glasses'
};

module.exports.TIMEFRAMES = {
  'now': 'Now',
  '3-6': '3 to 6 months',
  '6-12': '6 to 12 months',
  'next-year': 'Next year'
};
