const _ = require('lodash');

module.exports = function(req, res, next) {
  const body = _.cloneDeep(req.body);

  res.locals.request
    .set({
      notes: body.notes,
      status: body.status,
      updatedBy: req.user._id
    })
    .save()
    .then(() => {
      res.json({
        status: 'success',
        goback: true,
        redirect: body.back || '/admin/requests'
      });
    })
    .catch(next);
};
