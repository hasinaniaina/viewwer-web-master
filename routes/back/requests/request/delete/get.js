module.exports = function(req, res, next) {
  const request = res.locals.request;

  if (req.query.confirmed) {
    request
      .remove()
      .then(() => {
        res.redirect('/admin/requests');
      })
      .catch(next);
  } else {
    res.render('back/pages/requests/delete');
  }
};
