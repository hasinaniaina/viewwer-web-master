const mongoose = require('mongoose');
const Request = mongoose.model('Request');

module.exports = function(req, res, next) {
	var response = "" 
  	if (req.query.confirmed) {
    	const requests_id = req.body.data
		if(requests_id){
			if(requests_id.length > 1){
				for (let ids of requests_id){
					Request.findByIdAndRemove(ids, function (err, task) {  
						response = {
						    message: "task successfully deleted",
						    id: ids
						};
					});
				}
			}else{
				Request.findByIdAndRemove(requests_id, function (err, task) {  
						response = {
						    message: "task successfully deleted",
						    id: requests_id
						};
					});		
			}
			res.send(response);
		}

    }
};