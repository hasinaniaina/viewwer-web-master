const _ = require('lodash');
const Excel = require('exceljs');
const moment = require('moment-timezone');
const requests = require('../constants');

const TIMEZONE = 'Europe/Paris';

const logger = rootRequire('lib/logger');

module.exports = function (req, res, data) {
  const host = req.protocol + '://' + req.get('host');
  moment.locale(req.locale);
  moment.tz.setDefault(TIMEZONE);

  const workbook = new Excel.Workbook();
  const sheet = workbook.addWorksheet(req.__('Requests'));
  const columns = [];

  columns.push({header: req.__('View'), key: 'view', width: 5});
  columns.push({header: req.__('Date'), key: 'date', width: 15});
  columns.push({header: req.__('Developer'), key: 'developer', width: 20});
  columns.push({header: req.__('Type'), key: 'type', width: 10});
  columns.push({header: req.__('First Name'), key: 'firstName', width: 15});
  columns.push({header: req.__('Last Name'), key: 'lastName', width: 15});
  columns.push({header: req.__('Email'), key: 'email', width: 15});
  columns.push({header: req.__('Phone'), key: 'phone', width: 15});
  columns.push({header: req.__('Message'), key: 'message', width: 20});
  columns.push({header: req.__('Url'), key: 'referrer', width: 5});
  columns.push({header: req.__('Budget'), key: 'budget', width: 15});
  columns.push({header: req.__('Timeframe'), key: 'timeframe', width: 20});
  columns.push({header: req.__('Address'), key: 'address', width: 20});
  columns.push({header: req.__('Postal code'), key: 'postalCode', width: 20});
  columns.push({header: req.__('City'), key: 'city', width: 15});

  sheet.columns = columns;
  _.each(data, r => {
    const row = {};
    try {
      row.view = {
        text: req.__('Url'),
        hyperlink: host + '/admin/requests/' + r._id + '/edit'
      };
      row.date = moment(r.createdAt).format('lll');
      row.developer = (r.developer) ? r.developer.name : '';
      row.type = req.__(requests.TYPES[r.type]);
      row.firstName = r.firstName;
      row.lastName = r.lastName;
      row.email = r.email;
      row.phone = r.phone;
      row.message = r.message;
      row.referrer = {
        text: req.__('Url'),
        hyperlink: r.url
      };
      row.budget = getBudget(r.budgetFrom, r.budgetTo);
      row.timeframe = req.__(requests.TIMEFRAMES[r.timeframe]);
      row.address = r.address;
      row.postalCode = r.postalCode;
      row.city = r.city;
    } catch (e) {
      logger.error('Excel export error', e);
    }
    sheet.addRow(row);
  });
  res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  res.setHeader('Content-Disposition', 'attachment; filename=' + req.__('Requests') + '.xlsx');
  return workbook.xlsx.write(res);
};

function getBudget(budget_from, budget_to) {
  let b = '';
  if (budget_from) b += budget_from;
  if (budget_to) {
    if (b) b += ' - ';
    b += budget_to;
  }
  return b;
}
