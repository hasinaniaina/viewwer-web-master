const mongoose = require('mongoose');

const Request = mongoose.model('Request');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

const excel = require('./excel');

module.exports = function (req, res, next) {
  if (!req.user) return next(res.createError(403));
  Request.find(applyPermissions({status: 'new'}, req.user)).sort({createdAt: -1}).exec()
    .then(r => excel(req, res, r))
    .catch(next);
};
