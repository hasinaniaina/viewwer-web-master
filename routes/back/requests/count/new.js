const mongoose = require('mongoose');
const Request = mongoose.model('Request');
const applyPermissions = rootRequire('models/helpers/apply-permissions');

module.exports = function (req, res, next) {
  if (!req.user) return next(res.createError(403));
  Request.countDocuments(applyPermissions({status: 'new'}, req.user)).exec()
    .then(r => res.json({count: r}))
    .catch(next);
};
