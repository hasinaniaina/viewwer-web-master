const cookieParser = require('cookie-parser');

/**
 * Enables cookies support.
 */
function enableSessions() {
  return cookieParser();
}

module.exports = enableSessions();
