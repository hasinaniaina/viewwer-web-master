const express = require('express');

/**
 * Serves static files from the public storage directory.
 */
function serveStorage() {
  return express.static(process.env.STORAGE_PATH + '/public');
}

module.exports = serveStorage();
