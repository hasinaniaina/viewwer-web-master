/**
 * Triggers 404 error if no routes have been matched.
 */
function trigger404(req, res, next) {
  return next(res.createError(404));
}

module.exports = trigger404;
