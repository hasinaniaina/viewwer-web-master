const express = require('express');

function serveFavicons() {
  return express.static('public/favicons');
}

module.exports = serveFavicons();
