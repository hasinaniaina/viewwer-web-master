function checkRole(req, res, next) {
  if (req.user.role === 'user'
    && req.path.startsWith('/stats')) return next(res.createError(404));
  next();
}

module.exports = checkRole;
