const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const mongoose = require('mongoose');
const User = mongoose.model('User');

/**
 * Setups passport module to handle user auth.
 */
function enablePassport() {
  let baseUrl = process.env.BASE_URL;

  // setup login with local strategy (mongodb)
  passport.use(User.createStrategy());

  // setup login with Facebook
  passport.use(new FacebookStrategy({
    clientID: "698835517237289",
    clientSecret: "6ffbc68e50e52c8ed237eeca95f9bcef",
    callbackURL: baseUrl + "/auth/facebook/callback",
    profileFields: ['id', 'email', 'displayName']
  },
    function (accessToken, refreshToken, profile, done) {
      let fbEmail = profile.emails[0].value;

      User.findOne({ email : fbEmail }, function (err, user) {
        if (err) {          
          done(err);
        }
        else {
          if (user) done(null, user);
          else {          
            let newUser = new User({
              email: fbEmail,
              name: profile.displayName,
              loginProvider: 'facebook',
              providerDisplayName: 'Facebook',
              providerKey: profile.id
            });
            newUser.save(function(err) {
              if (err) {                
                done("Impossible de créer le compte.<br />L'adresse email " + fbEmail + "<br/>existe peut être déjà.");
              }
              else {
                done(null, newUser);
              }
            });
          }
        }
      });
    }
  ));

  // Use the GoogleStrategy within Passport.
  //   Strategies in passport require a `verify` function, which accept
  //   credentials (in this case, a token, tokenSecret, and Google profile), and
  //   invoke a callback with a user object.
  passport.use(new GoogleStrategy({
    clientID: "814045009841-g1cvncc5th3eanro03i8ue7u59umsrj2.apps.googleusercontent.com",
    clientSecret: "H4gd6u9akACBurREHW_CV7F-",
    callbackURL: baseUrl + "/auth/google/callback"
  },
  function(token, tokenSecret, profile, done) {      
      
      let googleEmail = profile.emails[0].value;

      User.findOne({ email : googleEmail }, function (err, user) {
        if (err) {          
          done(err);
        }
        else {
          if (user) done(null, user);
          else {
            let newUser = new User({
              email: googleEmail,
              name: profile.displayName,
              loginProvider: 'google',
              providerDisplayName: 'Google',
              providerKey: profile.id
            });

            newUser.save(function(err) {
              if (err) {                
                done("Impossible de créer le compte.<br />L'adresse email " + googleEmail + "<br/>existe peut être déjà.");
              }
              else {
                done(null, newUser);
              }
            });
          }
        }
      });
    }
  ));

  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());

  return passport.initialize();
}

module.exports = enablePassport();
