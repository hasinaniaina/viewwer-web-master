const httpErrors = require('http-errors');

/**
 * Extends res object with helpers
 */
function extendRes(req, res, next) {
  res.createError = httpErrors;

  next();
}

module.exports = extendRes;
