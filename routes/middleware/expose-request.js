/**
 * Exposes request properties to templates.
 */
function exposeRequest(req, res, next) {
  res.locals.req = req;

  next();
}

module.exports = exposeRequest;
