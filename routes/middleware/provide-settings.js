const mongoose = require('mongoose');
const Setting = mongoose.model('Setting');

function provideSettings(req, res, next) {
  Setting
    .findOne({})
    .exec()
    .then(function(settings) {
      res.locals.site = res.locals.site || {};
      res.locals.site.settings = settings || {};
      next();
    })
    .catch(next);
}

module.exports = provideSettings;
