/**
 * Forces not authorized visitors to go to the login page.
 */
function requireLogin(req, res, next) {
  if (!req.user) return res.redirect('/auth/log-in?back=' + req.originalUrl);
  next();
}

module.exports = requireLogin;
