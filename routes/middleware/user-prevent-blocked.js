/**
 * Deny access for blocked users.
 */
function preventBlocked(req, res, next) {
  if (req.user && req.user.role === 'blocked') return next(res.createError(403));
  next();
}

module.exports = preventBlocked;
