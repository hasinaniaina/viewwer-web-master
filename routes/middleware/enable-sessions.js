const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');

/**
 * Enables sessions support.
 */
function enableSessions() {
  return session({
    secret: process.env.SESSION_SALT,
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({
      mongooseConnection: mongoose.connection,
      stringify: false,
      touchAfter: 24 * 3600
    }),
    cookie: {
      maxAge: 30 * 24 * 60 * 60 * 1000
    }
  });
}

module.exports = enableSessions();
