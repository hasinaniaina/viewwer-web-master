/*
  based on https://github.com/YouneLL/trim-request
 */

function trimStringProperties(obj) {
  if (obj !== null && typeof obj === 'object') {

    for (let prop in obj) {
      // if the property is an object trim it too
      if (typeof obj[prop] === 'object') {
        return trimStringProperties(obj[prop]);
      }
      // if it's a string remove begin and end whitespaces
      if (typeof obj[prop] === 'string') {
        obj[prop] = obj[prop].trim();
      }
    }
  }
}

function trimRequest(req, res, next) {
  if (req.body) trimStringProperties(req.body);
  if (req.params) trimStringProperties(req.params);
  if (req.query) trimStringProperties(req.query);

  next();
}

module.exports = trimRequest;
