const bodyParser = require('body-parser');

/**
 * Enables parsing of CSP REPORT requests.
 */
function enableBodyParser() {
  return bodyParser.json({
    type: ['json', 'application/csp-report']
  });
}

module.exports = enableBodyParser();
