/* eslint-disable quotes */

const _ = require('lodash');

const csp = require('helmet-csp');

const awstask = rootRequire('lib/aws/aws-task');

function enableCsp() {

  const ALLOWED = [
    'https://*.jsdelivr.net',
    'https://*.googleapis.com',
    'https://*.gstatic.com',
    'https://*.ggpht.com',
    'https://*.talkjs.com',
    'https://www.googletagmanager.com',
    'https://www.google-analytics.com',
    'https://*.sentry-cdn.com',
    'https://sentry.io',
    'wss://*.talkjs.com',
    'https://dpdb.webvr.rocks',
    awstask.getPublicHost()
  ];

  const ALLOWED_IFRAMES = [
    '*.talkjs.com',
    'viewwer.com',
    'legacy.viewwer.com'
  ];

  return csp({
    // Specify directives as normal.
    directives: {
      defaultSrc: _.concat(["'self'"], ALLOWED),
      // 'unsafe-eval' - used in krpano
      scriptSrc: _.concat(["'self'", "'unsafe-inline'", "'unsafe-eval'"], ALLOWED),
      styleSrc: _.concat(["'self'", "'unsafe-inline'"], ALLOWED),
      fontSrc: _.concat(["'self'"], ALLOWED),
      imgSrc: _.concat(["'self'", 'data:'], ALLOWED),
      mediaSrc: _.concat(["'self'", 'data:'], ALLOWED),
      frameSrc: ALLOWED_IFRAMES,
      reportUri: '/report-violation',
      workerSrc: false  // This is not set.
    },

    // This module will detect common mistakes in your directives and throw errors
    // if it finds any. To disable this, enable "loose mode".
    loose: false,

    // Set to true if you only want browsers to report errors, not block them.
    // You may also set this to a function(req, res) in order to decide dynamically
    // whether to use reportOnly mode, e.g., to allow for a dynamic kill switch.
    reportOnly: !(process.env.CSP_ENFORCE === 'yes'),

    // Set to true if you want to blindly set all headers: Content-Security-Policy,
    // X-WebKit-CSP, and X-Content-Security-Policy.
    setAllHeaders: false,

    // Set to true if you want to disable CSP on Android where it can be buggy.
    disableAndroid: false,

    // Set to false if you want to completely disable any user-agent sniffing.
    // This may make the headers less compatible but it will be much faster.
    // This defaults to `true`.
    browserSniff: true
  });
}

module.exports = enableCsp();
