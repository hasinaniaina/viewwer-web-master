const flash = require('connect-flash');

/**
 * Enables flash messages support.
 */
function enableFlash() {
  return flash();
}

module.exports = enableFlash();
