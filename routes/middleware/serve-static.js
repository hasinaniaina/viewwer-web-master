const express = require('express');

/**
 * Serves static files from the /public directory.
 */
function serveStatic() {
  return express.static('public');
}

module.exports = serveStatic();
