const enforce = require('express-sslify');

function enableHttps() {
  return enforce.HTTPS({trustProtoHeader: true});
}

module.exports = enableHttps();
