const _ = require('lodash');

const RESTRICTED_EDITORS = [
  '/users',
  '/groups',
  '/phrases',
  '/layer-types',
  '/scene-types',
  '/sequences',
  '/tasks',
  '/settings',
  '/logs'
];

const RESTRICTED_DEVELOPERS = _.concat(RESTRICTED_EDITORS, [
  '/apptours',
  '/tours',
  '/files'
]);

/**
 * Forbid access to restricted routes by role check
 */
function checkRole(req, res, next) {
  if (req.user.role === 'admin') return next();

  if (req.user.role === 'user') return next(res.createError(403));

  if (req.user.role === 'editor') {

    if (_.find(RESTRICTED_EDITORS, r => (req.path.startsWith(r)))) return next(res.createError(403));

  } else if (req.user.role === 'developer') {

    if (_.find(RESTRICTED_DEVELOPERS, r => (req.path.startsWith(r)))) return next(res.createError(403));

  } else {

    return next(res.createError(403));

  }

  next();
}

module.exports = checkRole;
