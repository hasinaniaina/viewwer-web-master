const passport = require('passport');

/**
 * Authenticates user based on the session data.
 */
function authenticateSession() {
  return passport.session();
}

module.exports = authenticateSession();
