function requireLogin(req, res, next) {
  if (
    !req.user &&
    req.path !== '/search' &&
    req.path !== '/export' &&
    req.path !== '/rating' &&
    req.path !== '/messages/webhooks' &&
    req.path !== '/messages/create-visitor' &&
    req.path !== '/app/settings' &&
    req.path !== '/user/auth' &&
    req.path !== '/user/register-and-auth-with-access-token' &&
    req.path !== '/user/signup'
  ) {
    return res.status(403).json({});
  }

  next();
}

module.exports = requireLogin;
