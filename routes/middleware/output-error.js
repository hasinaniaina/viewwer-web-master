const _ = require('lodash');
const logger = rootRequire('lib/logger');

/**
 * Shows errors occurred during request processing.
 */
function outputError(err, req, res, next) { // eslint-disable-line no-unused-vars
  if (typeof err === 'string') err = new Error(err);

  err.status = err.status || 500;

  if (err.status === 403 && err.message === 'Forbidden') {
    err.message = req.method === 'GET' ?
      req.__('You are not allowed to view this page') :
      req.__('You are not allowed to perform this operation');
  }

  res.status(err.status);

  if (req.xhr) {
    res.end(err.message);
  } else {
    res.render('front/pages/error', {
      err: err,
      production: (process.env.NODE_ENV === 'production')
    });
  }

  const meta = {
    error: _.pick(err, ['name', 'code', 'stack', 'status']),
    user: _.get(req, 'user._id'),
    req: _.pick(req, ['url', 'headers', 'method', 'httpVersion', 'originalUrl', 'query', 'body'])
  };

  const loggerMethod = [404, 403, 401].indexOf(res.statusCode) >= 0 ? 'debug' : 'error';

  logger[loggerMethod](err.message, meta);
}

module.exports = outputError;
