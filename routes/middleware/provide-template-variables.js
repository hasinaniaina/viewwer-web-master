const _ = require('lodash');

function provideTemplateVariables(req, res, next) {
  res.locals.template = res.locals.template || {};
  res.locals.template.auth = !!(req.user);
  res.locals.template.authRole = _.get(req, 'user.role', '');
  res.locals.template.authId = _.get(req, 'user._id', '');
  res.locals.template.authName = _.get(req, 'user.name', '');
  res.locals.template.authEmail = _.get(req, 'user.email', '');
  res.locals.template.authPhone = _.get(req, 'user.phone', '');

  let mobile = false;
  if (req.query.mobile === 'true') {
    mobile = true;
  } else if (req.cookies.mobile === 'true') {
    mobile = true;
  }
  res.locals.template.mobile = mobile;

  next();
}

module.exports = provideTemplateVariables;
