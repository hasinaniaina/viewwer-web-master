const nunjucksEnv = require('../../lib/nunjucks-env');

function exposeEnv(req, res, next) {
  nunjucksEnv.addGlobal('env', process.env);
  next();
}

module.exports = exposeEnv;
