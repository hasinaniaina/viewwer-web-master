const bodyParser = require('body-parser');

/**
 * Enables parsing of POST requests.
 */
function enableBodyParser() {
  return bodyParser.urlencoded({extended: true, limit: '250mb', parameterLimit: 100000});
}

module.exports = enableBodyParser();
