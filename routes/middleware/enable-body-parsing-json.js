const bodyParser = require('body-parser');

/**
 * Enables parsing of POST requests.
 */
function enableBodyParser() {
  return bodyParser.json({extended: true, limit: '16mb'});
}

module.exports = enableBodyParser();
