const compression = require('compression');

function enableCompression() {
  return compression();
}

module.exports = enableCompression();
