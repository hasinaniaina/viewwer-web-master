/**
 * @file The list of supported locales
 */
module.exports = [
  {code: 'en', title: 'English'},
  {code: 'fr', title: 'Français', isDefault: true}
];
