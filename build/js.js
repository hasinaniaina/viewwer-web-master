const fs = require('fs');
const argv = require('argh').argv;
const report = require('./utils/report');
const watch = require('./utils/watch');
const globby = require('globby');
const seamster = require('seamster');
const UglifyJS = require('uglify-js');

function combine(area) {
  const files = globby.sync([
    'public/assets/shared/js/src/*.js',
    'public/assets/' + area + '/js/src/*.js'
  ]);

  const appCodeFile = 'public/assets/' + area + '/js/script.js';
  const appMapFile = appCodeFile + '.map';

  try {
    fs.unlinkSync(appMapFile);
  } catch (err) {
    if (err.code !== 'ENOENT') throw err;
  }

  seamster({
    ns: 'app',
    files: files,
    dest: appCodeFile,
    expose: true,
    sourceMap: Boolean(argv.dev)
  });

  if (argv.dev) {
    report([appCodeFile, appMapFile]);
  } else {
    const minified = UglifyJS.minify(fs.readFileSync(appCodeFile, 'utf-8'));

    if (minified.error) return console.error(minified.error);

    fs.writeFileSync(appCodeFile, minified.code);
    report(appCodeFile);
  }
}

function buildApp() {
  buildAppFront();
  buildAppBack();
}

function buildAppFront() {
  combine('front');
}

function buildAppBack() {
  combine('back');
}

buildApp();

if (argv.watch) {
  watch('public/assets/front/js/src/**/*.js', buildAppFront);
  watch('public/assets/back/js/src/**/*.js', buildAppBack);

  watch('public/assets/shared/js/src/**/*.js', function() {
    buildAppFront();
    buildAppBack();
  });
}
