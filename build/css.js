const fs = require('fs');
const argv = require('argh').argv;
const report = require('./utils/report');
const watch = require('./utils/watch');
const sass = require('node-sass');

function transpile(source) {
  const codeFile = source.replace('/src', '').replace('.scss', '.css');
  const mapFile = codeFile + '.map';

  const transpiled = sass.renderSync({
    file: source,
    outFile: codeFile,
    sourceMap: argv.dev,
    sourceMapContents: true,
    precision: 8
  });

  try {
    fs.unlinkSync(mapFile);
  } catch (err) {
    if (err.code !== 'ENOENT') throw err;
  }

  if (argv.dev) {
    fs.writeFileSync(codeFile, transpiled.css);
    fs.writeFileSync(mapFile, transpiled.map);
    report([codeFile, mapFile]);
  } else {
    const postcss = require('postcss');
    const autoprefixer = require('autoprefixer')({browsers: ['last 2 versions', 'ie >= 9']});
    const csswring = require('csswring');

    const optimized = postcss([autoprefixer, csswring]).process(transpiled.css);

    fs.writeFileSync(codeFile, optimized.css);
    report(codeFile);
  }
}

function buildApp() {
  buildAppFront();
  buildAppBack();
}

function buildAppFront() {
  transpile('public/assets/front/css/src/style.scss');
}

function buildAppBack() {
  transpile('public/assets/back/css/src/style.scss');
}

buildApp();

if (argv.watch) {
  watch('public/assets/front/**/*.scss', buildAppFront);

  watch('public/assets/back/**/*.scss', function() {
    buildAppFront();
    buildAppBack();
  });

  watch('public/assets/shared/**/*.scss', function() {
    buildAppFront();
    buildAppBack();
  });
}
