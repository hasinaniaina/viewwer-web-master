/**
 * Notifies about generated files
 * @param {string|string[]} files - path(s) to created files
 */
module.exports = function(files) {
  if (!Array.isArray(files)) files = [files];

  files.forEach(file => console.info(`Built ${file}`));
};
