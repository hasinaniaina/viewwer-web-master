const _ = require('lodash');
const chokidar = require('chokidar');

/**
 * Sets up file system change watcher that doesn't exit on errors
 * @param {string|string[]} glob - path(s) to watch
 * @param {Function} func - function to execute on change
 */
module.exports = function(glob, func) {
  function runFunc() {
    try {
      func();
    } catch (err) {
      console.error(err);
    }
  }

  chokidar
    .watch(glob, {ignoreInitial: true})
    .on('all', _.debounce(runFunc, 50));
};
