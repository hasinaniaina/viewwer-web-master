// populate process.env with values defined in the .env file
require('dotenv').config();

const logger = require('./lib/logger');

// accept "MONGODB_ADDON_URI" aы "DB_URI" for convenience when app is on Clever Cloud
if (!process.env.DB_URI && process.env.MONGODB_ADDON_URI) process.env.DB_URI = process.env.MONGODB_ADDON_URI;

// check that required settings have been provided
['DB_URI', 'SESSION_SALT', 'STORAGE_PATH'].forEach(name => {
  if (!process.env[name]) throw new Error('Please set ' + name + ' env. variable first.');
});

global.rootRequire = require('app-root-path').require;

const ServerShutdown = require('server-shutdown');
const serverShutdown = new ServerShutdown();

const mongoose = require('mongoose');

mongoose.Promise = Promise;

// there's not much we can do without a DB, so web server will start only after establishing a connection to MongoDB
mongoose.connection.once('open', () => {
  const configureI18n = require('./lib/setup/i18n');
  const preloadModels = require('./lib/setup/models');
  const createAdmin = require('./lib/setup/admin');
  const createSettings = require('./lib/setup/settings');

  preloadModels()
    .then(configureI18n)
    .then(createAdmin)
    .then(createSettings)
    .then(() => {
      const express = require('express');
      const app = express();

      app.set('x-powered-by', false);
      app.set('trust proxy', true);
      app.set('subdomain offset', process.env.DOMAIN.split('.').length);

      require('./lib/setup/templating')(app);
      require('./lib/setup/redirects')(app);
      require('./lib/setup/redirect-www')(app);
      require('./lib/setup/routing')(app);
      require('./lib/setup/params-loading')(app);
      require('./lib/setup/krpano')(app);
      require('./lib/setup/fs')();

      const http = require('http');
      const httpServer = http.createServer(app);

      serverShutdown.registerServer(httpServer);

      const httpPort = process.env.PORT || 80;
      httpServer.listen(httpPort, () => {
        logger.info('Application is running on port ' + httpPort);
      });
    })
    .catch(err => {
      logger.error(err, loggerErr => {
        if (loggerErr) console.error(err);

        process.exit(1);
      });
    });
});

mongoose.connection.on('error', (e) => {
  logger.error(e);
});

// connect to the DB
mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  keepAlive: true,
  keepAliveInitialDelay: 300000,
  reconnectTries: Number.MAX_VALUE
});

// allow to shutdown gracefully, without interrupting unfinished operations
function shutdown() {
  logger.info('Termination signal received, application is shutting down', () => {
    serverShutdown.shutdown(() => {
      mongoose.connection.close(process.exit);
    });
  });
}

process.on('SIGTERM', shutdown).on('SIGINT', shutdown).on('SIGHUP', shutdown);

process.on('unhandledRejection', (err, p) => {
  logger.error('Unhandled promise rejection', {err: err, stack: err ? err.stack : undefined, promise: p}, () => {
    process.exit(1);
  });
});
