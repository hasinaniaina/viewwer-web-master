const mongoose = require('mongoose');

const Log = new mongoose.Schema({
  timestamp: Date,
  level: String,
  message: String,
  meta: {},
  hostname: String,
  label: String
}, {
  capped: {
    size: (process.env.LOG_DB_CAP || 50) * 1000 * 1000
  },
  versionKey: false
});

Log.index({
  timestamp : -1,
  level : 1
});

module.exports = mongoose.model('Log', Log);
