const _ = require('lodash');

const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const counters = require('./plugins/counters');
const base = require('./base');

const COLL = 'Developer';
const COLL_APARTMENT = 'Apartment';
const COLL_BUILDING = 'Building';
const COLL_PROGRAM = 'Program';
const COLL_REQUEST = 'Request';

const developerSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},
  group: {type: String, ref: 'Group'},

  name: {type: String, required: true},
  alias: {
    type: String,
    validate: {
      validator: function (v) {
        return /^[a-zA-Z0-9][a-zA-Z0-9]{0,61}$/.test(v);
      }
    },
    index: {
      sparse: true,
      unique: true
    }
  },
  description: String,

  logoAvatar: String,
  logoAvatarOriginal: String,
  logoTourUrl: String,

  slider: {type: String, ref: 'ImageSet', autopopulate: true},

  requestGlasses: Boolean,

  isPublished: Boolean,

  counterRating: {type: Number, default: 0},
  counterShares: {type: Number, default: 0},
  counterRequests: {type: Number, default: 0},
  counterPrograms: {type: Number, default: 0},
  counterApartments: {type: Number, default: 0}

}), {
  timestamps: true
});
developerSchema.plugin(autopopulate);
developerSchema.plugin(counters);

developerSchema.statics.updateApartmentsCounter = function (developer_id) {
  return new Promise((resolve, reject) => {
    if (!developer_id) return resolve();

    this.model(COLL_PROGRAM).find({developer: developer_id}).distinct('_id').exec()
      .then(r => {
        if (_.isEmpty(r)) return;
        return this.model(COLL_BUILDING).find({program: {$in: r}}).distinct('_id').exec();
      })
      .then(r => {
        if (_.isEmpty(r)) return 0;
        return this.model(COLL_APARTMENT).countDocuments({building: {$in: r}}).exec();
      })
      .then(r => this.model(COLL).setCounter(developer_id, 'Apartments', r))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

developerSchema.statics.updateProgramsCounter = function (developer_id) {
  return new Promise((resolve, reject) => {
    if (!developer_id) return resolve();

    this.model(COLL_PROGRAM).countDocuments({developer: developer_id}).exec()
      .then(r => this.model(COLL).setCounter(developer_id, 'Programs', r))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

developerSchema.statics.updateRequestsCounter = function (developer_id) {
  return new Promise((resolve, reject) => {
    if (!developer_id) return resolve();

    this.model(COLL_REQUEST).countDocuments({developer: developer_id}).exec()
      .then(r => this.model(COLL).setCounter(developer_id, 'Requests', r))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

module.exports = mongoose.model(COLL, developerSchema);
