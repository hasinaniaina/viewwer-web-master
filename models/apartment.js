const _ = require('lodash');
const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const counters = require('./plugins/counters');
const base = require('./base');

const COLL = 'Apartment';

const apartmentSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},
  group: {type: String, ref: 'Group'},

  name: {type: String, required: true},
  description: String,

  // for search
  path: String,
  pathMustBeUpdated: Boolean,

  lot: String,
  type: {type: String, enum: ['t01', 't02', 't03', 't04', 't05', 't06', 't07']},
  area: Number,

  price: Number,

  // options
  house: Boolean,
  duplex: Boolean,
  elevator: Boolean,
  garage: Boolean,
  terrace: Boolean,
  garden: Boolean,
  basement: Boolean,
  parking: Boolean,
  balcony: Boolean,
  balconyArea: Number,

  floor: String,
  orientation: String,
  energyPerformance: String,

  saleType: {type: String, enum: ['sale', 'rent'], default: 'sale'},

  isSold: {type: Boolean, default: false},
  isSoldUpdatedAt: Date,

  building: {type: String, ref: 'Building', autopopulate: true},

  exteriorScene: String,
  exteriorPolygons: [{
    scene: String,
    points: [{
      atv: Number,
      ath: Number
    }]
  }],

  counterRating: {type: Number, default: 0},
  counterShares: {type: Number, default: 0},

  isPublished: Boolean
}), {
  timestamps: true,
  toObject: {
    virtuals: true
  }
  ,toJSON: {
    virtuals: true
  }
});

apartmentSchema.virtual('linkedTour', {
  ref: 'Tour',
  localField: '_id',
  foreignField: 'apartment',
  justOne: true,
  autopopulate: {
    maxDepth: 1
  }
});

apartmentSchema.virtual('linkedTourAdditional', {
  ref: 'Tour',
  localField: '_id',
  foreignField: 'apartmentsAdditional',
  justOne: true,
  autopopulate: {
    maxDepth: 1
  }
});

apartmentSchema.plugin(autopopulate);
apartmentSchema.plugin(counters);

apartmentSchema.statics.assignBuilding = function (apartments, building_id) {
  return this.model(COLL).updateMany(
    {_id: {$in: apartments}},
    {
      $set: {
        building: building_id,
        pathMustBeUpdated: true
      }
    }).exec();
};

apartmentSchema.statics.clearBuilding = function (building_id) {
  return this.model(COLL).updateMany(
    {building: building_id},
    {
      $set: {
        building: '',
        pathMustBeUpdated: true
      }
    }).exec();
};

apartmentSchema.statics.updatePath = function (apartment_id) {
  return new Promise((resolve, reject) => {
    this.model(COLL).findById(apartment_id).exec()
      .then(r => {
        const path = [];
        const building = _.get(r, 'building.name');
        const program = _.get(r, 'building.program.name');
        const developer = _.get(r, 'building.program.developer.name');

        if (developer) path.push(developer);
        if (program) path.push(program);
        if (building) path.push(building);

        return path.join(' ');
      })
      .then(r => this.model(COLL).updateOne({_id: apartment_id}, {
        $set: {
          path: r,
          pathMustBeUpdated: false
        }
      }).exec())
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

module.exports = mongoose.model(COLL, apartmentSchema);
