const _ = require('lodash');

module.exports = function (query, user, view_published) {
  if (user && user.role !== 'admin' && user.role !== 'editor') {
    const q = {
      $and: [
        {
          $or: [
            {user: {$exists: true, $ne: '', $eq: user._id}},
            {group: {$exists: true, $ne: '', $eq: (user.group) ? user.group._id : '__unknown'}},
            {developerPerm: {$exists: true, $ne: '', $eq: (user.developer) ? user.developer._id : '__unknown'}},
            {developerPerm: {$exists: true, $ne: '', $eq: (user.group && user.group.developer) ? user.group.developer._id : '__unknown'}}
          ]
        }
      ]
    };
    // or view published, ignoring user/group
    if (view_published) q.$and[0].$or.push({isPublished: true});
    if (!_.isEmpty(query)) q.$and.push(query);
    return q;
  } else {
    return query;
  }
};
