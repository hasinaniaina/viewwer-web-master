const _ = require('lodash');

module.exports = function (query) {
  if (!_.isEmpty(query)) {
    const q = {
      $and: [
        {role: {$nin: ['user', 'blocked']}}
      ]
    };
    q.$and.push(query);
    return q;
  } else {
    return {role: {$nin: ['user', 'blocked']}};
  }
};
