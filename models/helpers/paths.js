  const _ = require('lodash');

const mongoose = require('mongoose');

const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');
const Tour = mongoose.model('Tour');

module.exports.updateMarked = function () {
  return new Promise((resolve, reject) => {
    Promise.all([
      Program.find({pathMustBeUpdated: true}).distinct('_id').exec(),
      Building.find({pathMustBeUpdated: true}).distinct('_id').exec(),
      Apartment.find({pathMustBeUpdated: true}).distinct('_id').exec(),
      Tour.find({pathMustBeUpdated: true}).distinct('_id').exec()
    ])
      .then(results => {
        const promises = [];
        if (!_.isEmpty(results[0])) promises.push(updatePrograms(results[0]));
        if (!_.isEmpty(results[1])) promises.push(updateBuildings(results[1]));
        if (!_.isEmpty(results[2])) promises.push(updateApartments(results[2]));
        if (!_.isEmpty(results[3])) promises.push(updateTours(results[3]));
        if (!_.isEmpty(promises)) return Promise.all(promises);
      })
      .then(() => resolve())
      .catch(e => reject(e));
  });
};

function updatePrograms(programs) {
  return new Promise((resolve, reject) => {
    if (_.isEmpty(programs)) return resolve();

    Promise.all(_.map(programs, p => Program.updatePath(p)))
      .then(() => Building.find({program: {$in: programs}}))
      .then(r => updateBuildings(r))
      .then(() => Tour.find({program: {$in: programs}}))
      .then(r => updateTours(r))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
}

function updateBuildings(buildings) {
  return new Promise((resolve, reject) => {
    if (_.isEmpty(buildings)) return resolve();

    Promise.all(_.map(buildings, b => Building.updatePath(b)))
      .then(() => Apartment.find({building: {$in: buildings}}))
      .then(r => updateApartments(r))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
}

function updateApartments(apartments) {
  return new Promise((resolve, reject) => {
    Promise.all(_.map(apartments, a => Apartment.updatePath(a)))
      .then(() => Tour.find({apartment: {$in: apartments}}))
      .then(r => updateTours(r))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
}

function updateTours(tours) {
  return new Promise((resolve, reject) => {
    Promise.all(_.map(tours, t => Tour.updatePath(t)))
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
}
