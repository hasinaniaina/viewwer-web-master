const _ = require('lodash');

module.exports = function (query) {
  if (!_.isEmpty(query)) {
    const q = {
      $and: [
        {isPublished: true}
      ]
    };
    q.$and.push(query);
    return q;
  } else {
    return {isPublished: true};
  }
};
