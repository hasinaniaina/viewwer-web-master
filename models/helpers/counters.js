const _ = require('lodash');

const mongoose = require('mongoose');

const Developer = mongoose.model('Developer');
const Program = mongoose.model('Program');
const Building = mongoose.model('Building');
const Apartment = mongoose.model('Apartment');

function updateDevelopersCounters(developer_id) {
  return Promise.all([
    Developer.updateApartmentsCounter(developer_id),
    Developer.updateProgramsCounter(developer_id)
  ]);
}

module.exports.updateDevelopersCounters = updateDevelopersCounters;

module.exports.updateDevelopersCountersForProgram = function (program_id) {
  return new Promise((resolve, reject) => {
    if (!program_id) return resolve();

    Program.findById(program_id)
      .then(r => {
        const d = _.get(r, 'developer._id');
        if (d) return updateDevelopersCounters(d);
      })
      .then(() => resolve())
      .catch(e => reject(e));
  });
};

module.exports.updateDevelopersCountersForBuilding = function (building_id) {
  return new Promise((resolve, reject) => {
    if (!building_id) return resolve();

    Building.findById(building_id)
      .then(r => {
        const d = _.get(r, 'program.developer._id');
        if (d) return updateDevelopersCounters(d);
      })
      .then(() => resolve())
      .catch(e => reject(e));
  });
};

module.exports.updateDevelopersCountersForApartment = function (apartment_id) {
  return new Promise((resolve, reject) => {
    if (!apartment_id) return resolve();

    Apartment.findById(apartment_id)
      .then(r => {
        const d = _.get(r, 'building.program.developer._id');
        if (d) return updateDevelopersCounters(d);
      })
      .then(() => resolve())
      .catch(e => reject(e));
  });
};

