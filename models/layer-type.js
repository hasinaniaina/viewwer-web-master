const mongoose = require('mongoose');
const base = require('./base');

const LayerType = new mongoose.Schema(Object.assign({}, base, {
  name: {type: String, required: true},
  icon: String,
  weight: {type: Number, default: 0},
  /* base layer will be shown in sidebar */
  isBaseLayer: Boolean
}), {
  timestamps: true
});

module.exports = mongoose.model('LayerType', LayerType);
