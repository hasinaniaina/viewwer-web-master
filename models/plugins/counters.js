module.exports = function (schema) {
  schema.statics.increaseCounter = increaseCounter;
  schema.statics.decreaseCounter = decreaseCounter;
  schema.statics.setCounter = setCounter;
};

function increaseCounter(id, counter) {
  return this.updateOne(
    {
      _id: id
    },
    {
      $inc: {
        ['counter' + counter]: 1
      }
    })
    .exec();
}

function decreaseCounter(id, counter) {
  return this.updateOne(
    {
      _id: id,
      ['counter' + counter]: {
        $gt: 0
      }
    },
    {
      $inc: {
        ['counter' + counter]: -1
      }
    })
    .exec();
}

function setCounter(id, counter, value) {
  return this.updateOne(
    {
      _id: id
    },
    {
      $set: {
        ['counter' + counter]: value
      }
    })
    .exec();
}
