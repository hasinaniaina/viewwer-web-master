const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const base = require('./base');

const SequenceSchema = new mongoose.Schema(Object.assign({}, base, {
  name: {type: String, required: true},
  sceneTypesSequence: [{type: String, ref: 'SceneType', autopopulate: true}]
}), {
  timestamps: true
});
SequenceSchema.plugin(autopopulate);

module.exports = mongoose.model('Sequence', SequenceSchema);
