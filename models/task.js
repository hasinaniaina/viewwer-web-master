const mongoose = require('mongoose');

const base = require('./base');

const COLL = 'Task';

const taskSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},

  type: {type: String, enum: ['apptour', 'apptour-save'], required: true},

  status: {type: String, enum: ['new', 'processing', 'failed', 'completed'], default: 'new'},

  duration: Number,
  host: String,

  result: {},
  error: {type: String, enum: ['internal', 'timeout']},

  // all requests
  requests: [],
  // all results
  results: [],
  // all errors
  errorLogs: []
}), {
  timestamps: true
});

module.exports = mongoose.model(COLL, taskSchema);
