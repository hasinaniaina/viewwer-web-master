const mongoose = require('mongoose');
const base = require('./base');

const PhraseSchema = new mongoose.Schema(Object.assign({}, base, {
  name: {type: String, required: true},
  type: {type: String, enum: ['decoration'], default: 'decoration'}
}), {
  timestamps: true
});

module.exports = mongoose.model('Phrase', PhraseSchema);
