const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const autopopulate = require('mongoose-autopopulate');
const base = require('./base');

const COLL = 'User';

const Partner = mongoose.model('Partner');

const userSchema = new mongoose.Schema(Object.assign({}, base, {
  firstName: { type: String /*, required: true */}, // le temps de migrer les données, pas obligatoire
  lastName: { type: String /*, required: true */},
  email: { type: String, unique: true, required: true },
  phone: String,
  role: { type: String, enum: ['admin', 'editor', 'developer', 'user', 'blocked'], default: 'user' },

  // External logins (facebook, google...)
  loginProvider: String,        // internal name
  providerKey: String,          // provider user id
  providerDisplayName: String,  // display name: Facebook, Google...

  // email verification for mobile users
  verified: Boolean,
  verifyKey: String,

  group: {
    type: String,
    ref: 'Group',
    autopopulate: {
      maxDepth: 2
    }
  },

  // used for requests for example
  developer: {
    type: String,
    ref: 'Developer',
    autopopulate: {
      maxDepth: 1,
      select: '_id name'
    }
  },

  forcePasswordChange: Boolean,

  passwordResetKey: String,

  // Firebase
  firebaseRegistrationToken: String,

  partner: {
    type: String,
    ref: 'Partner',
    autopopulate: {
      maxDepth: 1,
      select: '_id name code'
    }    
  },
  partnershipValidation: { type: String, enum: ['allowed', 'disallowed', 'in_validation', 'unknown'], default: 'unknown' },

  isPro: Boolean,
  companyName: String,
  activity: String

}), {
    timestamps: true
  });

userSchema.virtual('name').get(function() {
  return this.firstName + ' ' + this.lastName;
});

userSchema.plugin(autopopulate);

userSchema.statics.assignGroup = function (users, group_id) {
  return this.model(COLL).updateMany(
    { _id: { $in: users } },
    { $set: { group: group_id } }).exec();
};

userSchema.statics.clearGroup = function (group_id) {
  return this.model(COLL).updateMany(
    { group: group_id },
    { $set: { group: '' } }).exec();
};

userSchema.statics.findOrCreate = require("find-or-create");

userSchema.plugin(passportLocalMongoose, {
  iterations: 10000,
  usernameField: 'email',
  usernameLowerCase: 'true',
  lastLoginField: 'login_last',
  attemptsField: 'login_attempts',
  limitAttempts: true,
  errorMessages: {
    UserExistsError: 'A user with the given email is already registered',
    IncorrectUsernameError: 'Incorrect email or password',
    IncorrectPasswordError: 'Incorrect email or password',
    MissingPasswordError: 'No password was given',
    AttemptTooSoonError: 'Account is currently locked. Try again later',
    TooManyAttemptsError: 'Account locked due to too many failed login attempts',
    NoSaltValueStoredError: 'Authentication not possible. No salt value stored',
    MissingUsernameError: 'No username was given'
  }
});

module.exports = mongoose.model(COLL, userSchema);
