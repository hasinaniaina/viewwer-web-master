const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const base = require('./base');

const requestSchema = new mongoose.Schema(Object.assign({}, base, {
  status: {type: String, enum: ['new', 'processed'], default: 'new'},

  ip: String,
  useragent: String,
  // source url
  url: String,

  type: {type: String, enum: ['apartment', 'info', 'glasses']},

  // customer's info
  firstName: String,
  lastName: String,
  timeframe: String,
  budgetFrom: Number,
  budgetTo: Number,
  email: String,
  phone: String,
  message: String,

  // for glasses requests
  address: String,
  postalCode: String,
  city: String,

  // notes editable by users
  notes: String,

  // email notifications sent
  notificationsEmailsSent: Boolean,
  notificationsEmails: [String],

  // for per developer permission control
  developerPerm: {type: String, ref: 'Developer'},

  developer: {
    type: String, ref: 'Developer',
    autopopulate: {
      maxDepth: 1,
      select: '_id name'
    }
  },
  program: {
    type: String, ref: 'Program',
    autopopulate: {
      maxDepth: 1,
      select: '_id name'
    }
  },
  building: {
    type: String, ref: 'Building',
    autopopulate: {
      maxDepth: 1,
      select: '_id name'
    }
  },
  apartment: {
    type: String, ref: 'Apartment',
    autopopulate: {
      maxDepth: 1,
      select: '_id name'
    }
  },

  updatedBy: {
    type: String, ref: 'User',
    autopopulate: {
      maxDepth: 1,
      select: '_id name email'
    }
  }
}), {
  timestamps: true
});
requestSchema.plugin(autopopulate);

module.exports = mongoose.model('Request', requestSchema);
