const mongoose = require('mongoose');
const base = require('./base');

const Setting = new mongoose.Schema(Object.assign({}, base, {

  krpanoLicensed: Boolean,

  // global logo in left sidebar bottom
  logoTourEnabled: Boolean,

  // url for global logo
  logoTourUrl: String,

  searchResultsPerPage: { type: Number, default: 9 },
  // search distance in meters
  // for manually entered address
  searchGeoMaxDistance: { type: Number, default: 10000 },
  // for user's geo location
  searchGeoMaxDistanceUser: { type: Number, default: 20000 },
  // search sold items, days
  searchSoldItems: { type: Number, default: 90 },

  // use Lambda for scene previews
  lambdaScenePreviews: Boolean,
  // for tours
  lambdaTours: Boolean,

  // maintenance modes
  maintenanceMobileApp: Boolean,

  // minimal app version
  mobileAppMinimalVersion: { type: Number, default: 1 },

  // mobile tours email notifications
  notifyNewAppTours: Boolean,

  // foolproof protection
  allowSceneTypesRemoval: Boolean,
  allowLayerTypesRemoval: Boolean

}), {
  timestamps: true
});

module.exports = mongoose.model('Setting', Setting);
