const _ = require('lodash');
const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const counters = require('./plugins/counters');
const base = require('./base');

const COLL = 'Program';

const programSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},
  group: {type: String, ref: 'Group'},

  name: {type: String, required: true},
  description: String,

  path: String,
  pathMustBeUpdated: Boolean,

  developer: {type: String, ref: 'Developer', autopopulate: true},
  aerial_url: String,

  address: String,
  location: {
    type: {type: String, enum: 'Point', default: 'Point'},
    coordinates: {type: [Number], default: [0, 0]}
  },

  /*
   preview,
   commercial launch,
   commencement of the works,
   work in progress,
   delivered
  */
  devStatus: {
    type: String,
    enum: [
      'preview',
      'commercial_launch',
      'commencement_works',
      'work_in_progress',
      'delivered'
    ]
  },

  available: String,

  decorationAndConveniences: [{type: String, ref: 'Phrase', autopopulate: true}],

  distanceShops: Number,
  distanceCultureAndEntertainment: Number,
  distanceTransports: Number,
  distanceSchools: Number,

  filePdf: String,

  counterRating: {type: Number, default: 0},
  counterShares: {type: Number, default: 0},

  logoProgram: String,
  logoProgramOriginal: String,

  slider: {type: String, ref: 'ImageSet', autopopulate: true},

  isPublished: Boolean
}), {
  timestamps: true
});
programSchema.plugin(autopopulate);
programSchema.plugin(counters);

programSchema.index({location: '2dsphere'});

programSchema.statics.assignDeveloper = function (programs, developer_id) {
  return this.model(COLL).updateMany(
    {_id: {$in: programs}},
    {
      $set: {
        developer: developer_id,
        pathMustBeUpdated: true
      }
    }).exec();
};

programSchema.statics.clearDeveloper = function (developer_id) {
  return this.model(COLL).updateMany(
    {developer: developer_id},
    {
      $set: {
        developer: '',
        pathMustBeUpdated: true
      }
    }).exec();
};

programSchema.statics.updatePath = function (program_id) {
  return new Promise((resolve, reject) => {
    this.model(COLL).findById(program_id).exec()
      .then(r => {
        const path = [];
        const developer = _.get(r, 'developer.name');
        if (developer) path.push(developer);
        return path.join(' ');
      })
      .then(r => this.model(COLL).updateOne({_id: program_id}, {
        $set: {
          path: r,
          pathMustBeUpdated: false
        }
      }).exec())
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

module.exports = mongoose.model(COLL, programSchema);

