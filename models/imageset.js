const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const base = require('./base');

const imageSetSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},
  group: {type: String, ref: 'Group'},

  name: {type: String, required: true},

  // only slider is used for now
  type: {type: String, enum: ['slider', 'gallery'], default: 'slider'},

  images: [String]
}), {
  timestamps: true
});
imageSetSchema.plugin(autopopulate);

module.exports = mongoose.model('ImageSet', imageSetSchema);
