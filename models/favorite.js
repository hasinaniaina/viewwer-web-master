const mongoose = require('mongoose');
const base = require('./base');

const COLL = 'Favorite';

const favoriteSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},

  entityType: {
    type: String,
    enum: ['Apartment', 'AppTour'],
    required: true
  },

  entityId: {type: String, required: true}
}), {
  timestamps: true,
  toObject: {
    virtuals: true
  }
  ,toJSON: {
    virtuals: true
  }
});

favoriteSchema.virtual('entity', {
  ref: doc => doc.entityType,
  localField: 'entityId',
  foreignField: '_id',
  justOne: true
});

module.exports = mongoose.model(COLL, favoriteSchema);
