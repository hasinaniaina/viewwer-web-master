const mongoose = require('mongoose');
const base = require('./base');

const partnerSchema = new mongoose.Schema(Object.assign({}, base, {
  // partner name
  name: { type: String, required: true },

  // code generated (random)
  code: { type: String, require: true, unique: true },

  // key used to access export URLs
  key : String,

  // enabled or not
  isEnabled: Boolean,

  // person managing the members of this partner
  email: { type: String, required: true },

  // filter for the ads
  filter: {}

}), {
    timestamps: true
  });

module.exports = mongoose.model('Partner', partnerSchema);
