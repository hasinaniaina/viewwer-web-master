const mongoose = require('mongoose');
const base = require('./base');
const autopopulate = require('mongoose-autopopulate');

const groupSchema = new mongoose.Schema(Object.assign({}, base, {
  name: {type: String, required: true},

  developer: {
    type: String,
    ref: 'Developer',
    autopopulate: {
      maxDepth: 1,
      select: '_id name'
    }
  }
}), {
  timestamps: true
});
groupSchema.plugin(autopopulate);

module.exports = mongoose.model('Group', groupSchema);
