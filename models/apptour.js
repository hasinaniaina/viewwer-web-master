const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const counters = require('./plugins/counters');
const base = require('./base');

const COLL = 'AppTour';

const appTourSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},

  type: {type: String, enum: ['t01', 't02', 't03', 't04', 't05', 't06', 't07'], default: 't01'},
  house: {type: Boolean, default: false},
  description: {type: String, default: ''},

  area: Number,
  price: Number,

  gasClass: {type: String, enum: ['N', 'A', 'B', 'C', 'D', 'E', 'F', 'G'], default: 'N'},
  energyClass: {type: String, enum: ['N', 'A', 'B', 'C', 'D', 'E', 'F', 'G'], default: 'N'},

  saleType: {type: String, enum: ['sale', 'rent'], default: 'sale'},

  isSold: {type: Boolean, default: false},

  previewTaskId: {type: String, ref: 'Task'},
  saveTaskId: {type: String, ref: 'Task'},

  tour: {type: Object, default: {}},
  settings: {type: Object, default: {}},

  thumbnail: String,

  address: String,
  location: {
    type: {type: String, enum: 'Point', default: 'Point'},
    coordinates: {type: [Number], default: [0, 0]}
  },

  generateStatus: {
    type: String,
    enum: ['processing', 'failed', 'completed'],
    default: 'processing'
  },

  moderationStatus: {
    type: String,
    enum: ['pending', 'approved', 'rejected'],
    default: 'pending'
  },

  moderationMessage: {type: String, default: ''},

  updatedBy: {
    type: String, ref: 'User',
    autopopulate: {
      maxDepth: 1,
      select: '_id name email'
    }
  },

  // locale
  // for notifications, to know user's locale
  locale: {type: String, default: 'fr'},

  counterRating: {type: Number, default: 0},
  counterShares: {type: Number, default: 0},

  isPublished: {type: Boolean, default: true},

  // partner code from the user that published this ad
  partnerCode: String
}), {
  timestamps: true
});
appTourSchema.plugin(autopopulate);
appTourSchema.plugin(counters);

appTourSchema.index({location: '2dsphere'});

module.exports = mongoose.model(COLL, appTourSchema);
