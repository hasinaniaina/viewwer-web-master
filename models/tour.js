const _ = require('lodash');
const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const counters = require('./plugins/counters');
const base = require('./base');

const COLL = 'Tour';

const hotspotSchema = new mongoose.Schema(Object.assign({}, base, {
  name: String,
  ath: Number,
  atv: Number,
  left: Number,
  top: Number,
  action: {},
  icon: String
}));

const previewSettingsSchema = new mongoose.Schema(Object.assign({}, base, {
  hlookat: Number,
  vlookat: Number,
  fov: Number,
  // planspot view direction adjustment
  headingoffset: Number
}));

const sceneSchema = new mongoose.Schema(Object.assign({}, base, {
  name: String,
  type: {type: String, ref: 'SceneType', autopopulate: true},
  layer: {type: String, ref: 'LayerType', autopopulate: true},
  file: {
    originalName: String,
    path: String,
    size: Number,
    width: Number,
    height: Number,
    hfov: Number,
    vfov: Number
  },
  isMain: Boolean,
  hotspots: [hotspotSchema],
  previewSettings: previewSettingsSchema
}));

const planspotSchema = new mongoose.Schema(Object.assign({}, base, {
  scene: String,
  left: Number,
  top: Number
}));

const planSchema = new mongoose.Schema(Object.assign({}, base, {
  width: Number,
  height: Number,
  image: String,
}));

const tourSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},
  group: {type: String, ref: 'Group'},

  name: {type: String, required: true},

  // for search
  path: String,
  pathMustBeUpdated: Boolean,

  // krpano generating
  generateStatus: {type: String, enum: ['processing', 'failed', 'completed']},
  generateStartedAt: Date,
  generateDuration: Number,

  type: {type: String, enum: ['interior', 'exterior']},
  // for exterior
  program: {type: String, ref: 'Program', autopopulate: true},
  // for interior
  apartment: {type: String, ref: 'Apartment', autopopulate: true},
  // additional apartments
  apartmentsAdditional: [{type: String, ref: 'Apartment', autopopulate: true}],

  plan: planSchema,
  planspots: [planspotSchema],

  scenes: [sceneSchema],
  /* used for on the fly panorama.xml generation */
  panorama: {},

  counterRating: {type: Number, default: 0},
  counterShares: {type: Number, default: 0},

  isPublished: Boolean,
  thumbnail: String
}), {
  timestamps: true
});
tourSchema.plugin(autopopulate);
tourSchema.plugin(counters);

tourSchema.statics.assignProgram = function (tour_id, program_id) {
  return this.model(COLL).updateOne(
    {_id: tour_id},
    {
      $set: {
        program: program_id,
        pathMustBeUpdated: true
      }
    }).exec();
};

tourSchema.statics.clearProgram = function (program_id) {
  return this.model(COLL).updateMany(
    {program: program_id},
    {
      $set: {
        program: '',
        pathMustBeUpdated: true
      }
    }).exec();
};

tourSchema.statics.assignApartment = function (tour_id, apartment_id) {
  return this.model(COLL).updateOne(
    {_id: tour_id},
    {
      $set: {
        apartment: apartment_id,
        pathMustBeUpdated: true
      }
    }).exec();
};

tourSchema.statics.clearApartment = function (apartment_id) {
  return this.model(COLL).updateMany(
    {apartment: apartment_id},
    {
      $set: {
        apartment: '',
        pathMustBeUpdated: true
      }
    }).exec();
};

tourSchema.statics.updatePath = function (tour_id) {
  return new Promise((resolve, reject) => {
    this.model(COLL).findById(tour_id).exec()
      .then(r => {
        if (!r) return '';

        const path = [];

        if (r.type === 'interior') {

          const apartment = _.get(r, 'apartment.name');
          const building = _.get(r, 'apartment.building.name');
          const program = _.get(r, 'apartment.building.program.name');
          const developer = _.get(r, 'apartment.building.program.developer.name');

          if (developer) path.push(developer);
          if (program) path.push(program);
          if (building) path.push(building);
          if (apartment) path.push(apartment);

        } else if (r.type === 'exterior') {

          const program = _.get(r, 'program.name');
          const developer = _.get(r, 'program.developer.name');

          if (developer) path.push(developer);
          if (program) path.push(program);

        }
        return path.join(' ');
      })
      .then(r => this.model(COLL).updateOne({_id: tour_id}, {
        $set: {
          path: r,
          pathMustBeUpdated: false
        }
      }).exec())
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

module.exports = mongoose.model(COLL, tourSchema);
