const _ = require('lodash');
const mongoose = require('mongoose');
const autopopulate = require('mongoose-autopopulate');
const base = require('./base');

const COLL = 'Building';

const buildingSchema = new mongoose.Schema(Object.assign({}, base, {
  user: {type: String, ref: 'User'},
  group: {type: String, ref: 'Group'},

  name: {type: String, required: true},
  description: String,

  path: String,
  pathMustBeUpdated: Boolean,

  program: {type: String, ref: 'Program', autopopulate: true},
  isPublished: Boolean
}), {
  timestamps: true
});
buildingSchema.plugin(autopopulate);

buildingSchema.statics.assignProgram = function (buildings, program_id) {
  return this.model(COLL).updateMany(
    {_id: {$in: buildings}},
    {
      $set: {
        program: program_id,
        pathMustBeUpdated: true
      }
    }).exec();
};

buildingSchema.statics.clearProgram = function (program_id) {
  return this.model(COLL).updateMany(
    {program: program_id},
    {
      $set: {
        program: '',
        pathMustBeUpdated: true
      }
    }).exec();
};

buildingSchema.statics.updatePath = function (building_id) {
  return new Promise((resolve, reject) => {
    this.model(COLL).findById(building_id).exec()
      .then(r => {
        const path = [];
        const program = _.get(r, 'program.name');
        const developer = _.get(r, 'program.developer.name');

        if (developer) path.push(developer);
        if (program) path.push(program);

        return path.join(' ');
      })
      .then(r => this.model(COLL).updateOne({_id: building_id}, {
        $set: {
          path: r,
          pathMustBeUpdated: false
        }
      }).exec())
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

module.exports = mongoose.model(COLL, buildingSchema);
