/**
 * The base for mongoose schemas, with _ids as normal strings.
 */

const shortid = require('shortid');

const base = {
  _id: {type: String, default: shortid.generate}
};

module.exports = base;
