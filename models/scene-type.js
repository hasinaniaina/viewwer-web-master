const mongoose = require('mongoose');
const base = require('./base');

const locales = {
  fr: {type: String, default: ''},
  en: {type: String, default: ''}
};

const localesWeight = {
  fr: {type: Number, default: 0},
  en: {type: Number, default: 0}
};

const SceneType = new mongoose.Schema(Object.assign({}, base, {
  name: {type: String, required: true},

  locales: locales,
  localesWeight: localesWeight,

  mobile: Boolean,

  icon: String
}), {
  timestamps: true
});

module.exports = mongoose.model('SceneType', SceneType);
