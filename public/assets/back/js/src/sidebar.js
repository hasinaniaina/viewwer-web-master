$(function () {

  var sidebar = $('#sidebar');
  if (!sidebar.length) return;

  var requestsCount = sidebar.find('.control.requests .count-new');
  var apptoursCount = sidebar.find('.control.apptours .count-new');
  var supportCount = sidebar.find('.control.support-messages .count-new');

  if (requestsCount.length) {
    $.get('/admin/requests/count/new')
      .then(function (r) {
        if (!r || !r.count) return;
        requestsCount.html(r.count);
        requestsCount.show();
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  if (apptoursCount.length) {
    $.get('/admin/apptours/count/new')
      .then(function (r) {
        if (!r || !r.count) return;
        apptoursCount.html(r.count);
        apptoursCount.show();
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  if (supportCount.length) {
    $.get('/api/messages/unreads/count')
      .then(function (r) {
        if (!r || !r.count) return;
        supportCount.html(r.count);
        supportCount.show();
      })
      .catch(function (e) {
        console.error(e);
      });
  }

});
