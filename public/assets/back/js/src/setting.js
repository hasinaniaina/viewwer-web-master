$(function () {
  var panel = $('.setting.panel');
  if (!panel.length) return;

  var SWAL_SUCCESS = {
    type: 'success',
    position: 'center',
    showConfirmButton: true,
    buttonsStyling: false,
    confirmButtonClass: 'btn btn-default'
  };

  var SWAL_FAIL = {
    type: 'error',
    position: 'center',
    showConfirmButton: true,
    buttonsStyling: false,
    confirmButtonClass: 'btn btn-default'
  };

  panel.find('.rebuild-paths').each(function () {
  })
    .on('click', function () {
      var b = $(this);
      b.addClass('busy');
      $
        .post('/admin/settings/rebuild-paths', {})
        .then(function () {
          app.swal(SWAL_SUCCESS);
        })
        .catch(function (e) {
          app.swal(SWAL_FAIL);
          console.error(e);
        })
        .always(function () {
          b.removeClass('busy');
        });
    });

  panel.find('.rebuild-counters').each(function () {
  })
    .on('click', function () {
      var b = $(this);
      b.addClass('busy');
      $
        .post('/admin/settings/rebuild-counters', {})
        .then(function () {
          app.swal(SWAL_SUCCESS);
        })
        .catch(function (e) {
          app.swal(SWAL_FAIL);
          console.error(e);
        })
        .always(function () {
          b.removeClass('busy');
        });
    });

  panel.find('.fix').each(function () {
  })
    .on('click', function () {
      var b = $(this);
      b.addClass('busy');
      $
        .post('/admin/settings/fix', {})
        .then(function () {
          app.swal(SWAL_SUCCESS);
        })
        .catch(function (e) {
          app.swal(SWAL_FAIL);
          console.error(e);
        })
        .always(function () {
          b.removeClass('busy');
        });
    });

  panel.find('.mail-test').each(function () {
  })
    .on('click', function () {
      var b = $(this);
      b.addClass('busy');
      $
        .post('/admin/settings/mail-test', {email: $('input[name="mailTest"]').val()})
        .then(function () {
          app.swal(SWAL_SUCCESS);
        })
        .catch(function (e) {
          app.swal(SWAL_FAIL);
          console.error(e);
        })
        .always(function () {
          b.removeClass('busy');
        });
    });

  panel.find('.fcm-test').each(function () {
  })
    .on('click', function () {
      var b = $(this);
      b.addClass('busy');
      $
        .post('/admin/settings/fcm-test', {token: $('input[name="fcmTest"]').val()})
        .then(function () {
          app.swal(SWAL_SUCCESS);
        })
        .catch(function (e) {
          app.swal(SWAL_FAIL);
          console.error(e);
        })
        .always(function () {
          b.removeClass('busy');
        });
    });
});
