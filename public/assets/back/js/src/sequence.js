$(function () {
  var panel = $('.sequence.panel');
  if (!panel.length) return;

  // to have place for dragging
  var h_reserve = 75;

  function recalculateHeight() {
    var sequence = panel.find('.list-sequence');
    var scenetypes = panel.find('.list-scenetypes');

    var s_minh = 0;
    $.each(sequence.children(), function (i, v) {
      s_minh += $(v).outerHeight(true);
    });

    var st_minh = 0;
    $.each(scenetypes.children(), function (i, v) {
      st_minh += $(v).outerHeight(true);
    });

    var maxh = Math.max(s_minh, st_minh);
    sequence.height(maxh + h_reserve);
    scenetypes.height(maxh + h_reserve);
  }

  recalculateHeight();

  panel.find('.list-sequence, .list-scenetypes').sortable({
    connectWith: '.connected',
    opacity: 0.5,
    items: '.scene-type',
    receive: function (event, ui) {
      var st = ui.item;
      if ($(this).hasClass('list-sequence')) {
        st.append('<input name="sceneTypesSequence" value="' + st.data('id') + '" class="hidden">');
        st.addClass('active');
      } else {
        var hidden = st.find('input[name="sceneTypesSequence"]');
        if (hidden.length) hidden.remove();
        st.removeClass('active');
      }
      recalculateHeight();
    }
  });

});
