$(function() {
  var panel = $('.apptour.panel');
  if (!panel.length) return;

  var addressInput = panel.find('input[name="address"]');
  var mapContainer = panel.find('.map');
  var locationLng = panel.find('input[name="location_lng"]');
  var locationLat = panel.find('input[name="location_lat"]');

  if (!locationLng.val()) {
    locationLng.val(2.2137);
    locationLat.val(46.2276);
  }

  var center = {lat: parseFloat(locationLat.val()), lng: parseFloat(locationLng.val())};

  var autocomplete = new google.maps.places.Autocomplete(addressInput[0], {
    types: ['geocode']
  });

  var map = new google.maps.Map(mapContainer[0], {
    zoom: 17,
    center: center,
    scrollwheel: false
  });

  var marker = new google.maps.Marker({
    position: center,
    map: map,
    draggable: true
  });

  map.addListener('click', function(e) {
    marker.setPosition(e.latLng);
  });

  autocomplete.addListener('place_changed', function() {
    var place = autocomplete.getPlace();

    if (place.geometry) {
      map.setCenter(place.geometry.location);
      marker.setPosition(place.geometry.location);
      map.setZoom(17);
    }
  });

  marker.addListener('position_changed', function() {
    var position = marker.getPosition();
    locationLng.val(position.lng());
    locationLat.val(position.lat());
  });

});
