$(function() {
  var back_button = $('.back-button');
  if (!back_button.length) return;

  back_button.on('click', function() {
    window.history.back();
  });
});
