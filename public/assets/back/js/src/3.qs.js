$(function () {

  app.qs = {
    toString: function (query) {
      return window.Qs.stringify(query);
    }
  };

  Object.defineProperty(app.qs, 'current', {
    get: function () {
      return window.Qs.parse(document.location.search.substr(1));
    }
  });

});
