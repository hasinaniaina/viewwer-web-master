$(function () {

  var panel = $('.apartment.panel');
  if (!panel.length) return;

  var buildingSelect = panel.find('select[name="building"]');

  var exteriorSettingsButton = panel.find('.exterior-settings-button');
  var exteriorFormGroup = panel.find('.exterior-settings-group');
  var exteriorSceneInput = panel.find('input[name="exteriorScene"]');
  var exteriorPolygonsInput = panel.find('input[name="exteriorPolygons"]');
  var exteriorSettingsModal = $('#exterior-settings-modal');
  var exteriorTourContainer = exteriorSettingsModal.find('.tour-exterior-settings');
  var scenesContainer = exteriorTourContainer.find('.scenes');

  app.tours = {};
  var tourId;

  changeBuilding(buildingSelect.val());

  buildingSelect.on('change', function () {
    changeBuilding(this.value);
  });

  function changeBuilding(building_id) {
    if (!building_id){
      disableExteriorSettings();
      return;
    }

    app
      .fetch({
        url: '/admin/buildings/' + building_id + '/exterior'
      })
      .then(function (r) {
        if (r.status === 'not_found') {
          disableExteriorSettings();
          return;
        }
        enableExteriorSettings();
        exteriorFormGroup.show();
        rebuildScenes(r);
        var scene = _.find(r.scenes, {_id: exteriorSceneInput.val()});
        loadExteriorTour(r, scene ? scene._id : '', exteriorPolygonsInput.val());
      })
      .catch(function (e) {
        console.error(e);
        disableExteriorSettings();
        exteriorFormGroup.hide();
      });
  }

  function disableExteriorSettings() {
    exteriorSettingsButton.attr('disabled', 'disabled');
  }

  function enableExteriorSettings() {
    exteriorSettingsButton.removeAttr('disabled');
  }

  exteriorSettingsButton.on('click', function () {
    exteriorSettingsModal.modal({show: true, keyboard: false});
  });

  exteriorSettingsModal.find('form').on('submit', function (e) {
    e.preventDefault();

    var scene = exteriorSettingsModal.find('.scene.active').data('_id');

    exteriorSceneInput.val(scene);
    exteriorPolygonsInput.val(JSON.stringify(app.tours[tourId].tourPolygons));

    exteriorSettingsModal.modal('hide');
  });

  function rebuildScenes(tour) {
    scenesContainer.empty();
    _.each(tour.scenes, function (scene) {
      if (!scene.layer || !scene.layer.isBaseLayer) return;
      var active = '';
      if (scene.isMain) {
        active = 'active';
      }
      scenesContainer.append('<div class="scene ' + active + '"' +
        ' data-_id="' + scene._id + '">' +
        '<div class="scene-icons">' +
        '<i class="scene-icon-main far fa-heart"/>' +
        '<i class="scene-icon-polygon far fa-hexagon"/>' +
        '</div>' +
        '<img src="/storage/tours/' + tour._id + '/scenes/' + scene._id + '/thumbnail.jpg" alt="">' +
        '<span class="title">' + scene.name + '</span></div>');
    });
  }

  function loadExteriorTour(tour, main_scene, polygons) {

    removepano('krpano-' + tourId);
    $('#panorama').empty();

    tourId = tour._id;
    var tourMainScene = main_scene;
    var tourPolygons = [];

    if (!_.find(tour.scenes, {_id: tourMainScene})) tourMainScene = '';
    try {
      tourPolygons = JSON.parse(polygons);
    } catch (e) {
    }

    app.tours[tourId] = {
      tourPolygons: tourPolygons,
      container: exteriorTourContainer,
      init: function () {
        if (!app.tours[tourId].container.length) return;

        embedpano({
          xml: '/storage/tours/' + tourId +
          '/final/panorama.xml?nohotspots=true' +
          '&noplan=true&novr=true&polygonal_editor=true&mainscene=' + tourMainScene + '&prevent_cache=' + new Date().getTime(),
          target: 'panorama',
          id: 'krpano-' + tourId,
        });

        app.tours[tourId].krpano = $('#krpano-' + tourId)[0];

        var leftSidebar = app.tours[tourId].container.find('.sidebar.left');
        leftSidebar.addClass('active');
      },
      loadPolygon: function (scene_id) {
        var p = _.find(tourPolygons, {scene: scene_id});
        if (p) {
          app.tours[tourId].krpano.call('pe_create_polygon();');
          for (var i = 0; i < p.points.length; i++) {
            app.tours[tourId].krpano.set('hotspot[polygon].point[' + i + '].ath', p.points[i].ath);
            app.tours[tourId].krpano.set('hotspot[polygon].point[' + i + '].atv', p.points[i].atv);
          }
        }
      },
      savePolygon: function (scene_id) {
        var currentPoints = app.tours[tourId].krpano.get('hotspot[polygon].point');

        var currentPoly = {
          scene: scene_id,
          points: []
        };

        if (!_.isEmpty(currentPoints)) {
          for (var i = 0; i < currentPoints.count; i++) {
            var ath = app.tours[tourId].krpano.get('hotspot[polygon].point[' + i + '].ath');
            var atv = app.tours[tourId].krpano.get('hotspot[polygon].point[' + i + '].atv');
            currentPoly.points.push({
              ath: ath,
              atv: atv
            });
          }
        }

        var polyToChange = _.find(tourPolygons, {scene: scene_id});
        if (polyToChange) {
          if (!_.isEmpty(currentPoints)) {
            polyToChange.points = currentPoly.points;
          } else {
            _.pull(tourPolygons, polyToChange);
          }
        } else {
          if (!_.isEmpty(currentPoints)) tourPolygons.push(currentPoly);
        }
      },
      changePolygonEvent: function (scene_id) {
        app.tours[tourId].savePolygon(scene_id);
      },
      createPolygonEvent: function (scene_id) {
        app.tours[tourId].container.find('.scene').each(function () {
          if ($(this).data('_id') === scene_id) {
            $(this).find('.scene-icon-polygon').show();
            return false;
          }
        });
        app.tours[tourId].savePolygon(scene_id);
      },
      removePolygonEvent: function (scene_id) {
        app.tours[tourId].container.find('.scene').each(function () {
          if ($(this).data('_id') === scene_id) {
            $(this).find('.scene-icon-polygon').hide();
            return false;
          }
        });
        app.tours[tourId].savePolygon(scene_id);
      },
      openScene: function (scene_id) {
        app.tours[tourId].container.find('.scene').each(function () {
          if (_.find(tourPolygons, {scene: $(this).data('_id')})) {
            $(this).find('.scene-icon-polygon').show();
          }

          if ($(this).data('_id') === scene_id) {
            $(this).find('.scene-icon-main').show();
            $(this).siblings().find('.scene-icon-main').hide();
          }

          if ($(this).data('_id') === scene_id) {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var scenes = app.tours[tourId].container.find('.scenes');
            var scroll = scenes.scrollTop() + $(this).offset().top - scenes.offset().top - 10;
            scenes.animate({
              scrollTop: scroll
            });
          }
        });

        // load scene
        app.tours[tourId].krpano.call('loadscene("scene_' + scene_id + '", null, null, BLEND(0.5))');

        app.tours[tourId].krpano.set('PE_SCENE_ID', scene_id);
        app.tours[tourId].krpano.set('PE_TOUR_ID', tourId);

        // load polygon for scene
        app.tours[tourId].loadPolygon(scene_id);
      }
    };

    app.tours[tourId].init();

    $(function () {
      /* change scenes */
      app.tours[tourId].container.on('click', '.scene', function () {
        var control = $(this);
        app.tours[tourId].openScene(control.data('_id'));
      });

      /* toggle sidebars */
      app.tours[tourId].container.on('click', '.sidebar .toggle .slide-in', function () {
        var sidebar = $(this).parents('.sidebar');
        sidebar.addClass('active');
      });

      app.tours[tourId].container.on('click', '.sidebar .toggle .slide-out', function () {
        $(this).parents('.sidebar').removeClass('active');
      });
    });
  }

});
