$(function () {
  var panel = $('.programs.panel');
  if (!panel.length) return;

  $('.programs.panel .actions .create').on('click', function () {
    $('#create-program-name').modal().find('input:first')[0].focus();
  });

  var exportButton = panel.find('.export');
  var exportForm = $('#export-programs');
  var exportSubmitButton = exportForm.find('.submit');

  var selectSold = exportForm.find('select[name="sold"]');
  var selectTypes = exportForm.find('select[name="types"]');
  var selectPrograms = exportForm.find('select[name="programs"]');

  selectTypes.selectpicker(_.assign({}, app.SELECT_PICKER_DEFAULTS, {
    noneSelectedText: selectTypes.data('none-selected-text')
  }));

  selectPrograms.selectpicker(_.assign({}, app.SELECT_PICKER_DEFAULTS, {
    noneSelectedText: selectPrograms.data('none-selected-text')
  }));

  exportButton.on('click', function () {
    exportForm.modal();
  });

  exportSubmitButton.on('click', function () {
    var request = {};

    if (selectSold.val()) request.sold = selectSold.val();
    request.types = selectTypes.val();
    request.programs = selectPrograms.val();

    window.open('/admin/programs/export' + '?' + app.qs.toString(request));
    exportForm.modal('hide');
  });

});
