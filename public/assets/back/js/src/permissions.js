$(function() {
  var permissions = $('#permissions-selects');
  if (!permissions.length) return;

  var users = permissions.data('users');

  var selectUser = permissions.find('select[name="user"]');
  var selectGroup = permissions.find('select[name="group"]');

  selectUser.selectpicker(app.SELECT_PICKER_DEFAULTS);
  selectGroup.selectpicker(app.SELECT_PICKER_DEFAULTS);

  selectUser.on('changed.bs.select', function () {
    var u = _.find(users, {user_id: this.value});
    if (u && u.group_id) {
      selectGroup.selectpicker('val', u.group_id);
    } else {
      selectGroup.selectpicker('val', '');
    }
  });
});
