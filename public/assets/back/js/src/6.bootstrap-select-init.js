$(function () {

  app.SELECT_PICKER_DEFAULTS = {
    dropupAuto: false,
    noneSelectedText: '',
    liveSearch: true,
    liveSearchPlaceholder: app.__('Search'),
    noneResultsText: app.__('Nothing found'),
    deselectAllText: app.__('Deselect All'),
    doneButtonText: app.__('Close'),
    size: 5,
    style: 'btn-link'
  };

  $('.selectpicker').selectpicker(app.SELECT_PICKER_DEFAULTS);
});
