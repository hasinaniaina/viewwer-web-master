$(function () {
  app.components = app.components || {};

  function Grid(container) {
    var instance = this;

    this.container = $(container);
    this.container.data('grid', this);

    this.table = this.container.find('table');

    this.thead = this.table.find('thead');
    this.tbody = this.table.find('tbody');
    this.tfoot = this.table.find('tfoot');
    this.pager = this.tfoot.find('.pages ol');
    this.limitSelect = this.tfoot.find('select[name="limit"]');

    this.settings = this.table.data('settings');

    this.thead.on('keyup', 'input', function (e) {
      //if (e.keyCode == 13) instance.applyFilters();
      // apply filters on typing
      instance.applyFilters();
    });

    this.thead.on('change', 'select', this.applyFilters.bind(this));

    this.thead.on('click', 'i.clear-filter', function () {
      var params = instance.table.data('params') || {filter: {}};

      delete params.filter;

      instance.thead.find('input').val('');
      instance.thead.find('select').val('-1');
      instance.fetch(params);
    });

    this.thead.on('click', '.sortable', function () {
      var control = $(this);
      var direction = control.is('.asc') ? 'desc' : 'asc';
      var params = instance.table.data('params') || {filter: {}};
      var suffix = (direction == 'desc' ? '-' : '');

      instance.thead.find('.sortable').removeClass('asc desc');
      control.addClass(direction);
      params.sort = suffix + control.data('by');
      instance.fetch(params);
    });

    this.pager.on('click', '.page:not(.current)', function () {
      var el = $(this);
      var documentsPerPage = parseInt(instance.limitSelect.val(), 10);
      var page = parseInt(el.text(), 10);
      var skip = (page - 1) * documentsPerPage;
      var params = instance.table.data('params');

      params.skip = skip;

      instance
        .fetch(params)
        .then(function () {
          el.addClass('current').siblings().removeClass('current');
        });
    });

    if (this.settings.limit === false) this.limitSelect.addClass('hide');

    this.limitSelect.on('change', function () {
      instance.applyFilters();
    });

    if (!location.search) {
      this.applyFilters();
      return;
    }

    var queryStringSettings = Qs.parse(location.search.replace('?', ''));
    if (queryStringSettings) {
      if (queryStringSettings.filter) {
        _.each(queryStringSettings.filter, function (value, key) {
          instance.thead.find('*[name="' + key + '"]').val(value.replace('regex:', ''));
        });
      }
      if (queryStringSettings.sort) {
        instance.thead.find('.sortable').removeClass('asc desc');
        var direction = 'asc';
        var field = queryStringSettings.sort;
        if (queryStringSettings.sort.substring(0, 1) === '-') {
          direction = 'desc';
          field = queryStringSettings.sort.substring(1);
        }
        var control = instance.thead.find('[data-by="' + field + '"]');
        if (control.length) control.addClass(direction);
      }
      instance.fetch(queryStringSettings);
    } else {
      this.applyFilters();
    }
  }

  Grid.prototype.setState = function (state) {
    this.container.attr('data-state', state);

    if (state === 'fetching') {
      this.container.addClass('loading');
    } else {
      this.container.removeClass('loading');
    }
  };

  Grid.prototype.applyFilters = function () {
    var params = this.table.data('params') || {filter: {}};
    params.filter = params.filter || {};

    this.thead.find('input[type="text"], select').each(function () {
      var el = $(this);
      var name = el.attr('name');
      var value = el.val();

      if (el.is('input') && value) {
        params.filter[name] = 'regex:' + value;
      } else if (el.is('select') && value != -1) {
        params.filter[name] = value;
      } else {
        delete params.filter[name];
      }
    });

    if (this.settings.limit !== false) params.limit = this.limitSelect.val();

    // always show the first page when changing filters
    params.skip = 0;


    return this.fetch(params);
  };

  Grid.prototype.fetch = function (params) {
    var instance = this;

    var requestParams = $.extend(true, {
      sort: instance.settings.sort,
      filter: instance.settings.filter
    }, params);


    if (history.replaceState) {
      history.replaceState(null, null,
        window.location.protocol + '//' +
        window.location.host +
        window.location.pathname + '?' +
        $.param(requestParams));
    }

    var stateTimer = setTimeout(function () {
      instance.setState('fetching');
    }, 250);

    return $.get(this.settings.dataSource, requestParams)
      .then(function (result) {
        clearTimeout(stateTimer);

        instance.tbody.html(result.rows);

        // reformat fields
        instance.tbody.find('.date').each(function () {
          var d = $(this).data('date');
          var f = $(this).data('format');
          $(this).html(moment(d).format(f));
        });

        if (!result.totalCount) {
          instance.tbody.html('<tr><td colspan="' + instance.thead.find('tr:first th').length + '"><p>' + instance.table.data('no-results') + '</p></td></tr>');
        }

        /* pager */

        if (instance.settings.limit !== false) {
          var documentsPerPage = parseInt(instance.limitSelect.val(), 10);
          var totalNuberOfPages = Math.ceil(result.totalCount / documentsPerPage);
          var currentPage = params ? params.skip / documentsPerPage + 1 : 1;
          var firstPage = currentPage > 5 && totalNuberOfPages > 10 ? currentPage - 4 : 1;
          var lastPage = currentPage + 9 < totalNuberOfPages ? currentPage + 9 : totalNuberOfPages;

          instance.pager.empty();

          for (var i = firstPage; i <= lastPage; i++) {
            var el = $('<li class="page">' + i + '</li>');

            if (i == currentPage) el.addClass('current');

            el.appendTo(instance.pager);
          }

          if (currentPage > 5 && totalNuberOfPages > 10) {
            instance.pager.prepend('<li>…</li>');
            instance.pager.prepend('<li class="page">1</li>');
          }

          if (lastPage < totalNuberOfPages) {
            instance.pager.append('<li>…</li>');
            instance.pager.append('<li class="page">' + totalNuberOfPages + '</li>');
          }
        }

        if (instance.pager.find('li').length) {
          instance.pager.show();
        } else {
          instance.pager.hide();
        }

        instance.table.data('params', params);
        instance.setState('idle');
      })
      .catch(function (jqXHR, status, message) {
        alert(message);
        instance.setState('error');
      });
  };

  Grid.prototype.refresh = function () {
    this.fetch(this.table.data('params'));
  };

  app.components.Grid = Grid;

  $('.grid').each(function () {
    new Grid(this);
  });
});
