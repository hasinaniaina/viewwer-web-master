$(function() {

  app.FileManager = function (options) {
    if (!options) throw new Error('Missing options');
    if (!options.path) throw new Error('Missing path');
    if (!options.container) throw new Error('Missing container option');
    if (!options.container.length) return;

    var container = options.container;

    var listContainer = this.listContainer = container.find('.list');
    var pathContainer = container.find('.path');

    var refreshButton = container.find('.actions .refresh');
    var deleteButton = container.find('.actions .delete');
    var createDirectoryButton = container.find('.actions .create-directory');
    var fileInput = container.find('input[type="file"]');

    var selected = options.selected || [];

    var currentPath;

    function list(path) {
      container.addClass('loading');

      var currentQuery = app.qs.current;
      var currentUrl = location.pathname + location.search;

      var newUrlParams = _.chain(currentQuery).clone().extend({path: path}).value();
      var newUrl = location.pathname + '?' + app.qs.toString(newUrlParams);

      if (options.pushState && currentUrl != newUrl) {
        history.pushState(null, null, newUrl);
      }

      currentPath = path;
      listContainer.empty();
      pathContainer.empty();

      var links = [];
      var steps = [];

      _.each(path.split('/'), function (step) {
        steps.push(step);

        var stepQuery = _.chain(currentQuery).clone().extend({path: steps.join('/')}).value();
        var stepHref = location.pathname + '?' + app.qs.toString(stepQuery);

        links.push('<a href="' + stepHref + '">' + step + '</a>');
      });

      pathContainer.append(links.join('/'));

      return app
        .fetch('./files/api/' + path + '/')
        .then(function (result) {
          if (!result.ok || !result.directory.length) {
            container.removeClass('loading');
            return;
          }

          var resources = _.orderBy(result.directory, ['isDirectory', 'name'], ['desc', 'asc']);

          _.each(resources, function (resource) {
            if (options.directories === false && resource.isDirectory) return;

            var extensionMatch = resource.name.match(/\.(.+)$/);
            var extension = extensionMatch ? extensionMatch[1].toLowerCase() : '';
            var resourcePath = '/storage/files/' + (currentPath + '/' + resource.name).replace('./', '');

            var el = $('<div />', {
              'class': resource.isDirectory ? 'directory' : 'file ' + extension
            });

            var link = $('<a />', {
              href: resourcePath
            });

            if (_.includes(['jpg', 'png', 'gif', 'svg'], extension)) {
              link.css({
                backgroundImage: 'url(' + resourcePath + ')'
              });
            }

            var description = $('<div class="description" />');

            description.append('<span class="name">' + resource.name + '</span>');
            description.append('<span class="size">' + filesize(resource.size) + '</span>');

            el.append(link, description);
            el.data('resource', resource);
            el.appendTo(listContainer);

            if (_.some(selected, {name: resource.name})) select(el);

            container.removeClass('loading');
          });
        });
    }

    // upload new files right after they are picked
    fileInput.on('change', function () {
      if (!this.files[0]) return;

      container.addClass('loading');

      var checked = [];

      _.each(this.files, function (file) {
        var filePath = './files/api/' + currentPath + '/' + _.deburr(file.name).replace(/[^a-zA-Z0-9\-.]/g, '_');

        checked.push(app.fetch(filePath));
      });

      Promise
        .all(checked)
        .then(function (results) {
          var existing = _.filter(results, {ok: true});

          function uploadFiles() {
            var formData = new FormData();

            _.each(fileInput[0].files, function (file) {
              formData.append('files[]', file);
            });

            $
              .ajax({
                url: './files/api/' + currentPath,
                method: 'post',
                cache: false,
                data: formData,
                processData: false,
                contentType: false
              })
              .then(function () {
                list(currentPath);
              })
              .catch(function (xhr, status, message) {
                alert(message);
              })
              .always(function () {
                fileInput.val(null);
                container.removeClass('loading');
              });
          }

          if (!existing.length) return uploadFiles();

          var question = app.__('These files already exist: %s. Overwrite?', _.map(existing, 'file.name'));

          if (confirm(question)) {
            uploadFiles();
          } else {
            container.removeClass('loading');
          }
        });
    });

    // refresh list
    refreshButton.on('click', function () {
      list(currentPath);
    });

    // create new directory
    createDirectoryButton.on('click', function () {
      var name = prompt(app.__('Directory name'));

      if (!name) return;

      name = _.deburr(name).replace(/[^a-zA-Z0-9\-.]/g, '_');

      app
        .fetch('./files/api/' + currentPath + '/' + name, 'POST')
        .then(function () {
          list(currentPath);
        });
    });

    // delete selected
    deleteButton.on('click', function () {
      if (!confirm(app.__('Are you sure?'))) return;

      deleteButton.addClass('disabled');

      listContainer.find('.active').each(function () {
        var el = $(this);
        var path = currentPath + '/' + el.find('.name').text();

        app
          .fetch('./files/api/' + path, 'DELETE')
          .then(function () {
            el.remove();
          });
      });
    });

    listContainer.on('click', 'a', function (e) {
      var link = $(this);
      var el = link.parent();
      var clicks = link.data('clicks') || 0;

      link.data('clicks', clicks++);

      setTimeout(function () {
        link.removeData('clicks');
      }, 350);

      if (clicks == 2) return false;

      if (options.multiple === false || !e.ctrlKey && !e.shiftKey) {
        deselect(listContainer.find('.active').not(el));
        listContainer.find('.active').not(el).removeClass('lastselected');
      }

      if (options.multiple !== false && e.shiftKey) {
        var lastSelectedEl = listContainer.find('.lastselected');
        var lastSelectedIdx = lastSelectedEl.index();
        var elIdx = el.index();

        deselect(listContainer.find('>div').not(lastSelectedEl));

        if (elIdx < lastSelectedIdx) {
          select(lastSelectedEl.prevUntil(el).add(el));
        } else {
          select(lastSelectedEl.nextUntil(el).add(el));
        }
      } else {
        if (el.is('.active')) {
          deselect(el);
        } else {
          select(el);
        }

        el.toggleClass('lastselected').siblings().removeClass('lastselected');
      }

      if (listContainer.find('.active').length) {
        deleteButton.removeClass('disabled');
      } else {
        deleteButton.addClass('disabled');
      }

      return false;
    });

    listContainer.on('dblclick', 'a', function () {
      if (options.dblClick === false) return;

      var link = $(this);
      var el = link.parent();

      if (el.is('.directory')) {
        list(currentPath + '/' + link.parent().find('.name').text());
        deleteButton.addClass('disabled');
      } else {
        window.open(link.attr('href'), '_blank');
      }

      return false;
    });

    // disabled due to file change on outside input arrow keys pressing
    /*
    $(document).on('keydown', function (e) {
      var activeEls = listContainer.find('.active');

      if (e.keyCode == 27 && activeEls.length) deselect(activeEls);

      if (e.keyCode == 13 && activeEls.length == 1) activeEls.find('a').dblclick();

      if (e.keyCode == 39) {
        if (activeEls.is(listContainer.find('>div:last'))) return;

        if (activeEls.length == 1) {
          deselect(activeEls);
          activeEls.removeClass('lastselected');
          select(activeEls.next());
          activeEls.next().addClass('lastselected');
        } else if (!activeEls.length) {
          select(listContainer.find('>div:eq(1)'));
        }
      }

      if (e.keyCode == 37) {
        if (activeEls.is(listContainer.find('>div:first'))) return;

        if (activeEls.length == 1) {
          deselect(activeEls);
          activeEls.removeClass('lastselected');
          select(activeEls.prev());
          activeEls.prev().addClass('lastselected');
        } else if (!activeEls.length) {
          select(listContainer.find('>div:last'));
        }
      }

      if (e.keyCode == 46 && activeEls.length) {
        deleteButton.click();
      }

      if (options.multiple !== false && e.keyCode == 65 && e.ctrlKey) {
        select(listContainer.find('>div'));

        return false;
      }
    });
    */

    if (options.pushState) {
      $(window).on('popstate', function () {
        list(app.qs.current.path);
      });
    }

    var select = this.selectEls = function (els) {
      els.addClass('active').each(function () {
        var resource = $(this).data('resource');

        if (!_.some(selected, {name: resource.name})) selected.push(resource);

        if (options.onSelected) options.onSelected(resource);
      });
    };

    var deselect = this.deselectEls = function (els) {
      if (!els) els = listContainer.find('> div');

      els.removeClass('active').each(function () {
        var resource = $(this).data('resource');

        selected = _.reject(selected, {name: resource.name});

        if (options.onSelected) options.onDeselected(resource);
      });
    };

    list(options.path);
  };

  app.FileManager.prototype.select = function (resource) {
    this.deselectEls();

    var target = this.listContainer.find('.name').filter(function () {
      return $(this).text() === resource.name;
    }).parents('.file:first');

    this.selectEls(target);
  };

});
