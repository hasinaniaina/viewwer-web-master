$(function () {
  $('.file-uploader').each(function () {

    var container = $(this);

    var SWAL_MESSAGE = {
      type: 'error',
      position: 'center',
      showConfirmButton: true,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-default'
    };

    var parameters = container.data('parameters');

    var currentUrl = parameters.currentUrl;
    var uploadUrl = parameters.uploadUrl;
    var removeUrl = parameters.removeUrl;
    var inputValue = parameters.inputValue;
    var format = parameters.format;
    var maxSize = parameters.maxSize;

    var fileUpload = container.find('.file-upload');

    var hiddenInput = container.find('.file-hidden-input');
    if (inputValue) hiddenInput.val(inputValue);

    var limitDescription = container.find('.limit');
    limitDescription.html(filesize(maxSize, {symbols: app.filesizeSymbols}));

    // actions
    var actionUpload = container.find('.action.upload');
    var actionRemove = container.find('.action.remove');

    var fileResult = container.find('.file-result');
    var placeholder = container.find('.placeholder');
    if (inputValue) {
      fileResult.show();
      if (removeUrl) actionRemove.show();
    } else {
      placeholder.show();
    }

    actionUpload.on('click', function (e) {
      e.preventDefault();
      fileUpload.click();
    });

    actionRemove.on('click', function (e) {
      e.preventDefault();
      if (!removeUrl) return;

      app.fetch({
        url: removeUrl,
        data: {
          name: hiddenInput.val()
        },
        method: 'POST'
      })
        .then(function () {
          hiddenInput.val('');
          fileUpload.val('');

          fileResult.attr('href', '');
          fileResult.hide();
          actionRemove.hide();

          placeholder.show();
        })
        .catch(function (e) {
          console.error(e);
        });
    });

    fileUpload.on('change', function () {
      if (this.files[0].type !== format) {
        app.swal(_.assign({}, SWAL_MESSAGE, {
          text: app.__('Wrong file format!')
        }));
        fileUpload.val('');
        return;
      }

      if (this.files[0].size > maxSize) {
        app.swal(_.assign({}, SWAL_MESSAGE, {
          text: app.__('File is too large!')
        }));
        fileUpload.val('');
        return;
      }

      container.addClass('loading');

      var reader = new FileReader();
      reader.readAsDataURL(this.files[0]);

      reader.onload = function (e) {
        app.fetch({
          url: uploadUrl,
          data: {
            file: e.target.result
          },
          method: 'POST'
        })
          .then(function (r) {
            hiddenInput.val(r.name);
            placeholder.hide();

            fileResult.attr('href', currentUrl + r.name);
            fileResult.show();

            if (removeUrl) actionRemove.show();
          })
          .catch(function (e) {
            console.error(e);
            app.swal(_.assign({}, SWAL_MESSAGE, {
              text: app.__('An error has occured while file upload!')
            }));
          })
          .always(function () {
            container.removeClass('loading');
          });
      };
    });
  });
});


