$(function () {
  var panel = $('.sign-up.panel');
  if (!panel.length) return;

  var form = panel.find('form');
  var password = form.find('input[name="password"]');
  var passwordStrength = form.find('.pass-strength-text');

  password.on('input', function () {
    var result = zxcvbn(password.val());

    if (password.val() !== '') {
      passwordStrength.html(app.PASS_STRENGTH[result.score].text);
      passwordStrength.css('color', app.PASS_STRENGTH[result.score].color);
    } else {
      passwordStrength.html('');
      passwordStrength.css('color', 'unset');
    }
  });
});
