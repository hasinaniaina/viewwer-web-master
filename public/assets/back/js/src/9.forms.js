$(function() {
  /* handle file uploads */

  var fileTemplate = $('#file-template').text();

  function filesPicked(e) {
    var control = $(e.target);
    var form = control.parents('form:first');
    var filesContainer = form.find('.files');
    var listContainer = form.find('.files .list');
    var files = e.target.files || e.originalEvent.dataTransfer.files;
    var format = filesContainer.data('format');

    function uploadFile(file) {
      var el = $(fileTemplate);
      var formData = new FormData();

      filesContainer.data('uploading', filesContainer.data('uploading') + 1);

      el.data('file', file).find('.name').text(file.name);
      el.find('.status .uploaded').text(filesize(0, {symbols: app.filesizeSymbols}));
      el.find('.status .total').text(filesize(file.size, {symbols: app.filesizeSymbols}));
      el.appendTo(listContainer);

      formData.append('image', file);

      $
        .ajax({
          url: form.find('.files').data('handler'),
          method: 'post',
          cache: false,
          data: formData,
          processData: false,
          contentType: false,
          xhr: function () {
            var xhr = new window.XMLHttpRequest();

            el.data('xhr', xhr);

            xhr.upload.addEventListener('progress', function (e) {
              if (e.lengthComputable) {
                el.find('progress').attr('value', e.loaded / e.total * 100);
                el.find('.status .uploaded').text(filesize(e.loaded, {symbols: app.filesizeSymbols}));
                el.find('.status .total').text(filesize(e.total, {symbols: app.filesizeSymbols}));
              }
            }, false);

            return xhr;
          }
        })
        .then(function (response) {
          filesContainer.data('uploaded', filesContainer.data('uploaded') + 1);
          el.data('finished', true);
          el.find('.status').text(el.find('.status').data('finished'));
          el.data('response', response);
        })
        .catch(function (xhr, status, message) {
          el.find('.status').addClass('error').text(message);
        })
        .always(function () {
          filesContainer.data('uploading', filesContainer.data('uploading') - 1);
          form.find('.drop input').val('');

          if (filesContainer.data('uploading') == 0 && filesContainer.data('uploaded') > 0) {
            form.find('button[type="submit"]').prop('disabled', false);
          }
        });
    }

    form.find('button[type="submit"]').prop('disabled', true);

    for (var i = 0; i < files.length; i++) {
      var file = files[i];

      // filter wrong format
      if (format) {
        if (file.type === format) uploadFile(file);
      } else {
        uploadFile(file);
      }
    }

    $(this).removeClass('active');

    return false;
  }

  $('form .files .select').on('click', function () {
    $(this).parents('.files:first').find('input').click();
  });

  $('form .files .drop')
    .on('dragover', function () {
      $(this).addClass('active');

      return false;
    })
    .on('dragleave', function () {
      $(this).removeClass('active');

      return false;
    })
    // ie11 fix
    .on('dragenter', function () {
      return false;
    })
    .on('drop', filesPicked);

  $('form .files input').on('change', filesPicked);

  $(document).on('click', 'form .files .abort', function () {
    var fileEl = $(this).parents('.file:first');
    var filesContainer = fileEl.parents('.files:first');
    var form = filesContainer.parents('form');

    if (fileEl.data('finished')) {
      filesContainer.data('uploaded', filesContainer.data('uploaded') - 1);
    } else {
      fileEl.data('xhr').abort();
      filesContainer.data('uploading', filesContainer.data('uploading') - 1);
    }

    if (!filesContainer.data('uploaded')) form.find('button[type="submit"]').prop('disabled', true);

    fileEl.remove();
  });

  /* handle regular submit */

  $(document).on('submit', 'form[action]:not([data-native])', function () {
    var form = $(this);
    var submitButton = form.find('button[type="submit"]');

    form.find('.errors, .success').remove();
    submitButton.prop('disabled', true).addClass('busy');

    app
      .fetch({
        url: form.attr('action'),
        method: form.attr('method'),
        data: form.serialize()
      })
      .then(function (response) {
        if (response.goback) {
          if (document.referrer) {
            window.location.replace(document.referrer);
          } else if (response.redirect) {
            location.href = response.redirect;
          }
        } else if (response.redirect) {
          location.href = response.redirect;
        } else if (response.message) {
          form.replaceWith('<p>' + response.message + '</p>');
        } else {
          form.trigger('submitted', response);
        }
      })
      .catch(function (err) {
        $('<div class="errors">' + err.message + '</div>').insertBefore(form.find('> .actions'));
        submitButton.prop('disabled', false).removeClass('busy');
      });

    return false;
  });
});
