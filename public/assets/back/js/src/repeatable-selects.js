$(function () {
  var repeater = $('.repeater-select');

  repeater.repeater({
    show: function () {
      var select = $(this).find('select');
      select.addClass('.selectpicker');
      select.selectpicker(app.SELECT_PICKER_DEFAULTS);
      $(this).slideDown();
    },
    hide: function (remove) {
      $(this).slideUp(remove);
    },
  });

  // default bootstrap-select
  var select = repeater.find('select');
  if (select.length) {
    select.addClass('.selectpicker');
    select.selectpicker(app.SELECT_PICKER_DEFAULTS);
  }
});
