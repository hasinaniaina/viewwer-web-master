$(function () {
  var panel = $('.reset-password.panel');
  if (!panel.length) return;

  var form = panel.find('form');
  var fakeSubmit = form.find('.fake-submit');
  var maskedPassword = form.find('.masked-pass');
  var hiddenPassword = form.find('.hidden-pass');
  var hiddenSubmit = form.find('.hidden-submit');

  fakeSubmit.on('click', function () {
    if (!form[0].checkValidity()) {
      /*
        If the form is invalid, submit it.
        The form won't actually submit;
        this will just cause the browser to
        display the native HTML5 error messages
        */
      hiddenSubmit.click();
    } else {
      hiddenPassword.val(maskedPassword.val());
      form.submit();
    }
  });

  var passwordStrength = form.find('.pass-strength-text');

  maskedPassword.on('input', function () {
    var result = zxcvbn(maskedPassword.val());

    if (maskedPassword.val() !== '') {
      passwordStrength.html(app.PASS_STRENGTH[result.score].text);
      passwordStrength.css('color', app.PASS_STRENGTH[result.score].color);
    } else {
      passwordStrength.html('');
      passwordStrength.css('color', 'unset');
    }
  });
});
