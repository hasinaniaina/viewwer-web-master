$(function () {
  var dev_del_all = $("#developer-del-all").prop('disabled',true);
  var prog_del_all = $("#program-del-all").prop('disabled',true);
  var request_del_all = $("#request-del-all").prop('disabled',true);

  function deleteAllData(button,url,modal){
	var id = [];

	button.prop('disabled',false)

  	button.on("click", function(){

		modal.modal('show')

		$(".confirm-del-all").on("click", function(){
			$(".documents table tbody tr").each(function(){
				id.push($(this).attr('data-id'))
			});

			$.ajax({
			url : url,
			type : 'POST', // On désire recevoir du HTML
			data : {"data":id},
			success : function(code_html, statut){ // code_html contient le HTML renvoyé
				window.location.reload()
			}
		});

		})
	});
  }

  setTimeout(function(){
	var data_id = $(".documents table tbody tr").first().attr('data-id')	
	var delete_all_modal_developer = $('#delete-all-developer')
	var delete_all_modal_program = $('#delete-all-program')
	var delete_all_modal_request = $('#delete-all-request')

	if(dev_del_all && data_id != undefined){
		deleteAllData(dev_del_all,'./developers/developer/deleteAll?confirmed=1',delete_all_modal_developer);
	}
  
	if(prog_del_all && data_id != undefined){
		deleteAllData(prog_del_all,'./programs/program/deleteAll?confirmed=1',delete_all_modal_program);
	}
  
	if(request_del_all && data_id != undefined){
		deleteAllData(request_del_all,'./requests/request/deleteAll?confirmed=1',delete_all_modal_request);
	}
  },500)

});
