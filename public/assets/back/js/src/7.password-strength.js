$(function() {

  app.PASS_STRENGTH = {
    0: {
      text: app.__('Worst password'),
      color: 'red'
    },
    1: {
      text: app.__('Bad password'),
      color: 'red'
    },
    2: {
      text: app.__('Weak password'),
      color: 'orange'
    },
    3: {
      text: app.__('Good password'),
      color: 'green'
    },
    4: {
      text: app.__('Strong password'),
      color: 'green'
    }
  };

});
