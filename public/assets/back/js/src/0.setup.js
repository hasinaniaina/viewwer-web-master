$(function () {
  var filesizeSymbols = {
    fr: {
      B: 'o',
      KB: 'ko',
      MB: 'Mo',
      GB: 'Go',
      TB: 'To'
    },
    en: {
      B: 'B',
      KB: 'KB',
      MB: 'MB',
      GB: 'GB',
      TB: 'TB'
    }
  };

  app.locale = $('html').attr('lang');

  app.filesizeSymbols = filesizeSymbols[app.locale];

  app.onSwitch = function (selector, e) {
    if (e) e.preventDefault();

    var target = $(selector);
    var settings = target.data('switch');
    var span = target.find('span');

    target.addClass('busy');

    app
      .fetch('./switch', settings)
      .then(function (data) {
        span.html(data.on ? span.data('off') : span.data('on'));
      })
      .always(function () {
        target.removeClass('busy');
      });
  };

  // for ie
  if (!String.prototype.includes) {
    String.prototype.includes = function (search, start) {
      if (typeof start !== 'number') {
        start = 0;
      }

      if (start + search.length > this.length) {
        return false;
      } else {
        return this.indexOf(search, start) !== -1;
      }
    };
  }

  if (navigator.appVersion.indexOf('MSIE 10') !== -1) {
    app.swal = function (options) {
      alert(options.text);
    };
  } else {
    app.swal = swal;
  }

  // disable cache
  $.ajaxSetup({cache: false});

});
