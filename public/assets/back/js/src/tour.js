
$(function () {
  var panel = $('.tour.panel');

  if (!panel.length) return;

  var tour = panel.data('tour');
  tour.updateOnlyProperties = true;

  var tourTitle = $('.tour-title');

  var sceneTypes = panel.data('sceneTypes');
  var layerTypes = panel.data('layerTypes');

  _.each(layerTypes, function (lt) {
    // mark base layer
    if (lt.isBaseLayer) lt.name += ' *';
  });

  var sequences = panel.data('sequences');

  var THUMB_WIDTH = panel.data('thumb-width');
  var THUMB_HEIGHT = panel.data('thumb-height');



  var uploadImagesModal = $('#upload-tour-images');
  var sceneTitleTemplate = $('#scene-title-template').text();
  var selectMainSceneModal = $('#select-tour-main-scene');
  var sceneMainTemplate = $('#scene-main-template').text();
  var adjustThumbnailModal = $('#adjust-scene-thumbnail');

  /* STEPS  */
  //#region STEP 1 Upload images
  panel.find('.step.upload-images > .btn').on('click', function () {
    tour.updateOnlyProperties = false;
    uploadImagesModal.modal('show');
  });

  /* file submission logic is defined in forms.js, this is post-upload handler */
  uploadImagesModal.find('form').on('submit', function () {
    uploadImagesModal.modal('hide');

    // cleanup
    $('.modal.scene').remove();


    // disable next steps
    panel.find('.step:not(".upload-images")').addClass('disabled').find('.btn').attr('disabled', true);

    //step 1 done
    panel.find('.step.upload-images').addClass('done');
    panel.find('.step.edit-scene').removeClass('disabled').find('.btn').removeAttr('disabled');

    return false;
  });

  function fillTypesSelect(select, types) {
    select.empty();
    select.append($('<option>').val('').text(''));
    select.append(
      _.map(types, function (st) {
        return $('<option>').val(st._id).text(st.name);
      })
    );
  }
  //#endregion

  //#region Step 2 scene title modal submission
  panel.find('.step.edit-scene > .btn').on('click', function () {
    tour.updateOnlyProperties = false;
    $('.modal.scene').remove();

    tour.scenes = [];

    // process uploaded files
    var fileEls = uploadImagesModal.find('.files .list .file');

    var index = -1;

    fileEls.each(function (idx) {
      var el = $(this);
      var scene = el.data('response').scene;

      if(index == -1)
      {
        if(!el.attr("data-finished") && typeof scene.name==="undefined") index = idx;
      }

      tour.scenes.push(scene);

      // create scene properties modals
      var modal = $(sceneTitleTemplate);
      if(fileEls.length == idx + 1){
        modal.find('button[type="submit"]').text(app.__('Proceed'));        
      }
      modal.find('img').attr('src', scene.file.path);
      modal.find('.number .value').text(idx + 1 + '/' + fileEls.length);
      modal.find('.file .name').text(scene.file.originalName);
      modal.find('.file .size').text('(' + filesize(scene.file.size) + ')');
      modal.find('input[name="name"]').val(scene.name || '');

      var sceneTypeSelect = modal.find('select[name="scene-type"]');
      fillTypesSelect(sceneTypeSelect, sceneTypes);
      sceneTypeSelect.val(scene.type);

      var layerTypeSelect = modal.find('select[name="layer-type"]');
      fillTypesSelect(layerTypeSelect, layerTypes);
      layerTypeSelect.val(scene.layer);

      modal.data('scene', scene);

      modal.appendTo('#content');
   
     
    });

    $('.selectpicker').selectpicker(app.SELECT_PICKER_DEFAULTS);

    $('.modal.scene:eq('+index+')').modal('show').find('input')[0].focus();
  });


  $(document).on('click', '#select-scenes-properties .btn.back-button', function () {
    var form = $(this).closest('form');
    var modal = form.parents('.modal');
    var prevModal = modal.prevAll('.modal.scene:eq(0)');
    if (prevModal.length) {
      modal.modal('hide');
      prevModal.modal('show').find('input')[0].focus();
    }
  });

  $(document).on('change', '#select-scenes-properties select[name="scene-type"]', function (e, triggered) {
    var value = this.value;
    var form = $(this).closest('form');
    var modal = form.parents('.modal');
    var layerTypeSelect = modal.find('select[name="layer-type"]');

    // change scene name to scene type name if selected by user
    if (!triggered) {
      var t = $(this).find('option:selected').text();
      var sceneName = modal.find('input[name="name"]');
      if (t) sceneName.val(t);
    }

    var scene = modal.data('scene');
    var sceneIndex = _.findIndex(tour.scenes, {_id: scene._id});

    var tourScenes = _.slice(tour.scenes, 0, (sceneIndex > -1) ? sceneIndex : tour.scenes.length);
    var filteredL = _.cloneDeep(layerTypes);

    filteredL = _.filter(filteredL, function (l) {
      var ts = _.find(tourScenes, function (s) {
        return (s.type === value && s.layer === l._id);
      });
      return !(ts);
    });

    fillTypesSelect(layerTypeSelect, filteredL);
    layerTypeSelect.val(scene.layer);
  });

  $(document).on('submit', '.modal.scene form', function () {
    var form = $(this);
    var modal = form.parents('.modal');
    var nextModal = modal.nextAll('.modal.scene:eq(0)');
    var scene = modal.data('scene');

    var sceneTypeSelect = modal.find('select[name="scene-type"]');
    var layerTypeSelect = modal.find('select[name="layer-type"]');

    scene.name = modal.find('input[name="name"]').val();
    scene.type = sceneTypeSelect.val();
    scene.layer = layerTypeSelect.val();

    if (nextModal.length) {
      var nextSceneType = nextModal.find('select[name="scene-type"]');
      var nextScene = nextModal.data('scene');

      fillTypesSelect(nextSceneType, sceneTypes);
      nextSceneType.val(nextScene.type);
      nextSceneType.trigger('change', true);
      nextSceneType.selectpicker('refresh');

      // restore old text on back and next navigation
      nextModal.find('input[name="name"]').val(nextScene.name);

      // no base scenes error only on last scene
      nextModal.find('.no-base-scenes.errors').addClass('hidden');

      modal.modal('hide');
      nextModal.modal('show').find('input')[0].focus();

    } else {
      var selectable = filterBaseScenes(tour.scenes, layerTypes);
      // if no base scene
      if (_.isEmpty(selectable)) {
        modal.find('.no-base-scenes.errors.hidden').removeClass('hidden');
        return false;
      }
      modal.modal('hide');
      // empty value if main scene select modal was cancelled
      selectMainSceneModal.find('select[name="sequence"]').val('');
      //step 2 done
      panel.find('.step.edit-scene').addClass('done');
      panel.find('.step.choose-mainscene').removeClass('disabled').find('.btn').removeAttr('disabled');
      panel.find('.step.scene-thumbnail').removeClass('disabled').find('.btn').removeAttr('disabled');
    }

    return false;
  });

  function fillMainSceneList(scenes) {
    scenes.forEach(function (scene) {
      var sceneMainEl = $(sceneMainTemplate);
      sceneMainEl.data('scene_id', scene._id);
      sceneMainEl.find('img').attr('src', scene.file.path);
      sceneMainEl.find('.name').text(scene.name);
      sceneMainEl.appendTo(selectMainSceneModal.find('.list'));

      // all not base scenes will be changed to isMain = false
      if (scene.isMain) {
        sceneMainEl.addClass('active');
        selectMainScene(tour.scenes, scene._id);
      }
    });

    // set default active scene
    if (!selectMainSceneModal.find('.list .scene.active').length) {
      selectMainSceneModal.find('.list .scene:eq(0)').addClass('active');
    }

    selectMainSceneModal.find('.list').sortable(
      {
        opacity: 0.5,
        placeholder: 'highlight'
      });
  }
  //#endregion
  //#region Step 3 main scene selection


  panel.find('.step.choose-mainscene > .btn').on('click', function () {
    tour.updateOnlyProperties = false;
    // create a list of scenes to pick the main from
    var selectable = filterBaseScenes(tour.scenes, layerTypes);
    //cleanup
    selectMainSceneModal.find('.list').empty();
    fillMainSceneList(selectable);

    selectMainSceneModal.modal('show');
  });
  selectMainSceneModal.on('click', '.scene', function () {
    $(this).addClass('active').siblings().removeClass('active');
    selectMainScene(tour.scenes, $(this).data('scene_id'));
  });

  selectMainSceneModal.find('select[name="sequence"]').on('change', function () {
    var selectable = filterBaseScenes(tour.scenes, layerTypes);
    var neworder = selectable;

    if (this.value) {
      var sequence = _.find(sequences, {_id: this.value});

      var reordered = [];
      _.each(sequence.sceneTypesSequence, function (sq) {
        reordered.push.apply(reordered, _.filter(selectable, {type: sq._id}));
      });

      var diff = _.differenceWith(selectable, reordered, function (o1, o2) {
        return (o1._id === o2._id);
      });
      neworder = _.concat(reordered, diff);
    }

    selectMainSceneModal.find('.list').sortable('destroy');
    selectMainSceneModal.find('.list').html('');

    fillMainSceneList(neworder);
  });

  selectMainSceneModal.find('form').on('submit', function () {

    // reordering scenes
    var new_order = [];
    selectMainSceneModal.find('.list').find('.scene').each(function () {
      var scene_id = $(this).data('scene_id');
      var is_main = $(this).hasClass('active');
      var s = _.find(tour.scenes, {_id: scene_id});
      s.isMain = is_main;
      new_order.push(s);
    });

    var diff = _.differenceWith(tour.scenes, new_order, function (o1, o2) {
      return (o1._id === o2._id);
    });

    tour.scenes = _.concat(new_order, diff);

    selectMainSceneModal.modal('hide');

    //step 3 done
    panel.find('.step.choose-mainscene').addClass('done');
    panel.find('.step.scene-thumbnail').removeClass('disabled').find('.btn').removeAttr('disabled');

    return false;
  });
  //#endregion
  //#region Step 4 adjust scene thumbnails

  var currentThumbSceneIdx;
  panel.find('.step.scene-thumbnail > .btn').on('click', function () {
    tour.updateOnlyProperties = false;
    currentThumbSceneIdx = 0;
    adjustSceneThumbnail(filterBaseScenes(tour.scenes, layerTypes)[currentThumbSceneIdx]);

  });

  function adjustSceneThumbnail(scene) {
    var scenePreview = adjustThumbnailModal.find('#scene-preview');

    scenePreview.empty().addClass('loading');
    adjustThumbnailModal.modal('show').find('.btn[type="submit"]').prop('disabled', true);
    if(currentThumbSceneIdx + 1 == filterBaseScenes(tour.scenes, layerTypes).length){
      adjustThumbnailModal.find('button[type="submit"]').text(app.__('Proceed'));                
    }
    adjustThumbnailModal.find('.number .value').text(
      (currentThumbSceneIdx + 1) + '/' + filterBaseScenes(tour.scenes, layerTypes).length
    );
    adjustThumbnailModal.find('.file .name').text(
      filterBaseScenes(tour.scenes, layerTypes)[currentThumbSceneIdx].file.originalName
    );

    var preview_xml = ['/storage/tours', tour._id, 'scenes', scene._id, 'preview/preview.xml'].join('/');

    app
      .fetch('./tours/' + tour._id + '/generate-scene-preview', {
        sceneId: scene._id,
        fileName: scene.file.path.split('/').pop()
      })
      .then(function () {
        // clever-cloud network storage workaround
        return new Promise(function (resolve, reject) {
          var interval = 1000;
          var retries = 5;
          var fetchXml = function () {
            $.ajax({
              type: 'GET',
              url: preview_xml,
            }).done(function () {
              resolve();
            }).fail(function (e) {
              console.error(e);
              if (retries > 0) {
                retries--;
                setTimeout(fetchXml, interval);
                return;
              }
              return reject('no_preview_xml');
            });
          };
          fetchXml();
        });
      })
      .then(function () {
        scenePreview.removeClass('loading');

        var previewSettings = _.mapKeys(scene.previewSettings, function (value, key) {
          return 'view.' + key;
        });

        embedpano({
          xml: preview_xml + '?prevent_cache=' + new Date().getTime(),
          target: 'scene-preview',
          webglsettings: {
            preserveDrawingBuffer: true
          },
          onready: function (krpano) {
            adjustThumbnailModal.data('krpano-id', krpano.getAttribute('id'));
            if (tour.plan && tour.plan.image) {
              krpano.call('sp_load_planmap(' + '/storage/tours/' + tour._id + '/' + tour.plan.image + '?' + (new Date()).getTime() + ');');
            }
            if (scene.previewSettings && !isNaN(scene.previewSettings.headingoffset)) {
              krpano.call('set(layer[sp_radar].headingoffset,' + scene.previewSettings.headingoffset + ');');
              krpano.call('set(layer[sp_radar].headingoffsetb,' + scene.previewSettings.headingoffset + ');');
            }
            adjustThumbnailModal.find('.btn[type="submit"]').prop('disabled', false);
          },
          vars: previewSettings
        });
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  /* thumbnail saving */
  adjustThumbnailModal.find('form').on('submit', function () {
    var scene = filterBaseScenes(tour.scenes, layerTypes)[currentThumbSceneIdx];

    if (scene.isMain) tour.thumbnail = ['/storage/tours', tour._id, 'scenes', scene._id, 'thumbnail.jpg'].join('/');

    var krpano = adjustThumbnailModal.find('#' + adjustThumbnailModal.data('krpano-id'))[0];

    // screenshot
    krpano.call('makescreenshot(' + THUMB_WIDTH + ', ' + THUMB_HEIGHT + ')');
    var sc = krpano.get('screenshot');
    // fallback
    if (!sc) sc = adjustThumbnailModal.find('canvas')[0].toDataURL('image/jpeg');
    scene.thumbnail = sc;

    // TODO sometimes headingoffset is NaN
    var headingoffset = krpano.get('layer[sp_radar].headingoffset');
    if (isNaN(headingoffset)) {
      headingoffset = 0;
      console.error('heading offset is NaN');
    }

    scene.previewSettings = {
      hlookat: krpano.get('view.hlookat'),
      vlookat: krpano.get('view.vlookat'),
      fov: krpano.get('view.fov'),
      headingoffset: headingoffset
    };

    adjustThumbnailModal.modal('hide');

    currentThumbSceneIdx++;

    if (currentThumbSceneIdx < filterBaseScenes(tour.scenes, layerTypes).length) {
      adjustSceneThumbnail(filterBaseScenes(tour.scenes, layerTypes)[currentThumbSceneIdx]);
    } else {
      panel.find('.step.scene-thumbnail').addClass('done');
      if (tour.plan && tour.plan.image) {
        panel.find('.step.create-planspots').removeClass('disabled').find('.btn').removeAttr('disabled');
      }
      panel.find('.step.create-planspots').removeClass('disabled').find('.btn').removeAttr('disabled');
      panel.find('.step.create-hotspots').removeClass('disabled').find('.btn').removeAttr('disabled');
      panel.find('.step.define-properties').removeClass('disabled').find('.btn').removeAttr('disabled');
    }

    return false;
  });
  //#endregion

  /* STEP 5 */

  // plan upload
  var planUploadModal = $('#plan-upload');
  var planUpload = planUploadModal.find('input[name="plan-upload"]');
  var planUploadContainer = planUploadModal.find('.plan-container');
  var planUploadImage = planUploadModal.find('.plan-image');
  var planCropperOptions = {viewMode: 2};
  var planCropOptions = {minWidth: 1000, maxWidth: 1000};

  var PLANSPOT_OFFSET = 25;
  var PLANSPOT_IMAGE = '/assets/shared/images/planspot.svg';
  var PLANSPOT_IMAGE_ATTRIBUE = '/assets/shared/images/planspot_attribue.svg';

  var planspotInitialCoords = {};
  var planspotMoved = false;

  // plan editor
  var planEditorModal = $('#plan-editor');
  var planEditorImage = planEditorModal.find('.plan-image');
  var planPlaceholder = planEditorModal.find('.placeholder');
  var planActionUpload = planEditorModal.find('.action.upload');
  var planActionRemove = planEditorModal.find('.action.remove');
  var planspotsContainer = planEditorModal.find('.planspots');
  var planspotMenuTemplate = $('#planspot-menu-template').text();
  var planspotAdjustModal = $('#planspot-adjust');

  panel.find('.step.create-planspots > .btn').on('click', function () {
    planEditorModal.modal('show');
  });

  if (!tour.plan) tour.plan = {};
  if (tour.plan && tour.plan.image) {
    planUploadContainer.find('.placeholder').remove();
    planReload(planEditorImage, tour.plan.image);
    planspotsReload();
    planActionRemove.show();
  } else {
    planPlaceholder.show();
  }

  function planReload(container, filename) {
    container.attr('src', '/storage/tours/' + tour._id + '/' + filename + '?' + (new Date()).getTime());
    container.css({height: 'auto'});
  }

  planUploadModal.find('form').on('submit', function (e) {
    e.preventDefault();

    planUploadContainer.addClass('loading');
    var imageData = planUploadImage.cropper('getCroppedCanvas', planCropOptions).toDataURL('image/jpeg');

    app
      .fetch({
        url: './tours/' + tour._id + '/save-plan',
        data: {
          plan: imageData
        },
        method: 'POST'
      })
      .then(function (r) {
        planUploadImage.cropper('destroy');

        tour.plan.width = r.width;
        tour.plan.height = r.height;
        tour.plan.image = r.image;
        planUploadContainer.removeClass('loading');

        planEditorImage.attr('src', imageData);
        planEditorImage.css({height: 'auto'});
        planEditorImage.show();
        planspotsReload();

        planPlaceholder.hide();
        planActionRemove.show();

        planUploadModal.modal('hide');
        planEditorModal.modal('show');
      })
      .catch(function (e) {
        console.error(e);
      });
  });

  planUpload.change(function () {
    if (!this.files || !this.files[0]) return;

    if (!/^image\/\w+$/.test(this.files[0].type)) {
      app.swal({
        type: 'error',
        text: app.__('Please choose an image file'),
        position: 'center',
        showConfirmButton: true,
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-default'
      });
      planUpload.val('');
      return;
    }

    var reader = new FileReader();
    reader.onload = function (e) {
      planUpload.val('');

      planUploadContainer.find('.placeholder').remove();
      planUploadImage.cropper('destroy').attr('src', e.target.result).cropper(planCropperOptions);
      planUploadContainer.removeClass('loading');

      planEditorModal.modal('hide');
      planUploadModal.modal('show');
    };

    planUploadContainer.addClass('loading');
    reader.readAsDataURL(this.files[0]);
  });

  planUploadModal.on('hide.bs.modal', function () {
    planEditorModal.modal('show');
  });

  planspotAdjustModal.on('hide.bs.modal', function () {
    planEditorModal.modal('show');
  });

  planActionUpload.on('click', function (e) {
    e.preventDefault();
    planUpload.click();
  });

  planActionRemove.on('click', function (e) {
    e.preventDefault();

    app
      .fetch({
        url: './tours/' + tour._id + '/remove-plan',
        data: {},
        method: 'POST'
      })
      .then(function () {
        tour.plan = {};
        planPlaceholder.show();
        planEditorImage.css({height: '0px'});
        planEditorImage.hide();
        planActionRemove.hide();
      })
      .catch(function (e) {
        console.error(e);
      });
  });

  function planspotsReload() {
    if (tour.plan && !_.isEmpty(tour.planspots)) {
      planspotsContainer.empty();

      tour.planspots.forEach(function (planspot) {

        //CHANGER LA COULEUR DU CAMERA EN FONCTION DE SON ATTRIBUTION
        var selectable = filterBaseScenes(tour.scenes, layerTypes);
        var PLANSPOT_IMG;

        selectable.forEach(function (s) {
          if (planspot.scene === s._id) { PLANSPOT_IMG = PLANSPOT_IMAGE_ATTRIBUE; }
          else { PLANSPOT_IMG = PLANSPOT_IMAGE; }
        });
        //FIN

        if (!planspot) return;
        var planspotEl = $('<img class="planspot" draggable="false" src="' + PLANSPOT_IMG + '" />');

        var ratio = tour.plan.width / planEditorImage.width();

        var imageWidth = planEditorImage.width();
        var imageHeight = tour.plan.height / ratio;

        var top = planspot.top;
        var left = planspot.left;
        if (top < 0) top = 0;
        if (top > imageHeight - PLANSPOT_OFFSET * 2) top = imageHeight - PLANSPOT_OFFSET * 2;
        if (left < 0) left = 0;
        if (left > imageWidth - PLANSPOT_OFFSET * 2) left = imageWidth - PLANSPOT_OFFSET * 2;

        planspotEl
          .css({top: top, left: left})
          .data('planspot', _.assign({}, planspot, {loaded: true}));

        planspotsContainer.append(planspotEl);
      });
    }
  }

  function planspotAdjust(scene) {

    if (!scene) return;
    planspotAdjustModal.data('scene_id', scene._id);

    var scenePreview = planspotAdjustModal.find('#planspot-adjust-scene');
    scenePreview.empty().addClass('loading');

    planEditorModal.modal('hide');
    planspotAdjustModal.modal('show');

    var preview_xml = ['/storage/tours', tour._id, 'scenes', scene._id, 'preview/preview.xml'].join('/');

    app
      .fetch('./tours/' + tour._id + '/generate-scene-preview', {
        sceneId: scene._id,
        fileName: scene.file.path.split('/').pop()
      })
      .then(function () {
        // clever-cloud network storage workaround
        return new Promise(function (resolve, reject) {
          var interval = 1000;
          var retries = 5;
          var fetchXml = function () {
            $.ajax({
              type: 'GET',
              url: preview_xml,
            }).done(function () {
              resolve();
            }).fail(function (e) {
              console.error(e);
              if (retries > 0) {
                retries--;
                setTimeout(fetchXml, interval);
                return;
              }
              return reject('no_preview_xml');
            });
          };
          fetchXml();
        });
      })
      .then(function () {
        scenePreview.removeClass('loading');

        var previewSettings = _.mapKeys(scene.previewSettings, function (value, key) {
          return 'view.' + key;
        });

        embedpano({
          xml: preview_xml + '?prevent_cache=' + new Date().getTime(),
          target: 'planspot-adjust-scene',
          webglsettings: {
            preserveDrawingBuffer: true
          },
          onready: function (krpano) {
            planspotAdjustModal.data('krpano-id', krpano.getAttribute('id'));
            if (tour.plan && tour.plan.image) {
              krpano.call('sp_load_planmap(' + '/storage/tours/' + tour._id + '/' + tour.plan.image + '?' + (new Date()).getTime() + ');');
            }
            if (scene.previewSettings && !isNaN(scene.previewSettings.headingoffset)) {
              krpano.call('set(layer[sp_radar].headingoffset,' + scene.previewSettings.headingoffset + ');');
              krpano.call('set(layer[sp_radar].headingoffsetb,' + scene.previewSettings.headingoffset + ');');
            }
          },
          vars: previewSettings
        });
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  planspotAdjustModal.find('form').on('submit', function () {
    planspotAdjustModal.modal('hide');

    try {

      var scene = _.find(tour.scenes, {_id: planspotAdjustModal.data('scene_id')});

      var krpano = planspotAdjustModal.find('#' + planspotAdjustModal.data('krpano-id'))[0];

      // TODO sometimes headingoffset is NaN
      var headingoffset = krpano.get('layer[sp_radar].headingoffset');
      if (isNaN(headingoffset)) {
        headingoffset = 0;
        console.error('heading offset is NaN');
      }

      scene.previewSettings.headingoffset = headingoffset;

    } catch (e) {
      console.error(e);
    }

    return false;
  });

  planEditorModal.find('form').on('submit', function (e) {
    e.preventDefault();

    var planspots = [];

    planEditorModal.find('.planspots .planspot').each(function () {
      var planspotEl = $(this);

      if (!planspotEl.data('planspot').scene) return;

      planspots.push({
        scene: planspotEl.data('planspot').scene,
        left: parseInt(planspotEl.css('left'), 10),
        top: parseInt(planspotEl.css('top'), 10)
      });
    });

    tour.planspots = planspots;

    panel.find('.step.create-planspots').addClass('done');
    planEditorModal.modal('hide');
  });

  // add new planspot when clicking the plan image
  planEditorModal.on('click', 'img.plan-image', function (e) {
    var menu = $('.planspot-menu');

    if (menu.is(':visible')) return menu.remove();

    var image = $(this);
    var container = image.parent();
    var containerOffset = container.offset();

    var planspotEl = $('<img class="planspot" draggable="false" src="' + PLANSPOT_IMAGE + '"/>');

    var left = e.pageX - containerOffset.left - PLANSPOT_OFFSET;
    var top = e.pageY - containerOffset.top - PLANSPOT_OFFSET;
    var imageWidth = image.width();
    var imageHeight = image.height();

    if (top < 0) top = 0;
    if (top > imageHeight - PLANSPOT_OFFSET * 2) top = imageHeight - PLANSPOT_OFFSET * 2;
    if (left < 0) left = 0;
    if (left > imageWidth - PLANSPOT_OFFSET * 2) left = imageWidth - PLANSPOT_OFFSET * 2;

    planspotEl
      .css({
        left: left,
        top: top
      })
      .data('planspot', {});

    planspotsContainer.append(planspotEl);
    createDraggablePlanspot(planspotEl);

    planspotEl.trigger('mouseup');
  });

  function createDraggablePlanspot(planspot) {
    new Draggable(planspot[0], {
      limit: planEditorImage[0],
      useGPU: false,
      onDragStart: function () {
        planspotsContainer.find('.planspot-menu').remove();
      },
      onDragEnd: function () {
        planspotMoved = true;
      }
    });
  }

  planEditorModal.on('mouseenter', '.planspot', function () {
    if ($(this).data('planspot').loaded) {
      $(this).data('planspot').loaded = false;
      createDraggablePlanspot($(this));
    }
  });

  planEditorModal.on('mousedown', '.planspot', function (e) {
    planspotMoved = false;
    planspotInitialCoords = {pageX: e.pageX, pageY: e.pageY};
  });

  planEditorModal.on('mouseup', '.planspot', function (e) {
    ['pageX', 'pageY'].forEach(function (prop) {
      if (Math.abs(e[prop] - planspotInitialCoords[prop]) > 3) planspotMoved = true;
    });

    if (planspotMoved) return;

    var planspotEl = $(this);
    var container = planspotEl.parent();

    container.find('.planspot-menu').remove();
    planspotEl.toggleClass('active').siblings().removeClass('active');

    if (!planspotEl.is('.active')) return;

    var menu = $(planspotMenuTemplate);
    var planspot = planspotEl.data('planspot');

    var selectable = filterBaseScenes(tour.scenes, layerTypes);

    selectable.forEach(function (s) {
      var el = $('<li><span>' + s.name + '</span></li>');
      el.data('scene_id', s._id);
      if (planspot.scene === s._id) {el.addClass('active');}
      menu.find('li.load-scene ul').append(el);
    });

    menu
      .appendTo(container)
      .css({
        left: planspotEl.position().left - menu.outerWidth() / 2 + planspotEl.outerWidth() / 2,
        top: planspotEl.position().top + planspotEl.outerHeight() + 10
      })
      .on('click', 'li.load-scene li', function () {
        var el = $(this);
        planspot.scene = el.data('scene_id');
        planspotEl.removeClass('active');
        planspotEl.attr('src', PLANSPOT_IMAGE_ATTRIBUE);   //CHANGER L IMAGE QUAND ON A SELECTIONNEE UNE SCENE
        menu.remove();
      })
      .on('click', 'li.adjust', function () {
        planspotAdjust(_.find(tour.scenes, {_id: planspot.scene}));
        menu.remove();
      })
      .on('click', 'li.delete', function () {
        planspotEl.remove();
        menu.remove();
      });
  });

  planEditorModal.on('click', '.planspot-menu li.with-options', function () {
    $(this).toggleClass('expanded');
  });

  planEditorModal.on('click', '.planspot-menu li.with-options li', function (e) {
    e.stopPropagation();
  });

  /* STEP 6 */

  var HOTSPOT_OFFSET = 15;

  var sceneHotspotsTemplate = $('#scene-hotspots-template').text();
  var hotspotMenuTemplate = $('#hotspot-menu-template').text();

  panel.find('.step.create-hotspots > .btn').on('click', function () {
    $('.modal.hotspots').remove();

    var selectable = filterBaseScenes(tour.scenes, layerTypes);

    selectable.forEach(function (scene, idx) {
      var modal = $(sceneHotspotsTemplate);
      if(idx + 1 == selectable.length){
        modal.find('button[type="submit"]').text(app.__('Proceed'));                
      }
      modal.find('.image img').attr('src', scene.file.path);
      modal.find('.scene .number').text(idx + 1 + '/' + selectable.length);
      modal.find('.scene .title').text(scene.name);
      modal.data('scene', scene);

      if (tour.plan && tour.plan.image) {
        planReload(modal.find('.plan-image'), tour.plan.image);
      } else {
        modal.find('.plan-container').hide();
      }

      modal.appendTo('#content');

      if (scene.hotspots) {

        var hotspotsContainer = modal.find('.hotspots');

        scene.hotspots.forEach(function (hotspot) {

          var hotspotIcon = '';

          var otherSceneId = _.get(hotspot, 'action.loadScene.scene');
          if (otherSceneId) {
            var otherScene = _.find(tour.scenes, {_id: otherSceneId});
            if (otherScene && otherScene.type) {
              var otherSceneType = _.find(sceneTypes, {_id: otherScene.type});
              if (otherSceneType && otherSceneType.icon) hotspotIcon = otherSceneType.icon;
            }
          }

          if (hotspot.icon) hotspotIcon = hotspot.icon;

          var hotspotSrc = hotspotIcon ? '/storage/files/hotspots/' + hotspotIcon : '/assets/shared/images/marker.png';
          var hotspotEl = $('<img class="hotspot" draggable="false" src="' + hotspotSrc + '" alt="X" />');

          hotspotEl
            .css({left: hotspot.left, top: hotspot.top})
            .data('hotspot', _.assign({}, hotspot, {loaded: true}));

          hotspotsContainer.append(hotspotEl);
        });
      }

      if (idx == 0) modal.modal('show');
    });
  });

  // add new hotspot when clicking the scene image
  $(document).on('click', '.modal.hotspots .image > img', function (e) {
    var menu = $('.hotspot-menu');

    if (menu.is(':visible')) return menu.remove();

    var image = $(this);
    var container = image.parent();
    var hotspotsContainer = container.find('.hotspots');
    var containerOffset = container.offset();

    var hotspotSrc = '/assets/shared/images/marker.png';
    var hotspotEl = $('<img class="hotspot" draggable="false" src="' + hotspotSrc + '" alt="X" />');

    var left = e.pageX - containerOffset.left - HOTSPOT_OFFSET;
    var top = e.pageY - containerOffset.top - HOTSPOT_OFFSET;
    var imageWidth = image.width();
    var imageHeight = image.height();

    if (top < 0) top = 0;
    if (top > imageHeight - HOTSPOT_OFFSET * 2) top = imageHeight - HOTSPOT_OFFSET * 2;
    if (left < 0) left = 0;
    if (left > imageWidth - HOTSPOT_OFFSET * 2) left = imageWidth - HOTSPOT_OFFSET * 2;

    if(hotspotsContainer.find('.hotspot').length > 0) top = hotspotsContainer.find('.hotspot:first').position().top

    hotspotEl
      .css({
        left: left,
        top: top
      })
      .data('hotspot', {});

    hotspotsContainer.append(hotspotEl);
    createDraggableHotspot(hotspotEl);

    hotspotEl.trigger('mouseup');
  });

  var hotspotInitialCoords = {};
  var hotspotMoved = false;
  var hotspotModalModal = $('#hotspot-modal');
  var hotspotUrlModal = $('#hotspot-url');
  var hotspotChangeIconModal = $('#hotspot-change-icon');

  function createDraggableHotspot(hotspot) {
    var container = hotspot.parents('.image:first');
    new Draggable(hotspot[0], {
      limit: container[0],
      useGPU: false,
      onDragStart: function () {
        container.find('.hotspot-menu').remove();
      },
      onDragEnd: function () {
        hotspotMoved = true;
      }
    });
  }

  $(document).on('mouseenter', '.modal.hotspots .hotspot', function () {
    if ($(this).data('hotspot').loaded) {
      $(this).data('hotspot').loaded = false;
      createDraggableHotspot($(this));
    }
  });

  $(document).on('mousedown', '.modal.hotspots .hotspot', function (e) {
    hotspotMoved = false;
    hotspotInitialCoords = {pageX: e.pageX, pageY: e.pageY};
  });

  $(document).on('mouseup', '.modal.hotspots .hotspot', function (e) {
    ['pageX', 'pageY'].forEach(function (prop) {
      if (Math.abs(e[prop] - hotspotInitialCoords[prop]) > 3) hotspotMoved = true;
    });

    var hotspotEl = $(this);
    var modal = hotspotEl.parents('.modal:first');
    var scene = modal.data('scene');
    var container = hotspotEl.parent();

    if (hotspotMoved) 
    {
        var modal = $('.modal.hotspots form').parents('.modal');
        if(hotspotEl.hasClass('free_mode')) return;
        modal.find('.hotspots .hotspot').each(function () 
        {
            var temp_hotspot = $(this);
            if(temp_hotspot.hasClass('free_mode')) return;          
            temp_hotspot.css({top: hotspotEl.position().top}).data('hotspot', {});
            createDraggableHotspot(temp_hotspot);         
        });
      
        return;
    }

    container.find('.hotspot-menu').remove();
    hotspotEl.toggleClass('active').siblings().removeClass('active');

    hotspotModalModal
      .add(hotspotUrlModal)
      .add(hotspotChangeIconModal)
      .off('hidden.bs.modal')
      .on('hidden.bs.modal', function () {
        modal.modal('show');
      });

    if (!hotspotEl.is('.active')) return;

    var menu = $(hotspotMenuTemplate);
    var hotspot = hotspotEl.data('hotspot');

    if (tour.scenes.length == 1) {
      menu.find('li.load-scene').hide();
    } else {

      var selectable = filterBaseScenes(tour.scenes, layerTypes);

      selectable.forEach(function (tourScene) {
        if (tourScene._id == scene._id) return;

        var el = $('<li><span>' + tourScene.name + '</span></li>');

        el.data('scene', tourScene);

        if (_.get(hotspot, 'action.loadScene.scene') == tourScene._id) el.addClass('active');

        menu.find('li.load-scene ul').append(el);
      });
    }

    if (_.get(hotspot, 'action.loadScene')) menu.find('li.load-scene').addClass('active');
    if (_.get(hotspot, 'action.openModal')) menu.find('li.open-modal').addClass('active');
    if (_.get(hotspot, 'action.openUrl')) menu.find('li.open-url').addClass('active');

    if(hotspotEl.hasClass('free_mode')) menu.find('li #mode').attr('checked','checked');
    else menu.find('li #mode').removeAttr('checked');

    menu
      .appendTo(container)
      .css({
        left: hotspotEl.position().left - menu.outerWidth() / 2 + hotspotEl.outerWidth() / 2,
        top: hotspotEl.position().top + hotspotEl.outerHeight() + 10
      })
      .on('click', 'li.load-scene li', function () {
        var el = $(this);
        hotspot.action = {loadScene: {scene: el.data('scene')._id}};
        updateHotspotIconBySceneId(hotspotEl, el.data('scene')._id);
        hotspotEl.removeClass('active');
        menu.remove();
      })
      .on('click', 'li.open-modal', function () {
        hotspotModalModal.find('input').val(_.get(hotspot, 'action.openModal.heading', ''));
        hotspotModalModal.find('textarea').data('tinymce').setContent(_.get(hotspot, 'action.openModal.content', ''));
        modal.modal('hide');
        hotspotModalModal.data('hotspot', hotspot).modal('show');
        updateHotspotIconBySceneId(hotspotEl);
        hotspotEl.removeClass('active');
        menu.remove();
      })
      .on('click', 'li.open-url', function () {
        hotspotUrlModal.find('input').val(_.get(hotspot, 'action.openUrl.url', ''));
        modal.modal('hide');
        hotspotUrlModal.data('hotspot', hotspot).modal('show');
        updateHotspotIconBySceneId(hotspotEl);
        hotspotEl.removeClass('active');
        menu.remove();
      })
      .on('click', 'li.change-icon', function () {
        modal.modal('hide');
        hotspotChangeIconModal.data('hotspotEl', hotspotEl).modal('show');
        hotspotEl.removeClass('active');
        menu.remove();
      }).on('click', 'li #mode', function () {
        if($(this).is(':checked')) hotspotEl.addClass('free_mode');
        else hotspotEl.removeClass('free_mode');
        menu.remove();
      }).on('click', 'li.delete', function () {
        hotspotEl.remove();
        menu.remove();
      });
  });

  $(document).on('click', '.modal.hotspots .plan-button', function () {
    $('.modal.hotspots .plan-content').slideToggle('slow');
  });

  $(document).on('click', '.hotspot-menu li.with-options', function () {
    $(this).toggleClass('expanded');
  });

  $(document).on('click', '.hotspot-menu li.with-options li', function (e) {
    e.stopPropagation();
  });

  $(document).on('click', '.modal.hotspots .btn.back-button', function () {
    var form = $(this).closest('form');
    var modal = form.parents('.modal');
    var prevModal = modal.prevAll('.modal.hotspots:eq(0)');
    if (prevModal.length) {
      modal.modal('hide');
      prevModal.modal('show').find('input')[0].focus();
    }
  });

  function updateHotspotIconBySceneId(elem, scene_id) {
    if (!elem || elem.data('hotspot').icon) return;
    var hotspotIcon = '';
    if (scene_id) {
      var scene = _.find(tour.scenes, {_id: scene_id});
      if (scene && scene.type) {
        var sceneType = _.find(sceneTypes, {_id: scene.type});
        if (sceneType && sceneType.icon) hotspotIcon = sceneType.icon;
      }
    }
    if (hotspotIcon) {
      elem.attr('src', '/storage/files/hotspots/' + hotspotIcon);
    } else {
      elem.attr('src', '/assets/shared/images/marker.png');
    }
  }

  hotspotModalModal.find('form').on('submit', function () {
    hotspotModalModal.data('hotspot').action = {
      openModal: {
        heading: $(this).find('input').val(),
        content: $(this).find('textarea').val()
      }
    };

    hotspotModalModal.modal('hide');
    $('.hotspot-menu li.open-modal').addClass('active').siblings().removeClass('active').find('li').removeClass('active');

    return false;
  });

  hotspotUrlModal.find('form').on('submit', function () {
    hotspotUrlModal.data('hotspot').action = {
      openUrl: {
        url: $(this).find('input').val()
      }
    };

    $('.hotspot-menu li.open-url').addClass('active').siblings().removeClass('active').find('li').removeClass('active');
    hotspotUrlModal.modal('hide');

    return false;
  });

  var fileManager = new app.FileManager({
    container: hotspotChangeIconModal.find('.file-manager'),
    path: 'hotspots',
    dblClick: false,
    multiple: false,
    directories: false,
  });

  hotspotChangeIconModal.on('shown.bs.modal', function () {
    var hotspotEl = hotspotChangeIconModal.data('hotspotEl');

    fileManager.select({name: hotspotEl.data('hotspot').icon});
  });

  hotspotChangeIconModal.on('submit', 'form', function () {
    var hotspotEl = hotspotChangeIconModal.data('hotspotEl');
    var selected = hotspotChangeIconModal.find('.file-manager .list .active a');

    if (selected.length) {
      hotspotEl.attr('src', selected.attr('href'));
      hotspotEl.data('hotspot').icon = selected.attr('href').split('/').pop();
    } else {
      hotspotEl.data('hotspot').icon = undefined;
      updateHotspotIconBySceneId(hotspotEl, _.get(hotspotEl.data('hotspot'), 'action.loadScene.scene'));
    }

    hotspotChangeIconModal.modal('hide');

    return false;
  });

  /* hotspots saving */
  $(document).on('submit', '.modal.hotspots form', function () {
    var form = $(this);
    var modal = form.parents('.modal');
    var nextModal = modal.nextAll('.modal.hotspots:eq(0)');
    var scene = modal.data('scene');
    var hotspots = [];
    var image = modal.find('.image > img');

    modal.find('.hotspots .hotspot').each(function () {
      var hotspotEl = $(this);

      if (!hotspotEl.data('hotspot').action) return;

      var hotspotData = {
        left: parseFloat(hotspotEl.css('left')),
        top: parseFloat(hotspotEl.css('top'))
      };

      hotspotData.ath = ((hotspotData.left + hotspotEl.width() / 2) * scene.file.width / image.width() / scene.file.width - 0.5) * scene.file.hfov;
      hotspotData.atv = ((hotspotData.top + hotspotEl.height() / 2) * scene.file.height / image.height() / scene.file.height - 0.5) * scene.file.vfov;

      hotspots.push(Object.assign(hotspotEl.data('hotspot'), hotspotData));
    });

    scene.hotspots = hotspots;
    modal.modal('hide');

    if (nextModal.length) {
      nextModal.modal('show');
    } else {
      panel.find('.step.create-hotspots').addClass('done');
    }

    return false;
  });

  /* STEP 4 */

  var definePropertiesModal = $('#define-tour-properties');
  var programSelectGroup = definePropertiesModal.find('.program-select-group');
  var apartmentSelectGroup = definePropertiesModal.find('.apartment-select-group');

  panel.find('.step.define-properties > .btn').on('click', function () {
    changeTourType(definePropertiesModal.find('select[name="type"]').val());
    definePropertiesModal.modal('show');
  });

  definePropertiesModal.find('form').on('submit', function () {
    definePropertiesModal.modal('hide');

    panel.find('.step.define-properties').addClass('done');
    panel.find('.step.finish').removeClass('disabled').find('.btn').removeAttr('disabled');

    return false;
  });

  definePropertiesModal.find('select[name="type"]').on('change', function () {
    changeTourType(this.value);
  });

  function changeTourType(type) {
    if (!type) {
      programSelectGroup.hide();
      apartmentSelectGroup.hide();
    } else if (type === 'exterior') {
      programSelectGroup.show();
      apartmentSelectGroup.hide();
    } else {
      programSelectGroup.hide();
      apartmentSelectGroup.show();
    }
  }

  /* STEP 4 */

  var tourSavedModal = $('#tour-saved');
  var tourSaveErrorModal = $('#tour-save-error');

  panel.find('.step.finish .btn').on('click', function () {
    tour.name = definePropertiesModal.find('input[name="name"]').val().trim();
    tourTitle.html(tour.name);

    tour.user = definePropertiesModal.find('select[name="user"]').val();
    tour.group = definePropertiesModal.find('select[name="group"]').val();
    tour.type = definePropertiesModal.find('select[name="type"]').val();
    tour.program = definePropertiesModal.find('select[name="program"]').val();
    tour.apartment = definePropertiesModal.find('select[name="apartment"]').val();
    tour.apartmentsAdditional = definePropertiesModal.find('select[name="apartmentsAdditional"]').val();

    if (tour.type === 'exterior') {
      tour.apartment = '';
      tour.apartmentsAdditional = [];
    } else {
      tour.program = '';
    }

    var button = $(this);

    button.prop('disabled', true).addClass('busy');

    app
      .fetch('./tours/' + tour._id + '/save', 'POST', {tour: tour})
      .then(function (r) {
        if (!r || r.status === 'failed') throw new Error();
        if (r.status === 'success') return;
        return new Promise(function (resolve, reject) {
          var statusInterval = 5 * 1000;
          var fetchStatus = function () {
            $.ajax({
              type: 'GET',
              url: './tours/' + tour._id + '/status/generate',
            }).done(function (m) {
              if (!m || m.generateStatus === 'failed') return reject();
              if (m.generateStatus === 'completed') return resolve();
              setTimeout(fetchStatus, statusInterval);
            }).fail(function () {
              return reject();
            });
          };
          fetchStatus();
        });
      })
      .then(function () {
        tour.updateOnlyProperties = true;
        tourSavedModal.modal('show');
      })
      .catch(function (e) {
        console.error(e);
        tourSaveErrorModal.modal('show');
      })
      .always(function () {
        button.prop('disabled', false).removeClass('busy');
      });
  });

  function filterBaseScenes(scenes, layer_types) {
    return _.filter(scenes, function (s) {
      var lt = _.find(layer_types, {_id: s.layer});
      return (lt && lt.isBaseLayer);
    });
  }

  function selectMainScene(scenes, scene_id) {
    scenes.forEach(function (scene) {
      scene.isMain = (scene._id === scene_id);
    });
  }

});
