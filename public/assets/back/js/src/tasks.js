$(function() {
  if (!$('.tasks.panel').length) return;
  $('.tasks.panel .actions .delete').on('click', function() {
    $('#delete-all-tasks').modal();
  });
});
