$(function () {
  var panel = $('.files.panel');

  if (panel.length) {
    new app.FileManager({
      container: $('.files.panel .file-manager'),
      path: app.qs.current.path,
      pushState: true
    });
  }
});
