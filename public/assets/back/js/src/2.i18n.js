$(function () {

  moment.locale(app.locale);

  app.localeStrings = $('html').data('locale-strings');

  app.__ = function () {
    var msg = app.localeStrings[arguments[0]] || arguments[0];

    if (arguments.length > 1) msg = vsprintf(msg, Array.prototype.slice.call(arguments, 1));

    return msg;
  };

});
