$(function () {
  var panel = $('.layer-type.panel');

  if (panel) {
    var iconInput = panel.find('input[name="icon"]');

    new app.FileManager({
      container: $('.layer-type.panel .file-manager'),
      path: 'hotspots',
      dblClick: false,
      multiple: false,
      directories: false,
      onSelected: function (resource) {
        iconInput.val(resource.name);
      },
      onDeselected: function () {
        iconInput.val(null);
      },
      selected: [{name: iconInput.val()}]
    });
  }
});
