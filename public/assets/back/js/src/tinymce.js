$(function() {
  tinymce.init({
    selector: 'textarea.html',
    skin: 'viewwer',
    branding: false,
    menubar: false,
    valid_elements: 'a[href|target],p,strong/b,ul,ol,li',
    plugins: [
      'lists link', 'contextmenu paste code'
    ],
    toolbar: 'undo redo | bold | bullist numlist | link | code',
    setup: function (editor) {
      $(editor.getElement()).data('tinymce', editor);
    },
    paste_auto_cleanup_on_paste: true,
    paste_postprocess: function (pl, o) {
      // remove &nbsp
      o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, '');
    }
  });
});
