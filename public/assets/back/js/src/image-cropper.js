$(function () {
  var imageDataUrl;

  $('body').on('click', '.image-cropper .action.upload', function (e) {
      e.preventDefault();

      var id_upload_link = $(this).data('iduploadlink');

      $('.image-cropper .image-upload').each(function(i, v){
        v = $(v);
        var id_input_image_upload = v.data('idinputimageupload');
        if(id_input_image_upload==id_upload_link) {
          v.trigger('click');
        }
      });
    }).on('change', '.image-cropper .image-upload', function () {

      var id_input_image_upload = $(this).data('idinputimageupload');
      var container = $(this).parent('.image-cropper');
      var imageCropModal = $(container.find('#image-crop-modal-template').text());
      var imageCropContainer = imageCropModal.find('.image-crop-container');
      imageCropContainer.addClass('loading');
      var imageCrop = imageCropModal.find('.image-crop');

      var parameters = container.data('parameters');
      var cropSettings = parameters.cropSettings;
      var cropperOptions = {
        viewMode: 0,
        aspectRatio: cropSettings.aspectRatio,
        minContainerWidth: 500,
        minContainerHeight: 300,
        fillColor: '#fff'
      };
      
      if (readImage(this, imageCrop, cropperOptions)) imageCropModal.modal('show');
    }).on('click','.action.crop', function (e) {
      e.preventDefault();

      var container = $(this).parents('.image-cropper');
      var imageCropModal = $(container.find('#image-crop-modal-template').text());
      var imageCropContainer = imageCropModal.find('.image-crop-container');
      var parameters = container.data('parameters');
      var cropSettings = parameters.cropSettings;
      var cropperOptions = {
        viewMode: 0,
        aspectRatio: cropSettings.aspectRatio,
        minContainerWidth: 500,
        minContainerHeight: 300,
        fillColor: '#fff'
      };
      var imageCrop = imageCropModal.find('.image-crop');
      imageCropContainer.addClass('loading');
      if (imageDataUrl) {
        readDataUrl(imageDataUrl, imageCrop, cropperOptions);
        imageCropModal.modal('show');
      } else if (parameters.initialImageOriginal) {
        readUrl(parameters.initialImageOriginal, imageCrop, cropperOptions);
        imageCropModal.modal('show');
      }
    }).on('ready', '.image-crop', function () {
      var image_cropId = $(this).data('idimagecrop'); 
      
      var container;
      var parameters;
      var cropSettings;
      var imageCropModal;
      var imageCrop;
      var cropperBox;
      var imageCropContainer;
      
      $('.image-cropper').each(function (i,v) {
           var conteneur = $(v);
           var id_imageupload = conteneur.find('.image-upload').data('idinputimageupload');
           if (image_cropId == id_imageupload) {
              container = conteneur;
           }
        }); 

       parameters = container.data('parameters');
       cropSettings = parameters.cropSettings;
       imageCropModal = $(container.find('#image-crop-modal-template').text());
       imageCrop = imageCropModal.find('.image-crop');
       cropperBox = imageCrop.find('.cropper-view-box');       
       if (cropSettings.circle) cropperBox.addClass('cropper-circle');

       // imageCropContainer = imageCropModal.find('.image-crop-container');  
       imageCropContainer = $(this).parents('.image-crop-container'); 
       imageCropContainer.removeClass('loading');

      //TEMPORAIRE
      // $('.image-crop-container').removeClass('loading');
    }).on('submit', '.modalform', function(e){
        e.preventDefault();      
        var imagecrop;
        var id_imagecrop;
        var imageCropContainer; 
        var placeholder;
        var imageResult;
        var actionCrop;
        var actionRemove;
        var parameters;
        var format;
        var cropSettings;
        var cropperOptions;
        var cropOptions;
        var saveOriginal;
        var hiddenInput;
        var hiddenInputOriginal;
        var imageCropModal;

        imagecrop = $(this).find('.image-crop');
        id_imagecrop = imagecrop.data('idimagecrop');
        imageCropContainer = $(this).find('.image-crop-container'); 

        imageCropContainer.addClass('loading');

        $('.image-cropper').each(function (i,v) {
           var container = $(v);
           var id_imageupload = container.find('.image-upload').data('idinputimageupload');
           if (id_imagecrop == id_imageupload) {
              placeholder = container.find('.placeholder');
              imageResult = container.find('.image-result');
              actionCrop =  container.find('.action.crop');
              actionRemove = container.find('.action.remove');
              imageCropModal = $(container.find('#image-crop-modal-template').text());

              // parameters
              parameters = container.data('parameters');

              // output format
              format = parameters.format;
              if (format) {
                format = 'image/' + format;
              } else {
                format = 'image/png';
              }

              cropSettings = parameters.cropSettings;
              cropperOptions = {
                viewMode: 0,
                aspectRatio: cropSettings.aspectRatio,
                minContainerWidth: 500,
                minContainerHeight: 300,
                fillColor: '#fff'
              };
              cropOptions = {width: cropSettings.width, height: cropSettings.height};

              saveOriginal = parameters.saveOriginal;
              hiddenInput = container.find('.image-hidden-input');
              hiddenInputOriginal = container.find('.image-hidden-input-original');


              return;
           }
        });

        placeholder.hide();
        imageResult.show();
        actionCrop.show();
        actionRemove.show();


        var imageData = imagecrop.cropper('getCroppedCanvas', cropOptions).toDataURL(format);
        imageResult.attr('src', imageData);

        convertURI((saveOriginal) ? imageDataUrl : '', format)
          .then(function (r) {
            var data = {
              image: imageData
            };

            if (saveOriginal && imageDataUrl && r) {
              data.imageOriginal = r;
            }

            return app.fetch({
              url: parameters.uploadUrl,
              data: data,
              method: 'POST'
            });
          })
          .then(function (r) {
            hiddenInput.val(r.name);
            if (saveOriginal) hiddenInputOriginal.val(r.nameOriginal);
            $('.modal').modal('hide');
            imageCropModal.modal('hide');
          })
          .catch(function (e) {
            console.error(e);
          });
    }).on('click', '.action.remove', function (e) {
      e.preventDefault();
      var container = $(this).parents('.image-cropper');
      var hiddenInput = container.find('.image-hidden-input');
      var files = [hiddenInput.val()];
      var parameters = container.data('parameters');
      var saveOriginal = parameters.saveOriginal;
      var hiddenInputOriginal = container.find('.image-hidden-input-original');
      var placeholder = container.find('.placeholder');
      var imageResult = container.find('.image-result');
      var actionCrop =  container.find('.action.crop');
      var actionRemove = $(this);


      if (saveOriginal && hiddenInputOriginal.val())
        files.push(hiddenInputOriginal.val());

      app.fetch({
        url: parameters.removeUrl,
        data: {
          files: files
        },
        method: 'POST'
      })
        .then(function () {
          hiddenInput.val('');
          hiddenInputOriginal.val('');

          imageResult.attr('src', '');
          imageResult.hide();
          placeholder.show();

          actionRemove.hide();
          actionCrop.hide();
        })
        .catch(function (e) {
          console.error(e);
        });
    });


  function readImage(input, imageCrop, cropperOptions) {
    if (!input.files || !input.files[0]) return false;

    if (!/^image\/\w+$/.test(input.files[0].type)) {
      app.swal({
        type: 'error',
        text: app.__('Please choose an image file'),
        position: 'center',
        showConfirmButton: true,
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-default'
      });
      $(input).val('');
      return false;
    }

    var reader = new FileReader();
    reader.onload = function (e) {
      
      $(input).val('');
      imageDataUrl = e.target.result;
      readDataUrl(imageDataUrl, imageCrop, cropperOptions);
    };

    reader.readAsDataURL(input.files[0]);
    return true;
  }

  function readUrl(url, imageCrop, cropperOptions) {
    imageCrop.cropper('destroy').attr('src', url).cropper(cropperOptions);
  }

  function readDataUrl(data, imageCrop, cropperOptions) {
    imageCrop.cropper('destroy').attr('src', data).cropper(cropperOptions);
  }

  function convertURI(URI, format) {
    return new Promise(function (resolve, reject) {
      if (!URI) return resolve();

      var canvas = document.createElement('canvas'),
        context = canvas.getContext('2d'),
        image = new Image();

      image.onload = function () {
        canvas.width = image.width;
        canvas.height = image.height;
        context.drawImage(image, 0, 0, canvas.width, canvas.height);
        resolve(canvas.toDataURL(format));
      };

      image.onerror = function (e) {
        reject(e);
      };

      image.src = URI;
    });
  }

  $('.image-cropper').each(function () {

    var container = $(this);

    // parameters
    var parameters = container.data('parameters');

    // actions
    var actionRemove = container.find('.action.remove');
    var actionCrop = container.find('.action.crop');

    var saveOriginal = parameters.saveOriginal;

    var imageResult = container.find('.image-result');
    var placeholder = container.find('.placeholder');

    if (parameters.initialImage) {
      imageResult.show();
      actionRemove.show();
      if (saveOriginal
        && parameters.inputValueOriginal) actionCrop.show();
    } else {
      placeholder.show();
    }
  });
});


