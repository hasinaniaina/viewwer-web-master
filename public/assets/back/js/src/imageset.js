$(function () {
	var panel = $('.imageset.panel');
	if (!panel.length) return;

	$('.images').sortable({
		opacity: 0.5,
		placeholder: 'highlight'
	});

	var imageset_add = $('#imageset_add');

	imageset_add.on('click', function(){
		var N = $(".image").length;
		var clone = $("#image-matrice").clone();
		clone.removeAttr('style');
		clone.removeAttr('id');
		clone.addClass('image');
		var imageUpload = clone.find('.image-upload');
		imageUpload = $(imageUpload);
		imageUpload.attr( "data-idinputimageupload", N );
		var actionUpload = clone.find('.action.upload');
		actionUpload = $(actionUpload);
		actionUpload.attr( "data-iduploadlink", N );

		var container = clone.find('.image-cropper');
		// var imageCropModal = $(container.find('#image-crop-modal-template').text());

		// var image_crop = imageCropModal.find('.image-crop');
		// image_crop = $(image_crop);
		// image_crop.attr("data-idimagecrop", N);
		// container.find('#image-crop-modal-template').text(imageCropModal.html());

		var imageCropModal_text = container.find('#image-crop-modal-template').text();
		imageCropModal_text = imageCropModal_text.replace("N", N);

		container.find('#image-crop-modal-template').text(imageCropModal_text);

		$(".images").append(clone);
	});
});
