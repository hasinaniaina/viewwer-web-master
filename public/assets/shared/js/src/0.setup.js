app.htmlEl = $('html');
app.environment = app.htmlEl.data('environment');

app.isUnloading = false;

$(window).on('unload', function() {
  app.isUnloading = true;
});
