$(function () {
  var container = $('#search-results');
  if (!container.length) return;

  var locale = container.data('locale');
  var results = container.find('.results');
  var moreTours = container.find('.more-tours');
  var searchSettings = container.find('.search-settings');
  var searchSettingButton = container.find('.search-controls .toggle');

  var sortSelect = container.find('select[name="sort"]');
  var noResults = container.find('.no-results');

  var apartmentResult = $('#apartment-result-template').text();
  var apartmentResultPlaceholder = $('#apartment-result-placeholder-template').text();

  var currentPage = 0;
  var searching = false;
  var noMoreResults = false;

  function search(options, cb) {

    var request = {
      placeType: 'all',
      saleType: 'all',

      budgetFrom: undefined,
      budgetTo: undefined,

      areaFrom: undefined,
      areaTo: undefined,

      roomsFrom: undefined,
      roomsTo: undefined,

      location: {
        placeId: undefined,
        lat: undefined,
        lng: undefined,
        user: undefined
      },
      page: currentPage,
      sort: {date: -1}
    };

    if (app.qs.getItem('placeType')) request.placeType = app.qs.getItem('placeType');
    if (app.qs.getItem('saleType')) request.saleType = app.qs.getItem('saleType');
    if (app.qs.getItem('budgetFrom')) request.budgetFrom = parseInt(app.qs.getItem('budgetFrom'), 10);
    if (app.qs.getItem('budgetTo')) request.budgetTo = parseInt(app.qs.getItem('budgetTo'), 10);
    if (app.qs.getItem('areaFrom')) request.areaFrom = parseInt(app.qs.getItem('areaFrom'), 10);
    if (app.qs.getItem('areaTo')) request.areaTo = parseInt(app.qs.getItem('areaTo'), 10);
    if (app.qs.getItem('roomsFrom')) request.roomsFrom = parseInt(app.qs.getItem('roomsFrom'), 10);
    if (app.qs.getItem('roomsTo')) request.roomsTo = parseInt(app.qs.getItem('roomsTo'), 10);

    if (app.qs.getItem('locationPlaceId')) request.location.placeId = app.qs.getItem('locationPlaceId');

    var l = app.qs.getItem('locationCoords');
    if (l && l.lat && l.lng) {
      var lat = parseFloat(l.lat);
      var lng = parseFloat(l.lng);

      if (!isNaN(lat) && !isNaN(lng)) {
        request.location.lat = lat;
        request.location.lng = lng;
      } else {
        console.error('lat/lng is not a number');
      }
    }
    if (app.qs.getItem('locationUser') === 'true') request.location.user = true;

    if (app.qs.getItem('sortOrder')) {
      request.sort = _.mapValues(app.qs.getItem('sortOrder'), function (v) {
        var s = parseInt(v, 10);
        if (!isNaN(s)) return s;
        console.error('sort order is not a number');
        return -1;
      });
    }

    app
      .fetch('/api/search', 'POST', request)
      .then(function (r) {

        if (_.isEmpty(r)) return cb(false);

        results.find('.result-placeholder').remove();
        results.find('.result').addClass('old');

        _.each(r, function (a, k) {
          var apt = $(apartmentResult);

          if (k === 0) apt.addClass('first-loaded');

          if (a.isSold) {
            apt.find('.sold-corner').removeClass('hidden');
            apt.find('.overlay').addClass('red');
            apt.find('.sold-corner .text').html(
              (a.saleType === 'rent') ? locale['rented'] : locale['sold']
            );
          }

          var type = locale[a.type];
          if (a.house) {
            type = locale['House'] + ' ' + type;
          } else {
            type = locale['Apt.'] + ' ' + type;
          }
          if (a.duplex) type += ' ' + locale['duplex'];
          apt.find('.type').html(type);

          if (a.price && !a.isSold) {
            apt.find('.price').html(a.price + '&nbsp;&euro;').removeClass('hidden');
          }

          if (a.area > 0) {
            apt.find('.area').html(a.area + 'm<sup>2</sup>').removeClass('hidden');
          }

          if (a.saleType === 'rent') {
            apt.find('.sale-type').html(locale['for_rent']).removeClass('hidden');
          }

          if (a.program && a.program.isPublished) {
            apt.find('.program').html('<a href="' + a.program.url + '">' + a.program.name + '</a>').removeClass('hidden');
          }

          if (a.thumbnail) {
            apt.find('.overlay').attr('href', a.url);
            apt.find('.interior').css('background-image', 'url("' + a.thumbnail + '")');
          } else {
            apt.find('.overlay').hide();
            apt.find('.interior').addClass('no-photo');
          }

          apt.appendTo(results);

          app.social({
            selector: apt.find('.social'),
            title: locale['Tour'],
            entityType: a.entityType,
            entityId: a.entityId,
            shareUrl: a.url,
            counterRating: a.counterRating,
            favoriteId: a.favoriteId
          });
        });

        // for nice last element width
        $(apartmentResultPlaceholder).appendTo(results);

        results.find('.result').not('old').show();
        cb(true);

      })
      .catch(function (e) {
        console.error(e);
        cb(false, e);
      });
  }

  searchSettingButton.on('click', function () {
    if (searchSettings.is(':visible')) {
      searchSettings.hide();
      searchSettingButton.removeClass('active');
    } else {
      searchSettings.velocity('fadeIn');
      searchSettingButton.addClass('active');
    }
    $('html').velocity('scroll', {offset: 0, mobileHA: false});
  });

  sortSelect.on('change', function () {
    var s = $(this).find(':checked').val().split('.');
    var sortOrder = {};
    sortOrder[s[0]] = (s[1] === 'asc') ? 1 : -1;
    app.qs.setItem('sortOrder', sortOrder);
    document.location.reload();
  });

  function setupSort() {
    var s = app.qs.getItem('sortOrder');
    if (s) {
      var k = _.keys(s)[0];
      sortSelect.val(k + '.' + ((s[k] === '1') ? 'asc' : 'desc'));
    }
    new Choices(sortSelect[0], {
      searchEnabled: false,
      position: 'bottom',
      shouldSort: false
    });
  }

  setupSort();

  $(window).scroll(_.throttle(function () {
    if ($(window).scrollTop() + $(window).height()
      >= $(document).height() - $(window).height()) fetchResults();
  }, 300));

  moreTours.on('click', function (e) {
    e.preventDefault();
    fetchResults();
  });

  fetchResults();

  function fetchResults(cb) {
    if (noMoreResults || searching) return;
    moreTours.addClass('budy');
    searching = true;

    search({}, function (r, e) {

      if (currentPage === 0) {
        app.loader.hide();
        if (!r || e) {
          noMoreResults = true;
          noResults.show();
          searchSettingButton.addClass('active');
          searchSettings.show();
        } else {
          moreTours.css({visibility: 'visible'});
        }
      }

      if (!r && !e) {
        noMoreResults = true;
        moreTours.css({visibility: 'hidden'});
      } else if (!e) {
        currentPage++;
      }

      moreTours.removeClass('busy');
      searching = false;

      if (cb !== undefined) cb(r, e);
    });
  }

});
