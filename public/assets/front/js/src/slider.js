$(function () {
  var slider = $('.slider');
  if (!slider.length) return;

  var swiperContainer = slider.find('.swiper-container');

  var options = {
    effect: 'fade',
    preloadImages: true,
    updateOnImagesReady: true
  };

  var c = slider.data('count');
  if (c && c > 1) {
    options = _.assign({}, options, {
      loop: true,
      autoplay: {
        delay: 3000,
        disableOnInteraction: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      }
    });
  }

  options.on = {
    imagesReady: function () {
      slider.removeClass('loading-circle');
    }
  };

  var swiper = new Swiper(swiperContainer, options);

  // ios slow layout update workaround
  $(window).on('orientationchange', function () {
    setTimeout(function() {
      swiper.update();
    }, 100);
  });
});
