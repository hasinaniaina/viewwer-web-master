$(function () {

  var container = $('#favorites');
  if (!container.length) return;

  container.find('.delete').each(function () {
    $(this).on('click', function (e) {
      e.preventDefault();

      var favoriteId = $(this).data('favorite-id');

      app.fetch('/api/favorites/' + favoriteId + '/delete', 'POST')
        .then(function () {
          $('#' + favoriteId).velocity('slideUp');
        })
        .catch(function (e) {
          console.error(e);
        });

    });
  });

});
