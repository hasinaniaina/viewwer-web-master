$(function () {
  $('select').each(function () {
    if ($(this).data('manual-init')) return;
    var searchEnabled = false;

    var noResultsText = '';
    if ($(this).data('search')) searchEnabled = true;
    if ($(this).data('search-no-results')) {
      noResultsText = $(this).data('search-no-results');
    }

    var searchPlaceholder = '';
    if ($(this).data('search-placeholder')) {
      searchPlaceholder = $(this).data('search-placeholder');
    }

    var hideSearchIfLessThan = $(this).data('search-hide-if-less');
    if (hideSearchIfLessThan) {
      if ($(this).find('option').length < hideSearchIfLessThan) searchEnabled = false;
    }

    var choices = new Choices($(this)[0], {
      searchEnabled: searchEnabled,
      searchFields: ['label'],
      itemSelectText: '',
      noResultsText: noResultsText,
      searchPlaceholderValue: searchPlaceholder,
      position: 'bottom',
      shouldSort: false,
      callbackOnCreateTemplates: function (template) {
        var classNames = this.config.classNames;

        return {
          item: function (data) {
            var t = '<div class="' +
              classNames.item + ' ' +
              'data-item ' +
              'data-id="' + data.id + '" ' +
              'data-value="' + data.value + '" ' +
              ((data.active) ? 'aria-selected="true"' : '') + ' ' +
              ((data.disabled) ? 'aria-disabled="true"' : '') +
              '>' + data.label + '</div>';
            return template(t);
          },
          choice: function (data) {
            var t = '<div class="' +
              classNames.item + ' ' +
              classNames.itemChoice + ' ' +
              ((data.disabled) ? classNames.itemDisabled : classNames.itemSelectable) + '" ' +
              'data-choice ' +
              ((data.disabled) ? 'data-choice-disabled aria-disabled="true"' : 'data-choice-selectable') + ' ' +
              'data-id="' + data.id + '" ' +
              'data-value="' + data.value + '"' +
              // custom data attribute to highlight selected item in list
              ((data.selected) ? 'data-custom-selected="true"' : '') +
              '>' + data.label + '</div>';
            return template(t);
          }
        };
      }
    });

    // prevent search input auto focus (opens keyboard on mobile devices)
    choices.passedElement.addEventListener('showDropdown', function() {
      if (searchEnabled) $(choices.input).prop({disabled: false});
    }, false);

    choices.passedElement.addEventListener('hideDropdown', function() {
      if (searchEnabled) $(choices.input).prop({disabled: true});
    }, false);

    if (searchEnabled) $(choices.input).prop({disabled: true});
  });
});
