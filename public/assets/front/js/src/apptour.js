$(function () {

  var container = $('#apptour');
  if (!container.length) return;

  var buttonBack = container.find('.back');

  buttonBack.on('click', function () {
    history.back();
  });

});
