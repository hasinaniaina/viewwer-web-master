$(function () {

  var container = $('#apartment');
  if (!container.length) return;

  var HEIGHT_TOUR_MINIMAL = 550;

  var info = container.find('.top-nav');
  var tours = container.find('.tours');

  var tourInterior = container.find('.tour.interior');
  var tourExterior = container.find('.tour.exterior');
  var tourAerial = container.find('.tour.aerial');

  var buttonInterior = '.control.interior';
  var buttonExterior = '.control.exterior';
  var buttonAerial = '.control.aerial';

  var sidebarRight = '.sidebar.right';

  var buttonBack = container.find('.back');

  buttonBack.on('click', function () {
    history.back();
  });

  if (tourExterior.length) {
    container.find(buttonInterior).show();
    container.find(buttonExterior).show();
    tourInterior.find(buttonInterior).addClass('active');
  }

  if (tourAerial.length) {
    container.find(buttonInterior).show();
    container.find(buttonAerial).show();
    tourInterior.find(buttonInterior).addClass('active');
  }

  $(buttonInterior).on('click', function () {
    container.find(sidebarRight).removeClass('active');

    container.find(buttonInterior).addClass('active');
    container.find(buttonExterior).removeClass('active');
    container.find(buttonAerial).removeClass('active');

    tourInterior.addClass('active');
    tourInterior.siblings().removeClass('active');
  });

  if (tourExterior.length) {
    $(buttonExterior).on('click', function () {
      container.find(sidebarRight).removeClass('active');

      container.find(buttonExterior).addClass('active');
      container.find(buttonInterior).removeClass('active');
      container.find(buttonAerial).removeClass('active');

      tourExterior.addClass('active');
      tourExterior.siblings().removeClass('active');
    });
  }

  if (tourAerial.length) {
    // workaround - iframe breaks layout width
    $(window).on('orientationchange', function () {
      loadAerial();
    });

    $(buttonAerial).on('click', function () {
      loadAerial();

      container.find(buttonAerial).addClass('active');
      container.find(buttonExterior).removeClass('active');
      container.find(buttonInterior).removeClass('active');

      tourAerial.addClass('active');
      tourAerial.siblings().removeClass('active');
    });
  }

  function loadAerial() {
    if (!tourAerial.length) return;
    var c = tourAerial.find('.frame-container');
    c.html('');
    c.html('<iframe src="' + tourAerial.data('url') + '" ' +
      'frameborder="0" ' +
      'allowFullScreen="true" ' +
      'webkitallowfullscreen="true" ' +
      'mozallowfullscreen="true"></iframe>');
  }

  /* fit tours to page for large displays */

  $(window).on('resize', _.debounce(function () {
    resizeTour();
  }));

  function resizeTour() {
    var availableHeight = $(window).height() - info.outerHeight();

    if (availableHeight > HEIGHT_TOUR_MINIMAL) {
      tours.height(availableHeight);
    }
  }

  resizeTour();

});
