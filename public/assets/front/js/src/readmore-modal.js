$(function () {
  $('.readmore-modal').each(function () {
    $(this).on('click', function (e) {
      e.preventDefault();
      BootstrapDialog.show({
        title: $(this).data('header'),
        message: $(this).data('text'),
        animate: false
      });
    });
  });
});
