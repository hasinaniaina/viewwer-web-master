$(function () {

  var containerLists = $('#stats-lists');
  if (!containerLists.length) return;

  var containerCounters = $('#stats-counters');

  containerCounters.find('.main-count').each(function () {
    var c = $(this).data('count');
    if (!c) return;
    var numAnim = new CountUp($(this)[0], 0, c, 0, 2, {useGrouping: false});
    if (!numAnim.error) {
      numAnim.start();
    } else {
      $(this).html(c);
    }
  });

  var exportButton = containerLists.find('button[name="export"]');
  var exportForm = $('#export-programs');
  var exportSubmitButton = exportForm.find('.submit');

  var selectSold = exportForm.find('select[name="sold"]');
  var selectTypes = exportForm.find('select[name="types"]');
  var selectPrograms = exportForm.find('select[name="programs"]');

  exportButton.on('click', function () {
    exportForm.modal();
  });

  exportSubmitButton.on('click', function () {
    var request = {};

    if (selectSold.val()) request.sold = selectSold.val();
    request.types = selectTypes.val();
    request.programs = selectPrograms.val();

    window.open('/admin/programs/export' + '?' + app.qs.toString(request));
    exportForm.modal('hide');
  });

});
