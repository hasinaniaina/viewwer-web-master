$(function () {

  var MAP_HEIGHT = 400;

  $('.address-modal').each(function () {
    $(this).on('click', function (e) {
      e.preventDefault();

      var modal = $(
        '<div>' +
        '<div class="address-modal-text">' + $(this).data('address') + '</div>' +
        '<div class="address-modal-map"></div>' +
        '</div>'
      );

      var mapContainer = modal.find('.address-modal-map');
      var location = $(this).data('location');

      if (location && location.coordinates[0]) {

        var center = {
          lng: location.coordinates[0],
          lat: location.coordinates[1]
        };

        var map = new google.maps.Map(mapContainer[0], {
          zoom: 17,
          center: center,
          scrollwheel: false
        });

        new google.maps.Marker({
          position: center,
          map: map
        });

        mapContainer.height(MAP_HEIGHT);
      }

      BootstrapDialog.show({
        title: $(this).data('header'),
        onshow: function (dialog) {
          dialog.setMessage(modal);
        },
        animate: false
      });
    });
  });
});
