$(function () {

  var button = $('#header-control-menu');
  if (!button.length) return;

  var menu = $('#user-menu');

  var requestsCount = menu.find('.control.requests .count-new');
  if (requestsCount.length) {
    app.fetch('/admin/requests/count/new')
      .then(function (r) {
        if (!r || !r.count) return;
        requestsCount.html(r.count);
        requestsCount.show();
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  var messagesCount = menu.find('.control.messages .count-new');
  if (messagesCount.length) {
    app.fetch('/api/messages/unreads/count')
      .then(function (r) {
        if (!r || !r.count) return;
        messagesCount.html(r.count);
        messagesCount.show();
      })
      .catch(function (e) {
        console.error(e);
      });
  }

  function openMenu() {
    if (menu.is(':visible')) return;
    button.addClass('active');
    menu.css({
      top: 0,
      right: button.right() + button.width() + 9
    });
    menu.show();

    document.addEventListener('mouseup', checkOutside);
    window.addEventListener('resize', closeMenu);
    window.addEventListener('scroll', closeMenu);
  }

  function closeMenu() {
    if (!menu.is(':visible')) return;
    button.removeClass('active');
    menu.hide();

    document.removeEventListener('mouseup', checkOutside);
    window.removeEventListener('resize', closeMenu);
    window.removeEventListener('scroll', closeMenu);
  }

  button.on('click', function () {
    if (menu.is(':visible')) {
      closeMenu();
    } else {
      openMenu();
    }
  });

  menu.find('.control.link').on('click', function () {
    window.location = $(this).data('url');
  });

  menu.find('.locale').on('click', function () {
    app.setLocale($(this).data('locale'));
  });

  var checkOutside = function (e) {
    if ($(e.target).parent().is(button)) return;
    if (!menu.is(e.target) && menu.has(e.target).length === 0) closeMenu();
  };

});
