$(function () {
  $('.tabs-nav').on('click', 'a:not(.btn-success)', function () {
    $(this).toggleClass('btn-default btn-success');
    $(this).siblings().removeClass('btn-success').addClass('btn-default');
  });
});
