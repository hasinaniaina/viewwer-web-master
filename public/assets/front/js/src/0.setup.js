$(function () {

  app.localStorage = {
    setItem: function (item, data) {
      try {
        localStorage.setItem(item, data);
      } catch (e) {
        console.error(e);
      }
    },
    getItem: function (item) {
      try {
        return localStorage.getItem(item);
      } catch (e) {
        console.error(e);
      }
    }
  };

  if (navigator.appVersion.indexOf('MSIE 10') !== -1) {
    app.swal = function (options) {
      alert(options.text);
    };
  } else {
    app.swal = swal;
  }

  // disable cache
  $.ajaxSetup({cache: false});

  // get position from right side
  $.fn.right = function () {
    return $(window).width() - (this.offset().left + this.outerWidth());
  };

  // for ie
  if (!String.prototype.includes) {
    String.prototype.includes = function (search, start) {
      if (typeof start !== 'number') {
        start = 0;
      }

      if (start + search.length > this.length) {
        return false;
      } else {
        return this.indexOf(search, start) !== -1;
      }
    };
  }

  app.onSwitch = function (selector, e) {
    if (e) e.preventDefault();

    var target = $(selector);
    var settings = target.data('switch');
    var span = target.find('span');

    target.addClass('busy');

    app
      .fetch('/admin/switch', settings)
      .then(function (data) {
        span.html(data.on ? span.data('off') : span.data('on'));
      })
      .always(function () {
        target.removeClass('busy');
      });
  };

  app.isIPhone = function () {
    return /iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  };

  $('html').css({opacity: 1});
});
