$(function () {

  app.apptours = {};

  var RIGHT_SIDEBAR_DELAY = 3000;
  var LOADING_ANIM_DELAY = 1000;
  var WIDTH_HIDE_SIDEBAR = 480;

  var ARGS_NORMAL = ', null, null, BLEND(0.3)';

  $('.apptour-partial').each(function () {
    var container = $(this);
    var tourId = container.data('tourid');
    var preview = container.data('preview');

    var timerRightSidebar;
    var timerLoadingAnim;

    app.apptours[tourId] = {
      container: container,
      init: function () {
        if (!container.length) return;

        var xml;

        if (preview) {
          xml = '/apptours/' + tourId + '/preview/tour.xml?_=' + new Date().getTime();
        } else {
          xml = '/apptours/' + tourId + '/tour.xml?_=' + new Date().getTime();
        }
        embedpano({
          xml: xml,
          target: 'panorama-' + tourId,
          id: 'krpano-' + tourId,
          bgcolor: '#ffffff',
          mobilescale: 1
        });

        app.apptours[tourId].krpano = $('#krpano-' + tourId)[0];
      },
      openScene: function (scene_id) {
        timerLoadingAnim = setTimeout(function () {
          container.addClass('scene-loading');
        }, LOADING_ANIM_DELAY);

        app.apptours[tourId].krpano.call('loadscene("scene_' + scene_id + '"' + ARGS_NORMAL + ');');

        container.find('.scene').each(function () {
          if ($(this).data('id') === scene_id) {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var scenes = container.find('.scenes');
            var scroll = scenes.scrollTop() + $(this).offset().top - scenes.offset().top - 10;
            scenes.animate({
              scrollTop: scroll
            });
            return false;
          }
        });
      },
      sceneLoaded: function () {
        clearTimeout(timerLoadingAnim);
        container.removeClass('scene-loading');
        container.removeClass('loading');
      },
      krpanoLoaded: function () {
        var leftSidebar = container.find('.sidebar.left');
        leftSidebar.addClass('active');

        if ($(window).width() >= WIDTH_HIDE_SIDEBAR) {
          var rightSidebar = container.find('.sidebar.right');
          rightSidebar.addClass('active');
          timerRightSidebar = setTimeout(function () {
            rightSidebar.removeClass('active');
          }, RIGHT_SIDEBAR_DELAY);
        }
      },
      krpanoResized: function (/*width, height*/) {
        var rightSidebar = container.find('.sidebar.right');
        rightSidebar.removeClass('active');
      },
      enableKrpanoToggle: function (name) {
        var toggle = $('.' + name + '.action-krpano-toggle');
        if (toggle.length) toggle.addClass('active');
      },
      disableKrpanoToggle: function (name) {
        var toggle = $('.' + name + '.action-krpano-toggle');
        if (toggle.length) toggle.removeClass('active');
      },
      vrEnabled: function () {
        // for ios devices webvr
        if (app.isIPhone()) {
          var t = $('#krpano-' + tourId);
          t.css('z-index', 9999);
          container.hide();
        }
        //
      },
      vrDisabled: function () {
        if (app.isIPhone()) {
          var t = $('#krpano-' + tourId);
          t.css('z-index', 'auto');
          container.show();
        }
        //
      },
      vrAvailable: function () {
        var vrControl = container.find('.action-krpano-toggle.vr');
        if (vrControl.length) vrControl.show();
      },
      gyroAvailable: function () {
        var gyroControl = container.find('.action.gyro');
        if (gyroControl.length) gyroControl.show();
      }
    };

    /* change scenes */
    container.on('click', '.scene', function () {
      var control = $(this);
      app.apptours[tourId].openScene(control.data('id'));
    });

    /* toggle sidebars */
    container.on('click', '.sidebar .toggle .slide-in', function () {
      var sidebar = $(this).parents('.sidebar');

      sidebar.addClass('active');

      if ($(window).width() < WIDTH_HIDE_SIDEBAR) {
        sidebar.siblings('.sidebar').removeClass('active');
      }
    });

    container.on('click', '.sidebar .toggle .slide-out', function () {
      $(this).parents('.sidebar').removeClass('active');
    });

    container.on('mouseleave', '.sidebar.right', function () {
      var that = $(this);
      timerRightSidebar = setTimeout(function () {
        that.removeClass('active');
      }, RIGHT_SIDEBAR_DELAY);
    });

    container.on('mouseenter', '.sidebar.right', function () {
      clearTimeout(timerRightSidebar);
    });

    /* actions */
    container.on('click', '.action', function () {
      $(this).toggleClass('active');
    });

    /* toggle fullscreen */
    var fullscreenControl = container.find('.action.fullscreen');

    if (window.screenfull && screenfull.enabled) {
      fullscreenControl.show();

      fullscreenControl.on('click', function () {
        screenfull.toggle();
      });

      screenfull.on('change', function () {
        fullscreenControl.toggleClass('active', screenfull.isFullscreen);
      });
    }

    /* toggle autorotate */
    var autorotateControl = container.find('.action.autorotate');

    autorotateControl.on('click', function () {
      app.apptours[tourId].krpano.call('switch("autorotate.enabled");');
    });

    /* toggle vr */
    var vrControl = container.find('.action-krpano-toggle.vr');

    vrControl.on('click', function () {
      if (app.isIPhone()) {

        // detect deviceorientation problem
        var timerId = setTimeout(function () {          
          // display a message when the deviceorientation event is not supported
          // explain how to enable the feature on the smartphone

          BootstrapDialog.show({
            animate: false,
            title: 'Information',
            message: "<p>Pour utiliser la VR, votre navigateur a besoin d'une autorisation pour accéder au gyroscope.</p>" +
                    "<p>Autorisez l'accès au gyroscope puis rechargez cette page pour utiliser la VR.</p>",
            buttons: [{
              label : "Plus d'infos",
              cssClass: 'btn-primary',
              action: function(dialogRef) {
                window.open("/pages/gyro-help");
                dialogRef.close();
              }
            }, {
              label: "Fermer",
              cssClass: 'btn-primary',
              action: function(dialogRef){
                dialogRef.close();
              }
            }]
        });

        }, 500);

        var tempDeviceOrientationCallBack = function() {
            // remove callback if the event happened
              window.removeEventListener('deviceorientation', tempDeviceOrientationCallBack, false);
              clearTimeout(timerId);
              
              // diplay the VR
              app.apptours[tourId].krpano.call('WebVR.toggleVR();');
        };

        window.addEventListener('deviceorientation', tempDeviceOrientationCallBack, false);        
      }
      else {
        app.apptours[tourId].krpano.call('WebVR.toggleVR();');
      }
    });

    /* toggle gyro */
    var gyroControl = container.find('.action.gyro');

    gyroControl.on('click', function () {
      app.apptours[tourId].krpano.call('switch(plugin[gyro].enabled);');
    });

    app.apptours[tourId].init();
  });

});
