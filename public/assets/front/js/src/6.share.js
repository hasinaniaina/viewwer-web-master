$(function () {
  // based on https://github.com/delfimov/JS-Share
  app.share = function (element) {

    var defaultOptions = {
      type: '',
      url: location.href,
      title: document.title,
      popup_width: 626,
      popup_height: 436,
      rating_data: ''
    };

    var options = _.assign(
      defaultOptions,
      _getData(element, defaultOptions)
    );

    var withoutPopup = [
      'viber',
      'telegram',
      'whatsapp'
    ];

    var shares = {
      facebook: function (options) {
        return 'https://www.facebook.com/sharer.php'
          + '?u=' + encodeURIComponent(options.url);
      },

      twitter: function (options) {
        return 'http://twitter.com/share?'
          + 'text=' + encodeURIComponent(options.title)
          + '&url=' + encodeURIComponent(options.url);
      },

      telegram: function (options) {
        return 'tg://msg_url?url=' + encodeURIComponent(options.url);
      },

      whatsapp: function (options) {
        return 'whatsapp://send?text=' + encodeURIComponent(options.url);
      },

      viber: function (options) {
        return 'viber://forward?text=' + encodeURIComponent(options.url);
      },

      linkedin: function (options) {
        return 'https://www.linkedin.com/shareArticle?url=' + encodeURIComponent(options.url) + '&mini=true';
      },

      tumblr: function (options) {
        return 'http://tumblr.com/widgets/share/tool?'
          + 'canonicalUrl=' + encodeURIComponent(options.url)
          + '&posttype=link';
      },

      blogger: function (options) {
        return 'https://www.blogger.com/blog-this.g?'
          + 'u=' + encodeURIComponent(options.url)
          + '&n=' + encodeURIComponent(options.title);
      },

      pocket: function (options) {
        return 'https://getpocket.com/save?url=' + encodeURIComponent(options.url);
      }
    };

    var link = shares[options.type](options);

    // should we try to redirect user to share link
    var tryLocation = true;

    // if we must try to open a popup window
    if (withoutPopup.indexOf(options.type) === -1) {
      // we try, and if we succeed, we will not redirect user to share link location
      tryLocation = _popup(link, options) === null;
    }

    if (tryLocation) window.open(link, '_blank');

    app.fetch('/api/rating',
      'POST',
      JSON.parse(options.rating_data))
      .then(function () {
      })
      .catch(function (e) {
        console.error(e);
      });

    // open popup window for sharing
    function _popup(url, options) {
      return window.open(url, '',
        'toolbar=0,status=0,scrollbars=1,width=' + options.popup_width + ',' +
        'height=' + options.popup_height);
    }

    function _getData(el, defaultOptions) {
      var data = {};
      for (var key in defaultOptions) {
        var value = el.getAttribute('data-' + key);
        if (value) {
          data[key] = value;
        }
      }
      return data;
    }
  };
});
