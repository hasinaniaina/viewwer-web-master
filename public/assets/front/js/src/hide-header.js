$(function () {
  var h = $('header.main-header');
  if (!h.length) return;
  if (h.hasClass('fixed')) return;

  var headroom = new Headroom(h[0], {
    // vertical offset in px before element is first unpinned
    offset: 150,
    tolerance: {
      up: 20,
      down: 20
    }
  });
  headroom.init();
});
