$(function () {
  app.loader = {
    show: function () {
      $('#loader').show();
    },
    hide: function () {
      $('#loader').hide();
    }
  };
});
