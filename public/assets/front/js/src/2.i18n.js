$(function () {
  app.setLocale = function (locale) {
    Cookies.set('locale', locale, {expires: 365, domain: $('html').data('domain')});
    location.reload();
  };
});
