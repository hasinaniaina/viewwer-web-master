$(function() {
  var apartments_list = $('.apartments-list');
  if (!apartments_list.length) return;

  apartments_list.on('change', function() {
    location.href = '/apartments/' + $(this).find(':checked').val();
  });
});
