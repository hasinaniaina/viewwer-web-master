$(function () {
  app.lazy = {
    ll: new LazyLoad({
      elements_selector: '.lazy'
    }),
    update: function () {
      this.ll.update();
    }
  };
});
