$(function () {
  var scrolltop = $('#scrolltop');
  if (!scrolltop.length) return;

  $(window).scroll(_.throttle(function () {
    var s = $(window).scrollTop();
    var w = $(window).height();
    if (s > w) {
      if (scrolltop.is(':visible')) return;
      scrolltop.velocity('stop');
      scrolltop.velocity('fadeIn', {duration: 'fast'});
    } else {
      if (!scrolltop.is(':visible')) return;
      scrolltop.velocity('stop');
      scrolltop.velocity('fadeOut', {duration: 'fast'});
    }
  }, 300));

  scrolltop.on('click', function () {
    if (navigator.userAgent && navigator.userAgent.includes('MSIE 10.0')) {
      $('body').velocity('scroll', {offset: 0, mobileHA: false});
    } else {
      $('html').velocity('scroll', {offset: 0, mobileHA: false});
    }
  });
});
