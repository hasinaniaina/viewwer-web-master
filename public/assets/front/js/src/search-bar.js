$(function () {
  var container = $('#search-bar');
  if (!container.length) return;

  var RANGE_BUDGET_SALE = {min: 0, max: 505000, from: 0, to: 505000, step: 5000};
  var RANGE_BUDGET_RENT = {min: 0, max: 2550, from: 0, to: 2550, step: 50};
  var RANGE_AREA = {min: 0, max: 155, from: 0, to: 155, step: 5};
  var RANGE_ROOMS = {min: 1, max: 8, from: 1, to: 8, step: 1};

  var TEXT_MINMAX = container.data('text-minmax');

  function rangeOnFinish(data, from, to) {
    if (data.from !== data.min && data.from !== data.max) {
      app.qs.setItem(from, data.from);
    } else {
      app.qs.removeItem(from);
    }
    if (data.to !== data.max && data.to !== data.min) {
      app.qs.setItem(to, data.to);
    } else {
      app.qs.removeItem(to);
    }
  }

  function applyParameterToSettings(parameter, settings, prop) {
    if (app.qs.getItem(parameter)) {
      var p = parseInt(app.qs.getItem(parameter), 10);
      if (!isNaN(p) && (p < settings.max && p > settings.min)) {
        settings[prop] = p;
      } else {
        app.qs.removeItem(parameter);
      }
    }
  }

  container.find('input[name="range-budget"]').ionRangeSlider();
  var rangeBudget = container.find('input[name="range-budget"]').data('ionRangeSlider');

  rangeBudget.update(_.assign({}, {
    type: 'double',
    hide_min_max: true,
    prettify: function (data) {
      if (data === this.max) return TEXT_MINMAX;
      return data + '&nbsp;&euro;';
    },
    onFinish: function (data) {
      rangeOnFinish(data, 'budgetFrom', 'budgetTo');
    }
  }, getBudgetSettings()));

  function getBudgetSettings() {
    var settings;

    if (app.qs.getItem('saleType') === 'rent') {
      settings = _.assign({}, RANGE_BUDGET_RENT);
    } else {
      settings = _.assign({}, RANGE_BUDGET_SALE);
    }

    applyParameterToSettings('budgetFrom', settings, 'from');
    applyParameterToSettings('budgetTo', settings, 'to');

    return settings;
  }

  container.find('input[name="range-area"]').ionRangeSlider();
  var rangeArea = container.find('input[name="range-area"]').data('ionRangeSlider');

  rangeArea.update(_.assign({}, {
    type: 'double',
    hide_min_max: true,
    prettify: function (data) {
      if (data === this.max) return TEXT_MINMAX;
      return data + '&nbsp;m<sup>2</sup>';
    },
    onFinish: function (data) {
      rangeOnFinish(data, 'areaFrom', 'areaTo');
    }
  }, function () {
    var settings = _.assign({}, RANGE_AREA);

    applyParameterToSettings('areaFrom', settings, 'from');
    applyParameterToSettings('areaTo', settings, 'to');

    return settings;
  }()));

  container.find('input[name="range-rooms"]').ionRangeSlider();
  var rangeRooms = container.find('input[name="range-rooms"]').data('ionRangeSlider');

  rangeRooms.update(_.assign({}, {
    type: 'double',
    hide_min_max: true,
    prettify: function (data) {
      if (data === this.max) return TEXT_MINMAX;
      return data;
    },
    onFinish: function (data) {
      rangeOnFinish(data, 'roomsFrom', 'roomsTo');
    }
  }, function () {
    var settings = _.assign({}, RANGE_ROOMS);

    applyParameterToSettings('roomsFrom', settings, 'from');
    applyParameterToSettings('roomsTo', settings, 'to');

    return settings;
  }()));

  var saleType = container.find('input[name="sale-type"]');

  saleType.on('change', function () {
    if (_.includes(['sale', 'rent'], this.value)) {
      app.qs.setItem('saleType', this.value);
    } else {
      app.qs.removeItem('saleType');
    }

    rangeBudget.update(getBudgetSettings());
  });

  var placeType = container.find('select[name="place-type"]');

  placeType.on('change', function () {
    if (_.includes(['house', 'apartment'], this.value)) {
      app.qs.setItem('placeType', this.value);
    } else {
      app.qs.removeItem('placeType');
    }
  });

  // google maps
  var address = container.find('input[name="address"]');
  var addressesDropdown = container.find('.addresses-dropdown');

  var mapsService = new google.maps.places.AutocompleteService();
  var placesService = new google.maps.places.PlacesService(container.find('.address-map').get(0));
  var geocoderService = new google.maps.Geocoder;

  var MAP_SETTINGS = {
    types: ['geocode'],
    componentRestrictions: {country: 'fr'}
  };

  function showPredictions(preds) {

    if (_.isEmpty(preds)) return;

    address.addClass('active');
    addressesDropdown.find('li').addClass('old');

    var list = $('<ul>');

    _.each(preds, function (p) {
      var el = $('<li>').append(
        '<div class="list-main">' +
        '<span>' +
        '<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">' +
        '<path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z" class="">' +
        '</path>' +
        '</svg>' +
        '</span>' +
        p.structured_formatting.main_text +
        '</div>' +
        '<div class="list-secondary">' +
        _.get(p, 'structured_formatting.secondary_text', '') +
        '</div>');

      el.data('place_id', p.place_id);
      el.data('place_main', p.structured_formatting.main_text);
      el.data('place_description', p.description);

      list.append(el);
    });

    list
      .on('click', 'li', function () {
        var el = $(this);

        placesService.getDetails({
          placeId: el.data('place_id')
        }, function (place, status) {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            address.val(el.data('place_main'));
            app.qs.setItem('locationText', el.data('place_main'));
            app.qs.setItem('locationPlaceId', el.data('place_id'));
            app.qs.setItem('locationCoords', {
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng()
            });
            toggleLocationClear(true);
          } else {
            console.error('Places service getDetails error');
          }
          closeAddresses();
        });
      });

    addressesDropdown.find('.old').each(function () {
      $(this).remove();
    });

    addressesDropdown.append(list);
    addressesDropdown.show();
  }

  address.on('click', function () {
    closeAddresses();
  });

  address.on('input', function () {
    clearAddressError();
    clearLocationStorage();

    if (!$(this).val()) {
      toggleLocationClear(false);
      closeAddresses();
      return;
    }

    mapsService.getPlacePredictions(_.assign({}, MAP_SETTINGS, {
      input: $(this).val()
    }), showPredictions);
  });

  function closeAddresses() {
    checkAddress();
    address.removeClass('active');
    addressesDropdown.hide();
    addressesDropdown.empty();
  }

  function checkAddress() {
    if (address.val() &&
      address.val() !== app.qs.getItem('locationText')) {
      address.addClass('wrong-input');
    } else {
      address.removeClass('wrong-input');
    }
  }

  function clearAddressError() {
    address.removeClass('wrong-input');
  }

  $(document).on('mouseup touchend', function (e) {
    if (!addressesDropdown.is(e.target) &&
      addressesDropdown.has(e.target).length === 0) closeAddresses();
  });

  var searchButton = container.find('.search-button');
  searchButton.on('click', function () {
    var query = '';
    if (!_.isEmpty(app.qs.current)) {
      query = '?' + app.qs.toString(app.qs.current);
    }
    location.href = '/search' + query;
  });

  // geo location
  var SWAL_MESSAGE = {
    type: 'error',
    position: 'center',
    showConfirmButton: true,
    buttonsStyling: false,
    confirmButtonClass: 'btn btn-default'
  };

  var geoLocation = container.find('.location');
  var geoLocationIcon = geoLocation.find('.icon');
  var geoLocationIconClear = geoLocation.find('.icon-clear');

  function toggleLocationClear(enable) {
    if (enable) {
      geoLocationIcon.hide();
      geoLocationIconClear.show();
    } else {
      geoLocationIcon.show();
      geoLocationIconClear.hide();
    }
  }

  geoLocation.on('click', function () {

    clearLocationStorage();
    clearAddressError();
    address.val('');

    if (geoLocationIconClear.is(':visible')) {
      toggleLocationClear(false);
      return;
    }

    if (!navigator.geolocation) {
      app.swal(_.assign({}, SWAL_MESSAGE, {
        text: geoLocation.data('geo-message-not-enabled')
      }));
      return;
    }

    geoLocation.addClass('active');

    navigator.geolocation.getCurrentPosition(
      function success(position) {
        var latlng = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        geocoderService.geocode({'location': latlng}, function (results, status) {

          geoLocation.removeClass('active');

          if (status !== 'OK') {
            console.error('Geocoder failed due to: ' + status);
            app.swal(_.assign({}, SWAL_MESSAGE, {
              text: geoLocation.data('geo-message-error')
            }));
            return;
          }

          if (results[0]) {
            app.qs.setItem('locationText', results[0].formatted_address);
            app.qs.setItem('locationPlaceId', results[0].place_id);
            app.qs.setItem('locationCoords', latlng);
            app.qs.setItem('locationUser', true);
            address.val(results[0].formatted_address);
            toggleLocationClear(true);
          } else {
            console.error('Geocoder no results');
            app.swal(_.assign({}, SWAL_MESSAGE, {
              text: geoLocation.data('geo-message-error')
            }));
          }
        });
      },
      function error(e) {
        geoLocation.removeClass('active');
        console.error(e);
        var message = geoLocation.data('geo-message-error');
        if (e.code === e.PERMISSION_DENIED) {
          message = geoLocation.data('geo-message-denied');
        }
        app.swal(_.assign({}, SWAL_MESSAGE, {
          text: message
        }));
      }
    );
  });

  function clearLocationStorage() {
    app.qs.removeItem('locationText');
    app.qs.removeItem('locationPlaceId');
    app.qs.removeItem('locationCoords');
    app.qs.removeItem('locationUser');
  }

  function loadPreviousValues() {

    if (_.includes(['sale', 'rent'], app.qs.getItem('saleType'))) {
      saleType.filter(function () {
        return this.value === app.qs.getItem('saleType');
      }).trigger('click');
    } else {
      saleType.filter(function () {
        return this.value === 'all';
      }).trigger('click');
    }

    if (app.qs.getItem('placeType')) {
      placeType.val(app.qs.getItem('placeType')).change();
    }

    if (app.qs.getItem('locationText')) {
      toggleLocationClear(true);
      address.val(app.qs.getItem('locationText'));
    }

  }

  loadPreviousValues();

  new Choices(placeType[0], {
    searchEnabled: false,
    itemSelectText: '',
    position: 'bottom',
    shouldSort: false
  });

});
