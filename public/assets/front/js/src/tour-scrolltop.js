$(function () {

  /* scroll to top */
  var SCROLLTOP_HEIGHT = 75;
  var scrolltop = $('#tour-scrolltop');
  if (!scrolltop.length) return;

  redrawScrollButton();

  $(window).scroll(_.throttle(function () {
    redrawScrollButton();
  }, 300));

  $(window).resize(_.throttle(function () {
    redrawScrollButton();
  }, 300));

  function redrawScrollButton() {
    var windowHeight = window.innerHeight;

    if ($(window).scrollTop() + windowHeight
      >= $(document).height() - SCROLLTOP_HEIGHT) {
      scrolltop.removeClass('down');
    } else {
      scrolltop.addClass('down');
    }

    if ($(document).height() >= windowHeight + SCROLLTOP_HEIGHT) {
      if (scrolltop.is(':visible')) return;
      scrolltop.velocity('stop');
      scrolltop.velocity('fadeIn', {duration: 'fast'});
    } else {
      if (!scrolltop.is(':visible')) return;
      scrolltop.velocity('stop');
      scrolltop.velocity('fadeOut', {duration: 'fast'});
    }
  }

  scrolltop.on('click', function () {
    var offset = 0;
    var windowHeight = window.innerHeight;
    if (scrolltop.hasClass('down')) offset = $(document).height() - windowHeight;
    if (navigator.userAgent && navigator.userAgent.includes('MSIE 10.0')) {
      $('body').velocity('scroll', {offset: offset, mobileHA: false});
    } else {
      $('html').velocity('scroll', {offset: offset, mobileHA: false});
    }
  });

});
