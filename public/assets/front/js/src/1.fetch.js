$(function () {

  /**
   * Signatures:
   * app.fetch(url) - GET url
   * app.fetch(url, data) - GET url + data as query parameters
   * app.fetch(url, method) - METHOD url
   * app.fetch(url, method, data) METHOD url + data as query parameters (GET) or body (POST)
   * app.fetch({url: String, method: String, data: Object})
   */
  app.fetch = function () {
    var options = (arguments.length === 1 && typeof arguments[0] === 'object' ? arguments[0] : {
      url: arguments[0],
      method: typeof arguments[1] === 'string' ? arguments[1] : 'GET',
      data: (typeof arguments[1] === 'object' ? arguments[1] : arguments[2])
    });

    var settings = {
      url: options.url,
      method: options.method,
      cache: false,
      xhrFields: {
        withCredentials: true
      }
    };

    if (options.method === 'POST') {
      settings.data = JSON.stringify(options.data);
      settings.contentType = 'application/json; charset=UTF-8';
    } else {
      settings.data = options.data;
    }

    return $.ajax(settings)
      .then(function (response) {
        if (response.error) {
          var err = new Error();

          err.responseJSON = response;

          throw err;
        }

        return response;
      })
      .catch(function (response) {
        if (window.app.isUnloading) return;

        var error = _.get(response, 'responseJSON.error');
        var message = error ? (error.message || error.code) : response.responseText || response.statusText || 'Unknown error';

        if (error) console.error(error);
        if (message) console.error(message);

        throw new Error(message);
      });
  };

});
