$(function () {

  app.tours = {};

  var RIGHT_SIDEBAR_DELAY = 3000;
  var LOADING_ANIM_DELAY = 1000;

  var HEIGHT_HIDE_DEV_LOGO = 550;
  var WIDTH_HIDE_SIDEBAR = 480;

  var ARGS_NORMAL = ', null, null, BLEND(0.3)';
  var ARGS_KEEPVIEW = ', null, KEEPVIEW, BLEND(0.3)';

  var LAYERS = '.tour-scene-layers .controls';

  $('.tour-partial').each(function () {
    var container = $(this);
    var tour = container.data('tour');
    if (!tour) return;
    var tourId = tour._id;

    var tourMainScene = container.data('mainscene');
    var tourPolygons = container.data('polygons');

    var hideSidebarRight = container.data('hide-sidebar-right');
    var tourDelay = container.data('delay');

    if (!_.find(tour.scenes, {_id: tourMainScene})) {
      tourMainScene = '';
      tourPolygons = [];
    }

    var timerRightSidebar;
    var timerLoadingAnim;

    var currentScene;
    var sameTypeScenes = [];

    app.tours[tourId] = {
      tour: tour,
      container: container,
      init: function () {
        if (!container.length) return;

        embedpano({
          xml: '/storage/tours/' + tourId + '/final/panorama.xml?mainscene=' + tourMainScene + '&prevent_cache=' + new Date().getTime(),
          target: 'panorama-' + tourId,
          id: 'krpano-' + tourId,
          bgcolor: '#ffffff',
          mobilescale: 1
        });

        app.tours[tourId].krpano = $('#krpano-' + tourId)[0];
      },
      loadPolygon: function (scene_id) {
        var p = _.find(tourPolygons, {scene: scene_id});
        if (p) {
          app.tours[tourId].krpano.call('pe_create_polygon();');
          app.tours[tourId].krpano.call('addhotspot(polygon);');
          app.tours[tourId].krpano.call('hotspot[polygon].loadstyle(polygonal_hotspot_style);');
          for (var i = 0; i < p.points.length; i++) {
            app.tours[tourId].krpano.set('hotspot[polygon].point[' + i + '].ath', p.points[i].ath);
            app.tours[tourId].krpano.set('hotspot[polygon].point[' + i + '].atv', p.points[i].atv);
          }
        }
      },
      openScene: function (scene_id, keep_view) {
        timerLoadingAnim = setTimeout(function () {
          container.addClass('scene-loading');
        }, LOADING_ANIM_DELAY);

        currentScene = scene_id;

        if (keep_view) {
          app.tours[tourId].krpano.call('loadscene("scene_' + scene_id + '"' + ARGS_KEEPVIEW + ');');
        } else {
          app.tours[tourId].krpano.call('loadscene("scene_' + scene_id + '"' + ARGS_NORMAL + ');');
        }

        container.find('.scene').each(function () {
          if ($(this).data('_id') === scene_id) {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var scenes = container.find('.scenes');
            var scroll = scenes.scrollTop() + $(this).offset().top - scenes.offset().top - 10;
            scenes.animate({
              scrollTop: scroll
            });
            return false;
          }
        });

        app.tours[tourId].krpano.call('activate_planspot("planspot_' + scene_id + '")');

        // load polygon for scene
        app.tours[tourId].loadPolygon(scene_id);
      },
      openUrl: function (url) {
        window.open(url, '_blank');
      },
      openModal: function (heading, content) {
        BootstrapDialog.show({
          title: heading,
          message: content,
          animate: false
        });
      },
      enableKrpanoToggle: function (name) {
        var toggle = $('.' + name + '.action-krpano-toggle');
        if (toggle.length) toggle.addClass('active');
      },
      disableKrpanoToggle: function (name) {
        var toggle = $('.' + name + '.action-krpano-toggle');
        if (toggle.length) toggle.removeClass('active');
      },
      sceneLoaded: function () {
        var thisScene = _.find(tour.scenes, {_id: currentScene});

        if (!_.find(sameTypeScenes, {_id: currentScene})) {

          sameTypeScenes = _.filter(tour.scenes, function (s) {
            return (s.type && s.type._id &&
              thisScene.type && thisScene.type._id &&
              s.type._id === thisScene.type._id);
          });

          sameTypeScenes = _.orderBy(sameTypeScenes, function(s) {
            return _.get(s, 'layer.weight', 0);
          });

          var layersList = container.find(LAYERS);
          layersList.empty();

          if (!_.isEmpty(sameTypeScenes) && sameTypeScenes.length > 1) {
            _.each(sameTypeScenes, function (s) {
              layersList.append(
                '<div class="control ' + ((s._id === currentScene) ? 'active' : '') + '" ' +
                'onclick="' + 'app.tours[\'' + tourId + '\'].openScene(\'' + s._id + '\', true);" ' +
                'data-scene="' + s._id + '"' +
                '>' +
                '<img src="/storage/files/hotspots/' + _.get(s, 'layer.icon') + '">' +
                '</div>'
              );
            });
          }
        } else {
          container.find(LAYERS).children().each(function () {
            if ($(this).data('scene') === currentScene) {
              $(this).addClass('active');
              $(this).siblings().removeClass('active');
            }
          });
        }

        clearTimeout(timerLoadingAnim);
        container.removeClass('scene-loading');
        container.removeClass('loading');
      },
      krpanoResized: function (width, height) {
        if ($(window).width() <= WIDTH_HIDE_SIDEBAR) {
          var rightSidebar = container.find('.sidebar.right');
          rightSidebar.removeClass('active');
        }

        var logoDeveloper = container.find('.logo-developer');
        var logoGlobal = container.find('.logo-global');
        if (height < HEIGHT_HIDE_DEV_LOGO) {
          logoDeveloper.hide();
          logoGlobal.hide();
        } else {
          logoDeveloper.show();
          logoGlobal.show();
        }
      },
      krpanoLoaded: function () {
        var leftSidebar = container.find('.sidebar.left');
        leftSidebar.addClass('active');

        if ($(window).width() >= WIDTH_HIDE_SIDEBAR) {
          var rightSidebar = container.find('.sidebar.right');
          if (!hideSidebarRight) {
            rightSidebar.addClass('active');
            timerRightSidebar = setTimeout(function () {
              rightSidebar.removeClass('active');
            }, RIGHT_SIDEBAR_DELAY);
          }
        }
      },
      vrEnabled: function () {
        // for ios devices webvr
        if (app.isIPhone()) {
          var t = $('#krpano-' + tourId);
          t.css('z-index', 9999);
          container.hide();
        }
        //
      },
      vrDisabled: function () {
        if (app.isIPhone()) {
          var t = $('#krpano-' + tourId);
          t.css('z-index', 'auto');
          container.show();
        }
        //
      },
      vrAvailable: function () {
        var vrControl = container.find('.action-krpano-toggle.vr');
        if (vrControl.length) vrControl.show();
      },
      gyroAvailable: function () {
        var gyroControl = container.find('.action.gyro');
        if (gyroControl.length) gyroControl.show();
      }
    };

    /* delayed tour initialization */
    setTimeout(function () {
      app.tours[tourId].init();
    }, tourDelay ? tourDelay : 0);

    /* change scenes */
    container.on('click', '.scene', function () {
      var control = $(this);
      app.tours[tourId].openScene(control.data('_id'));
    });

    /* toggle sidebars */
    container.on('click', '.sidebar .toggle .slide-in', function () {
      var sidebar = $(this).parents('.sidebar');

      sidebar.addClass('active');

      if ($(window).width() < WIDTH_HIDE_SIDEBAR) {
        sidebar.siblings('.sidebar').removeClass('active');
      }
    });

    container.on('click', '.sidebar .toggle .slide-out', function () {
      $(this).parents('.sidebar').removeClass('active');
    });

    container.on('mouseleave', '.sidebar.right', function () {
      var that = $(this);
      timerRightSidebar = setTimeout(function () {
        that.removeClass('active');
      }, RIGHT_SIDEBAR_DELAY);
    });

    container.on('mouseenter', '.sidebar.right', function () {
      clearTimeout(timerRightSidebar);
    });

    /* actions */
    container.on('click', '.action', function () {
      $(this).toggleClass('active');
    });

    /* toggle fullscreen */
    var fullscreenControl = container.find('.action.fullscreen');

    if (window.screenfull && screenfull.enabled) {
      fullscreenControl.show();

      fullscreenControl.on('click', function () {
        screenfull.toggle();
      });

      screenfull.on('change', function () {
        fullscreenControl.toggleClass('active', screenfull.isFullscreen);
      });
    }

    /* toggle autorotate */
    var autorotateControl = container.find('.action.autorotate');

    autorotateControl.on('click', function () {
      app.tours[tourId].krpano.call('switch("autorotate.enabled");');
    });

    /* toggle plan */
    var planControl = container.find('.action.plan');
    if (app.tours[tourId].tour.plan) planControl.show();

    planControl.on('click', function () {
      app.tours[tourId].krpano.call('switch_plan();');
    });

    /* toggle vr */
    var vrControl = container.find('.action-krpano-toggle.vr');

    vrControl.on('click', function () {
      if (app.isIPhone()) {

        // detect deviceorientation problem
        var timerId = setTimeout(function () {          
          // display a message when the deviceorientation event is not supported
          // explain how to enable the feature on the smartphone

          BootstrapDialog.show({
            animate: false,
            title: 'Information',
            message: "<p>Pour utiliser la VR, votre navigateur a besoin d'une autorisation pour accéder au gyroscope.</p>" +
                    "<p>Autorisez l'accès au gyroscope puis rechargez cette page pour utiliser la VR.</p>",
            buttons: [{
              label : "Plus d'infos",
              cssClass: 'btn-primary',
              action: function(dialogRef) {
                window.open("/pages/gyro-help");
                dialogRef.close();
              }
            }, {
              label: "Fermer",
              cssClass: 'btn-primary',
              action: function(dialogRef){
                dialogRef.close();
              }
            }]
        });

        }, 500);

        var tempDeviceOrientationCallBack = function() {
            // remove callback if the event happened
              window.removeEventListener('deviceorientation', tempDeviceOrientationCallBack, false);
              clearTimeout(timerId);
              
              // diplay the VR
              app.tours[tourId].krpano.call('WebVR.toggleVR();');
        };

        window.addEventListener('deviceorientation', tempDeviceOrientationCallBack, false);        
      }
      else {
        app.tours[tourId].krpano.call('WebVR.toggleVR();');
      }
    });

    /* toggle gyro */
    var gyroControl = container.find('.action.gyro');

    gyroControl.on('click', function () {
      app.tours[tourId].krpano.call('switch(plugin[gyro].enabled);');
    });

    /* toggle legal info */
    var legalControl = container.find('.action-no-toggle.legal');

    legalControl.on('click', function () {
      app.tours[tourId].openModal($(this).data('header'), $(this).data('text'));
    });

  });

});
