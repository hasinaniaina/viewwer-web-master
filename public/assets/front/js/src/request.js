$(function () {

  var requestApartmentButton = $('#request-apartment-button');
  var requestInfoButton = $('#request-info-button');
  var requestGlassesButton = $('#request-glasses-button');

  if (!requestApartmentButton.length &&
    !requestInfoButton.length &&
    !requestGlassesButton.length) return;

  var formApartment = $($('#request-apartment-modal-template').text());
  var formInfo = $($('#request-info-modal-template').text());
  var formGlasses = $($('#request-glasses-modal-template').text());

  if (requestApartmentButton.length) {
    requestApartmentButton.on('click', function () {
      if ($(this).hasClass('disabled')) return;
      bootstrapDialog(formApartment, $(this));
    });
  }

  if (requestInfoButton.length) {
    requestInfoButton.on('click', function () {
      if ($(this).hasClass('disabled')) return;
      fillSimilarFields(formGlasses, formInfo);
      bootstrapDialog(formInfo, $(this));
    });
  }

  if (requestGlassesButton.length) {
    requestGlassesButton.on('click', function () {
      if ($(this).hasClass('disabled')) return;
      fillSimilarFields(formInfo, formGlasses);
      bootstrapDialog(formGlasses, $(this));
    });
  }

  function fillSimilarFields(src_form, dst_form) {
    if (!src_form || !dst_form) return;

    var src_type = src_form.data('type');
    var dst_type = dst_form.data('type');

    var fields = [
      'input[name="firstName"]',
      'input[name="lastName"]',
      'input[name="email"]'
    ];

    if ((src_type === 'info' && dst_type === 'glasses') ||
      (src_type === 'glasses' && dst_type === 'info')) {
      _.each(fields, function (f) {
        var s = src_form.find(f);
        var d = dst_form.find(f);
        if (d && d.val()) return;
        if (!s || !s.val()) return;
        d.val(s.val());
      });
    }
  }

  function bootstrapDialog(form, button) {
    BootstrapDialog.show({
      animate: false,
      title: '',
      autodestroy: false,
      onshow: function (dialog) {
        dialog.setMessage(form);
      },
      buttons: [{
        label: '<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">' +
        '<path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z" class=""></path>' +
        '</svg>' + form.data('button-text'),
        cssClass: 'btn btn-success btn-icon',
        action: function (dialog) {
          sendForm(form, button, dialog);
        }
      }]
    });
  }

  function sendForm(form, button, dialog) {
    var fields = [];
    form.find('input, select').each(function () {
      fields.push($(this));
    });

    var validated = true;
    _.each(fields, function (f) {
      f.removeClass('wrong-input');
      var v = f[0].checkValidity();
      if (!v) {
        f.addClass('wrong-input');
        validated = false;
      }
    });

    if (!validated) {
      form[0].setAttribute('data-balloon-visible', '');
      setTimeout(function () {
        form[0].removeAttribute('data-balloon-visible');
      }, 4000);
      return;
    }

    var referrer = form.data();

    var data = {
      type: referrer.type,
      url: window.location.href,
      apartment: referrer.apartment,
      building: referrer.building,
      program: referrer.program,
      developer: referrer.developer,
    };

    _.each(fields, function (f) {
      if (f.attr('name') === 'budget') {
        var b = f.val().split('-');
        data.budgetFrom = b[0];
        data.budgetTo = b[1];
      } else {
        data[f.attr('name')] = f.val().trim();
      }
    });

    app.fetch('/request', 'POST', data)
      .then(function () {
        app.swal({
          type: 'success',
          text: form.data('success'),
          position: 'center',
          showConfirmButton: false,
          timer: 3000
        });
        button.addClass('disabled');
        dialog.close();
      })
      .catch(function () {
        app.swal({
          type: 'error',
          text: form.data('failed'),
          position: 'center',
          showConfirmButton: false,
          timer: 3000
        });
        dialog.close();
      });
  }

});
