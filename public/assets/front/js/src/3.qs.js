$(function () {
  app.qs = {
    toString: function (query) {
      return window.Qs.stringify(query);
    },
    getItem: function (key) {
      return this.current[key];
    },
    setItem: function (key, value) {
      var params = _.cloneDeep(this.current);
      params[key] = value;
      this.refresh(params);
    },
    removeItem: function (key) {
      // prevent security warning on frequent refresh
      if (this.current[key] === undefined) return;
      var params = _.cloneDeep(this.current);
      delete params[key];
      this.refresh(params);
    },
    refresh: function (params) {
      var baseUrl = [location.protocol, '//', location.host, location.pathname].join('');
      var query = '';
      if (!_.isEmpty(params)) query = '?' + this.toString(params);
      window.history.replaceState({}, '', baseUrl + query);
    }
  };

  Object.defineProperty(app.qs, 'current', {
    get: function () {
      return window.Qs.parse(document.location.search.substr(1));
    }
  });
});
