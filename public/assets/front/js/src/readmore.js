$(function () {
  if (!$('.readmore').length) return;

  app.readmore = {
    refresh: readMore
  };

  var MAX_HEIGHT = 200;
  var THRESHOLD = 50;

  function readMore(resize) {

    $('.readmore').each(function () {
      var container = $(this);
      var content = container.find('.content');
      var fadeout = content.find('.fadeout');
      var expand = container.find('.expand');

      // creating collapsed blocks
      if (!content.data('readmore') && !resize) {
        if (content.height() > MAX_HEIGHT + THRESHOLD) {
          if (content.data('readmore') !== 'collapsed') {
            content.data('readmore', 'collapsed');
            expand.show();
            fadeout.addClass('active');
            content.css('max-height', MAX_HEIGHT);
          }
        }
      }

      // recalculate height
      if (content.data('readmore') === 'collapsed' && resize) {
        if (content[0].scrollHeight > MAX_HEIGHT + THRESHOLD) {
          content.css('max-height', MAX_HEIGHT);
          expand.css('opacity', 1);
          fadeout.addClass('active');
        } else {
          content.css('max-height', '');
          expand.css('opacity', 0);
          fadeout.removeClass('active');
        }
      }

      expand.on('click', function () {
        expandBlock();
        return false;
      });

      fadeout.on('click', function () {
        expandBlock();
        return false;
      });

      function expandBlock() {
        if (content.data('readmore') === 'expanded') return;
        content.data('readmore', 'expanded');
        expand.velocity({opacity: 0});
        fadeout.removeClass('active');
        content.velocity({'max-height': content[0].scrollHeight}, {
          complete: function () {
            content.css('max-height', '');
          }
        });
      }
    });
  }

  $(window).on('resize', _.debounce(function () {
    readMore(true);
  }));

  readMore();
});
