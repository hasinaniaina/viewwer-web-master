$(function () {
  app.social = function (options) {

    var selector = options.selector;

    var title = selector.data('title');
    var urlbase = selector.data('urlbase');
    var entityType = selector.data('entity-type');
    var entityId = selector.data('entity-id');
    var shareUrl = selector.data('share-url');
    var integrateUrl = selector.data('integrate-url');
    var shareCurrent = !!(selector.data('share-current-page'));
    var mobile = selector.data('mobile');
    var favoriteId = selector.data('favorite-id');

    if (options.counterRating) selector.find('.like .count').html(options.counterRating);

    if (options.title) title = options.title;
    if (options.entityType) entityType = options.entityType;
    if (options.entityId) entityId = options.entityId;
    if (options.shareUrl) shareUrl = options.shareUrl;
    if (options.favoriteId) favoriteId = options.favoriteId;

    var liked = JSON.parse(app.localStorage.getItem('liked') || '[]');

    selector.find('.like')
      .each(function () {
        if (_.includes(liked, entityId)) {
          var icon = $(this).find('.icon');
          var iconLiked = $(this).find('.icon-liked');
          if (icon.length && iconLiked.length) {
            icon.hide();
            iconLiked.show();
          }
          $(this).addClass('active');
        }
        selector.css({opacity: 1});
      })
      .on('click', function () {

        var action = 'like';
        var liked = JSON.parse(app.localStorage.getItem('liked') || '[]');
        if (_.includes(liked, entityId)) action = 'dislike';

        var icon = $(this).find('.icon');
        var iconLiked = $(this).find('.icon-liked');

        if (action === 'like') {
          if (icon.length && iconLiked.length) {
            icon.hide();
            iconLiked.show();
            iconLiked.addClass('bounce');
            setTimeout(function () {
              iconLiked.removeClass('bounce');
            }, 1000);
          }
          $(this).addClass('active');
        } else {
          if (icon.length && iconLiked.length) {
            iconLiked.hide();
            iconLiked.removeClass('bounce');
            icon.show();
          }
          $(this).removeClass('active');
        }

        var count = $(this).find('.count');
        if (count.length) {
          var c = parseInt(count.text(), 10);
          if (isNaN(c)) c = 0;
          c = c + (action === 'like' ? 1 : -1);
          if (c <= 0) c = '';
          count.text(c);
        }

        if (action === 'like') {
          liked.push(entityId);
        } else {
          liked = _.without(liked, entityId);
        }

        app.localStorage.setItem('liked', JSON.stringify(liked));

        app.fetch('/api/rating',
          'POST', {
            entityType: entityType,
            entityId: entityId,
            action: action
          })
          .then(function () {
          })
          .catch(function (e) {
            console.error(e);
          });

      });

    var favoriteButton = selector.find('.favorite');

    if (favoriteButton) {
      if (favoriteId) favoriteButton.addClass('active');

      favoriteButton.on('click', function () {
        if (favoriteId) {
          app.fetch('/api/favorites/' + favoriteId + '/delete',
            'POST', {})
            .then(function () {
              favoriteId = '';
              favoriteButton.removeClass('active');
            })
            .catch(function (e) {
              console.error(e);
            });
        } else {
          app.fetch('/api/favorites',
            'POST', {
              entityType: entityType,
              entityId: entityId
            })
            .then(function (r) {
              favoriteId = r.favoriteId;
              favoriteButton.addClass('active');
              var iconAdded = favoriteButton.find('.icon-added');
              if (iconAdded.length) {
                iconAdded.addClass('bounce');
                setTimeout(function () {
                  iconAdded.removeClass('bounce');
                }, 1000);
              }
            })
            .catch(function (e) {
              console.error(e);
            });
        }
      });
    }

    var shareButton = selector.find('.share');
    if (!shareButton) return;

    var url;
    if (shareCurrent) {
      url = window.location.href;
    } else {
      url = urlbase + shareUrl;
    }

    if (mobile) {
      shareButton.on('click', function () {
        var vu = new URL(url);
        vu.searchParams.append('share', true);
        vu.protocol = 'viewwer';
        window.open(vu, '_blank');
      });
      return;
    }

    shareButton.on('click', function () {
      var t = $($('#social-modal-template').text());

      // share url
      var inputUrl = t.find('#social-modal-url');
      inputUrl.val(url);

      // iframe url
      if (integrateUrl) {
        var inputIntegrate = t.find('#social-modal-integrate');
        inputIntegrate.val(generateIFrame(urlbase + integrateUrl));
        t.find('.integrate').css('display', 'block');
      }

      t.find('input').each(function () {
        var clipboard = new ClipboardJS($(this)[0]);
        clipboard.on('success', function (e) {
          e.clearSelection();
          var balloon = $(e.trigger).parent()[0];
          balloon.setAttribute('data-balloon-visible', '');
          setTimeout(function () {
            balloon.removeAttribute('data-balloon-visible');
          }, 2000);
        });
      });

      t.find('.share-button').each(function () {
        this.setAttribute('data-title', title);
        this.setAttribute('data-url', url);
        this.setAttribute('data-rating_data',
          JSON.stringify({
            entityType: entityType,
            entityId: entityId,
            action: 'share'
          }));
      });

      BootstrapDialog.show({
        animate: false,
        title: '',
        onshow: function (dialog) {
          dialog.setMessage(t);
          dialog.getModalBody().find('.social-modal').css({display: 'block'});
        }
      });

      function generateIFrame(url) {
        return '<iframe src="' + url + '" ' +
          'width="100%" height="100%" ' +
          'frameborder="0" ' +
          'allowFullScreen="true" ' +
          'webkitallowfullscreen="true" ' +
          'mozallowfullscreen="true"></iframe>';
      }
    });
  };

  // app.share uses data-attributes of each share button
  $(document).on('click', '.share-button', function () {
    app.share(this);
  });

  $('.social').each(function () {
    app.social({selector: $(this)});
  });
});
