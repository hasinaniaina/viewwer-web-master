$(function () {
  var cm = $('#cookies-message');
  if (!cm.length) return;

  if (Cookies.get('cookies_accepted')) return;

  var message = cm.data('message');
  var buttonAgreeText = cm.data('button-agree');
  var buttonLinkText = cm.data('button-link');
  var link = cm.data('link');

  var n = new Noty({
    theme: 'cookies',
    text: message,
    layout: 'bottomCenter',
    buttons: [
      Noty.button(buttonLinkText, 'btn btn-link', function () {
        window.open(link, '_blank');
      }, {}),
      Noty.button(buttonAgreeText, 'btn btn-success', function () {
        Cookies.set('cookies_accepted', true, {expires: 365, domain: $('html').data('domain')});
        n.close();
      }, {}),
    ]
  }).show();
});
