$(function () {
  var container = $('#program');

  if (!container.length) return;

  var aerial = container.find('.aerial');

  // workaround - iframe breaks layout width
  $(window).on('orientationchange', function () {
    loadAerial();
  });

  function loadAerial() {
    aerial.html('');
    aerial.removeClass('loading-circle');
    aerial.html('<iframe src="' + aerial.data('url') + '" ' +
      'frameborder="0" ' +
      'allowFullScreen="true" ' +
      'webkitallowfullscreen="true" ' +
      'mozallowfullscreen="true"></iframe>');
  }

  setTimeout(loadAerial, 3000);

  var mapContainer = container.find('.map');
  var location = mapContainer.data('location');

  if (!location || location.coordinates[0] === 0) return;

  var center = {
    lng: location.coordinates[0],
    lat: location.coordinates[1]
  };

  var map = new google.maps.Map(mapContainer[0], {
    zoom: 17,
    center: center,
    scrollwheel: false
  });

  var marker = new google.maps.Marker({
    position: center,
    map: map
  });

  var infowindow = new google.maps.InfoWindow({
    content: mapContainer.data('address')
  });

  marker.addListener('click', function () {
    infowindow.open(map, marker);
  });

  var itemsMainDiv = ('.MultiCarousel');
  var itemsDiv = ('.MultiCarousel-inner');
  var itemWidth = "";

  $('.leftLst, .rightLst').click(function () {
      var condition = $(this).hasClass("leftLst");
      if (condition)
          click(0, this);
      else
          click(1, this)
  });

  ResCarouselSize();

  $(window).resize(function () {
      ResCarouselSize();
  });

  //this function define the size of the items
  function ResCarouselSize() {
      var incno = 0;
      var dataItems = ("data-items");
      var itemClass = ('.item');
      var id = 0;
      var btnParentSb = '';
      var itemsSplit = '';
      var sampwidth = $(itemsMainDiv).width();
      var bodyWidth = $('body').width();
      $(itemsDiv).each(function () {
          id = id + 1;
          var itemNumbers = $(this).find(itemClass).length;
          btnParentSb = $(this).parent().attr(dataItems);
          itemsSplit = btnParentSb.split(',');
          $(this).parent().attr("id", "MultiCarousel" + id);
          if (bodyWidth >= 1200) {
            incno = itemsSplit[1];
            itemWidth = sampwidth / incno;
        }
        else {
            incno = itemsSplit[0];
            itemWidth = sampwidth / incno;
        }
          $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
          $(this).find(itemClass).each(function () {
              $(this).outerWidth(itemWidth);
          });

          $(".leftLst").addClass("over");
          $(".rightLst").removeClass("over");

      });
  }


  //this function used to move the items
  function ResCarousel(e, el, s) {
      var leftBtn = ('.leftLst');
      var rightBtn = ('.rightLst');
      var translateXval = '';
      var divStyle = $(el + ' ' + itemsDiv).css('transform');
      var values = divStyle.match(/-?[\d\.]+/g);
      var xds = Math.abs(values[4]);
      if (e == 0) {
          translateXval = parseInt(xds) - parseInt(itemWidth * s);
          $(el + ' ' + rightBtn).removeClass("over");

          if (translateXval <= itemWidth / 2) {
              translateXval = 0;
              $(el + ' ' + leftBtn).addClass("over");
          }
      }
      else if (e == 1) {
          var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
          translateXval = parseInt(xds) + parseInt(itemWidth * s);
          $(el + ' ' + leftBtn).removeClass("over");

          if (translateXval >= itemsCondition - itemWidth / 2) {
              translateXval = itemsCondition;
              $(el + ' ' + rightBtn).addClass("over");
          }
      }
      $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
  }

  //It is used to get some elements from btn
  function click(ell, ee) {
      var Parent = "#" + $(ee).parent().attr("id");
      var slide = $(Parent).attr("data-slide");
      ResCarousel(ell, Parent, slide);
  }

  //open

  loadGallery(true, 'a.thumbnail');

  function loadGallery(setIDs, setClickAttr){
      var current_image,
          selector,
          counter = 0;

      function updateGallery(selector) {
          var $sel = selector;
          current_image = $sel.data('image-id');
          $('#image-gallery-caption').text($sel.data('caption'));
          $('#image-gallery-title').text($sel.data('title'));
          $('#image-gallery-image').attr('src', $sel.data('image'));
              }

      if(setIDs == true){
          $('[data-image-id]').each(function(){
              counter++;
              $(this).attr('data-image-id',counter);
          });
      }
      $(setClickAttr).on('click',function(){
          updateGallery($(this));
      });
    }

});

