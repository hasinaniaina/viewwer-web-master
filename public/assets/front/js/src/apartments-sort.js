$(function () {

  $('#apartments .sort select').on('change', function () {
    var prop = $(this).val().split('.')[0];
    var order = $(this).val().split('.')[1];

    $('#apartments .building').each(function () {
      var list = $(this).find('.list');
      var apts = list.find('.apartment');
      var vals = [];

      apts.each(function (idx) {
        var apt = $(this);
        var val = apt.data(prop);

        if (prop === 'area' || prop === 'price') val = parseFloat(val);

        vals.push({idx: idx, val: val});
      });

      vals = _.orderBy(vals, 'val', order).reverse();

      vals.forEach(function (val) {
        apts.eq(val.idx).prependTo(list);
      });
    });

    app.lazy.update();
  });

});
