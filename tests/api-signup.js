require('dotenv').config();

const shortid = require('shortid');

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const USER_SIGNUP = {
  // bad name
  name: 'viewwer',
  email: 'bad email',
  password: shortid() + shortid(),
  // optional fields
  phone: '+123456789'
};

const TIMEOUT_REQUEST = 15 * 1000;

describe('User', () => {

  let cookie;

  it('registration - bad request', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/signup`)
      .send(USER_SIGNUP)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(USER_SIGNUP));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('error').eql('bad_request');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('registration - reserved user name', (done) => {
    USER_SIGNUP.email = `mobile-test-${shortid()}@viewwer.com`;
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/signup`)
      .send(USER_SIGNUP)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(USER_SIGNUP));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('error').eql('wrong_user_name');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('registration', (done) => {
    // allowed name
    USER_SIGNUP.name = USER_SIGNUP.email.split('@')[0];

    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/signup`)
      .send(USER_SIGNUP)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(USER_SIGNUP));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('auth', (done) => {
    let auth = {
      email: USER_SIGNUP.email,
      password: USER_SIGNUP.password
    };
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(auth)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(auth));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('profile', (done) => {
    chai.request(settings.HOST)
      .get(`${settings.URL_USER}/profile`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('user exists error', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/signup`)
      .send(USER_SIGNUP)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(USER_SIGNUP));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('failed');
        res.body.should.have.property('error').eql('user_exists');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
