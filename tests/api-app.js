require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const URL_APP = '/api/app';

const TIMEOUT_REQUEST = 15 * 1000;

describe('App', () => {

  it('settings', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_APP}/settings`)
      .end((err, res) => {
        mlog.log('RESPONSE:\n', JSON.stringify(res.body, 0, 4));

        res.body.should.have.property('app');
        res.body.app.minimumVersion.should.be.a('number');
        res.body.app.maintenance.should.be.a('boolean');
        res.body.app.urlPasswordReset.should.be.a('string');
        res.body.app.urlMessages.should.be.a('string');

        res.body.should.have.property('thumbnails');
        res.body.thumbnails.width.should.be.a('number');
        res.body.thumbnails.height.should.be.a('number');

        res.body.scenes.types.should.be.a('array');
        res.body.scenes.types[0].id.should.be.a('string');
        res.body.scenes.types[0].locales.should.be.a('object');
        res.body.scenes.types[0].localesWeight.should.be.a('object');
        res.body.scenes.types[0].iconUrl.should.be.a('string');

        res.body.apartmentTypes.should.be.a('array');
        res.body.apartmentTypes[0].type.should.be.a('string');
        res.body.apartmentTypes[0].locales.should.be.a('object');

        res.body.should.have.property('signup');
        res.body.signup.minimalPasswordScore.should.be.a('number');
        res.body.signup.minimalPasswordLength.should.be.a('number');
        res.body.signup.minimalUserNameLength.should.be.a('number');

        res.body.locales.should.be.a('array');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
