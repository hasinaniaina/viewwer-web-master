require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

// not requires auth yet
const URL_SEARCH = '/api/search';

const SEARCH_REQUEST = {
  // all / apartment / house
  placeType: 'all',
  // all / sale / rent
  saleType: 'all',

  budgetFrom: 0,
  budgetTo: 500000,

  areaFrom: 0,
  areaTo: 150,

  roomsFrom: 1,
  roomsTo: 7,

  location: {
    // google placeId not necessary yet
    placeId: undefined,
    // float
    lat: undefined,
    // float
    lng: undefined,
    // boolean - is user's coordinates
    user: undefined
  },

  // for pagination
  page: 0,
  sort: {date: -1}
};

const TIMEOUT_REQUEST = 15 * 1000;

describe('Search', () => {

  it('fetch results', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_SEARCH}`)
      .send(SEARCH_REQUEST)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(URL_SEARCH, 0, 4));
        mlog.log('RESPONSE:', JSON.stringify(res.body, 0, 4));

        res.body.should.be.a('array');
        res.body[0].should.have.property('entityType');
        res.body[0].should.have.property('entityId');
        res.body[0].should.have.property('isFavorite');
        res.body[0].should.have.property('favoriteId');
        res.body[0].should.have.property('createdAt');
        res.body[0].should.have.property('url');
        res.body[0].should.have.property('thumbnail');
        res.body[0].should.have.property('type');
        res.body[0].should.have.property('duplex');
        res.body[0].should.have.property('house');
        res.body[0].should.have.property('price');
        res.body[0].should.have.property('area');
        res.body[0].should.have.property('isSold');
        res.body[0].should.have.property('program');
        res.body[0].should.have.property('counterRating');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
