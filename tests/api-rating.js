require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const URL_TOURS = '/api/tours';

// not requires auth yet
const URL_RATING = '/api/rating';

const RATING_LIKE = {
  entityType: 'AppTour',
  entityId: 'apptour id',
  action: 'like'
};

const RATING_DISLIKE = {
  entityType: 'AppTour',
  entityId: 'apptour id',
  action: 'dislike'
};

const RATING_SHARE = {
  entityType: 'AppTour',
  entityId: 'apptour id',
  action: 'share'
};

const TIMEOUT_REQUEST = 15 * 1000;

describe('Rating', () => {

  let cookie;

  it('auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('test tour id', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_TOURS}/list`)
      .set('Cookie', cookie)
      .end((err, res) => {
        res.body.should.be.a('array');
        res.body[0].should.be.a('object');

        const entityId = res.body[0].entityId;
        mlog.log('APPTOUR ID:', entityId);

        RATING_LIKE.entityId = entityId;
        RATING_DISLIKE.entityId = entityId;
        RATING_SHARE.entityId = entityId;

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('like', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_RATING}`)
      .send(RATING_LIKE)
      .end((err, res) => {
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('dislike', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_RATING}`)
      .send(RATING_DISLIKE)
      .end((err, res) => {
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('share', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_RATING}`)
      .send(RATING_SHARE)
      .end((err, res) => {
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
