const fs = require('fs');
const path = require('path');
const URL = require('url').URL;

require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const TEST_URL = 'https://viewwer.com/storage/tours/rJ18BWDnQ/scenes/SyFsUOgwam/SyFsUOgwam.jpg';

const APPTOUR_FILES = path.resolve(__dirname, 'apptour');

const URL_TASK = '/api/task';

const TASK_APPTOUR_CREATE = {
  type: 'apptour'
};

const TASK_APPTOUR_REMOVE_FILE = {
  file: 'remove.jpg'
};

const TASK_APPTOUR_START_BUILD = {
  preset: 'build',
  scenes: [
    {
      // scene id in app
      sceneId: '0',
      // image to download
      url: TEST_URL
    },
    {
      sceneId: '1',
      // image file uploaded earlier
      file: 'test1.jpg'
    },
    {
      sceneId: '2',
      file: 'test2.jpg'
    },
    {
      // start scene
      isMain: true,
      sceneId: '3',
      file: 'test3.jpg'
    }
  ],
  thumbnails: [
    {
      sceneId: '0',
      url: TEST_URL
    },
    {
      sceneId: '1',
      file: 'thumb1.jpg'
    },
    {
      sceneId: '2',
      file: 'thumb2.jpg'
    },
    {
      sceneId: '3',
      file: 'thumb3.jpg'
    },
  ]
};

/*
  Tour must be built before settings changing
  Settings applies automatically on preview url refresh
 */
const TASK_APPTOUR_START_SETTINGS = {
  preset: 'settings',
  settings: {
    sceneTypes: [
      {
        sceneId: '0',
        type: 'scene_type_id1'
      },
      {
        sceneId: '1',
        type: 'scene_type_id2'
      },
      {
        sceneId: '2',
        type: 'scene_type_id3'
      },
      {
        sceneId: '3',
        type: 'scene_type_id4'
      },
    ],
    hotspots: [
      {
        sceneId: '0',
        atv: 0,
        ath: 0,
        action: {
          loadScene: {
            // scene id in app
            sceneId: '1'
          }
        }
      },
      {
        sceneId: '1',
        atv: -30,
        ath: -90,
        action: {
          loadScene: {
            // scene id in app
            sceneId: '2'
          }
        }
      },
      {
        sceneId: '2',
        atv: 30,
        ath: 90,
        action: {
          loadScene: {
            // scene id in app
            sceneId: '3'
          }
        }
      },
      {
        sceneId: '3',
        atv: 0,
        ath: 0,
        action: {
          loadScene: {
            // scene id in app
            sceneId: '0'
          }
        }
      }
    ]
  }
};

const TASK_APPTOUR_SAVE_CREATE = {
  type: 'apptour-save'
};

const TASK_APPTOUR_START_SAVE = {
  previewTaskId: 'build and preview taskId',

  type: 't05',
  house: true,
  description: 'Apartment description',
  address: 'Faubourg de France, Belfort, France',
  longitude: 6.85561789999997,
  latitude: 47.6365409,
  area: 77,
  price: 10000,
  gasClass: 'G',
  energyClass: 'E',
  saleType: 'sale'
};

const TIMEOUT_APPTOUR = 120 * 1000;
const TIMEOUT_UPLOAD = 60 * 1000;
const TIMEOUT_REQUEST = 30 * 1000;

const INTERVAL_STATUS = 5000;

describe('AppTour', () => {

  let cookie;

  // build and preview task
  let taskIdPreview;
  let uploadUrl;
  let uploadApiKey;
  let resultUrlPreview;

  // tour save task
  let taskIdSaveTour;
  let resultUrlSave;

  it('auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('task creation - build and preview', (done) => {
    chai.request(settings.HOST)
      .post(URL_TASK)
      .set('Cookie', cookie)
      .send(TASK_APPTOUR_CREATE)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(TASK_APPTOUR_CREATE));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.body.should.have.property('taskId');
        res.body.should.have.property('uploadUrl');
        res.body.should.have.property('uploadApiKey');

        taskIdPreview = res.body.taskId;
        uploadUrl = res.body.uploadUrl;
        uploadApiKey = res.body.uploadApiKey;

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('task status for new task', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_TASK}/${taskIdPreview}`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('new');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('images upload', (done) => {
    const u = new URL(uploadUrl);
    chai.request(u.origin)
      .post(u.pathname)
      .set('apikey', uploadApiKey)

      .attach('image', fs.readFileSync(`${APPTOUR_FILES}/test1.jpg`), 'test1.jpg')
      .attach('image', fs.readFileSync(`${APPTOUR_FILES}/test2.jpg`), 'test2.jpg')
      .attach('image', fs.readFileSync(`${APPTOUR_FILES}/test3.jpg`), 'test3.jpg')
      .attach('image', fs.readFileSync(`${APPTOUR_FILES}/thumb1.jpg`), 'thumb1.jpg')
      .attach('image', fs.readFileSync(`${APPTOUR_FILES}/thumb2.jpg`), 'thumb2.jpg')
      .attach('image', fs.readFileSync(`${APPTOUR_FILES}/thumb3.jpg`), 'thumb3.jpg')
      .attach('image', fs.readFileSync(`${APPTOUR_FILES}/remove.jpg`), 'remove.jpg')

      .type('form')
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_UPLOAD);

  it('image deletion', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_TASK}/${taskIdPreview}/remove-file`)
      .set('Cookie', cookie)
      .send(TASK_APPTOUR_REMOVE_FILE)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(TASK_APPTOUR_REMOVE_FILE));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('build tour', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_TASK}/${taskIdPreview}/start`)
      .set('Cookie', cookie)
      .send(TASK_APPTOUR_START_BUILD)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(TASK_APPTOUR_START_BUILD));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('task status', (done) => {
    let timer = setInterval(function () {
      chai.request(settings.HOST)
        .get(`${URL_TASK}/${taskIdPreview}`)
        .set('Cookie', cookie)
        .end((err, res) => {
          mlog.log('RESPONSE:', JSON.stringify(res.body));
          if (!res.body || res.body.status === 'failed') throw new Error('task failed');
          if (res.body && res.body.status !== 'processing') {
            res.body.should.have.property('status').eql('completed');
            res.body.should.have.property('result');
            res.body.result.should.have.property('url');
            resultUrlPreview = res.body.result.url;
            clearInterval(timer);
            done();
          }
        });
    }, INTERVAL_STATUS);
  }).timeout(TIMEOUT_APPTOUR);

  it('get result', (done) => {
    chai.request(settings.HOST)
      .get(resultUrlPreview)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('changing settings', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_TASK}/${taskIdPreview}/start`)
      .set('Cookie', cookie)
      .send(TASK_APPTOUR_START_SETTINGS)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(TASK_APPTOUR_START_SETTINGS));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('task status', (done) => {
    let timer = setInterval(function () {
      chai.request(settings.HOST)
        .get(`${URL_TASK}/${taskIdPreview}`)
        .set('Cookie', cookie)
        .end((err, res) => {
          mlog.log('RESPONSE:', JSON.stringify(res.body));
          if (!res.body || res.body.status === 'failed') throw new Error('task failed');
          if (res.body && res.body.status !== 'processing') {
            res.body.should.have.property('status').eql('completed');
            res.body.should.have.property('result');
            res.body.result.should.have.property('url');
            resultUrlPreview = res.body.result.url;
            clearInterval(timer);
            done();
          }
        });
    }, INTERVAL_STATUS);
  }).timeout(TIMEOUT_APPTOUR);

  it('get result', (done) => {
    chai.request(settings.HOST)
      .get(resultUrlPreview)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('task creation - save tour', (done) => {
    chai.request(settings.HOST)
      .post(URL_TASK)
      .set('Cookie', cookie)
      .send(TASK_APPTOUR_SAVE_CREATE)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(TASK_APPTOUR_SAVE_CREATE));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.body.should.have.property('taskId');

        taskIdSaveTour = res.body.taskId;

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('task status for new task', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_TASK}/${taskIdSaveTour}`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('new');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('save tour', (done) => {
    // previous task id
    TASK_APPTOUR_START_SAVE.previewTaskId = taskIdPreview;

    chai.request(settings.HOST)
      .post(`${URL_TASK}/${taskIdSaveTour}/start`)
      .set('Cookie', cookie)
      .send(TASK_APPTOUR_START_SAVE)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(TASK_APPTOUR_START_SAVE));
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('task status', (done) => {
    let timer = setInterval(function () {
      chai.request(settings.HOST)
        .get(`${URL_TASK}/${taskIdSaveTour}`)
        .set('Cookie', cookie)
        .end((err, res) => {
          mlog.log('RESPONSE:', JSON.stringify(res.body));
          if (!res.body || res.body.status === 'failed') throw new Error('task failed');
          if (res.body && res.body.status !== 'processing') {
            res.body.should.have.property('status').eql('completed');
            res.body.should.have.property('result');
            res.body.result.should.have.property('entityType');
            res.body.result.should.have.property('entityId');
            res.body.result.should.have.property('url');

            resultUrlSave = res.body.result.url;
            clearInterval(timer);
            done();
          }
        });
    }, INTERVAL_STATUS);
  }).timeout(TIMEOUT_APPTOUR);

  it('get result without auth', (done) => {
    chai.request(settings.HOST)
      .get(resultUrlSave)
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('get result using cookies', (done) => {
    chai.request(settings.HOST)
      .get(resultUrlSave)
      .set('Cookie', cookie)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
