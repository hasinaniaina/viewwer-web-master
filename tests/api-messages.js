require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const URL_MESSAGES = '/api/messages';

const WEBHOOK_SEND = {
  type: 'message.sent',
  data: {
    message: {
      senderId: 'test senderId',
      text: 'test text'
    }
  }
};

const TIMEOUT_REQUEST = 15 * 1000;

describe('Messages', () => {

  let cookie;

  it('webhooks', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_MESSAGES}/webhooks`)
      .send(WEBHOOK_SEND)
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('unreads count', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_MESSAGES}/unreads/count`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.count.should.be.a('number');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
