module.exports.HOST = (process.env.HOST) ? process.env.HOST : `http://${process.env.DOMAIN}:${process.env.PORT || 80}`;

module.exports.URL_USER = '/api/user';

module.exports.USER_AUTH = {
  email: 'mobile-test@viewwer.com',
  password: 'vdr3453SD_glhk'
};

module.exports.USER_AUTH_BLOCKED = {
  email: 'mobile-test-blocked@viewwer.com',
  password: 'vdr3453SD_glhk'
};

const REGEXP_SESSION = /^(.*?);/;

module.exports.getSession = function(set_cookie) {
  let session = REGEXP_SESSION.exec(set_cookie);
  return session[1];
};
