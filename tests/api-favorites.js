require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const URL_FAVORITES = '/api/favorites';

const FAVORITE_CREATE = {
  entityType: 'AppTour',
  entityId: 'ID'
};

const TIMEOUT_REQUEST = 15 * 1000;

describe('Favorites', () => {

  let cookie;
  let favoriteId;

  it('auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('favorite create', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_FAVORITES}`)
      .set('Cookie', cookie)
      .send(FAVORITE_CREATE)
      .end((err, res) => {

        mlog.log('REQUEST:', JSON.stringify(FAVORITE_CREATE));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.body.should.have.property('favoriteId');

        favoriteId = res.body.favoriteId;

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('favorites list', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_FAVORITES}/list`)
      .set('Cookie', cookie)
      .end((err, res) => {
        res.body.should.be.a('array');
        res.body[0].should.be.a('object');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('favorite fetch', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_FAVORITES}/${favoriteId}`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:\n', JSON.stringify(res.body, 0, 4));

        res.body.should.have.property('favoriteId').eql(favoriteId);
        res.body.should.have.property('entityType').eql(FAVORITE_CREATE.entityType);
        res.body.should.have.property('entityId').eql(FAVORITE_CREATE.entityId);
        res.body.url.should.be.a('string');
        res.body.entity.thumbnail.should.be.a('string');
        res.body.entity.type.should.be.a('string');
        res.body.entity.house.should.be.a('boolean');
        res.body.entity.counterRating.should.be.a('number');
        res.body.entity.moderationStatus.should.be.a('string');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('favorite delete', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_FAVORITES}/${favoriteId}/delete`)
      .set('Cookie', cookie)
      .send({})
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('favorite fetch deleted', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_FAVORITES}/${favoriteId}`)
      .set('Cookie', cookie)
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);
});
