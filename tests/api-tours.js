require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const URL_TOURS = '/api/tours';

const TOUR_UPDATE = {
  type: 't03',
  house: false,
  description: 'updated description',
  address: 'updated address',
  longitude: 11.1,
  latitude: 22.2,
  area: 66,
  price: 9999,
  gasClass: 'C',
  energyClass: 'D',
  saleType: 'rent',
  isSold: true,
  isPublished: false
};

const TOUR_DELETE = {};

const TIMEOUT_REQUEST = 15 * 1000;

describe('Tours', () => {

  let cookie;
  let tour;

  it('auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('tours list', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_TOURS}/list`)
      .set('Cookie', cookie)
      .end((err, res) => {
        res.body.should.be.a('array');
        res.body[0].should.be.a('object');

        tour = res.body[0];

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('tour fetch', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_TOURS}/${tour.entityId}`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        chai.assert.deepEqual(res.body, tour);

        res.body.entityType.should.be.a('string');
        res.body.entityId.should.be.a('string');

        res.body.thumbnail.should.be.a('string');
        res.body.url.should.be.a('string');
        res.body.createdAt.should.be.a('string');

        res.body.type.should.be.a('string');
        res.body.house.should.be.a('boolean');
        res.body.description.should.be.a('string');

        res.body.address.should.be.a('string');
        res.body.longitude.should.be.a('number');
        res.body.latitude.should.be.a('number');

        res.body.area.should.be.a('number');
        res.body.price.should.be.a('number');
        res.body.gasClass.should.be.a('string');
        res.body.energyClass.should.be.a('string');
        res.body.saleType.should.be.a('string');
        res.body.isSold.should.be.a('boolean');

        res.body.moderationStatus.should.be.a('string');
        res.body.moderationMessage.should.be.a('string');

        res.body.counterRating.should.be.a('number');

        res.body.generateStatus.should.be.a('string');
        res.body.isPublished.should.be.a('boolean');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('tour update', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_TOURS}/${tour.entityId}`)
      .set('Cookie', cookie)
      .send(TOUR_UPDATE)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(TOUR_UPDATE));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('tour check', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_TOURS}/${tour.entityId}`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('type').eql(TOUR_UPDATE.type);
        res.body.should.have.property('house').eql(TOUR_UPDATE.house);
        res.body.should.have.property('description').eql(TOUR_UPDATE.description);

        res.body.should.have.property('address').eql(TOUR_UPDATE.address);
        res.body.should.have.property('longitude').eql(TOUR_UPDATE.longitude);
        res.body.should.have.property('latitude').eql(TOUR_UPDATE.latitude);

        res.body.should.have.property('area').eql(TOUR_UPDATE.area);
        res.body.should.have.property('price').eql(TOUR_UPDATE.price);
        res.body.should.have.property('gasClass').eql(TOUR_UPDATE.gasClass);
        res.body.should.have.property('energyClass').eql(TOUR_UPDATE.energyClass);
        res.body.should.have.property('saleType').eql(TOUR_UPDATE.saleType);
        res.body.should.have.property('isSold').eql(TOUR_UPDATE.isSold);
        res.body.should.have.property('isPublished').eql(TOUR_UPDATE.isPublished);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('tour delete', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_TOURS}/${tour.entityId}/delete`)
      .set('Cookie', cookie)
      .send(TOUR_DELETE)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('success');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('tour fetch deleted', (done) => {
    chai.request(settings.HOST)
      .get(`${URL_TOURS}/${tour.entityId}`)
      .set('Cookie', cookie)
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);
});
