require('dotenv').config();

const shortid = require('shortid');

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const USER_UPDATE_PROFILE = {
  name: 'updated_name',
  phone: 'updated_phone'
};

const USER_CHANGE_PASSWORD = {
  oldPassword: settings.USER_AUTH.password,
  newPassword: shortid()
};

const TIMEOUT_REQUEST = 15 * 1000;

describe('User', () => {

  let cookie;
  let oldProfile;

  it('profile without auth', (done) => {
    chai.request(settings.HOST)
      .get(`${settings.URL_USER}/profile`)
      .set('Cookie', 'wrong-session')
      .end((err, res) => {
        res.should.have.status(403);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('wrong auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send({})
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));
        res.body.should.have.property('status').eql('failed');
        res.body.should.have.property('error').eql('wrong_credentials');
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH)
      .end((err, res) => {

        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('/admin', (done) => {
    chai.request(settings.HOST)
      .get('/admin')
      .set('Cookie', cookie)
      .end((err, res) => {
        res.should.have.status(403);
        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('profile', (done) => {
    chai.request(settings.HOST)
      .get(`${settings.URL_USER}/profile`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('name');
        res.body.should.have.property('phone');

        oldProfile = res.body;

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('profile update', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/profile`)
      .set('Cookie', cookie)
      .send(USER_UPDATE_PROFILE)
      .end((err, res) => {

        mlog.log('REQUEST:', JSON.stringify(USER_UPDATE_PROFILE));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('profile check', (done) => {
    chai.request(settings.HOST)
      .get(`${settings.URL_USER}/profile`)
      .set('Cookie', cookie)
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('name').eql(USER_UPDATE_PROFILE.name);
        res.body.should.have.property('phone').eql(USER_UPDATE_PROFILE.phone);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('profile restore', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/profile`)
      .set('Cookie', cookie)
      .send(oldProfile)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(oldProfile));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('change password', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/change-password`)
      .set('Cookie', cookie)
      .send(USER_CHANGE_PASSWORD)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(USER_CHANGE_PASSWORD));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('auth changed password', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send({
        email: settings.USER_AUTH.email,
        password: USER_CHANGE_PASSWORD.newPassword
      })
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('restore password', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/change-password`)
      .set('Cookie', cookie)
      .send({
        oldPassword: USER_CHANGE_PASSWORD.newPassword,
        newPassword: settings.USER_AUTH.password
      })
      .end((err, res) => {
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('auth blocked account', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH_BLOCKED)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH_BLOCKED));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('failed');
        res.body.should.have.property('error').eql('blocked');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
