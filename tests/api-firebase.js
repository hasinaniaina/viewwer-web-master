require('dotenv').config();

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const mlog = require('mocha-logger');

chai.use(chaiHttp);

const settings = require('./settings');

const URL_REGISTRATION_TOKEN = '/api/app/firebase/registration-token';

const REGISTRATION_TOKEN = {
  token: 'test'
};

const TIMEOUT_REQUEST = 15 * 1000;

describe('Firebase', () => {

  let cookie;

  it('auth', (done) => {
    chai.request(settings.HOST)
      .post(`${settings.URL_USER}/auth`)
      .send(settings.USER_AUTH)
      .end((err, res) => {
        mlog.log('REQUEST:', JSON.stringify(settings.USER_AUTH));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');
        res.header.should.have.property('set-cookie');

        cookie = settings.getSession(res.header['set-cookie']);

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

  it('update registration token', (done) => {
    chai.request(settings.HOST)
      .post(`${URL_REGISTRATION_TOKEN}`)
      .set('Cookie', cookie)
      .send(REGISTRATION_TOKEN)
      .end((err, res) => {

        mlog.log('REQUEST:', JSON.stringify(REGISTRATION_TOKEN));
        mlog.log('RESPONSE:', JSON.stringify(res.body));

        res.body.should.have.property('status').eql('success');

        done();
      });
  }).timeout(TIMEOUT_REQUEST);

});
