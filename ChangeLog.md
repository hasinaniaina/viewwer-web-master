# Changelog
Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
et nous utilisons [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.1] - 2019-08-16
- Correction message email de validation

## [1.2.0] - 2019-08-16

### Ajouté
- Gestion des partenaires (avec un code secret)
- Export XML/JSON des annonces pour les partenaires
- Script (Crontab Clever Cloud) pour exporter sur un FTP
- Mise à jour interface admin

### Modifié
- Le nom de l'utilisateur est désormais stocké sur deux champs (nom et prénom)

## [1.1.0] - 2019-06-19

### Ajouté
- Bannière affichée en haut des visites pour promouvoir l'app mobile (Smart App Banner) - Safari seulement
- Possibilité de se connecter avec Facebook ou Google (web)
- Création d'une page générée pour afficher le ChangeLog (/admin/changelog) - accessible uniquement aux admins connectés
- Connexion avec Facebook ou Google (api - pour le mobile)

## [1.0.1] - 2019-05-29

### Ajouté
- Création d'un fichier ChangeLog.md qui liste les changements notables sur le projet.

### Modifié

### Corrigé
- Réalité virtuelle : détection du problème de gyroscope et affichage d'une procédure qui explique comment autoriser l'accès (iOS Safari).

### Supprimé

## [1.0.0] - 2019-04-01
Version initiale avant le changement de Gitlab.
