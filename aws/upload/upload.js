const express = require('express');
const router = express.Router();

const Busboy = require('busboy');

const AWS = require('aws-sdk');
AWS.config.update(
  {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET,
    region: process.env.AWS_REGION_S3
  }
);
const s3 = new AWS.S3();

// for streaming to S3
const s3writeable = require('s3-writeable');

const AWS_DIR_TASKS = 'tasks';
const AWS_DIR_SOURCE = 'src';

router.post('/upload/task/:taskId', function (req, res) {
  if (req.headers.apikey !== process.env.API_KEY) {
    return res.status(403).json({});
  }
  multipartUpload(req, req.params.taskId)
    .then(() => {
      res.json({
        status: 'success'
      });
    })
    .catch(() => {
      res.json({
        status: 'failed'
      });
    });
});

function multipartUpload (req, task_id) {
  return new Promise(function (resolve, reject) {
    const busboy = new Busboy({headers: req.headers});

    let processing = 0, interrupted = false;

    function readyToResolve() {
      if (!processing && !interrupted) resolve();
    }

    function fileSuccess() {
      if (interrupted) return;

      processing--;
      readyToResolve();
    }

    function fileFailure(err) {
      if (interrupted) return;

      interrupted = true;
      reject(err);
    }

    busboy.on('file', function (fieldName, file, filename) {
      processing++;

      const stream = s3writeable(s3).createWriteStream({
        Bucket: process.env.AWS_BUCKET,
        Key: AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_SOURCE + '/' + sanitize(filename)
      });

      file
        .pipe(stream)
        .on('end', fileSuccess)
        .on('error', fileFailure);
    });

    busboy.on('finish', readyToResolve);

    req.pipe(busboy);
  });
}

function sanitize(filename) {
  return filename.replace(/[^a-zA-Z0-9\-.]/g, '_');
}

module.exports = router;
