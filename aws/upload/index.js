// populate process.env with values defined in the .env file
require('dotenv').config();

const express = require('express');
const app = express();
const http = require('http');
const httpServer = http.createServer(app);

app.use('/', require('./upload'));

// error handler
app.use(function(err, req, res, next) {
  if (!err) return next();
  res.status(err.status || 500).end();
});

app.use(function(req, res/*, next*/) {
  res.status(404).end();
});

httpServer.listen(process.env.PORT || 80, function() {
  console.info('Application is running');
});

process.on('unhandledRejection', function(err, p) {
  console.error('Unhandled promise rejection', {err: err, stack: err ? err.stack : undefined, promise: p}, function() {
    process.exit(1);
  });
});
