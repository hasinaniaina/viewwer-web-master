require('dotenv').config();

const _ = require('lodash');
const path = require('path');
const fs = require('fs-extra');

const execFile = require('child_process').execFile;
const EXEC_OPTIONS = {maxBuffer: 10000 * 1024};

const AWS_DIR_TASKS = 'tasks';
const AWS_DIR_SOURCE = 'src';
const AWS_DIR_RESULT = 'result';

const DIR_SOURCE = '/tmp/src/';
const DIR_RESULT = '/tmp/result/';

const KRPANO_BIN_SOURCE = 'krpano/bin/krpanotools';
const KRPANO_BIN = '/tmp/krpanotools';

const KRPANO_LOG = DIR_RESULT + 'krpano.log';

const AWS = require('aws-sdk');
AWS.config.update(
  {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET,
    region: process.env.AWS_REGION_S3
  }
);
AWS.util.update(AWS.S3.prototype, {
  addExpect100Continue: function addExpect100Continue(/*req*/) {
    //console.log('Depreciating this workaround, because introduced a bug');
    //console.log('Check: https://github.com/andrewrk/node-s3-client/issues/74');
  }
});

const awsS3 = new AWS.S3();
const s3 = require('s3-client');
const client = s3.createClient({
  s3Client: awsS3,
});

function registerKrpano(target) {
  return new Promise((resolve, reject) => {
    const key = process.env.KRPANO_KEY;
    if (!key) return reject('no_license_key');

    execFile(KRPANO_BIN, ['register', key], (err, stdout) => {
      if (stdout.indexOf('ERROR') !== -1) return reject(stdout);
      if (err) return reject(err);

      execFile(KRPANO_BIN, ['protect', '-o=' + path.resolve(target)], (err, stdout) => {
        if (stdout.indexOf('ERROR') !== -1) return reject(stdout);
        if (err) return reject(err);

        resolve();
      });
    });
  });
}

function download(task_id) {
  let start = Date.now();
  return new Promise((resolve, reject) => {
    const prefix = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_SOURCE;
    const d = client.downloadDir({
      localDir: DIR_SOURCE,
      s3Params: {
        Bucket: process.env.AWS_BUCKET,
        Prefix: prefix
      },
      getS3Params: function (localFile, s3Object, callback) {
        if (endsWith(localFile, '/')) {
          callback(null, null); // Skip folders
        } else {
          callback(null, {});
        }
      }
    });
    d.on('error', function (err) {
      console.error('unable to download:', err.stack);
      reject(err);
    });
    d.on('end', function () {
      resolve(Date.now() - start);
    });
  });
}

function removeResults(task_id) {
  let start = Date.now();
  return new Promise((resolve, reject) => {
    const prefix = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_RESULT + '/';
    const d = client.deleteDir({
      Bucket: process.env.AWS_BUCKET,
      Prefix: prefix
    });
    d.on('error', function (err) {
      reject(err);
    });
    d.on('end', function () {
      resolve(Date.now() - start);
    });
  });
}

function upload(task_id) {
  let start = Date.now();
  return new Promise((resolve, reject) => {

    const prefix = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_RESULT;
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Prefix: prefix
    };

    const u = client.uploadDir({
      localDir: DIR_RESULT,
      s3Params: params
    });
    u.on('error', function (err) {
      console.error('unable to upload:', err.stack);
      reject(err);
    });
    u.on('progress', function () {
    });
    u.on('end', function () {
      resolve(Date.now() - start);
    });
  });
}

function generate(parameters) {
  let start = Date.now();

  const sceneFiles = _.map(parameters.scenes, s => {
    return path.resolve(DIR_SOURCE + '/' + s.id + '.jpg');
  });

  let params = ['makepano'];

  let flags = {};

  if (parameters.preset === 'preview') {
    flags = {
      config: path.resolve('krpano/presets/preview/config.txt'),
      xmltemplate: path.resolve('krpano/presets/preview/template.xml'),
      xmlpath: path.resolve(DIR_RESULT + 'preview.xml')
    };
  } else if (parameters.preset === 'final') {
    flags = {
      config: path.resolve('krpano/presets/final/config.txt'),
      xmltemplate: path.resolve('krpano/presets/final/template.xml'),
      xmltemplate_view: path.resolve('krpano/presets/final/view-template.xml'),
      xmlpath: path.resolve(DIR_RESULT + 'panorama.xml')
    };
  }

  flags.tilepath = path.resolve(DIR_RESULT + '%BASENAME%.tiles/l%Al[_c]_%Av_%Ah.jpg');
  flags.tempcubepath = path.resolve(DIR_RESULT + 'tmp/%BASENAME%_cube%UID%');

  _.each(flags, (v, k) => params.push('-' + k + '=' + v));
  params = params.concat(sceneFiles);

  return new Promise((resolve, reject) => {
    const logStream = fs.createWriteStream(KRPANO_LOG);
    const child = execFile(KRPANO_BIN, params, EXEC_OPTIONS,
      (error, stdout) => {
        if (stdout.indexOf('ERROR') !== -1) return reject(stdout);
        if (error) return reject(error);
        resolve(Date.now() - start);
      });

    child.stdout.on('data', function () {
    }).pipe(logStream);

    child.stderr.on('data', function () {
    }).pipe(logStream);
  });
}

/*
  process itself may be frozen and thawed for reuse,
  so temporary files must be cleaned before and after
 */
function clean() {
  fs.emptyDirSync(DIR_SOURCE);
  fs.emptyDirSync(DIR_RESULT);
  fs.removeSync(KRPANO_BIN);
}

function start(task_id, parameters) {
  let duration_generate;
  let duration_download = 0;
  let duration_remove_results;
  let duration_upload;

  clean();

  return new Promise((resolve, reject) => {
    download(task_id)
      .then(r => {
        if (r) duration_download += r;

        // license
        // copy binary to writable tmp
        fs.copySync(KRPANO_BIN_SOURCE, KRPANO_BIN);
        if (process.env.NODE_ENV === 'production') {
          return registerKrpano(DIR_SOURCE + 'krpano.js');
        }
      })
      .then(() => generate(parameters))
      .then(r => {
        duration_generate = r;
        if (process.env.NODE_ENV === 'production') {
          return removeResults(task_id);
        }
      })
      .then(r => {
        duration_remove_results = r;
        if (process.env.NODE_ENV === 'production') {
          return upload(task_id);
        }
      })
      .then(r => {
        duration_upload = r;
        if (process.env.NODE_ENV === 'production') clean();
      })
      .then(() => {
        const r = {
          taskId: task_id,
          result: {
            durationDownload: duration_download,
            durationGenerate: duration_generate,
            durationRemoveResults: duration_remove_results,
            durationUpload: duration_upload
          }
        };
        resolve(r);
      })
      .catch(e => reject(e));
  });
}

exports.handler = (event) => {
  return new Promise((resolve, reject) => {
    start(event.taskId, event.parameters)
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

function endsWith(str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
