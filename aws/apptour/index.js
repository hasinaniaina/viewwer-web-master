require('dotenv').config();

const _ = require('lodash');
const async = require('async');
const url = require('url');
const path = require('path');
const fs = require('fs-extra');

const rp = require('request-promise-native');

const execFile = require('child_process').execFile;
const EXEC_OPTIONS = {maxBuffer: 10000 * 1024};

const xmlJs = require('xml-js');
const shortid = require('shortid');

const { promisify } = require('util');
const sizeOf = promisify(require('image-size'));

const AWS_DIR_TASKS = 'tasks';
const AWS_DIR_SOURCE = 'src';
const AWS_DIR_RESULT = 'result';

const DIR_SOURCE = '/tmp/src/';
const DIR_RESULT = '/tmp/result/';

const KRPANO_BIN_SOURCE = 'krpano/bin/krpanotools';
const KRPANO_BIN = '/tmp/krpanotools';

const KRPANO_LOG = DIR_RESULT + 'krpano.log';

const DOWNLOAD_LIMIT = 10;

const AWS = require('aws-sdk');
AWS.config.update(
  {
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET,
    region: process.env.AWS_REGION_S3
  }
);
AWS.util.update(AWS.S3.prototype, {
  addExpect100Continue: function addExpect100Continue(/*req*/) {
    //console.log('Depreciating this workaround, because introduced a bug');
    //console.log('Check: https://github.com/andrewrk/node-s3-client/issues/74');
  }
});

const awsS3 = new AWS.S3();
const s3 = require('s3-client');
const client = s3.createClient({
  s3Client: awsS3,
});

function registerKrpano(target) {
  return new Promise((resolve, reject) => {
    const key = process.env.KRPANO_KEY;
    if (!key) return reject('no_license_key');

    execFile(KRPANO_BIN, ['register', key], (err, stdout) => {
      if (stdout.indexOf('ERROR') !== -1) return reject(stdout);
      if (err) return reject(err);

      execFile(KRPANO_BIN, ['protect', '-o=' + path.resolve(target)], (err, stdout) => {
        if (stdout.indexOf('ERROR') !== -1) return reject(stdout);
        if (err) return reject(err);

        resolve();
      });
    });
  });
}

function download(task_id, file, filename) {
  return new Promise((resolve, reject) => {
    const d = client.downloadFile({
      localFile: file,
      s3Params: {
        Bucket: process.env.AWS_BUCKET,
        Key: AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_SOURCE + '/' + filename
      }
    });
    d.on('error', function (err) {
      reject(err);
    });
    d.on('end', function () {
      resolve();
    });
  });
}

function downloadFiles(task_id, objects) {
  let start = Date.now();
  return new Promise((resolve, reject) => {
    async.eachLimit(objects, DOWNLOAD_LIMIT, (o, cb) => {
      if (o.file) {
        download(task_id, o.fullpath, o.file)
          .then(() => cb())
          .catch(e => cb(e));
      } else if (o.url) {
        rp({
          url: o.url,
          encoding: null
        })
          .then(r => fs.writeFile(o.fullpath, Buffer.from(r)))
          .then(() => cb())
          .catch(e => cb(e));
      } else {
        cb(new Error('unknown file source'));
      }
    }, err => {
      if (err) return reject(err);
      resolve(Date.now() - start);
    });
  });
}

function imageSizes(objects) {
  let start = Date.now();
  return new Promise((resolve, reject) => {
    async.each(objects, (o, cb) => {
      sizeOf(o.fullpath)
        .then(d => {
          o.height = d.height;
          o.width = d.width;
          o.imageType = d.type;
          cb();
        })
        .catch(e => cb(e));
    }, err => {
      if (err) return reject(err);
      resolve(Date.now() - start);
    });
  });
}

function removeResults(task_id) {
  let start = Date.now();
  return new Promise((resolve, reject) => {
    const prefix = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_RESULT + '/';
    const d = client.deleteDir({
      Bucket: process.env.AWS_BUCKET,
      Prefix: prefix
    });
    d.on('error', function (err) {
      reject(err);
    });
    d.on('end', function () {
      resolve(Date.now() - start);
    });
  });
}

function upload(task_id) {
  let start = Date.now();
  return new Promise((resolve, reject) => {

    const prefix = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_RESULT;
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Prefix: prefix,
      ACL: 'public-read'
    };

    const u = client.uploadDir({
      localDir: DIR_RESULT,
      s3Params: params
    });
    u.on('error', function (err) {
      console.error('unable to upload:', err.stack);
      reject(err);
    });
    u.on('progress', function () {
    });
    u.on('end', function () {
      resolve(Date.now() - start);
    });
  });
}

function generate(scenes) {
  let start = Date.now();

  const sceneFiles = _.map(scenes, s => s.fullpath);

  let params = ['makepano'];

  const flags = {
    config: path.resolve('krpano/presets/final/config.txt'),
    xmltemplate: path.resolve('krpano/presets/final/template.xml'),
    xmltemplate_view: path.resolve('krpano/presets/final/view-template.xml'),
  };

  flags.xmlpath = path.resolve(path.join(DIR_RESULT, '/tour/tour.xml'));
  flags.tilepath = path.resolve(path.join(DIR_RESULT, '/tour/%BASENAME%.tiles/l%Al[_c]_%Av_%Ah.jpg'));
  flags.tempcubepath = path.resolve(path.join(DIR_RESULT, '/tmp/%BASENAME%_cube%UID%'));

  _.each(flags, (v, k) => params.push('-' + k + '=' + v));
  params = params.concat(sceneFiles);

  return new Promise((resolve, reject) => {
    const logStream = fs.createWriteStream(KRPANO_LOG);
    const child = execFile(KRPANO_BIN, params, EXEC_OPTIONS,
      (error, stdout) => {
        if (stdout.indexOf('ERROR') !== -1) return reject(stdout);
        if (error) return reject(error);
        resolve(Date.now() - start);
      });

    child.stdout.on('data', function () {
    }).pipe(logStream);

    child.stderr.on('data', function () {
    }).pipe(logStream);
  });
}

/*
  process itself may be frozen and thawed for reuse,
  so temporary files must be cleaned before and after
 */
function clean() {
  fs.emptyDirSync(DIR_SOURCE);
  fs.emptyDirSync(DIR_RESULT);
  fs.removeSync(KRPANO_BIN);
}

function processXml(file, target, scenes, thumbnails) {
  const xml = fs.readFileSync(file, 'utf8');
  const json = {
    scenes: scenes,
    thumbnails: thumbnails
  };
  json.panorama = xmlJs.xml2js(xml, {compact: true});
  fs.outputJsonSync(target, json);
}

function start(task_id, parameters) {
  if (parameters.preset === 'build') return presetBuild(task_id, parameters);
  if (parameters.preset === 'settings') return presetSettings(task_id, parameters);
  throw new Error('unknown preset');
}

function presetBuild(task_id, parameters) {
  let duration_generate;
  let duration_download;
  let duration_upload;

  clean();

  const scenes = [];
  _.each(parameters.scenes, o => {
    const id = shortid();
    const dir = path.join(DIR_RESULT, '/scenes/', id, '/');
    const filename = id + '.jpg';
    scenes.push(_.assign({}, o, {
      id: id,
      dir: dir,
      filename: filename,
      fullpath: path.resolve(path.join(dir, filename))
    }));
  });

  const thumbnails = [];
  _.each(parameters.thumbnails, o => {
    const scene = _.find(scenes, {sceneId: o.sceneId});
    if (!scene) return;
    thumbnails.push(_.assign({}, o, {
      dir: scene.dir,
      filename: 'thumbnail.jpg',
      fullpath: path.resolve(path.join(scene.dir, '/thumbnail.jpg'))
    }));
  });

  return new Promise((resolve, reject) => {
    Promise.all(_.map(scenes, s => fs.ensureDir(s.dir)))
      .then(() => downloadFiles(task_id, _.concat(scenes, thumbnails)))
      .then(r => {
        duration_download = r;
        // license
        // copy binary to writable tmp
        fs.copySync(KRPANO_BIN_SOURCE, KRPANO_BIN);
        return registerKrpano(DIR_SOURCE + 'krpano.js');
      })
      // for fov calculation
      .then(() => imageSizes(_.concat(scenes, thumbnails)))
      .then(() => generate(scenes))
      .then(r => {
        duration_generate = r;
        processXml(
          path.resolve(path.join(DIR_RESULT, '/tour/tour.xml')),
          path.resolve(path.join(DIR_RESULT, '/tour/tour.json')),
          scenes,
          thumbnails
        );

        // clear settings
        fs.outputJsonSync(path.resolve(path.join(DIR_RESULT, '/tour/settings.json')), {});

        if (process.env.NODE_ENV === 'production') {
          return removeResults(task_id);
        }
      })
      .then(() => {
        if (process.env.NODE_ENV === 'production') {
          return upload(task_id);
        }
      })
      .then(r => {
        duration_upload = r;
        if (process.env.NODE_ENV === 'production') clean();
      })
      .then(() => {
        const r = {
          taskId: task_id,
          result: {
            durationDownload: duration_download,
            durationGenerate: duration_generate,
            durationUpload: duration_upload,
            scenes: scenes,
            thumbnails: thumbnails,
            urlJson: getPublicUrl(task_id, 'tour/tour.json'),
            urlXml: getPublicUrl(task_id, 'tour/tour.xml')
          }
        };
        resolve(r);
      })
      .catch(e => reject(e));
  });
}

function presetSettings(task_id, parameters) {

  clean();

  return new Promise((resolve, reject) => {
    fs.ensureDir(path.join(DIR_RESULT, '/tour/'))
      .then(() => fs.outputJson(path.join(DIR_RESULT, '/tour/settings.json'), parameters.settings))
      .then(() => {
        if (process.env.NODE_ENV === 'production') return upload(task_id);
      })
      .then(() => {
        if (process.env.NODE_ENV === 'production') clean();
      })
      .then(() => {
        const r = {
          taskId: task_id,
          result: {}
        };
        resolve(r);
      })
      .catch(e => reject(e));
  });
}

function getPublicUrl(task_id, filename) {
  let key = AWS_DIR_TASKS + '/' + task_id + '/' + AWS_DIR_RESULT + '/' + filename;
  return getPublicUrlHttp(process.env.AWS_BUCKET, key);
}

function getPublicUrlHttp(bucket, key) {
  const parts = {
    protocol: 'http:',
    hostname: bucket + '.s3.amazonaws.com',
    pathname: '/' + encodeSpecialCharacters(key),
  };
  return url.format(parts);
}

function encodeSpecialCharacters(filename) {
  // these characters are valid in URIs, but S3 does not like them for some reason.
  return encodeURI(filename).replace(/[!'()* ]/g, function (char) {
    return '%' + char.charCodeAt(0).toString(16);
  });
}

exports.handler = (event) => {
  return new Promise((resolve, reject) => {
    start(event.taskId, event.parameters)
      .then(r => resolve(r))
      .catch(e => reject(e));
  });
};

if (process.env.NODE_ENV === 'development') {
  const parameters = {
    preset: 'build',
    scenes: [
      {
        sceneId: '0',
        url: process.env.TEST_URL
      },
      {
        isMain: true,
        sceneId: '1',
        url: process.env.TEST_URL
      }
    ],
    thumbnails: [
      {
        sceneId: '0',
        url: process.env.TEST_URL
      },
      {
        sceneId: '1',
        url: process.env.TEST_URL
      }
    ]
  };
  console.log(parameters);
  start('test', parameters)
    .then(r => console.log(JSON.stringify(r, null, 4)))
    .catch(e => console.error(e));
}
